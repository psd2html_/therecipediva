<?php
    chdir('..');
    include('../config.php');

    $sql = "
      SELECT node.nid AS nid,
      node.title AS node_title
      FROM node node
      WHERE node.type in ('recipe_groceries')
    ";

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        $array_groceries[$row['nid']] = $row['node_title'];
    }

    if (isset($array_groceries))
    {
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        // code in json data
        return json_encode($array_groceries);
    }