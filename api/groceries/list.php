<?php
    include('../config.php');

    $sql = "
      SELECT
        recipe_mygroceries.aisle AS aisle,
        recipe_mygroceries.uid AS ruid,
        recipe_mygroceries.title AS title
        profile_values.uid AS puid,
        profile_values.value AS value
        FROM  recipe_mygroceries
        LEFT JOIN profile_values AS pv
        ON recipe_mygroceries.uid = pv.uid
        WHERE ruid = 1
    ";

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        $array_groceries[$row['nid']] = $row['node_title'];
    }

    if (isset($array_groceries))
    {
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        // code in json data
        return json_encode($array_groceries);
    }