<?php
    /* Title: Add recipe to Grocery
     * Type: require authorization
     *
     * URL request: /api/user/addgrocery/{reciepe_ID}/{user_ID}
     *
     * In data: empty
     * Out data: json: {"message":"successfully"}
     *
     * return other messages: ""
    */
?>
<?php

    chdir('..');
    include('../config.php');

    $recipe_id  = $_GET['recipe_id'];
    $user_id    = $_GET['user_id'];

    // if user authorized
    if (user_is_logged_in() == true)
    {
        $sql = "
            SELECT
                recipe_mygroceries.nid,
                recipe_mygroceries.uid,
                recipe_mygroceries.title
            FROM recipe_mygroceries
            WHERE recipe_mygroceries.uid = " .$user_id . "
            AND recipe_mygroceries.nid = " . $recipe_id;

        $count = count(db_query($sql));

        if ($count)
        {
            $error = array(
                'error' => '1',
                'errorcode' => 'This id recipe has added to Groceries.'
            );
            header('HTTP/1.1 200 OK');
            header('Content-type: application/json');
            return json_encode($error);
        }
        else
        {

            $sql = "
                SELECT

            ";

            // add user reciepe
            // harcodede 24: `aisle` field
            $sql = "
                INSERT INTO recipe_mygroceries
                VALUES ('". $user_id ."', 'NULL', '24', 'NULL',   '". $recipe_id ."', '". title ."', '0')
            ";
            $result = db_query($sql);
        }
    }
    else
    {
        // User NOT login
        $error = array(
            'error' => '1',
            'errorcode' => 'User not login.'
        );
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        return json_encode($error);
    }
?>