<?php
    /* Title: Add recipe to Recipies box
     * Type: require authorization
     *
     * URL request: api/user/addrecipe/{reciepe_ID}/{user_ID}
     *
     * In data: empty
     * Out data: json: {"message":"successfully"}
     *
     * return other messages: "User not login." and "This id recipe has added."
    */
?>
<?php
    chdir('..');
    include('../config.php');

    $recipe_id  = $_GET['recipe_id'];
    $user_id    = $_GET['user_id'];

    /*
    $user_array = array(
        'uid' => $user_id
    );
    $obj_user = user_load($user_array);


    $user_auth_array = array(
        'name' => $obj_user->mail,
        'pass' => 'qwerty'
    );
*/

    $array = array(
        'name' => 'admin@admin.com',
        'pass' => 'admin'
    );
    $account = user_authenticate($array);


    function custom_favorite_nodes_add($nid, $fid = 0)
    {
        global $user;
        $is_success = FALSE;

        $node = node_load($nid);
        $new_nid = 0;

        if($node->type == CONTENT_TYPE_RECIPE && $node->field_recipe_original[0]["value"] == 0){
            $new_nid = clone_node_save($nid);
        }
        else{
            $new_nid = $nid;
        }

        if ($new_nid != 0) {
            if(!db_query("DELETE FROM {favorite_nodes} WHERE nid = %d AND uid = %d AND fid = %d", $new_nid, $user->uid, $fid)){
            }
            elseif(!db_query("INSERT INTO {favorite_nodes} (nid, uid, last, fid) VALUES (%d, %d, %d, %d)", $new_nid, $user->uid, time(), $fid)){
            }
            else{
                $is_success = TRUE;
            }
        }

        $return = ($is_success) ? $new_nid : "false";

        return $return;
    }


    /*
     * if user authorized
     */
    if (user_is_logged_in() == true)
    {
        $result = custom_favorite_nodes_add('13278', '13278');

        $url = 'http://therecipediva.local/favorite_nodes/add/13278/';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_GET, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);






        /*
        $sql = "
            SELECT *
            FROM favorite_nodes
            WHERE favorite_nodes.uid = 1
            AND favorite_nodes.nid = " . $recipe_id;

        $result = db_query($sql);
        $result_array = db_fetch_array($result);
        */


        /*
         * if in user added reciepe
         */
        /*
        if (!empty($result_array))
        {
            $error = array(
                'error' => '1',
                'errorcode' => 'This id recipe has added.'
            );
            header('HTTP/1.1 200 OK');
            header('Content-type: application/json');
            echo json_encode($error);
        }
        else
        {
            // add user reciepe
            $sql = "
                INSERT INTO favorite_nodes
                VALUES ('". $recipe_id ."', '". $user_id ."', '". time() ."', '0')
            ";
            $result = db_query($sql);
            if ($result)
            {
                $ok = array(
                    'message' => 'successfully'
                );
                header('HTTP/1.1 200 OK');
                header('Content-type: application/json');
                echo json_encode($ok);
            }
        }
        */
    }
/*
else
{
    // User NOT login
    $error = array(
        'error' => '1',
        'errorcode' => 'User not login.'
    );
    header('HTTP/1.1 200 OK');
    header('Content-type: application/json');
    echo json_encode($error);
}
*/


/*
    // get current URL
    $pageURL = $_SERVER["REQUEST_URI"];

    $pattern = '|/api/user/addrecipe/(\d+)$|';
    preg_match($pattern, $pageURL, $macthes);

    // get ID category
    $cat_ID = $macthes[1];


    $recipes_id = 18815;
    $user_id = 1;

    $url = 'http://therecipediva.local/favorite_nodes/checked/node/78712';


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_GET, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);

    echo '<pre>';
    print_r($response);
    echo '</pre>';

    echo '<pre>';
    print_r(curl_getinfo($ch));
    echo '</pre>';

    echo "\n\nCURL error number:";
    echo '<pre>';
    print_r(curl_errno($ch));
    echo '</pre>';

    echo 'RESULT: '.$response;
    curl_close($ch);











    // TESTING
    /*
    $check = false;

    if ($check)
    {
        $error = array(
            'error' => '1',
            'errorcode' => 'The recipe has been added.'
        );
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        return json_encode($error);
    }
    else
    {
        // TESTING
        $recipes_id = '18815';

        $url = 'http://therecipediva.local/favorite_nodes/add/'. $recipes_id;
        $ch = curl_init();
    }
    */

