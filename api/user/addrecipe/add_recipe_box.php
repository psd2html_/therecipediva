<?php
    /* Title: Add recipe to Recipies box
     * Type: require authorization
     *
     * URL request: api/user/addrecipe/{reciepe_ID}/{user_ID}
     *
     * In data: empty
     * Out data: json: {"message":"successfully"}
     *
     * return other messages: "User not login." and "This id recipe has added."
    */
?>
<?php
    chdir('..');
    include('../config.php');

    $recipe_id  = $_GET['recipe_id'];
    $user_id    = $_GET['user_id'];

    $user_array = array(
        'uid' => $user_id
    );
    $obj_user = user_load($user_array);

    $user_auth_array = array(
        'name' => $obj_user->mail,
        'pass' => 'qwerty'
    );

    /*
     * User login
     */
    $account = user_authenticate($user_auth_array);


    /*
     * Add reciepe box
     */
    function custom_favorite_nodes_add($nid, $fid = 0)
    {
        global $user;
        $is_success = FALSE;

        $node = node_load($nid);
        $new_nid = 0;

        if($node->type == CONTENT_TYPE_RECIPE && $node->field_recipe_original[0]["value"] == 0){
            $new_nid = clone_node_save($nid);
        }
        else{
            $new_nid = $nid;
        }
        if ($new_nid != 0) {
            if(!db_query("DELETE FROM {favorite_nodes} WHERE nid = %d AND uid = %d AND fid = %d", $new_nid, $user->uid, $fid)){
            }
            elseif(!db_query("INSERT INTO {favorite_nodes} (nid, uid, last, fid) VALUES (%d, %d, %d, %d)", $new_nid, $user->uid, time(), $fid)){
            }
            else{
                $is_success = TRUE;
            }
        }

        $return = ($is_success) ? $new_nid : "false";

        return $return;
    }


    /*
     * user authorized
     */
    if (user_is_logged_in() == true)
    {
        $result = custom_favorite_nodes_add($recipe_id, $recipe_id);

        if (is_numeric($result))
        {
            $ok = array(
                'message' => 'Reciepe add to box.'
            );
            header('HTTP/1.1 200 OK');
            header('Content-type: application/json');
            echo json_encode($ok);
        }
        else
        {
            $error = array(
                'error' => '1',
                'errorcode' => 'Reciepe failed add to box.'
            );
            header('HTTP/1.1 200 OK');
            header('Content-type: application/json');
            echo json_encode($error);
        }
    }
    else
    {
        $error = array(
            'error' => '1',
            'errorcode' => 'User not login.'
        );
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        echo json_encode($error);
    }