<?php
    include('../config.php');

    //$input = $_POST;

    // parse get json data
    //$data = json_decode($input);

    /*
    $array['access'] = time();
    $array['created'] = time();
    $array['name'] = 'test777998888';
    $array['pass'] = 'qwerty';
    $array['mail'] = 'test@test.com';
    $array['status'] = '1';
    */

    $array = array(
        'access' => time(),
        'created' => time(),
        'name' => 'test777',
        'pass' => 'qwerty',
        'mail' => 'test@test.com',
        'status' => '1'
    );


    $user = @user_load($array);
    if ($user)
    {
        // {"error":"1","errorcode":["empty_pers","empty_mail","empty_phone","empty_letter","empty_scode"]}
        $error = array(
            'error' => '1',
            'errorcode' => 'This user already exists'
        );

        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        return json_encode($error);
    }
    else
    {
        $result = user_save('', $array, '');
        if ($result)
        {
            header('HTTP/1.1 200 OK');
            header('Content-type: application/json');
            return json_encode($result);
        }
        else
        {
            $error = array(
                'error' => '1',
                'errorcode' => 'User not registered.'
            );
            header('HTTP/1.1 200 OK');
            header('Content-type: application/json');
            return json_encode($error);
        }
    }






