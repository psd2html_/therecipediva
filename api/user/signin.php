<?php
    /* Title: User sign in
     * Type: anonymous
     *
     * URL request: /api/user/signin
     *
     * In data:
        {
            email:    string
            password: string
        }
     * Out data: User object JSON
     *
     * return other messages: "User not authenticated."
    */
?>
<?php
    include('../config.php');

    $array = array(
        'name' => 'test@test.com',
        'pass' => 'qwerty'
    );
    $account = user_authenticate($array);

    if ($account)
    {
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        global $user;
        print $user->uid;
        echo json_encode($account);
    }
    else
    {
        $error = array(
            'error' => '1',
            'errorcode' => 'User not authenticated.'
        );
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        return json_encode($error);
    }

