<?php
    include('../config.php');


    $sql = '
      SELECT node.nid AS nid,
      node.title AS node_title,
      RAND() AS _random
      FROM node node
      ORDER BY _random ASC
      LIMIT 1
    ';

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        $array_random['id'] = $row['nid'];
        $array_random['title'] = $row['node_title'];
    }

    if (isset($array_random))
    {
        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        // code in json data
        echo json_encode($array_random);
    }