<?php
    include('../config.php');

    // INPUT DATA !!!!!!!!!!!!
    // EXAMPLE DATA JSON
    //$input = '{"keyword" : "Test site new"}';

    $input = $_POST;

    // parse get json data
    $data = json_decode($input);
    $keyword = $data->keyword;

    $sql = "
      SELECT node.nid AS nid, node.title AS node_title
      FROM node
      LEFT JOIN content_type_recipe node_data_field_ingredients
      ON node.vid = node_data_field_ingredients.vid
      WHERE node_data_field_ingredients.field_ingredients_value LIKE '%". $keyword ."%'
      OR node.title LIKE '%". $keyword ."%'
      OR node_data_field_ingredients.field_preparation_value LIKE '%". $keyword ."%'
    ";

    $result = db_query($sql);

    /*
     * return $row['node_title']
     * return $row['nid']
     */
    while ($row = db_fetch_array($result))
    {
        $array_title[] = $row['node_title'];
    }

    if (isset($array_title))
    {
        $recipe_search = array_unique($array_title);

        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        // code in json data
        return json_encode($recipe_search);
    }