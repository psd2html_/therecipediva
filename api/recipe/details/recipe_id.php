<?php
    chdir('..');
    include_once('../config.php');




    // get current URL
    $pageURL = $_SERVER["REQUEST_URI"];

    $pattern = '|/api/recipe/details/(\d+)$|';
    preg_match($pattern, $pageURL, $macthes);

    // get ID category
    $recipe_ID = $macthes[1];

    $sql = "
      SELECT node.nid AS nid,
      node.title AS node_title
      FROM node node
      WHERE node.nid = ". $recipe_ID;


    $result = db_query($sql);
    /*
     * return $row['node_title']
     * return $row['nid']
     */
    while ($row = db_fetch_array($result))    {

        $array['id'] = $row['nid'];
        $array['title'] = $row['node_title'];
    }

    $recipe_details = array_unique($array);

    // code in json data
    return json_encode($recipe_details);