<?php
    include('../config.php');

    // get current URL
    $pageURL = $_SERVER["REQUEST_URI"];

    $pattern = '|/api/recipe/(\d+)$|';
    preg_match($pattern, $pageURL, $macthes);

    // get ID category
    $cat_ID = $macthes[1];

    $sql = "
      SELECT node_revisions.vid AS vid,
      node.title AS node_title,
      node.nid AS node_nid
      FROM node_revisions node_revisions
      LEFT JOIN node node
      ON node_revisions.nid = node.nid
      WHERE node_revisions.vid
      IN ( SELECT tn.vid FROM term_node tn WHERE tn.tid = ". $cat_ID .")";
    $result = db_query($sql);

    /*
     * return $row['node_title']
     * return $row['nid']
     */
    while ($row = db_fetch_array($result))
    {
        $array_title[] = $row['node_title'];
    }

    if (isset($array_title))
    {
        $recipe_search = array_unique($array_title);

        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        // code in json data
        return json_encode($recipe_search);
    }