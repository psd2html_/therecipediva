<?php
    include('../config.php');

    // INPUT DATA !!!!!!!!!!!!
    // EXAMPLE DATA JSON
    //$input = '{"include" : {"ingredients" : "salt, pepper"}, "exclude" : {"ingredients" : "butter"}}';

    $input = $_POST;

    // parse get json data
    $data = json_decode($input);


    $sql = '
    SELECT node.nid AS nid,
      node.title AS node_title
    FROM node
    LEFT JOIN content_type_recipe node_data_field_ingredients
    ON node.vid = node_data_field_ingredients.vid
    ';

    $include = explode(',', $data->include->ingredients);
    $exclude = explode(',', $data->exclude->ingredients);

    $flag = false;
    $x = 0;
    $y = 0;

    if (!empty($include))
    {
        foreach ($include as $value)
        {
            $x++;
            if ($x == 1)
            {
                $flag = true;
                $sql .= ' WHERE node_data_field_ingredients.field_ingredients_value LIKE "%%' . $value . '%%"';
            }
            else
            {
                $sql .= ' AND node_data_field_ingredients.field_ingredients_value LIKE "%%' . $value . '%%"';
            }

        }
    }

    if (!empty($exclude))
    {
        foreach ($exclude as $value)
        {
            $y++;
            if (($flag == false) and ($y == 1))
            {
                $flag = true;
                $sql .= ' WHERE node_data_field_ingredients.field_ingredients_value NOT LIKE "%%' . $value . '%%"';
            }
            else
            {
                $sql .= ' AND node_data_field_ingredients.field_ingredients_value NOT LIKE "%%' . $value . '%%"';
            }
        }
    }

    $result = db_query($sql);

    /*
     * return $row['node_title']
     * return $row['nid']
     */
    while ($row = db_fetch_array($result))
    {
        $array_title[] = $row['node_title'];
    }

    if (isset($array_title))
    {
        $recipe_ingredients = array_unique($array_title);

        header('HTTP/1.1 200 OK');
        header('Content-type: application/json');
        // code in json data
        return json_encode($recipe_ingredients);
    }