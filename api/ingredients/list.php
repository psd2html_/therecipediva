<?php
    include('../config.php');

    $sql = '
        SELECT node.nid AS nid,
        node_data_field_ingredients.field_ingredients_value AS ingredients,
        node.type AS node_type,
        node.vid AS node_vid
        FROM node node
        LEFT JOIN content_type_recipe node_data_field_ingredients
        ON node.vid = node_data_field_ingredients.vid
        WHERE node_data_field_ingredients.field_ingredients_value
        IS NOT NULL
    ';
    $result = db_query($sql);

    while ($row = db_fetch_object($result))
    {
        $array[] = explode("\n", $row->ingredients);
    }

    foreach ($array as $item)
    {
        foreach ($item as $value)
        {
            $ingredients[] = strip_tags($value);
        }
    }
    $ingredients = array_unique($ingredients);
    return json_encode($ingredients);

?>