<?php
// $Id: forum.admin.inc,v 1.8.2.1 2009/05/26 08:13:00 goba Exp $

/**
 * @file
 * Administrative page callbacks for the forum module.
 */

function forum_form_main($type, $edit = array()) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || !empty($_POST['confirm'])) {
    return drupal_get_form('forum_confirm_delete', $edit['tid']);
  }
  if (arg(0) == 'adminrecipe' && arg(1) == 'feedback' && arg(2) == 'edit') {
  	$edit = taxonomy_get_term($edit);
  }
  $output = "";
  switch ($type) {
    case 'forum':
      $output = drupal_get_form('forum_form_forum', $edit);
      break;
    case 'container':
      $output = drupal_get_form('forum_form_container', $edit);
      break;
	case 'section':
      $output = drupal_get_form('forum_form_section', $edit);
      break;
  }
  // Start added by Quantic - 2010/06/17
  if (arg(0) == 'adminrecipe' && arg(1) == 'divagossip') {
  	return theme('divagossip_admin_container_add', $edit, $output);
  } else if (arg(0) == 'adminrecipe' && arg(1) == 'feedback') {
  	return theme('feedback_section_add', $edit, $output);
  }
  // End added by Quantic - 2010/06/17
  return $output;
}

/**
 * Returns a form for adding a forum to the forum vocabulary
 *
 * @param $edit Associative array containing a forum term to be added or edited.
 * @ingroup forms
 * @see forum_form_submit()
 */
function forum_form_forum(&$form_state, $edit = array()) {
  $edit += array(
    'name' => '',
    'description' => '',
    'tid' => NULL,
    'weight' => 0,
  );
  $form['name'] = array('#type' => 'textfield',
    '#title' => t('Forum name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 255,
    '#description' => t('Short but meaningful name for this collection of threaded discussions.'),
    '#required' => TRUE,
  );
  $form['description'] = array('#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit['description'],
    '#description' => t('Description and guidelines for discussions within this forum.'),
  );
  $form['parent']['#tree'] = TRUE;
  $form['parent'][0] = _forum_parent_select($edit['tid'], t('Parent'), 'forum');
  $form['weight'] = array('#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $edit['weight'],
    '#description' => t('Forums are displayed in ascending order by weight (forums with equal weights are displayed alphabetically).'),
  );

  $form['vid'] = array('#type' => 'hidden', '#value' => variable_get('forum_nav_vocabulary', ''));
  $form['submit' ] = array('#type' => 'submit', '#value' => t('Save'));
  if ($edit['tid']) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['tid'] = array('#type' => 'hidden', '#value' => $edit['tid']);
  }
  $form['#submit'][] = 'forum_form_submit';
  $form['#theme'] = 'forum_form';
  return $form;
}

/**
 * Process forum form and container form submissions.
 */
function forum_form_submit($form, &$form_state) {
  if ($form['form_id']['#value'] == 'forum_form_container') {
    $container = TRUE;
    $type = t('forum container');
  }
  else {
    $container = FALSE;
    $type = t('forum');
  }
  $status = taxonomy_save_term($form_state['values']);
  switch ($status) {
    case SAVED_NEW:
      if ($container) {
        $containers = variable_get('forum_containers', array());
        $containers[] = $form_state['values']['tid'];
        variable_set('forum_containers', $containers);
      }
      drupal_set_message(t('Created new @type %term.', array('%term' => $form_state['values']['name'], '@type' => $type)));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('The @type %term has been updated.', array('%term' => $form_state['values']['name'], '@type' => $type)));
      break;
  }
  // Start added by Quantic - 2010/06/17
  if (arg(0) == 'adminrecipe') {
  	
  	// Quantic START 20100903
  	$tid = $form_state['values']['tid'];
    $directory = file_create_path(variable_get('taxonomy_image_path', 'category_pictures'));
    $validators = array();
    $file_header = file_save_upload('header_image_upload', $validators, $directory, FILE_EXISTS_RENAME);
	
    if (is_object($file_header)) {
	  // If no errors while uploading, save term-image relation into DB
      $filepath = is_object($file_header)?$file_header->filepath: "";
      $filename_header = is_object($file_header)? str_replace($directory."/","",$file_header->filepath) : "";
      recipe_utils::get_resize_image_path($file_header->filepath, 542);
      
      if (taxonomy_image_forum_add($tid, $filename_header)) {
        // Successfully added to DB, make sure Cron doesn't delete this file.
        if($form['hdf_delete_image_header']['#value'] == 1 && $form['hdf_old_image_header']['#value'] != "" && file_exists($form['hdf_old_image_header']['#value']) ){
        	file_delete($form['hdf_old_image_header']['#value']);
        }
        file_set_status($file_header, FILE_STATUS_PERMANENT);
        drupal_set_message(t('Image uploaded as @name.', array('@name' => $filepath)));
      }
      else
        drupal_set_message(t('Error while adding image [tid = !tid, path = @path].', array('!tid' => $tid, '@path' => $filepath)), 'error');
    }
    // Quantic END 20100903
    
  	$form_state['redirect'] = 'adminrecipe/divagossip';
  } else {
  	$form_state['redirect'] = 'admin/content/forum';
  }
  // End added by Quantic - 2010/06/17
  return;
}

/**
 * Helper function for adding an image to a term
 */
function taxonomy_image_forum_add($tid, $filename) {
  $objTerm = db_fetch_object(db_query('SELECT term_image.*  FROM {term_image} WHERE tid=%d', $tid));
  if ($objTerm != null && $objTerm->tid == $tid) {
  	$filename = recipe_utils::isNull($filename)? $objTerm->path : $filename;
    if (db_query("UPDATE {term_image} SET path = '%s' WHERE tid = %d", $filename, $tid)) {
	    cache_clear_all("taxonomy_image:$tid", 'cache_tax_image');
	    return TRUE;
	  }
	  else {
	    return FALSE;
	  }
  }else{
	  if (db_query("INSERT INTO {term_image} (tid, path) VALUES ('%d', '%s')", $tid, $filename)) {
	    cache_clear_all("taxonomy_image:$tid", 'cache_tax_image');
	    return TRUE;
	  }
	  else {
	    return FALSE;
	  }
  }
}

/**
 * Check an external link.
 */
function forum_form_container_validate($form, &$form_state) {	
  if ($form['form_id']['#value'] == 'forum_form_container') {
  	if (isset($_FILES['files'])){  		
  		_check_file_upload_taxonomy('header_image_upload', 'Image' , arg(4), $form['hdf_delete_image_header']['#value']);
	}
  }
}

/**
 * Returns a form for adding a container to the forum vocabulary
 *
 * @param $edit Associative array containing a container term to be added or edited.
 * @ingroup forms
 * @see forum_form_submit()
 */
function forum_form_container(&$form_state, $edit = array()) {
  $edit += array(
    'name' => '',
    'description' => '',
    'tid' => NULL,
    'weight' => 0,
  );
  // Handle a delete operation.
  $form['name'] = array(
    '#title' => t('Container name'),
    '#type' => 'textfield',
    '#default_value' => $edit['name'],
    '#maxlength' => 255,
    '#description' => t('Short but meaningful name for this collection of related forums.'),
    '#required' => TRUE
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit['description'],
    '#description' => t('Description and guidelines for forums within this container.')
  );
  $form['parent']['#tree'] = TRUE;
  $form['parent'][0] = _forum_parent_select($edit['tid'], t('Parent'), 'container');
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $edit['weight'],
    '#description' => t('Containers are displayed in ascending order by weight (containers with equal weights are displayed alphabetically).')
  );

  $form['vid'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('forum_nav_vocabulary', ''),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  if ($edit['tid']) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['tid'] = array('#type' => 'value', '#value' => $edit['tid']);
  }
  $form['#submit'][] = 'forum_form_submit';
  $form['#theme'] = 'forum_form';
  return $form;
}

/**
 * Returns a form for adding a section to the vocabulary
 *
 * @param $edit Associative array containing a section term to be added or edited.
 * @ingroup forms
 * @see forum_form_submit()
 */
function forum_form_section(&$form_state, $edit = array()) {
  	$edit += array(
		    'name' => '',
		    'description' => '',
		    'tid' => NULL,
		    'weight' => 0,
	  	);
  	$form['name'] = array(
		'#title' => t('Section name'),
		'#type' => 'textfield',
		'#default_value' => $edit['name'],
		'#maxlength' => 255,
		'#required' => TRUE,
  	);

  	$form['parent']['#tree'] = TRUE;
  	$form['parent'][0] = _forum_parent_select($edit['tid'], t('Parent'), 'section');

	$form['weight'] = array('#type' => 'textfield',
		'#title' => t('Section order'),
		'#description' => MSG_FEEDBACK_SECTION_ORDER_DES,
		'#maxlength' => 2,
		'#size' => 6,
		'#default_value' => $edit['weight'],
		'#required' => false,
		'#weight' => 9,
		'#element_validate' => array('section_weight_validator'),
		);
  	$form['vid'] = array(
	    '#type' => 'hidden',
	    '#value' => FEEDBACK_SECTION_VID,
  	);
  	$form['submit'] = array(
	    '#type' => 'submit',
	    '#value' => t('Save'),
  	);

	if ($edit['tid']) {
    	$form['tid'] = array('#type' => 'value', '#value' => $edit['tid']);
  	}
  	$form['#submit'][] = 'section_form_submit';
  	$form['submit']['#weight'] = 10;
  	$form['submit']['#value'] = "";
	$form['submit']['#attributes']['onClick'] = "return saveNode('Save');";
	$form['submit']['#name'] = 'btnSave';
	$form['submit']['#attributes']['class'] = 'bt_forum_save';
  	$form['#theme'] = 'forum_form';
  	return $form;
}

/**
 * Process section form submissions.
 */
function section_form_submit($form, &$form_state) {
  $status = taxonomy_save_term($form_state['values']);
  $form_state['redirect'] = 'adminrecipe/feedback/sections';
  return;
}

/**
 * Implementation of hook_form_validator().
 */
function section_weight_validator($element, &$form_state) {
   	$weight = trim($element['#value']);
   	if ($weight != ''){
   		if (is_numeric($element['#value']) == false) {
   			form_error($element, t(ERR_MSG_ISNUMERIC, array('@field_name' => 'Section order')));
   		}
   	}
}

/**
 * Returns a confirmation page for deleting a forum taxonomy term.
 *
 * @param $tid ID of the term to be deleted
 */
function forum_confirm_delete(&$form_state, $tid) {
  $term = taxonomy_get_term($tid);

  $form['tid'] = array('#type' => 'value', '#value' => $tid);
  $form['name'] = array('#type' => 'value', '#value' => $term->name);

  return confirm_form($form, t('Are you sure you want to delete the forum %name?', array('%name' => $term->name)), 'admin/content/forum', t('Deleting a forum or container will also delete its sub-forums, if any. To delete posts in this forum, visit <a href="@content">content administration</a> first. This action cannot be undone.', array('@content' => url('admin/content/node'))), t('Delete'), t('Cancel'));
}

/**
 * Implementation of forms api _submit call. Deletes a forum after confirmation.
 */
function forum_confirm_delete_submit($form, &$form_state) {
  taxonomy_del_term($form_state['values']['tid']);
  drupal_set_message(t('The forum %term and all sub-forums and associated posts have been deleted.', array('%term' => $form_state['values']['name'])));
  watchdog('content', 'forum: deleted %term and all its sub-forums and associated posts.', array('%term' => $form_state['values']['name']));

  $form_state['redirect'] = 'admin/content/forum';
  return;
}

/**
 * Form builder for the forum settings page.
 *
 * @see system_settings_form()
 */
function forum_admin_settings() {
  $form = array();
  $number = drupal_map_assoc(array(5, 10, 15, 20, 25, 30, 35, 40, 50, 60, 80, 100, 150, 200, 250, 300, 350, 400, 500));
  $form['forum_hot_topic'] = array('#type' => 'select',
    '#title' => t('Hot topic threshold'),
    '#default_value' => variable_get('forum_hot_topic', 15),
    '#options' => $number,
    '#description' => t('The number of posts a topic must have to be considered "hot".'),
  );
  $number = drupal_map_assoc(array(10, 25, 50, 75, 100));
  $form['forum_per_page'] = array('#type' => 'select',
    '#title' => t('Topics per page'),
    '#default_value' => variable_get('forum_per_page', 25),
    '#options' => $number,
    '#description' => t('Default number of forum topics displayed per page.'),
  );
  $forder = array(1 => t('Date - newest first'), 2 => t('Date - oldest first'), 3 => t('Posts - most active first'), 4 => t('Posts - least active first'));
  $form['forum_order'] = array('#type' => 'radios',
    '#title' => t('Default order'),
    '#default_value' => variable_get('forum_order', '1'),
    '#options' => $forder,
    '#description' => t('Default display order for topics.'),
  );
  return system_settings_form($form);
}

/**
 * Returns an overview list of existing forums and containers
 */
function forum_overview(&$form_state) {
  module_load_include('inc', 'taxonomy', 'taxonomy.admin');

  $vid = variable_get('forum_nav_vocabulary', '');
  $vocabulary = taxonomy_vocabulary_load($vid);
  $form = taxonomy_overview_terms($form_state, $vocabulary);

  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#term'])) {
      $term = $form[$key]['#term'];
      $form[$key]['view']['#value'] = l($term['name'], 'forum/'. $term['tid']);
      if (in_array($form[$key]['#term']['tid'], variable_get('forum_containers', array()))) {
        $form[$key]['edit']['#value'] = l(t('edit container'), 'admin/content/forum/edit/container/'. $term['tid']);
      }
      else {
        $form[$key]['edit']['#value'] = l(t('edit forum'), 'admin/content/forum/edit/forum/'. $term['tid']);
      }
    }
  }

  // Remove the alphabetical reset.
  unset($form['reset_alphabetical']);

  // The form needs to have submit and validate handlers set explicitly.
  $form['#theme'] = 'taxonomy_overview_terms';
  $form['#submit'] = array('taxonomy_overview_terms_submit'); // Use the existing taxonomy overview submit handler.
  $form['#validate'] = array('taxonomy_overview_terms_validate');
  $form['#empty_text'] = '<em>'. t('There are no existing containers or forums. Containers and forums may be added using the <a href="@container">add container</a> and <a href="@forum">add forum</a> pages.', array('@container' => url('admin/content/forum/add/container'), '@forum' => url('admin/content/forum/add/forum'))) .'</em>';
  return $form;
}

/**
 * Returns a select box for available parent terms
 *
 * @param $tid ID of the term which is being added or edited
 * @param $title Title to display the select box with
 * @param $child_type Whether the child is forum or container
 */
function _forum_parent_select($tid, $title, $child_type) {

  $parents = taxonomy_get_parents($tid);
  if ($parents) {
    $parent = array_shift($parents);
    $parent = $parent->tid;
  }
  else {
    $parent = 0;
  }
  
  // Start modified by Quantic - 2010/070/14
  if ($child_type == 'section') {
  	$vid = FEEDBACK_SECTION_VID;
  } else {
  	$vid = variable_get('forum_nav_vocabulary', '');
  }
  // End modified by Quantic - 2010/070/14
  $children = taxonomy_get_tree($vid, $tid);

  // A term can't be the child of itself, nor of its children.
  foreach ($children as $child) {
    $exclude[] = $child->tid;
  }
  $exclude[] = $tid;

  $tree = taxonomy_get_tree($vid);
  $options[0] = '<'. t('root') .'>';
  if ($tree) {
    if ($child_type == 'section') {
    	foreach ($tree as $term) {
	      if ($term->parents[0] == 0) {
	      	$options[$term->tid] = $term->name;
	      }
	    }
    } else {
	    foreach ($tree as $term) {
	      if (!in_array($term->tid, $exclude)) {
	        $options[$term->tid] = str_repeat(' -- ', $term->depth) . $term->name;
	      }
	    }
    }
  }
  if ($child_type == 'container') {
    $description = t('Containers are usually placed at the top (root) level, but may also be placed inside another container or forum.');
  }
  else if ($child_type == 'forum') {
    $description = t('Forums may be placed at the top (root) level, or inside another container or forum.');
  }

  return array('#type' => 'select', '#title' => $title, '#default_value' => $parent, '#options' => $options, '#description' => $description, '#required' => TRUE);
}
