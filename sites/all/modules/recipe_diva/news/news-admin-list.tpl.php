<div id="admin_border_content">
    <form name="frmNewsList" method="post" id="news-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/news', 'txtSearch');return false;">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">News Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/news', 'txtSearch');return false;"/>
            </div>
		</div>
		<?php 
		    $delete_q = drupal_query_string_encode($_GET, array('q'));	    	
    		$title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
    		$title_q = $title_q != "" ? $title_q.'&':"";
    		
		?>
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmNewsList, 'chk_', this)" />
			</div>
      		<div id="admin_home_title1">
      			<a href="javascript:changePaging('news-form', '<?echo C_BASE_PATH?>adminrecipe/news?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="title")?"desc":"asc"?>&order=title', '0');">
      			<span class="admin_title">Title</span></a>
			</div>
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('news-form', '<?echo C_BASE_PATH?>adminrecipe/news?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
      			<span class="admin_title">Created</span></a>
			</div>
      		<div id="admin_home_status">
      			<a href="javascript:changePaging('news-form', '<?echo C_BASE_PATH?>adminrecipe/news?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="status")?"desc":"asc"?>&order=status', '0');">
      			<span class="admin_title">Status</span></a>
			</div>
      		<div id="admin_home_status">
      			<a href="javascript:changePaging('news-form', '<?echo C_BASE_PATH?>adminrecipe/news?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="order")?"desc":"asc"?>&order=order', '0');">
      			<span class="admin_title">Order</span></a>
			</div>	  	
  	  	  	<div id="admin_home_op_link" class="admin_title">Operations</div>
      	</div>      	
      	<?php 
		$news_id = 0;
	    foreach ($newslist as $row) {	    	
	    	$nodeLink = url("adminrecipe/news/".$row->nid."/edit", array('query' => drupal_get_destination()));	    	
	    ?>
	    <div id="admin_recipes_content_inner<?php print $news_id % 2 ? "":"01"?>">
	    	<div class="admin_title" id="admin_recipes_col_del">
	    		<input type="checkbox" onclick="checkAllChange(document.frmNewsList, 'chk_', this, document.frmNewsList.chkAll)" value="<?php print $row->nid?>" name="chk_<?php print $news_id?>">
	    	</div>
	        <div id="admin_home_title1">
	        	<a href="<?php print $nodeLink?>"><?php print $row->title?></a>
	        </div>
	        <div id="admin_home_created">
	        	by <a class="by_author" title="View user profile." href="<?php print C_BASE_PATH."user/".$row->name?>"><?php print $row->name?></a>
	        	<br><span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
        	</div>
	        <div id="admin_home_status">
	        	<?php print $arr_status[$row->field_news_status_value]?>
	        </div>
	        <div id="admin_home_status">
	        	<?php print $row->field_news_order_value?>
	        </div>	        
	        <div class="admin_title" id="admin_home_op_link">
	        	<a onclick="return deleteAdminItem('frmNewsList', '<?php print C_BASE_PATH."adminrecipe/news?".$delete_q?>', '<?php print $row->nid?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
	        	</a>
	        </div>
      	</div>
	    <?php $news_id++;} ?>
      	<input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/news"?>">
      	<input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmNewsList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/news?".$delete_q; ?>')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmNewsList', '<?php print C_BASE_PATH."adminrecipe/news/add/recipe-news?destination=adminrecipe/news/"; ?>');" />
		</div>		
    	<?php print $pager ?>
	</div>
	</form>
</div>
