<?php
//	$Id:	divatips-add.tpl.php,v	1.0	2010/05/18	10:49:00	dries	Exp	$

/**
	*	@file	divatips-add.tpl.php
	*	Theme	implementation	to	display	the	node	information	and	post	comment	for	node.
	*
	*	Available	variables:
	*	-	$node:	node	information
	*
	*	@see	template_preprocess_divatips_add()
	*	@see	theme_divatips_add()
	*/
/*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');
drupal_add_js(C_SCRIPT_PATH.'jquery.filestyle.js');*/
global	$user;
global	$img_err_msg;
$page_title	=	get_page_title_divatips_add(1);
drupal_set_title($page_title);
//	Checks	validate	input	data
$err_mess	=	create_divatips_error($img_err_msg);
?>
<div	id="news_p">
	<div	id="news_p_b">
		<div	id="news_border">
			<div	id="news_p_content"	class="news_p_content_font">
				<div	id="divatips_add_content_b">
					<div	id="divatips_add_content"	class="bg-none">
						<div	class="registration_p_title"><?php	print	$page_title;	?></div>
						<div	id="divatips_add_content_space">&nbsp;
						</div>
						<?php	print	$err_mess;	?>
						<!--<div	id="divatips_add_content_inner">
							<span	class="recipes_upload_lbl">Your	name:	</span><?php	print	recipe_utils::create_author_link($user->name,	$user->name,	'new_divatips_author');	?>
						</div>-->
							<?php	print	$preview_form	?>
							<?php	print	$tip_form	?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--	END	FEATURE	RECIPES	-->
<script	type="text/javascript">
	$(document).ready(function(){
		//	load	radio	buttons
		onload_radio_checkbox('divatips_content_status_radio');
		onload_radio_checkbox('divatips_publish_status_radio');
		//onload_radio_checkbox('divatips_location_status_checkbox');
		//	load	image	buttons
		$("input[type=file]").filestyle({
				image:	"<?php	print	C_IMAGE_PATH."button/browse_btn.gif"?>",
				imageheight	:	25,
				imagewidth	:	75,
				width	:	170
		});
		//	hidden	input	image	field
		var	divImagePreview	=	document.getElementById("imagefield-preview");
		if	(divImagePreview	!=	undefined)	{
			onload_image_upload();
		}

		onload_radio_checkbox('divatips_tip_type_radio');

	});


</script>
