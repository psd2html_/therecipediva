<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-admin-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_admin_list()
 * @see theme_divatips_admin_list()
 */
drupal_set_title("Update Description");
?>
<div id="admin_border_content">
	<div class="registration_p_title">Update Description</div>
	<div id="divatips_add_content_space">&nbsp;
	</div>
    <div id="admin_recipe_content">
    	<form name="frmUpdate" method="post" id="divatips-update-form" >
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnUpdateTip" value="Update Divatips"  onClick="submitForm('frmUpdate', '', 'Tip');" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnUpdateRecipe" value="Update Recipe"  onClick="submitForm('frmUpdate', '', 'Recipe');" />
		</div>
    	<input type="hidden" name="op" value="">
    	</form>
	</div>
</div>
