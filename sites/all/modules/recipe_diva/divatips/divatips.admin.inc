<?php
// $Id: divatips.module,v 1.0 2010/06/09 01:31:26 drumm Exp $

/**
 * Call back function
 * 
 * @param $nid node id
 * 
 */
function divatips_list($nid = 0){
	$op = $_POST['op'];
	divatips_delete_items();
	// Collect the posted search query
	$q = trim($_POST['text']);
	if ($q != '' && $op != 'Delete' && $op != 'DeleteItem') {
   		$q = preg_replace('/\s+/',' ', $q);
	   	drupal_goto("adminrecipe/divatips/search", 'text='.urlencode($q).'&page=0');
	   	return;
	}
	
	$tip_list = get_divatips_list();
	$output = theme('divatips_admin_list', $tip_list, null);
	return $output;
}

/**
 * Call back function
 * 
 */
function divatips_search_list(){
	divatips_delete_items();
	$tip_list = get_divatips_list();
	$output = theme('divatips_admin_list', $tip_list, null);
	return $output;
}

/**
 * Delete diva tips
 */
function divatips_delete_items() {
	$op = $_POST['op'];
	if ($op == 'Delete') {
		// Delete selected items
		adminrecipe_delete($_POST);
	} else if ($op == 'DeleteItem') {
		$node_id = $_POST['delId'];
		node_delete($node_id);
	}
}
/**
 * Get diva tip list
 * 
 * @param $arr_status status array
 * @return diva tip list
 */
function get_divatips_list() {
	$output = "";
	// Create condition stament for searching data
	$stament  = "";
	//$text_search = $_POST['txtSearch'];
	$text_search = '';
	if ($_GET['text'] != '') {
		$text_search = urldecode($_GET['text']);
		$text_search = trim($text_search);
	}
	$args = array();
	if ($text_search != '') {
		$text_search = recipe_utils::sql_escape_string($text_search);
		// Get search condition
		$obj_search = recipe_utils::get_search_cond(array('v.title', 'v.body'), $text_search);
		$args = $obj_search->params;
		$stament = " where ".$obj_search->condition;
	}
	// define array status
	$arr_status = array(SUBMITTED_STATUS => SUBMITTED_STATUS_VALUE, 
						APPROVED_STATUS => APPROVED_STATUS_VALUE);
	// define array content status
	$arr_content_status = array(ARCHIVED_STATUS => ARCHIVED_STATUS_VALUE, 
								INUSE_STATUS => INUSE_STATUS_VALUE);
	// Create select sql stament
	$sql = create_admin_diva_tips_list_query($stament);
	// Create sql stament for counting the total number of rows from select stament 
	$count_sql  = create_count_admin_diva_tips_list_query($stament);
	// Execute sql stament
	//$result = pager_query($sql, ADMIN_TIP_PER_PAGE, 0, $count_sql, $text_search , $text_search, $text_search );
	$result = pager_query($sql, ADMIN_TIP_PER_PAGE, 0, $count_sql, $args );
	$destination = drupal_get_destination();
	$index = 0;
	// Fetch data from database
	while ($data = db_fetch_object($result)) {
		if ($index % 2) {
			$div = '<div id="admin_recipes_content_inner">';
		} else {
			$div = '<div id="admin_recipes_content_inner01">';
		}
		$output .= $div.
		      		'<div id="admin_recipes_col_del" class="admin_title">' .
		      		'<input type="checkbox" name="chk_'.$index.'" value="'.$data->nid.'" onclick="checkAllChange(document.frmDivatipList, \'chk_\', this, document.frmDivatipList.chkAll)" /></div>
			        <div id="admin_divatips_title"><a href="'.url("adminrecipe/divatips/$data->nid/edit", array('query' => $destination)).'">'.$data->title.'</a></div>
			        <div id="admin_divatips_created">'. recipe_utils::create_profile_link($data, 'admin', false).'</div>
			        <div id="admin_divatips_status">'.$arr_status[$data->field_tip_status_value].'</div>'.
			        '<div id="admin_divatips_content_status">'.$arr_content_status[$data->field_tip_content_status_value].'</div>'.
			        '<div id="admin_divatips_op_link"><a href="javascript:deleteAdminItem(\'frmDivatipList\', \'\', \''.$data->nid.'\')">Delete</a></div>
		      	</div>';
		$index = $index + 1;
	}
	return $output;
}

/**
 * Call back function
 */
function divatips_update() {
	$op = $_POST['op'];
	if ($op == 'Tip') {
		// Update description of divatips
		$sel_sql = "select nid, field_tip_description_value from content_type_recipe_tips";
		$data = db_query($sel_sql);
		//$update_sql_all = "";
		while ($record = db_fetch_object($data)) {
			$update_sql = "update {content_type_recipe_tips} set field_tip_description_value = '%s' where nid = %d";
			$description = trim($record->field_tip_description_value);
			$description = strip_tags($description, "<br><br />");
			$arr_patern = array("<br>", "<br />");
			$arr_replace = array("\n", "\n");
			$description = preg_replace($arr_patern, $arr_replace, $description);
			//db_query($update_sql, $description, $record->nid);
			$node = node_load($record->nid);
			$node->field_tip_description[0]['value'] = $description;
			node_save($node);
		}
	} else if ($op == 'Recipe') {
		// Update description of recipes
		$sel_sql = "select n.nid, v.body from {node} as n inner join {node_revisions} as v on (n.nid = v.nid) where n.type = 'recipe'";
		$data = db_query($sel_sql);
		while ($record = db_fetch_object($data)) {
			$description = trim($record->body);
			$description = strip_tags($description, "<br><br />");
			$arr_patern = array("<br>", "<br />");
			$arr_replace = array("\n", "\n");
			$description = preg_replace($arr_patern, $arr_replace, $description);
			$node = node_load($record->nid);
			$node->body = $description;
			node_save($node);
		}
	}
	return theme("divatips_admin_update");
}

/**
 * Create the query to get diva tips list
 * 
 * @param $stament SQL stament
 * @param $limit limit of select stament
 * 
 * @return sql query string
 */
function create_admin_diva_tips_list_query($stament = "", $limit = "") {
	$sql = "select v.title, v.body, n.nid, n.created, n.uid, c.field_tip_status_value, c.field_tip_content_status_value, c.field_tip_description_value, f.filepath, u.name ";
	$sql .= create_admin_sub_sql($stament, $limit);
	return $sql;
}

/**
 * Create the query to get the number of rows of diva tips list
 * 
 * @param $stament SQL stament
 * @param $limit limit of select stament
 * 
 * @return sql query string
 */
function create_count_admin_diva_tips_list_query($stament = "", $limit = "") {
	$sql = "select count(c.nid) ";
	$sql .= create_admin_sub_sql_count($stament, $limit);
	return $sql;
}

/**
 * Get sub sql stament
 * 
 * @param $stament SQL stament
 * @param $limit limit of select stament
 * 
 * @return string of sub sql
 */
function create_admin_sub_sql($stament = "", $limit = "") {
	$order = $_GET['order'];
	$divatip_list_header = recipe_utils::create_divatip_list_header_array();
	$sql .= "from content_type_recipe_tips as c ";
	$sql .= "inner join node_revisions as v on (c.nid = v.nid and c.vid = v.vid) ";
	$sql .= "inner join node as n on (n.nid = c.nid and n.vid = c.vid and n.status = '1') ";
	$sql .= "left join files as f on (f.fid = c.field_image_fid) ";
	$sql .= "left join users as u on (u.uid = n.uid) ";
	$sql .= $stament;
	if (isset($order) && $order != '') {
		$sql .= tablesort_sql($divatip_list_header);
	} else {
		$sql .= "order by n.created desc ";
	}
  	$sql .= $limit;
	return $sql;
}

/**
 * Get sub sql stament
 * 
 * @param $stament SQL stament
 * @param $limit limit of select stament
 * 
 * @return string of sub sql
 */
function create_admin_sub_sql_count($stament = "", $limit = "") {
	$sql .= "from content_type_recipe_tips as c ";
	$sql .= "inner join node_revisions as v on (c.nid = v.nid and c.vid = v.vid) ";
	$sql .= "inner join node as n on (n.nid = c.nid and n.vid = c.vid and n.status = '1') ";
	$sql .= "left join files as f on (f.fid = c.field_image_fid) ";
	$sql .= "left join users as u on (u.uid = n.uid) ";
	$sql .= $stament;
  	$sql .= $limit;
	return $sql;
}
?>
