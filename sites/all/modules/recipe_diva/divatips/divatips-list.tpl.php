<?php
// $Id: divatips-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_list()
 * @see theme_divatips_list()
 */

//if(isset($_GET['cid'])){
//    $site_url = C_SITE_URL.C_BASE_PATH;
//    drupal_add_link(array("rel"=>"canonical", "href"=>$site_url."divatips"));
//}
/*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
drupal_add_js(C_SCRIPT_PATH.'simplegallery.js');
?>
<!-- START DIVA TIPS RECIPES -->
<!-- display folder to choose for user's favorite -->
<?php
print recipe_utils::create_foler_selection('recipe_tips', 'My saved tip');
$inuse_tip_bar = recipe_db::get_admin_upload_image_url(DIVA_TIPS_PAGE, 1);
$archived_tip_bar = recipe_db::get_admin_upload_image_url(DIVA_TIPS_PAGE, 2);
//$destination = drupal_get_destination();
//$login_url = url('user/login', array('query' => $destination));
?>
<div id="Divatips_p">
    <?php if($tip_type == HEALTH_TIP_TYPE) {?>
    <div id="Divatips_p_t_contain">
        <div id="tips_contain">
            <div class="tips_content">
                <div class="divatips_link_hid">
                    <a href="<?php print C_BASE_PATH.'divatips';?>">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="30" width="150" />
                    </a>
                </div>
                <div class="healthtips_link">
                    <a href="<?php print C_BASE_PATH.'divatips?type=1';?>">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="45" width="148" />
                    </a>
                </div>
                <div  class="divatips_tabs_contain_hid">
                       <img src="<? print C_IMAGE_PATH ?>border/divatips_tab_hid.png" width="638" height="65" />
              </div>
                <div  class="healttips_tabs_contain">
                    <img src="<? print C_IMAGE_PATH ?>border/healthtips_tab.png" width="638" height="65" />
                </div>
          </div>
        </div>
    </div>
    <?php } else {?>
    <div id="Divatips_p_t_contain">
        <div id="tips_contain">
            <div class="tips_content">
                <div class="divatips_link">
                    <a href="<?php print C_BASE_PATH.'divatips';?>">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="45" width="150" />
                    </a>
                </div>
                <div class="healthtips_link_hid">
                    <a href="<?php print C_BASE_PATH.'divatips?type=1';?>">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="30" width="148" />
                    </a>
                </div>
                <div  class="divatips_tabs_contain">
                       <img src="<? print C_IMAGE_PATH ?>border/divatips_tab.png" width="638" height="65" />
                </div>
                <div  class="healttips_tabs_contain_hid">
                    <img src="<? print C_IMAGE_PATH ?>border/healthtips_tab_hid.png" width="638" height="65" />
                </div>
          </div>
        </div>
    </div>
    <?php }?>
    <div id="Divatips_p_b">
        <div id="Divatips_p_content">
        <div id="Divatips_p_content_inner_img">
            <div id="divatips_gallery"></div>
        </div>
            <div id="Divatips_p_content_inner">
                <div id="divatip_des">&nbsp;</div>
            </div>

        </div>
    </div>
</div>
    <!-- END DIVA TIPS RECIPES -->
      <div id="div_space"><img src="<?php print C_IMAGE_PATH ?>space.gif" width="1" height="15" /></div>
    <!-- START DIVA TIPS RECIPES -->
<div id="TipArchive_p">
    <?php if($tip_type == HEALTH_TIP_TYPE) {?>
    <div id="Health_TipArchive_p_t_contain">
        <div></div>
    </div>
    <?php } else {?>
    <div id="TipArchive_p_t_contain" style="background: url(<?php print $archived_tip_bar ? C_BASE_PATH.$archived_tip_bar : C_IMAGE_PATH.'tiparchive_top.gif'?>) no-repeat top left;">
        <div></div>
    </div>
    <?php }?>
    <div id="TipArchive_p_b">
        <div id="TipArchive_p_content">
            <div id="TipArchive_p_content_inner">
                <div id="TipArchive_Search_Form">
                    <form name="frmDivatips" action="<?php print url("divatips", array('query' => 'type='.$tip_type.'&page=0')); ?>" method="post" id="divatips-form">
                    <div id="searchfor_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                         <div id="searchfor_input">
                               <input type="text" name="text" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['text']); ?>"/>
                        </div>
                        <div class="archive_search_btn">
                            <input type="button" name="btnSearch" value="" class="bt_search" onClick="javascript:submitForm('frmDivatips', '', 'Search');"/>
                        </div>
                     </div>
                     <input type="hidden" name="page" value="">
                      <input type="hidden" name="op" value="">
                     </form>
                     <?php print $archived_item ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var divatip_des_div = document.getElementById("divatip_des");
var divatip_des = [<?php print $divatip_des ?>];
var nodeids = [<?php print $nodeids ?>];
var simpleGallery_navpanel={
        panel: {height:'45px', opacity:0, paddingTop:'0px', fontStyle:'bold 11px Verdana'}, //customize nav panel container
        images: ['<?php print C_IMAGE_PATH ?>previous.png', '<?php print C_IMAGE_PATH ?>next.png', '<?php print C_IMAGE_PATH ?>circle_r.png', '<?php print C_IMAGE_PATH ?>circle.png'], //nav panel images (in that order)
        imageSpacing: {offsetTop:[6, 6, 6 ,6, 6, 6, 6], spacing:5}, //top offset of left, play, and right images, PLUS spacing between the 3 images
        slideduration: 50 //duration of slide up animation to reveal panel
    }
var mygallery = new simpleGallery( {
    wrapperid: "divatips_gallery", //ID of main gallery container,
    dimensions: [627, 238], //width/height of gallery in pixels. Should reflect dimensions of the images exactly
    imagearray: [<?php print $divatip_img ?>],
    autoplay: [true, 5000, 5], //[auto_play_boolean, delay_btw_slide_millisec, cycles_before_stopping_int]
    persist: false, //remember last viewed slide and recall within same session?
    fadeduration: 500, //transition duration (milliseconds)
    oninit: function() { //event that fires when gallery has initialized/ ready to run

        //Keyword "this": references current gallery instance (ie: try this.navigate("play/pause"))
        $("img[id^=navImage]").css("filter", "");
        var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
        var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
        if (jQuery.browser.msie && (ie55 || ie6)) {
            $("img[id^=navImage]").attr("style","");
            $("div.navpanellayer").pngFix();
            $("span[id^=navImage]").css("cursor","pointer");
            $("span[id^=navImage]").css("top","6px");
            $("span[id^=navImage]").css("margin-right","5px");

            $("span[id^=navImage]").click(function(){
                $("img[id="+this.id+"]").click();
            });
        }
    },
    onslide: function(curslide, i) { //event that fires after each slide is shown
        //Keyword "this": references current gallery instance
        //curslide: returns DOM reference to current slide's DIV (ie: try alert(curslide.innerHTML)
        //i: integer reflecting current image within collection being shown (0=1st image, 1=2nd etc)
        var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
        var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
        if (jQuery.browser.msie && (ie55 || ie6)) {
            var style = "";
            for (var j = 0; j < divatip_des.length; j++) {
                style = "cursor:pointer; top:6px; margin-right:5px; DISPLAY: inline-block; BACKGROUND: none transparent scroll repeat 0% 0%;";
                if (j == i) {
                    $("img[id=navImage"+(j + 1)+"]").attr("src", simpleGallery_navpanel.images[2]);
                    style += " FILTER: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+simpleGallery_navpanel.images[2]+"', sizingMethod='scale');WIDTH: 11px; POSITION: relative; HEIGHT: 19px;";
                }
                else {
                    $("img[id=navImage"+(j + 1)+"]").attr("src", simpleGallery_navpanel.images[2]);
                    style += " FILTER: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+simpleGallery_navpanel.images[3]+"', sizingMethod='scale');WIDTH: 13px; POSITION: relative; HEIGHT: 19px;";
                }
                $("span[id=navImage"+(j + 1)+"]").attr("style",style);
            }

        }
        else{
            for (var j = 0; j < divatip_des.length; j++) {
                if (j == i) {
                    $("#navImage" + (j + 1)).attr("src", simpleGallery_navpanel.images[2]);
                }
                else {
                $("#navImage" + (j + 1)).attr("src", simpleGallery_navpanel.images[3]);
                }
            }
        }
        <?php //if (user_is_logged_in()) { ?>
        /*if (nodeids[i] != '') {
            var add_tip = '<a href="javascript:addFaviroteNode_new(\'<?php print url("favorite_nodes/checked/node/"); ?>'  + nodeids[i] + '\', \'<?php print url("favorite_nodes/add/"); ?>' + nodeids[i] + '\', \'<?php print ADD_MY_SAVED_TIP_MSG;?>\', \'<?php print MSG_ADDED_TIP;?>\', \'<?php print ERR_MSG_ADD_FAVORITE_NODE;?>\');">';
            add_tip += '<img alt="Add to My saved tips" src="<? print C_IMAGE_PATH ?>button/addtomysavetips_btn.gif"/></a><span id="nid_saved" style="display:none;">'  + nodeids[i] + '</span>';
            addtomytip_div.innerHTML = add_tip;
        }*/
        <?php //} ?>
        divatip_des_div.innerHTML = divatip_des[i];
         <?php if (user_is_logged_in()) { ?>
        if (nodeids[i] != '') {
            var add_tip = '<a href="javascript:addFaviroteNode_new(\'<?php print url("favorite_nodes/checked/node/"); ?>'  + nodeids[i] + '\', \'<?php print url("favorite_nodes/add/"); ?>' + nodeids[i] + '\', \'<?php print ADD_MY_SAVED_TIP_MSG;?>\', \'<?php print MSG_ADDED_TIP;?>\', \'<?php print ERR_MSG_ADD_FAVORITE_NODE;?>\');">';
            add_tip += '<img alt="Add to My saved tips" src="<? print C_IMAGE_PATH ?>button/addtomysavetips_btn.gif"/></a><span id="nid_saved" style="display:none;">'  + nodeids[i] + '</span>';
            var addtomytip_div = document.getElementById("addtomysavetips");
            addtomytip_div.innerHTML = add_tip;
        }
        <?php } ?>
    }
})
</script>
