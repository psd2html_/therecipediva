<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-admin-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_admin_list()
 * @see theme_divatips_admin_list()
 */
$search_url = url("adminrecipe/divatips");
?>
<div id="admin_border_content">
    <form name="frmDivatipList" method="post" id="divatips-form" onSubmit="javascript:submitForm('frmDivatipList', '<?php print $search_url;?>', 'Search');">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Diva Tips Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" name="text" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['text']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="javascript:submitForm('frmDivatipList', '<?php print $search_url;?>', 'Search');"/>
            </div>
		</div>
		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmDivatipList, 'chk_', this)" />
			</div>
      		<!--<div id="admin_divatips_title" class="admin_title">Title</div>
            <div id="admin_divatips_created" class="admin_title">Created</div>
  	  	  	<div id="admin_divatips_status" class="admin_title">Status</div>
  	  	  	<div id="admin_divatips_content_status" class="admin_title">Content Status</div>-->
  	  	  	<?php print $header; ?>
  	  	  	<div id="admin_divatips_op_link" class="admin_title">Operations</div>
      	</div>
      	<?php print $tip_list; ?>
      	<input type="hidden" name="page" value="">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmDivatipList', 'chk_', '')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmDivatipList', '<?php print url("adminrecipe/divatips/add"); ?>');" />
		</div>
    	<?php print $pager ?>
	</div>
	</form>
</div>