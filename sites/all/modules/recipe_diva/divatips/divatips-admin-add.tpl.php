<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-admin-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_admin_list()
 * @see theme_divatips_admin_list()
 */
global $user;
$page_title = get_page_title_divatips_add(2);
//$err_mess = create_divatips_error();
drupal_set_title($page_title);
?>
<div id="admin_border_content">
	<div class="registration_p_title"><?php print $page_title; ?></div>
	<div id="divatips_add_content_space">&nbsp;
	</div>
	<?php print $err_mess; ?>
    <!--<div id="divatips_add_content_inner">
		<span class="recipes_upload_lbl">Your name: </span><?php print recipe_utils::create_author_link($user->name, $user->name, 'new_divatips_author'); ?>
	</div>-->
    <?php print $tip_form; ?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		// load radio buttons
		//onload_radio_checkbox('divatips_content_status_radio');
		//onload_radio_checkbox('divatips_publish_status_radio');
		// load image buttons
		// hidden input image field
		var divImagePreview = document.getElementById("imagefield-preview");
		if (divImagePreview != undefined) {
			onload_image_upload();
		}
	});	
</script>