<?php
// $Id: divatips.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $node: diva tip information
 *
 * @see template_preprocess_divatips()
 * @see theme_divatips()
 */

//$tip_content = $node->body ;
$tip_content = $_POST['body'];
$tip_content = trim($tip_content);
if ($tip_content == '') {
	$tip_content = '<div>&nbsp;</div>';
}

//$tip_title = $node->title;
$tip_title = $_POST['title'];
// Get user profile link
if (is_array($node)) {
	$author_info = recipe_utils::create_author_info($node['uid'], $node['name'], time());
} else {
	$author_info = recipe_utils::create_author_info($node->uid, $node->name, $node->created);
}

drupal_set_title("Diva Tip Preview");
$start_body = '<div id="news_p">
					<div id="news_p_b">
   	  					<div id="news_border">';
$end_body = '</div></div></div>';
if (arg(0) == 'adminrecipe') {
	$start_body = '';
	$end_body = '';
}
?>
<?php print $start_body; ?>
<!-- START FEATURE RECIPES -->
<!--<div id="news_p">
    <div id="news_p_b">
   	  	<div id="news_border">-->
   	  		<div id="news_p_content" class="news_p_content_font">
	  	  		
          		<div id="img_contain">
          		<div id="recipe_right_content" style="width: 530px;">
          			<!-- test -->
          			
		  	  		<!-- start image -->
			        <?php if (file_exists($node->field_image[0]['filepath'])) :
			          		$width = recipe_utils::getImageWidthValue($node->field_image[0]['filepath'],320, false);
			          		// fix-height  = 309
					?>
			            <div id="tip_image" class="divatips_p_image"><img alt="<? print $tip_title?>" src="<? print C_BASE_PATH.$node->field_image[0]['filepath']; ?>" width="<?php print $width; ?>" /></div>
	        		<?php endif; ?>
		  	  		<!-- end image -->
		  	  		
		  	  		<div id="tip_title" class="divatips_p_title"><? print $tip_title?></div>
			  	  	<!-- addtotip here-->
	   		  		<div id="tip_date" class="new_p_posted"><?php print $author_info; ?></div>
	   		  		<?php
	   		  		//$description =  $node->field_tip_description[0]['value'];
	   		  		$description =  $_POST['field_tip_description'][0]['value'];
	   		  		$description = trim($description);
	   		  		$read_more_url = "javascript:expand_description('divatips_p_excerpt_desc', 'divatips_p_description');";
	   		  		$excerpt = recipe_utils::get_excerpt($description, NUMBER_WORDS_DESCRIPTION , $read_more_url);
	   		  		if ($excerpt == '') {
	   		  			$excerpt = "&nbsp;";
	   		  		}
	   		  		if ($description == '') {
	   		  			$description = "&nbsp;";
	   		  		}
	   		  		?>
	   		  		<div id="divatips_p_excerpt_desc" style="display:none;">
	   		  			<?php print $excerpt; ?>
	   		  		</div>
	   		  		<div id="divatips_p_description">
	   		  			<?php print $description; ?>
	   		  		</div>
					
		          	<!-- end test -->
	            	<div id="news_p_menu_tip">
		           		<div style="float: left; position: relative; left: 0; z-index: 11">
		                  	<img alt="Diva Tip Tab" src="<? print C_IMAGE_PATH ?>border/divatip_tab.png" width="541" height="50"/>
		                  	<!--<div id="divatips_addtomysavedtips"><a href="#"><img src="<? print C_IMAGE_PATH ?>button/addtomysavetips_btn.gif" width="135" height="18" /></a></div>-->
		                </div>
		                <!--<div style="float: left; position: absolute; left: 148px; top: 0px; z-index: 10">
		                  	<div class="reviews_counter"><?php print $comment_count; ?></div>
		                    <a href="#"><img src="<? print C_IMAGE_PATH ?>border/Reviews_tab02.png" width="153" height="35"/></a>
		                </div>
		                <div style="float: left; position: absolute; left: 148px; top: 0px; z-index: 12">
							<a href="#"><img src="<? print C_IMAGE_PATH ?>space.gif" width="153" height="35" style="cursor:pointer;"/></a>
		                </div>-->
	            	</div>

				</div>
				
			</div>	
          <!-- content -->
       
		<div id="div_tip" style="<?php print $display_tip; ?>" class="content_tip">
          	<div id="divatiptab_content" class="bg-none">
				<div id="divatiptab_content_tip"><? print $tip_content ?></div>
			</div>
        </div>
        <!-- content -->
      </div>
      <!--</div>
  </div>
</div>-->
<?php print $end_body; ?>
<!-- END FEATURE RECIPES -->
<script type="text/javascript">
	$(document).ready(function() {
			var divImage = document.getElementById("tip_image");
			var divTitle = document.getElementById("tip_title");
			var divDate = document.getElementById("tip_date");
			var divDesc = document.getElementById("divatips_p_description");
			var divExcDesc = document.getElementById("divatips_p_excerpt_desc");
			var contentHeight = divTitle.offsetHeight + divDate.offsetHeight + divDesc.offsetHeight;
			var imageHeight = 0;
			if (divImage != undefined) {
				imageHeight = divImage.offsetHeight
			}
			if (contentHeight > imageHeight) {
				divDesc.style.display = 'none';
				divExcDesc.style.display = '';
			}
			ResizeImage('divatiptab_content_tip', 525);
			ResizeImage('divatips_p_excerpt_desc', 525);
		});
</script>