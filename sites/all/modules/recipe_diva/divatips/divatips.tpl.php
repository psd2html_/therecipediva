<?php
// $Id: divatips.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $node: diva tip information
 *
 * @see template_preprocess_divatips()
 * @see theme_divatips()
 */
if(!isset($_GET["cid"])){
    $urlCur = "divatips/".arg(1);
    $urlBack = "divatips/".strtolower(recipe_utils::removeWhiteSpace($node->title))."-".$node->nid;
    if($urlCur != $urlBack) {
        //drupal_goto($urlBack, NULL, NULL, 301);
    }
}
/*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
drupal_add_js('sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js');


$description =  $node->field_tip_description[0]['value'];
$description = nl2br(trim($description));
$description = str_replace('"','\'',$node->title.' - '.$description);

$site_url = C_SITE_URL.C_BASE_PATH;
drupal_set_html_head('<meta property="og:title" content="'.$node->title.'"/>
                      <meta property="og:type" content="food"/>
                      <meta property="og:url" content="'.$site_url . "divatips/" . recipe_utils::removeWhiteSpace($node->title) . "-" . $node->nid.'"/>
                      <meta property="og:image" content="'.$site_url . (empty($node->field_image[0]['filepath']) ? 'sites/all/themes/recipe_diva/images/logo.png':$node->field_image[0]['filepath']).'"/>
                      <meta property="og:site_name" content="TheRecipeDiva"/>
                      <meta property="fb:admins" content="6230637"/>
                      <meta property="og:description" content="The Recipe Diva\'s Tip for '.$node->title.'" />
                    ');
//if(isset($_GET['cid'])){
//    drupal_add_link(array("rel"=>"canonical", "href"=>$site_url."divatips/".recipe_utils::removeWhiteSpace($node->title)."-".$node->nid));
//}
// set breadcrumb
$breadcrumb[] = l(t('TheRecipeDiva'), $site_url);
$breadcrumb[] = l(t('Diva Tips'), 'divatips');
$breadcrumb[] = '<span>' . t($node->title) . '</span>';
drupal_set_breadcrumb($breadcrumb);

$display_tip = "";
$display_review = "";
$display_comment_lst = "";
$display_form = "display:none;";
$cid = $_GET['cid'];
if ($cid > 0) {
    $display_tip = "display:none;";
} else if ($cid == '0') {
    $display_review = "";
    $display_tip = "display:none;";
    $display_comment_lst = "display:none;";
    $display_form = "";
} else {
    $display_review = "display:none;";
    $display_comment_lst = "display:none;";
    $display_form = "display:none;";
}
// Checks permission of logged in user
$check_perm = check_permission($node);

// Get posted user
//$post_user = user_load(array('uid' => $node->uid));
global $user;
// Get user profile link

if($node->field_tip_type[0]['value'] == HEALTH_TIP_TYPE) {
    $author_roles = get_roles_by_user_name($node->name);
    if($author_roles == C_ADMIN_USER) {
        $author_name  = DEFAULT_HEALTHTIPS_AUTHOR;
    } else {
        $author_name  = $node->name;
    }
} else {
    $author_name  = $node->name;
}

$author_info = recipe_utils::create_author_info($node->uid, $author_name, $node->created);
// Get body content of selected tip
$tip_content = $node->body;
$tip_content = trim($tip_content);
if ($tip_content == '') {
    $tip_content = '<div>&nbsp;</div>';
}
drupal_set_title($node->title);

// Get comment list of node
$result = recipe_db::get_node_comment($node->nid);
// Get review link
//$review_link = url("comment/reply/".$node->nid);
$review_link = url("divatips/$node->nid", array('query' => 'cid=0'));
$img_review = '<img alt="Write preview" src="'.C_IMAGE_PATH.'button/writereview.gif" style="cursor:pointer;" />';
$review_link = '<a href="'.$review_link.'">'.$img_review.'</a>';
//$review_link = '<a href="javascript:write_review()">'.$img_review.'</a>';
$destination = drupal_get_destination();
$login_url = url('user/login', array('query' => $destination));
$login_link = '<a href="'.$login_url.'">'.$img_review.'</a>';
$comment_content = "";
if (user_is_logged_in()) {
    $comment_content = '<div id="writereview">'.$review_link.'</div>';
    //$comment_content = '<div style="float:right;padding-top:10px;">'.$review_link.'</div>';
} else {
    $comment_content = '<div id="writereview">'.$login_link.'</div>';
}

$comment_count = comment_num_all($node->nid);
$index = 0;
// Get comment list
while ($comment = db_fetch_object($result)){
    // Create links comment
    $links = create_comment_update_links($comment);
    // Add style
    $div_style = "";
    $div_vote_style = "divatiptab_vote";

//	if ($index == 0) {
//		$div_vote_style = "divatiptab_vote0";
//	}
    if ($index == ($comment_count - 1)) {
        $div_style = 'style="background: none;"';
    }
    $fivestar_view = theme('fivestar_static', $comment->value, variable_get('fivestar_stars_'. $node->type, 5));
    //$profile_link = recipe_utils::create_author_link($comment->uid, $comment->name);
    //$date = format_date($comment->timestamp, 'custom', 'F d Y');
    $comment_content .= '<div id="divatiptab_content" '.$div_style.'>';
    $comment_content .= '	<div id="'.$div_vote_style.'">'.$fivestar_view .'</div>';

    $comment_content .= '		<div id="divatiptab_content_inner">';
    //$comment_content .= 			'<span class="by">Reviewed By</span>: '.$profile_link.' '.$date .'<br />' ;
    $comment_content .= '<div id="divatiptab_author">'.recipe_utils::create_author_info($comment->uid, $comment->name, $comment->timestamp).'</div>' ;
    $comment_content .= '<div id="review_content">'.$comment->comment.'</div>';
    $comment_content .= $links;
    $comment_content .= '	</div>';
    $comment_content .= '</div>';
    $index = $index + 1;
}

if ($comment_count == 0) {
    $comment_count = "";
    $comment_content .= '<div>&nbsp;</div>';
} else {
    $comment_count = "(".$comment_count.")";
}
$comment_content .="<br>";

// create error message
$err_mess = create_review_error();
?>

<!-- START FEATURE RECIPES -->
<!-- create delete confirmation form -->
<?php print recipe_utils::create_delete_confirm_form($node->nid); ?>

<!-- display folder to choose for user's favorite -->
<?php print recipe_utils::create_foler_selection($node->type, 'My Saved Tip'); ?>
<form name="frmDivatipDetail" method="post" action="<?php print url("divatips/$node->nid/delete");?>">
    <input type="hidden" name="op" value="">
    <input type="hidden" name="delId" value="">
</form>
<div id="news_p">
    <div id="news_p_b">
             <div id="news_border">
                 <div id="news_p_content" class="news_p_content_font">

                  <div id="img_contain">
                  <div id="recipe_right_content" style="width: 613px;">
                      <!-- test -->

                        <!-- start image -->
                    <?php if (file_exists($node->field_image[0]['filepath'])) :
                              $width = recipe_utils::getImageWidthValue($node->field_image[0]['filepath'],320, false);
                              // fix-height  = 309
                    ?>
                        <div id="tip_image" class="divatips_p_image"><img alt="<? print $node->title?>" src="<? print C_BASE_PATH.$node->field_image[0]['filepath']; ?>" width="<?php print $width; ?>" /></div>
                    <?php endif; ?>
                        <!-- end image -->

                        <div id="tip_title" class="divatips_p_title"><span id="nid_saved" style="display:none;"><?php print $node->nid?></span><h1 style="font-size: 27px; font-weight: normal;"><? print $node->title?></h1></div>
                        <!-- addtotip here-->
                         <div id="tip_date" class="new_p_posted"><?php print $author_info; ?></div>
                         <?php
                         $description =  $node->field_tip_description[0]['value'];
                         $description = nl2br(trim($description));
                         $read_more_url = "javascript:expand_description('divatips_p_excerpt_desc', 'divatips_p_description');";
                         $excerpt = recipe_utils::get_excerpt( $node->field_tip_description[0]['value'], strlen($data->field_tip_description_value) , $read_more_url);
                         if ($excerpt == '') {
                             $excerpt = "&nbsp;";
                         }
                         if ($description == '') {
                             $description = "&nbsp;";
                         }
                         $width = recipe_utils::getImageWidthValue($node->field_image[0]['filepath'], RECIPE_IMAGE_WIDTH);
                         $style = "float: left; padding-top:10px;" ;
                         $width_fb_like = 613;
                         $bg_width = 600 - ($width + 5);
                         if ($width){
                            $style = "float: right;" . " width:" . $bg_width . "px; padding-top:10px;" ;
                            $width_fb_like= $bg_width;
                         }
                         ?>
                         <div id="divatips_p_excerpt_desc" style="display:none;">
                             <?php print $excerpt; ?>
                         </div>
                         <div id="divatips_p_description">
                             <?php print $description; ?>

                         </div>
                         <div id="divatips_p_description_fb" style="<?php print $style?>">
                             <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
                             <fb:like href="<?php echo $site_url . "divatips/" . recipe_utils::removeWhiteSpace($node->title) . "-" . $node->nid?>" send="true" width="<?php print $width_fb_like?>" show_faces="false" font=""></fb:like>
                         </div>

                         <!-- add to saved tips -->
                         <?php
                         if (user_is_logged_in()) {
                             $addtip_link = '<a href="javascript:addFaviroteNode_new(\''.url("favorite_nodes/checked/node/". $node->nid).'\', \''.url("favorite_nodes/add/". $node->nid).'\', \''.ADD_MY_SAVED_TIP_MSG.'\', \''.MSG_ADDED_TIP.'\', \''.ERR_MSG_ADD_FAVORITE_NODE.'\');">';
                         } else {
                             $addtip_link = '<a href="'.$login_url.'">';
                         }
                         ?>
                         <!--<div id="divatips_detail_space">&nbsp;</div>-->
                    <!-- add to saved tips -->
                      <!-- end test -->
                    <div id="news_p_menu_tip" style="<?php print $display_tip; ?>">
                           <div style="float: left; position: relative; left: 0; z-index: 11">
                              <img alt="Diva Tip Tab" src="<? print ($node->field_tip_type[0]['value'] == 1) ? C_IMAGE_PATH.'border/healthtip_tab.png' : C_IMAGE_PATH.'border/divatip_tab.png'?>" width="624" height="50"/>
                              <div id="divatips_addtomysavedtips"><?php print $addtip_link; ?><img alt="Add to My saved tips" src="<? print C_IMAGE_PATH ?>button/addtomysavetips_btn.gif" /></a></div>
                        </div>
                        <div style="float: left; position: absolute; left: 148px; top: 0px; z-index: 10">
                              <div class="reviews_counter"><?php print $comment_count; ?></div>
                            <span onclick="change_tab(2);"><img alt="Review Tab" src="<? print C_IMAGE_PATH ?>border/Reviews_tab02.png" width="153" height="35"/></span>
                        </div>
                        <div style="float: left; position: absolute; left: 148px; top: 0px; z-index: 12">
                            <div onclick="change_tab(2);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="153" height="35" style="cursor:pointer;"/></div>
                        </div>
                    </div>
                    <div id="news_p_menu_comment" style="<?php print $display_review; ?>">
                             <div>
                                 <span onclick="change_tab(1);"><img alt="Diva Tip Tab" src="<? print ($node->field_tip_type[0]['value'] == 1) ? C_IMAGE_PATH.'border/healthtips_tabs02.png' : C_IMAGE_PATH.'border/divatips_tabs02.png'?>" width="541" height="50" style="cursor:pointer;"/></span>
                                 <div id="divatips_addtomysavedtips"><?php print $addtip_link; ?><img alt="Add to My saved tips" src="<? print C_IMAGE_PATH ?>button/addtomysavetips_btn.gif" /></a></div>
                               <div style="float: left; position: absolute; right: 3px; top: -1px;">
                            <div id="reviews_counter_contain"><?php print $comment_count; ?></div>
                            <img alt="Review Tab" src="<? print C_IMAGE_PATH ?>border/Reviews_tab01.png" width="474" height="50"/>
                            </div>
                          </div>
                    </div>

                </div>

            </div>
          <!-- content -->

        <div id="div_tip" style="<?php print $display_tip; ?>" class="content_tip">
            <?php if ($check_perm != 0) : ?>

            <div id="update_divatip_link">

                <div class="update_divatip_link_detail">
                    <a href="javascript:deletePageItem('frmDivatipDetail', '', '<?php print $node->nid;?>', '<?php print CONF_MSG_TIP_DEL; ?>')"><img alt="Delete" src="<?php print C_IMAGE_PATH; ?>button/delete_icon_btn.gif"/></a>
                </div>
                <div class="update_divatip_link_detail">
                    <?php
                    $destination = drupal_get_destination();
                    $image = '<img alt="Edit" src="'.C_IMAGE_PATH.'button/edit_icon_btn.gif"/>';
                    ?>
                    <?php print l($image, "divatips/$node->nid/edit", array('query' => $destination, 'html' => true)); ?>
                </div>
            </div>
            <?php endif; ?>
              <div id="divatiptab_content" class="bg-none">
                <div id="divatiptab_content_tip"><? print $tip_content ?></div>
            </div>
        </div>
        <div id="div_comment" style="<?php print $display_review; ?>" class="content_tip">
            <div id="comment_list" style = "<?php print $display_comment_lst; ?>">
            <? print $comment_content ?>
            </div>
            <div id="comment_form" style = "<?php print $display_form; ?>">
                <?php print $err_mess; ?>
                <?php print $preview_form ?>
                <div id="divatips_comment_content_contain">
                    <span class="recipes_upload_lbl"><img alt="Write preview" src="<? print C_IMAGE_PATH ?>label/writereview_lbl.gif"/></span>
                </div>
                <div id="divatips_comment_content_contain">
                    <div id="newtopic_title">Your name:</div>
                    <div id="newtopic_name"><?php print recipe_utils::create_author_link($user->name, $user->name, 'newtopic_name');; ?></div>
                </div>
                <div id="divatips_comment_content_contain">
                    <div id="newtopic_title">Rate this tip:<span class="newtopic_title_red">*</span></div>
                </div>
                <?php print $comment_form; ?>
            </div>
        </div>
        <!-- content -->
      </div>
     </div>
    </div>
</div>
<!-- END FEATURE RECIPES -->
<script type="text/javascript">
    $(document).ready(function() {
            var divImage = document.getElementById("tip_image");
            var divTitle = document.getElementById("tip_title");
            var divDate = document.getElementById("tip_date");
            var divDesc = document.getElementById("divatips_p_description");
            var divExcDesc = document.getElementById("divatips_p_excerpt_desc");
            var contentHeight = divTitle.offsetHeight + divDate.offsetHeight + divDesc.offsetHeight;
            var imageHeight = 0;
            if (divImage != undefined) {
                imageHeight = divImage.offsetHeight
            }
            if (contentHeight > imageHeight) {
                divDesc.style.display = 'none';
                divExcDesc.style.display = '';
            }
            <?php if ($cid > 0 || $cid == '0'): ?>
            $('#news_p_menu_comment').pngFix();
            <?php else: ?>
            $('#news_p_menu_tip').pngFix();
            <?php endif; ?>
            ResizeImage('div_comment', 525);
            ResizeImage('divatiptab_content_tip', 525);
            ResizeImage('divatips_p_excerpt_desc', 525);
        });

    function change_tab(flag) {
        var divTip = document.getElementById("div_tip");
        var divComment = document.getElementById("div_comment");
        var divTipImage = document.getElementById("news_p_menu_tip");
        var divTipComment = document.getElementById("news_p_menu_comment");
        var divCommentList = document.getElementById("comment_list");
        var divCommentForm = document.getElementById("comment_form");
        if (flag == '1') {
            divTip.style.display = '';
            divComment.style.display = 'none';
            divCommentList.style.display = 'none';
            divTipImage.style.display = '';
            divTipComment.style.display = 'none';
            divCommentForm.style.display = 'none';
            $('#news_p_menu_tip').pngFix();
            ResizeImage('divatiptab_content_tip', 525);
        } else {
            divTip.style.display = 'none';
            divComment.style.display = '';
            divCommentList.style.display = '';
            divTipImage.style.display = 'none';
            divTipComment.style.display = '';
            divCommentForm.style.display = 'none';
            $('#news_p_menu_comment').pngFix();
            ResizeImage('div_comment', 520);
            $('#news_p_menu_comment').pngFix();
        }
    }
    /*
    function write_review() {
        var divCommentList = document.getElementById("comment_list");
        var divCommentForm = document.getElementById("comment_form");
        var frm = document.frmCommentForm;
        divCommentForm.style.display = '';
        divCommentList.style.display = 'none';
        tinyMCE.get('edit-comment').setContent('');
        frm.fivestar_rating.selectedIndex = 1;
        alert(frm.fivestar_rating.selectedIndex);
        //frm.cbbStatus.options[frm.cbbStatus.selectedIndex].text;
        //alert(tinyMCE.get('edit-comment').getContent() );
    }*/
</script>
 <script type="text/javascript">
<!--//--><![CDATA[//><!--

  function mceToggle(id, linkid) {
    element = document.getElementById(id);
    link = document.getElementById(linkid);
    img_assist = document.getElementById('img_assist-link-'+ id);

    if (tinyMCE.getEditorId(element.id) == null) {
      tinyMCE.addMCEControl(element, element.id);
      element.togg = 'on';
      link.innerHTML = 'disable rich-text';
      link.href = "javascript:mceToggle('" +id+ "', '" +linkid+ "');";
      if (img_assist)
        img_assist.innerHTML = '';
      link.blur();
    }
    else {
      tinyMCE.removeMCEControl(tinyMCE.getEditorId(element.id));
      element.togg = 'off';
      link.innerHTML = 'enable rich-text';
      link.href = "javascript:mceToggle('" +id+ "', '" +linkid+ "');";
      if (img_assist)
        img_assist.innerHTML = img_assist_default_link;
      link.blur();
    }
  }

//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

  tinyMCE.init({
    mode : "exact",
    theme : "advanced",
    relative_urls : false,
    document_base_url : "<?php print C_BASE_PATH;?>",
    language : "en",
    safari_warning : false,
    entity_encoding : "raw",
    verify_html : false,
    preformatted : false,
    convert_fonts_to_spans : true,
    remove_linebreaks : true,
    apply_source_formatting : true,
    theme_advanced_resize_horizontal : false,
    theme_advanced_resizing_use_cookie : false,
    plugins : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_path_location : "none",
    theme_advanced_resizing : true,
    theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,numlist,bullist,indent,outdent,separator,undo,redo,removeformat",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    elements : "comment"
  });

//--><!]]>
</script>
