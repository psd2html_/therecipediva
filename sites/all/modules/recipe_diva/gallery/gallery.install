<?php
// $Id: gallery.install,v 1.5.2.2 2011/03/30 10:40:56 goba Exp $

/**
 * Implementation of hook_install().
 */
function gallery_install() {
  // Create tables
  drupal_install_schema('gallery');
}

/**
 * Implementation of hook_schema().
 */
function gallery_schema() {

    $schema['gallery'] = array(
    'description' => 'Stores information for uploaded files.',
    'fields' => array(
      'fid' => array(
        'description' => 'Primary Key: Unique files ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'uid' => array(
        'description' => 'The {users}.uid of the user who is associated with the file.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      'filename' => array(
        'description' => 'Name of the file.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''),
      'filepath' => array(
        'description' => 'Path of the file relative to Drupal root.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''),
      'filemime' => array(
        'description' => 'The file MIME type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''),
      'filesize' => array(
        'description' => 'The size of the file in bytes.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      'status' => array(
        'description' => 'A flag indicating whether file is temporary (1) or permanent (0).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'timestamp' => array(
        'description' => 'UNIX timestamp for when the file was added.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      ),
    'indexes' => array(
      'uid' => array('uid'),
      'status' => array('status'),
      'timestamp' => array('timestamp'),
      ),
    'primary key' => array('fid'),
    );

    return $schema;
}

/**
 * Implementation of hook_uninstall().
 *
 * This will automatically remove the MySQL database tables for profile_location.
 *
 */
function gallery_uninstall() {
  // Remove tables
  drupal_uninstall_schema('gallery');

  variable_del('gallery');
}
