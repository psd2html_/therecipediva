<div id="admin_border_content">
    <form name="frmStaticpageList" method="post" id="staticpage-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/staticpage', 'txtSearch');return false;">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Static Page Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/staticpage', 'txtSearch');return false;"/>
            </div>
		</div>
		<?php 
		    $delete_q = drupal_query_string_encode($_GET, array('q'));	    	
    		$title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
    		$title_q = $title_q != "" ? $title_q.'&':"";
    		
		?>		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmStaticpageList, 'chk_', this)" />
			</div>
      		<div id="admin_home_title">
      			<a href="javascript:changePaging('staticpage-form', '<?echo C_BASE_PATH?>adminrecipe/staticpage?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="title")?"desc":"asc"?>&order=title', '0');">
      			<span class="admin_title">Title</span></a>
			</div>
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('staticpage-form', '<?echo C_BASE_PATH?>adminrecipe/staticpage?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
      			<span class="admin_title">Created</span></a>
			</div>
      		<div id="admin_mydiva_cow">
      			<a href="javascript:changePaging('staticpage-form', '<?echo C_BASE_PATH?>adminrecipe/staticpage?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="page")?"desc":"asc"?>&order=page', '0');">
      			<span class="admin_title">Page</span></a>
			</div>  	  	  	
  	  	  	<div id="admin_home_op_link" class="admin_title">Operations</div>
      	</div>      	
      	<?php 
		$staticpage_id = 0;	    			
	    foreach ($staticpagelist as $row) {
	    	$nodeLink = url("adminrecipe/staticpage/".$row->nid."/edit", array('query' => drupal_get_destination()));	    	
	    ?>
	    <div id="admin_recipes_content_inner<?php print $staticpage_id % 2 ? "":"01"?>">
	    	<div class="admin_title" id="admin_recipes_col_del">
	    		<input type="checkbox" onclick="checkAllChange(document.frmStaticpageList, 'chk_', this, document.frmStaticpageList.chkAll)" value="<?php print $row->nid?>" name="chk_<?php print $staticpage_id?>">
	    	</div>
	        <div id="admin_home_title">
	        	<a href="<?php print $nodeLink?>"><?php print $row->title?></a>
	        </div>
	        <div id="admin_home_created">
	        	by <a class="by_author" title="View user profile." href="<?php print C_BASE_PATH."user/".$row->name?>"><?php print $row->name?></a>
	        	<br><span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
        	</div>
	        <div id="admin_mydiva_cow">
	        	<?php print $arr_page[$row->field_static_page_position_value]?>
	        </div>
	        <div class="admin_title" id="admin_home_op_link">
	        	<a onclick="return deleteAdminItem('frmStaticpageList', '<?php print C_BASE_PATH."adminrecipe/staticpage?".$delete_q?>', '<?php print $row->nid?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
	        	</a>
	        </div>
      	</div>
	    <?php $staticpage_id++;} ?>
      	<input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/staticpage"?>">
      	<input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmStaticpageList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/staticpage?".$delete_q; ?>')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmStaticpageList', '<?php print C_BASE_PATH."adminrecipe/staticpage/add/recipe-static-page?destination=adminrecipe/staticpage/"; ?>');" />
		</div>		
    	<?php print $pager ?>
	</div>
	</form>
</div>