<div id="dialogConfirm" style="display:none" title="<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => "item"))?>">
</div>

<?php
    $aisleOption = '<select name="aisle_option" id="ddlAisle">
                    <option value="0">Select Aisle</option>';
    foreach ($ailse as $row) {
        $aisleOption.='<option value="'.$row->id.'">'.$row->name.'</option>';
    }
    $aisleOption .= '</select>';
    $user_role = recipe_utils::getUserRole();
    $hasPer =  $user_role == C_ADMIN_USER || $user_role == C_PREMIUM_USER ? true : false || $user_role == C_REGISTER_USER ? true : false;
?>

<form id="groceries-form" method="post" accept-charset="UTF-8" action="<?echo C_BASE_PATH?>mydiva/grocerylist/saveall">
    <div class="groceries_content_inner">
        <div class="messages error" style="display:none" id="divError">
            <ul></ul>
        </div>
        <div class="groceries_title">
            <div class="groceries_content">
                <div id="groceries_col_container_select">
                    <div id="groceries_col001">
                        <input type="text" id="txtTitle" maxlength="128" >
                    </div>
                    <div id="groceries_col03">
                        <?print $aisleOption?>
                    </div>
                    <div id="groceries_col04_1">
                        <img style="cursor:pointer" <?php print (!$hasPer)?'onclick="showConfirmBecomePrimium();return false"':'onclick="addGrocery()"' ?> src="<?echo C_IMAGE_PATH?>button/add_tolist_btn_1.gif">
                    </div>
                </div>
            </div>
        </div>
        <div class="groceries_select_content">
            <div id="groceries_col_btn_container">
                <div id="groceries_col06" style="text-align:left">
                    <span style="color:#DD0021;font-weight:bold">Select:&nbsp;&nbsp;</span> <a style="color:#431a15" href="javascript:checkedAllGroceries(true)">All&nbsp;</a> | <a style="color:#431a15" href="javascript:checkedAllGroceries(false)">&nbsp;None</a>
                </div>
                <div id="groceries_col08">
                    <img width="6" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                    <img style="cursor:pointer" onclick="deleteAllGrocery()" src="<?echo C_IMAGE_PATH?>button/clear_grocery_fields_btn.gif">
                </div>
                <div id="groceries_col07">
                    <a target="_blank" href="<?echo C_BASE_PATH?>groceries_print"><img src="<?echo C_IMAGE_PATH?>button/print_grocery_btn.gif"></a>
                    <img width="6" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                    <a href="javascript:mailRecipe('0','My Grocery List');">
                    <img src="<?echo C_IMAGE_PATH?>button/send_grocery_btn.gif"></a>
                </div>
            </div>
        </div>
        <div id="groceries_m_col">
            <div id="div_groceries_content">
                <?php
                    $aisle_id = -1;
                    foreach ($groceries as $row) {
                        if($aisle_id != $row->aisle_id){
                            if($aisle_id != -1){
                                print '</div>';
                            }
                            $aisle_id = $row->aisle_id;
                            print '<div class="groceries_title" id="groceries_title_'.$row->aisle_id.'"><div class="groceries_aisle_title">'.$row->aisle_name."</div>";
                        }?>
                        <div class="groceries_content" id="groceries_content_<?php print $row->nid?>" name="<?php print $row->nid?>">
                            <div id="groceries_col_container" name="groceries_col_container">
                                <div id="groceries_col01">
                                    <div class="grocery_checkbox"><input type="checkbox" id="<?php print $row->nid?>" name="chk_<?php print $row->nid?>"></div>
                                    <div class="grocery_title" id="div_grocery_title_<?php print $row->nid?>"><?php print $row->title?></div>
                                </div>
                                <div id="groceries_col09" style="display:none;">
                                    <?php if($row->recipe_id != ""){?>
                                    <a target="_blank" href="<?php print C_BASE_PATH."recipe/".$row->recipe_title."-".$row->recipe_id?>"><img style="cursor:pointer" title="Recipe" src="<?echo C_IMAGE_PATH?>button/link_recipe_btn.gif"></a>
                                    <?php }?>
                                    <img style="cursor:pointer" onclick="deleteGrocery('<?php print $row->nid?>')" src="<?echo C_IMAGE_PATH?>button/del_grocery_btn.gif" title="Delete">
                                    <img style="cursor:pointer" onclick="popupEditGrocery('<?php print $row->nid?>',false)" src="<?echo C_IMAGE_PATH?>button/edit_groceries_btn.gif" title="Edit">
                                </div>
                            </div>
                        </div>

                <?php }
                    if($aisle_id != -1){
                        print '</div>';
                    }
                ?>
            </div>

            <div class="groceries_title">
                <div id="spanError"></div>
            </div>
            <div class="groceries_note_title">
                Notes
            </div>
            <div class="groceries_content">
                <div id="groceries_col_container">
                    <div id="groceries_col05">
                        <textarea class="myprofile_textarea" onblur="saveNotes()" name="txtNote" id='txtNote'><?php print $groceries_note?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="" id="hidListnid" name="hidListnid">
</form>
<?include(".".C_BASE_PATH.'/sites/all/themes/recipe_diva/recipe_mail.php');?>
<div id="dialogConfirmBecomePrimium" style="display:none;text-align:center;" title="<?php print INFO_MSG_PREMIUM_FUNCTION?>">
    <div style="margin:7px 0px 0px 5px;text-align:center;">
        <input type="button" value="Yes, sign me up!" style="width:110px;" onclick="document.location.href='<?php print C_BASE_PATH?>become_premium'">
        <input type="button" value="No thanks" style="width:110px;"  onclick="closeDialogConfirmBecomePrimium();">
    </div>
</div>
<div id="div_grocery" style="display:none;overflow:hidden;cursor:move">
    <div id="email_recipe_contain">
        <div class="email_recipe_top">
            <div>&nbsp;</div>
        </div>
        <div class="email_recipe_m">
            <div class="email_recipe_content">
                <div id="email_popup">
                    <div class="pre_membership_p_title_1">Edit Grocery</div>
                    <div id="dot_bg1">
                        <img width="1" height="14" src="<?php print C_IMAGE_PATH;?>space.gif">
                    </div>
                </div>
                <div id="popup_left_col">
                      <div id="mail_content">
                          <div id="mail_content_col" style="font-weight:normal;">
                            <div id="div_error_grocery_message" class="message error" style="display:none;">
                                <ul style="margin-bottom:0px;"><span id="message_grocery_error"></span></ul>
                            </div>
                        </div>
                      </div>
                      <div id="mail_titles">
                        <div id="grocery_content_col">
                            <input type="text" name="txtEditGroceryTitle" id="txtEditGroceryTitle" maxlength="128" />
                            &nbsp;&nbsp;<?print str_replace('id="ddlAisle"','id="ddlEditGroceryAisle"',$aisleOption)?>
                        </div>
                      </div>
                      <div style="text-align:left;">
                          <div id="mail_title_col" style="padding-top:5px;">&nbsp;</div>
                          <img id="btn_save_grocery" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/save_btn.gif">
                          <img onclick="javascript:$('#div_grocery').dialog('close')" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/cancel_btn_1.gif">
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function saveNotes(){
        var note = $("#txtNote").val();
        if(note == "<?php print INFO_MSG_GROCERIES_NOTES?> "){
            note = "";
        }
        $.post("<?php print C_BASE_PATH."mydiva/grocerylist/savenotes"?>",
           { txtNote: note},
           function(data){
       },"text");

    }

    function popupEditGrocery(nid,disAisle){
        $("#txtEditGroceryTitle").val($.trim($("#div_grocery_title_"+nid).html()));
        var aisle_id =$("#groceries_content_"+nid).parents("div[id^=groceries_title_]")[0].id;
        aisle_id = aisle_id.replace("groceries_title_","");

        $("#ddlEditGroceryAisle").val(aisle_id);
        if(disAisle){
            $("#ddlEditGroceryAisle").attr('disabled', 'disabled');
        }
        else{
            $("#ddlEditGroceryAisle").attr('disabled', '');
        }

        $("#btn_save_grocery").unbind();
        $("#btn_save_grocery").click(function(){editGrocery(nid,aisle_id);});
        $("#div_grocery").dialog(
            { modal: true },
            { resizable: false },
            { height: 270},
            { width: 550},
            { draggable: false},
            { buttons:
                {
                    "Close": function() {
                                $(this).dialog("close");
                            }
                }
            },
            { open: function(event, ui) {
                        $("div[class^=ui-dialog-titlebar]").hide();
                        $("div[class^=ui-dialog-buttonpane]").hide();
                        $("#div_grocery").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                    }
            },
            { beforeclose: function(event, ui) {
                        $("#message_grocery_error").html("");
                        $("#div_error_grocery_message").hide();
                        $("#div_email").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                        $("div[class^=ui-dialog-titlebar]").show();
                        $("div[class^=ui-dialog-buttonpane]").show();
                    }
            }
        );
        $("#txtEditGroceryTitle").focus();
    }
    function addGrocery(nid) {
        var errorMsg = "";
        $("#divError > ul").html("");
        $("#divError").hide();
        if($.trim($("#txtTitle").val()) == "" || $("#txtTitle").val() == "<?php print INFO_MSG_GROCERIES_TITLE?> "){
            errorMsg = "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Grocery Title'))?></li>";

        }

        if($("#ddlAisle").val() == 0){
            errorMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Grocery Aisle'))?></li>";
        }

        if(errorMsg != ""){
            $("#divError > ul").html(errorMsg);
            $("#divError").show();
            $("#divError").focus();
        }
        else
        {
            // Check Duplicate Grocery
            /*
            var newAisle = $("div[id=groceries_title_" + $("#ddlAisle").val()+"]");
            if(newAisle.length > 0 ){
                if(checkDulicateGrocery($("#txtTitle").val(),$("#txtQuantity").val(),$("#ddlUnit").val(),newAisle[0].id,0)){
                    errorMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'grocery'))?></li>";
                    $("#divError > ul").html(errorMsg);
                    $("#divError").show();
                    $("#divError").focus();
                    return false;
                }
            }
            */

            $("#spanError").html("");
            $.post("<?php print C_BASE_PATH."mydiva/grocerylist/add"?>",
                       { txtTitle: $("#txtTitle").val(), ddlAisle:$("#ddlAisle").val() },
                       function(data){
                            var json = eval("(" + data + ")");
                            var nid = json['nid'];
                            if (json['status'] == "success" && nid != 0) {
                                var divGro = "<div class=\"groceries_content\" id=\"groceries_content_"+nid+"\">";
                                    divGro += "<div id=\"groceries_col_container\" name=\"groceries_col_container\">";
                                    divGro += "	<div id=\"groceries_col01\"><div class=\"grocery_checkbox\"><input type=\"checkbox\" name=\"chk_" + nid + "\">";
                                    divGro += "	</div><div class=\"grocery_title\" id=\"div_grocery_title_"+nid+"\">" + $("#txtTitle").val();
                                    divGro += "	</div></div>";
                                    divGro += " <div id=\"groceries_col09\" style=\"display:none\">";
                                    divGro += "  <img style=\"cursor:pointer\" onclick=\"deleteGrocery('" + nid + "')\" src=\"<?echo C_IMAGE_PATH?>button/del_grocery_btn.gif\">";
                                    divGro += "  <img style=\"cursor:pointer\" onclick=\"popupEditGrocery('" + nid + "',false)\" src=\"<?echo C_IMAGE_PATH?>button/edit_groceries_btn.gif\"></div>";
                                    divGro += "	</div>";
                                    divGro += "</div>";

                                var aisleArray = $("div[id^=groceries_title_]");
                                var isAppend = false;
                                for (i = 0; i < aisleArray.length; i++) {
                                    var groceries_title_id = "groceries_title_" + $("#ddlAisle").val();
                                    if(aisleArray[i].id == groceries_title_id){
                                        $("#" + aisleArray[i].id).append(divGro);
                                        isAppend = true;
                                    }
                                }
                                if(!isAppend){
                                    divGro = "<div class=\"groceries_title\" id=\"groceries_title_"+$("#ddlAisle").val()+"\"><div class=\"groceries_aisle_title\">" + $("#ddlAisle option:selected").text() + "</div>" + divGro + "</div>";
                                    $("#div_groceries_content").prepend(divGro);
                                }
                                $("div[name=groceries_col_container]").mouseover(function() {
                                            $(this).find("#groceries_col09").show();
                                            $(this).css("background-color","#fcebed");
                                          }).mouseout(function(){
                                            $(this).find("#groceries_col09").hide();
                                            $(this).css("background-color","transparent");
                                          });

                                clearGroceryFields();
                            }
                            else{
                                $("#spanError").text(json['status']);
                            }
                       },"text");
            }

    }
    function deleteGrocery(nid) {
        $("#dialogConfirm").attr("title", "<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => "item"))?>");
        $("#ui-dialog-title-dialogConfirm").text("<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => "item"))?>");
        $("#dialogConfirm").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "Yes": function() {
                                var url = "<?php print C_BASE_PATH."mydiva/grocerylist/delete/"?>" + nid;
                                $.get(url, function(data){
                                        var json = eval("(" + data + ")");
                                        if(json['status'] == "success"){
                                            $("#groceries_content_" + nid).remove();
                                            var aisleArray = $("div[id^=groceries_title_]");
                                            for (i = 0; i < aisleArray.length; i++) {
                                                if($("#" + aisleArray[i].id).children().size() == 1){
                                                    $("#" + aisleArray[i].id).remove();
                                                    return;
                                                }
                                            }
                                        }
                                        else{
                                            // Error alert(json['status']);
                                        }
                                   },"text");
                                $(this).dialog("close");
                            },
                    "No": function() {
                                $(this).dialog("close");
                            }
                }
            }
        );
    }
    function editGrocery(nid, aisle_id) {
        var url = "<?php print C_BASE_PATH."mydiva/grocerylist/edit/"?>" + nid;
        var groceryvalue = $("#txtEditGroceryTitle").val();
        var aislevalue = $("#ddlEditGroceryAisle").val();
        $("#message_grocery_error").html("");
        if($.trim(groceryvalue) != ""){
            $("#div_error_grocery_message").hide();
            $.post(url,{txtGroceryTitle:groceryvalue,ddlAisle:aislevalue} , function(data){
                    var json = eval("(" + data + ")");
                    if(json['status'] == "success"){
                        $('#div_grocery').dialog('close');
                        $("#div_grocery_title_"+nid).html(groceryvalue);
                        if(aisle_id != $("#ddlEditGroceryAisle").val()){
                            var aisleArray = $("div[id^=groceries_title_]");
                            var isAppend = false;
                            for (i = 0; i < aisleArray.length; i++) {
                                var groceries_title_id = "groceries_title_" + aislevalue;
                                if(aisleArray[i].id == groceries_title_id){
                                    $("#" + groceries_title_id).append($("#groceries_content_" + nid));
                                    isAppend = true;
                                }
                            }
                            if(!isAppend){
                                divGro = "<div class=\"groceries_title\" id=\"groceries_title_"+aislevalue+"\"><div class=\"groceries_aisle_title\">" + $("#ddlEditGroceryAisle option:selected").text() + "</div></div>";
                                $("#div_groceries_content").prepend(divGro);
                                $("#groceries_title_" + aislevalue).append($("#groceries_content_" + nid));
                            }

                            var aisleArray = $("div[id^=groceries_title_]");
                            for (i = 0; i < aisleArray.length; i++) {
                                if($("#" + aisleArray[i].id).children().size() == 1){
                                    $("#" + aisleArray[i].id).remove();
                                    return;
                                }
                            }
                        }
                    }
                    else{
                        // Error alert(json['status']);
                    }
               },"text");
        }
        else{
            errorMessage = "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Grocery Title'));?>" + "</li>";
            $("#message_grocery_error").html(errorMessage);
            $("#div_error_grocery_message").show()
        }
    }

    function saveAllGrocery(){
        var groceryArray = $("div[id^=groceries_content_]");
        var nid = "";
        var errorMsg = "";
        var aisleID = "";

        $("#spanError").text("");
        $("#hidListnid").val("");
        $("#divError > ul").html("");
        $("#divError").hide();

        // Check required
        for (i = 0; i < groceryArray.length; i++) {
            nid = groceryArray[i].id.replace("groceries_content_","");
            if($.trim($("input[name=txtTitle_" + nid + "]").val()) == ""){
                $("#hidListnid").val("error");
                break;
            }
            $("#hidListnid").val("," + nid + $("#hidListnid").val());
        }

        if($("#hidListnid").val() == "error"){
            $("#hidListnid").val("");
            //$("#spanError").text("+ Please Input Requires Fields");
            $("#divError > ul").html("<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Title'))?></li>");
            $("#divError").show();
            $("#divError").focus();
            return;
        }
        else{
            // Check Duplicate Grocery
            /*
            var aisleArray = $("div[id^=groceries_title_]");
            var i = 0;
            var j = 0;

            for (i = 0; i < aisleArray.length; i++) {
                var groceryArray = $("#"+aisleArray[i].id).find("div[id^=groceries_content_]");
                aisleID = aisleArray[i].id.replace("groceries_title_","");
                for (j = 0; j < groceryArray.length; j++) {
                    nid = groceryArray[j].id.replace("groceries_content_","");
                    if(checkDulicateGrocery($("input[name=txtTitle_" + nid + "]").val()
                            , $("input[name=txtQuantity_" + nid + "]").val(), $("select[name=ddlUnit_" + nid + "]").val()
                            , aisleArray[i].id, nid)){
                        errorMsg += "<li>Grocery is duplicate in \""+$("#ddlAisle option[value="+aisleID+"]").text()+"\".</li>";
                        break;
                    }
                }
            }
            */
            if(errorMsg != ""){
                $("#divError > ul").html(errorMsg);
                $("#divError").show();
                $("#divError").focus();
                $("#hidListnid").val("");
                return;
            }
        }

        if($("#hidListnid").val() != ""){
            $("#hidListnid").val($("#hidListnid").val().substring(1));
        }
        $("#groceries-form").submit();
    }

    function clearGroceryFields(){
        $("#txtTitle").val("");
        $("#ddlAisle").val(0);
        $.watermark.show('#txtTitle');
    }
    function deleteAllGrocery(){
        var aisleArray = $("div[id^=groceries_title_]");
        var listCB = $("input[type='checkbox'][name^=chk_]:checked");
        var listChecked = "";

        for(i=0;i<listCB.length;i++){
            listChecked += "," + listCB[i].id;
        }

        if(aisleArray.length > 0 && listChecked != ""){
            listChecked = listChecked.substring(1);
            $("#dialogConfirm").attr("title", "<?php print CONF_MSG_GROCERIES_DEL?>");
            $("#ui-dialog-title-dialogConfirm").text("<?php print CONF_MSG_GROCERIES_DEL?>");
            $("#dialogConfirm").dialog(
                { modal: true },
                { resizable: false },
                { minHeight: 0 },
                { buttons:
                    {
                        "Yes": function() {
                                    var url = "<?echo C_BASE_PATH?>mydiva/grocerylist/deleteall";
                                    $.post(url,
                                    { hidListID: listChecked},
                                    function(data){
                                            var json = eval("(" + data + ")");
                                            if(json['status'] == "success"){
                                                //$("#div_groceries_content").html("");
                                                for(i=0;i<listCB.length;i++){
                                                    $("#groceries_content_" + listCB[i].id).remove();
                                                }
                                                var aisleArray = $("div[id^=groceries_title_]");
                                                for (i = 0; i < aisleArray.length; i++) {
                                                    if($("#" + aisleArray[i].id).children().size() == 1){
                                                        $("#" + aisleArray[i].id).remove();
                                                    }
                                                }
                                            }
                                            else{
                                                // Error alert(json['status']);
                                            }
                                       },"text");
                                    $(this).dialog("close");
                                },
                        "No": function() {
                                    $(this).dialog("close");
                                }
                    }
                }
            );
        }
    }
    function mailGrocery(url_base, nid){
        window.open(url_base + 'mail_recipe.php?nid='+nid, null, 'target=_blank, status=0, toolbar=0, scrollbars=1, menubar=1,height=670, width=565');
    }
    function checkDulicateGrocery(name, quantity, unit, div_aisle_id, curnid){
        var groceryArray = $("#"+div_aisle_id).find("div[id^=groceries_content_]");
        var result = false;
        for (i = 0; i < groceryArray.length; i++) {
            nid = groceryArray[i].id.replace("groceries_content_","");
            if(curnid == nid){continue;}
            if($.trim($("input[name=txtTitle_" + nid + "]").val()).toUpperCase() == $.trim(name).toUpperCase()
                && $.trim($("input[name=txtQuantity_" + nid + "]").val()).toUpperCase() == $.trim(quantity).toUpperCase()
                && $("select[id=ddlUnit_" + nid + "]").val() == unit)
                {
                    result = true;
                    break;
                }
        }
        return result;
    }
    function checkedAllGroceries(ischecked){
        $("input[type='checkbox'][name^=chk_]").attr("checked",ischecked);
    }

    $(document).ready(function(){
         $("#ddlAisle").append($("#ddlAisle option[value=24]"));
         $("#ddlEditGroceryAisle").append($("#ddlEditGroceryAisle option[value=24]"));
         $("#ddlEditGroceryAisle option[value=0]").remove();

         $("div[name=groceries_col_container]").mouseover(function() {
            $(this).find("#groceries_col09").show();
            $(this).css("background-color","#fcebed");
          }).mouseout(function(){
            $(this).find("#groceries_col09").hide();
            $(this).css("background-color","transparent");
          });
         $('#txtTitle').watermark('<?php print INFO_MSG_GROCERIES_TITLE?> ');
         $('#txtNote').watermark('<?php print INFO_MSG_GROCERIES_NOTES?> ');
    });
</script>
