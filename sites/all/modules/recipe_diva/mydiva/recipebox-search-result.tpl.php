<?php
//    if(isset($_GET['txtKeyword'])){
//        $site_url = C_SITE_URL.C_BASE_PATH;
//        drupal_add_link(array("rel"=>"canonical", "href"=>$site_url."mydiva/recipebox"));
//    }

    if (arg(1) == "recipebox"){
        $linkFolder = C_BASE_PATH."mydiva/recipebox/";
        $contName = "recipes";
        $errName = "recipe";
    }else{
        $linkFolder = C_BASE_PATH."mydiva/tip/";
        $contName = "tips";
        $errName = "tip";
    }

    if (sizeof($recipes)>0){

    global $pager_total_items;
    $page_number = $_GET['page'];
    $total_record = $pager_total_items[0];
    $from_record = $page_number == 0 ? 1 : $page_number * MYDIVA_RESULT_PER_PAGE;
    $to_record = ($page_number + 1) * MYDIVA_RESULT_PER_PAGE;
    $to_record = $to_record > $total_record ? $total_record : $to_record;

    if($from_record == $to_record && $page_number > 0){
        $page_number = $page_number - 1;
        $from_record = $page_number == 0 ? 1 : $page_number * MYDIVA_RESULT_PER_PAGE;
    }

    $user_role = recipe_utils::getUserRole();
?>

<div id="mydiva_r_col_contain">
    <?php print $form_sort?>
    <div id="showing_lbl"><?php print "Showing ".$from_record."-".$to_record." of ".$total_record;?>
        </div>
    <?php foreach ($recipes as $objRecipe){?>
    <div id="mydiva_center_container">
        <?php
        $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
        $width = recipe_utils::getImageWidthValue($file_path,60);
        if($width){?>
        <div id="mydiva_img">
            <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,60) ?> />
        </div>
        <div id="mydiva_center_content">
        <?php }else{
            if (arg(1) == "recipebox"){?>
                <div id="mydiva_img">
                    <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                </div>
                <div id="mydiva_center_content">
            <?php }else{?>
                <div id="mydiva_center_content1">
            <?php }?>
        <?php }?>
            <div>
                <span class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></span><br />
                <?php if($objRecipe['postuser']!=''){?>
                <span class="by">by</span> <a class="by_author" href="<?php print C_BASE_PATH . 'user/'. $objRecipe['postuser']?>"><?php print $objRecipe['postuser']?></a>
                <?php }else{
                    print '<span class="by">by '.C_UNKNOWN_USER.'</span>';
                }?>

            </div>
            <div id="vote_contain">
                <?php print $objRecipe['voting']?>
            </div>
        </div>
        <div id="mydiva_btn" style="display:none">
            <?php //if($_POST['folder_name'] != "" && $_POST['folder_name'] != "mycookbook"){?>
                <?php if(arg(1) == "recipebox" && $objRecipe['uid'] == $user->uid){?>
                <div><img alt="Edit" style="cursor:pointer;" onclick="window.location = '<?php print C_BASE_PATH."recipes/".$objRecipe['recipe_org_id']."/edit/org/".recipe_utils::removeWhiteSpace($objRecipe['recipe_title'])."?".drupal_get_destination(); ?>';"  src="<?php print C_IMAGE_PATH?>button/btn_edit.gif"></div>
                <?php }?>
                <div><img alt="Delete recipe" style="cursor:pointer;" onclick="deleteRecipe('<?php print $objRecipe['nid']?>')"  src="<?php print C_IMAGE_PATH?>button/delete_btn.gif"></div>
                <div><img alt="Show list folder" style="cursor:pointer;" onclick="showListFolder('<?php print $objRecipe['nid']?>')"  src="<?php arg(1) == "recipebox"? print C_IMAGE_PATH."button/addtocook_btn.gif": print C_IMAGE_PATH."button/to_folder_btn.gif"?>"></div>
            <?php //}?>
        </div>
    </div>
    <?php }
    print $paging;?>
</div>

<script>
    function deleteRecipe(nid) {
        $("#dialogConfirm").attr("title", "<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => $errName))?>");
        $("#ui-dialog-title-dialogConfirm").text("<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => $errName))?>");
        $("#dialogConfirm").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "Yes": function() {


                                $(".ui-dialog").hide();

                                var url = "<?php print $linkFolder."delete_node/"?>" + nid + "/" + $("#edit-folder-id").val();
                                $.post(url, { folder_name: $("#edit-folder-name").val()}, function(data){
                                        var json = eval("(" + data + ")");
                                        if(json['status'] == "success"){
                                            $("#edit-submit").click();
                                        }
                                   }, "text");
                                //$(this).dialog("close");
                            },
                    "No": function() {
                                $(this).dialog("close");
                            }
                }
            }
        );
    }

    function showListFolder(nid) {
        $("#dialog").dialog(
                            { modal: true },
                            { resizable: false },
                            { minHeight: 30 },
                            { buttons:
                                {
                                    "OK": function() {
                                        addFaviroteNode(nid,$("#ddlFolderDropdown").val());
                                        $(this).dialog("close");
                                        $("#ddlFolderDropdown").val(0);
                                    },
                                    "Cancel": function() { $(this).dialog("close");$("#ddlFolderDropdown").val(0);}
                                }
                            }
                        );
    }

    function addFaviroteNode(nid, fid){
        var url = "<?php print C_BASE_PATH."favorite_nodes/add/"?>" + nid + "/" + fid;
        $.post(url, function(data){
            if(data != "false"){
                showInfoMessage("This <?php print $errName?> has been added to '" + $("#ddlFolderDropdown").find("option[value='"+fid+"']").text() + "'");
            }
            else{
                showInfoMessage("Error add to '" + $("#ddlFolderDropdown").find("option[value='"+fid+"']").text() + "'");
            }
        });
    }

    $(document).ready(function(){
        $("div[id=mydiva_center_container]").mouseover(function() {
            $(this).find("#mydiva_btn").show();
          }).mouseout(function(){
            $(this).find("#mydiva_btn").hide();
          });
    });

</script>
<?php }else{ ?>
<div id="mydiva_r_col_contain">
    <?php print $form_sort?>
    <div id="showing_lbl">
        We're sorry, we could not find any <?php print $contName?> <?php print $_GET['txtKeyword']==""?'':'containing: "'.$_GET['txtKeyword'].'"'?>
    </div>
</div>
<?php }?>








