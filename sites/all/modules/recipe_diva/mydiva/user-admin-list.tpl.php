<div id="admin_border_content">
    <form name="frmUserList" method="post" id="user-admin-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/user', 'txtSearch');return false;">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">User Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/user', 'txtSearch');return false;"/>
            </div>
		</div>
		<?php 
		    $delete_q = drupal_query_string_encode($_GET, array('q'));	    	
    		$title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
    		$title_q = $title_q != "" ? $title_q.'&':"";
    		
		?>		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmUserList, 'chk_', this)" />
			</div>
  	  	  	
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('user-admin-form', '<?echo C_BASE_PATH?>adminrecipe/user?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="name")?"desc":"asc"?>&order=name', '0');">
      			<span class="admin_title">Name</span></a>
			</div>
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('user-admin-form', '<?echo C_BASE_PATH?>adminrecipe/user?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
      			<span class="admin_title">Created</span></a>
			</div>
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('user-admin-form', '<?echo C_BASE_PATH?>adminrecipe/user?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="role")?"desc":"asc"?>&order=role', '0');">
      			<span class="admin_title">Role</span></a>
			</div>			
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('user-admin-form', '<?echo C_BASE_PATH?>adminrecipe/user?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="status")?"desc":"asc"?>&order=status', '0');">
      			<span class="admin_title">Status</span></a>
			</div>
      		<div id="admin_mydiva_cow">
      			<a href="javascript:changePaging('user-admin-form', '<?echo C_BASE_PATH?>adminrecipe/user?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="cow")?"desc":"asc"?>&order=cow', '0');">
      			<span class="admin_title">Cook of The Week</span></a>
			</div>
			  	  	  	
  	  	  	<div id="admin_home_op_link" class="admin_title">Operations</div>
      	</div>      	
      	<?php 
		$user_id = 0;	    			
	    foreach ($userlist as $row) {
	    	$userLink = url("adminrecipe/user/edit/".$row->uid, array('query' => drupal_get_destination()));	    	
	    ?>
	    <div id="admin_recipes_content_inner<?php print $user_id % 2 ? "":"01"?>">
	    	<div class="admin_title" id="admin_recipes_col_del">
	    		<input type="checkbox" onclick="checkAllChange(document.frmUserList, 'chk_', this, document.frmUserList.chkAll)" value="<?php print $row->uid?>" name="chk_<?php print $user_id?>">
	    	</div>
	        <div id="admin_home_created">
	        	<a href="<?php print $userLink?>"><?php print $row->name?></a>
	        </div>
	        <div id="admin_home_created">
	        	<span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
        	</div>	        
	        <div id="admin_home_created">
	        	<?php print $row->role_name?>
	        </div>	
	        <div id="admin_home_created">
	        	<?php print $row->status==1? "Active" : "Blocked"?>
	        </div>
	        <div id="admin_mydiva_cow">
	        	<?php print $row->cow == "1" ? "Yes" : "No"?>
	        </div>
	        <div class="admin_title" id="admin_home_op_link">
	        	<a onclick="return deleteAdminItem('frmUserList', '<?php print C_BASE_PATH."adminrecipe/user?".$delete_q?>', '<?php print $row->uid?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
	        	</a>
	        </div>
      	</div>
	    <?php $user_id++;} ?>
      	<input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/user"?>">
      	<input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmUserList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/user?".$delete_q; ?>')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmUserList', '<?php print C_BASE_PATH."adminrecipe/user/add?destination=adminrecipe/user/"; ?>');" />
		</div>		
    	<?php print $pager ?>
	</div>
	</form>
</div>