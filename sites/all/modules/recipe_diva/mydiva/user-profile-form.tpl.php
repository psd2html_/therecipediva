<?php
    $form['timezone']['#title'] = "";
    $form['timezone']['#collapsible'] = 0;
    $form['timezone']['timezone_name']['#description']['#attributes'] = array('class'=>'');
    $form['timezone']['timezone_name']['#description'] = '<span id="myprofile_warning">Select your current local time. If in doubt, choose the timezone that is closest to your location which has the same rules for daylight saving time. Dates and times throughout this site will be displayed using this time zone.</span>';
    $form['timezone']['timezone_name']['#title'] = '';
    $form['timezone']['timezone_name']['#attributes'] = array('class'=>'myprofile_select');
    unset($form['timezone']['#type']);
    global $user;
    $current_user = user_load($user->uid);
    profile_load_profile($current_user);

//	$user_role = recipe_utils::getUserRole();
//	$hasPer =  $user_role == C_ADMIN_USER || $user_role == C_PREMIUM_USER ? true : false;
//
//	if(!$hasPer){
//		drupal_add_css(C_CSS_PATH.'jquery-ui.css');
//		drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
//	  	drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');
//	}
//    drupal_add_js(C_SCRIPT_PATH.'jquery.filestyle.js');
?>

<?php //print '<pre>'. check_plain(print_r($form,1)). '</pre>';?>
<?php //print drupal_render($form)?>

<div id="mydiva_profiles_content_inner">
    <div id="myprofile_m_col">
        <img alt="Update my profile" src="<?echo C_IMAGE_PATH?>label/update_myprofile_lbl.gif"></div>
    <div class="messages error" style="display:none" id="divError">
        <ul></ul>
    </div>
    <div id="myprofile_l_col">
        <div id="myprofile_input_left">
            <div id="avatar">
                <?php if(file_exists($current_user->picture)){?>
                    <img id="imgAvartar" alt="<?php print $current_user->nane?>" src="<?php print C_BASE_PATH.$current_user->picture.'" '.recipe_utils::getImageWidthHeight($current_user->picture,138,168) ?> />
                    <img id="imgNoAvartar" style="display:none;" width="138" height="168" alt="<?echo $current_user->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" src="<?echo C_IMAGE_PATH?>photo/<?echo $current_user->profile_gender=="0"? "male":"female"?>138.gif">
                <?php }else{?>
                    <img id="imgNoAvartar" width="138" height="168" alt="<?echo $current_user->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" src="<?echo C_IMAGE_PATH?>photo/<?echo $current_user->profile_gender=="0"? "male":"female"?>138.gif">
                <?php }?>
            </div>
            <br/>
            <?php if(file_exists($current_user->picture)){?>
                <img id="btnRemoveImage" alt="Remove Avartar" src="<?echo C_IMAGE_PATH?>button/remove_btn.gif" onclick="removeImage()" style="cursor:pointer">
            <?php }?>
        </div>
        <div id="myprofile_input_left">
            <img alt="Update Photo" src="<?echo C_IMAGE_PATH?>label/update_photo_lbl.gif">
            <input type="file" size="23" maxlength="128" class="myprofile_input_upload" name="uploadFile" id="uploadFile" />
            <br><span id="myprofile_warning">Maximum Filesize: <em>6MB</em><br>
                Allowed Extensions: <em>png gif jpg jpeg</em><br>
                Note: <em>Upload time will be impacted by large images</em></span>
            <div id="div_upload_file_error" class="spanError"></div>
        </div>
        <div id="myprofile_input_left">
            <img alt="Gender" src="<?echo C_IMAGE_PATH?>label/gender_lbl.gif">
            <br>
            <span id="div_gender">
            <div id="gender_m">
                <img alt="Male" src="<?echo C_IMAGE_PATH?>label/male_lbl.gif"><br>
                <div class="rdo_male">
                <label class="label_radio" onclick="showDefaultImage(0)">
                <input <?php print $current_user->profile_gender=="0"? 'checked="checked"' : '' ?> name="rdoGender" id="rdoGender" value="0" type="radio">
                </label></div>
            </div>
            <div id="gender_f">
                <img alt="Female" src="<?echo C_IMAGE_PATH?>label/female_lbl.gif"><br>
                <div class="rdo_female">
                <label class="label_radio" onclick="showDefaultImage(1)">
                <input <?php print $current_user->profile_gender=="1"? 'checked="checked"' : '' ?> name="rdoGender" id="rdoGender" value="1" type="radio">
                </label></div>
            </div>
            </span>
        </div>
        <div id="myprofile_input_left">
            <img alt="About me" src="<?echo C_IMAGE_PATH?>label/aboutme_lbl.gif">
            <br>
            <textarea class="myprofile_textarea" maxlength="500" name="txtAboutMe" id="txtAboutMe"><?php print $current_user->profile_about_me?></textarea>
        </div>
    </div>
    <div id="myprofile_r_col">
        <div id="myprofile_input_right">
            <div class="admin_require_field"><img alt="Username" src="<?echo C_IMAGE_PATH?>label/username_lbl.gif"></div>
            <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
            <input type="text" maxlength="30" class="myprofile_input" value="<?php print $current_user->name?>" name="txtUserName" id="txtUserName">
        </div>
        <div id="myprofile_input_right">
            <div class="admin_require_field"><img alt="Full name" src="<?echo C_IMAGE_PATH?>label/full_name_lbl.gif"></div>
            <!--<div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>-->
            <input type="text" class="myprofile_input" maxlength="128" name="txtFullName" id="txtFullName" value="<?php print htmlspecialchars($current_user->profile_full_name)?>">
            <div id="div_fullname_error" class="spanError"></div>
        </div>
        <div id="myprofile_input_right">
            <img alt="Birthdate" src="<?echo C_IMAGE_PATH?>label/birtdate_lbl.gif">
            <input type="text" class="myprofile_input" maxlength="10" name="txtBirthDay" id="txtBirthDay" value="<?php print $current_user->profile_birth_date?>">
            <span id="myprofile_warning">format MM/DD/YYYY</span>
            <div id="div_birthday_error" class="spanError"></div>
        </div>
        <div id="myprofile_input_right">
            <img alt="Cooking Level" src="<?echo C_IMAGE_PATH?>label/cooking_level_lbl.gif">
            <select class="myprofile_select" name="ddlLevel" id="ddlLevel">
                <option <?php print $current_user->profile_cooking_level=="-1"? 'selected="true"' : '' ?> value="-1">Not rated</option>
                <option <?php print $current_user->profile_cooking_level=="0"? 'selected="true"' : '' ?> value="0">Beginner</option>
                <option <?php print $current_user->profile_cooking_level=="1"? 'selected="true"' : '' ?> value="1">Intermediate</option>
                <option <?php print $current_user->profile_cooking_level=="2"? 'selected="true"' : '' ?> value="2">Expert</option>
                <option <?php print $current_user->profile_cooking_level=="3"? 'selected="true"' : '' ?> value="3">Professional</option>
            </select>
        </div>
        <div id="myprofile_input_right">
            <img alt="Profession" src="<?echo C_IMAGE_PATH?>label/profession_lbl.gif">
            <input type="text" class="myprofile_input" maxlength="128" name="txtProfession" id="txtProfession" value="<?php print htmlspecialchars($current_user->profile_profession)?>">
        </div>
        <div id="myprofile_input_right">
            <div class="admin_require_field"><img alt="Country" src="<?echo C_IMAGE_PATH?>label/country_lbl.gif"></div>
            <!--<div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>-->
            <select class="myprofile_select" name="ddlCountry" id="ddlCountry" onchange="loadState()">
                <option value=""></option>
                <?php 	$countries = profile_location_countries();
                        foreach ($countries as $key => $value) {
                            if($key == $current_user->profile_country){
                                print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                            }
                            else{
                                print '<option value="'.$key.'">'.$value.'</option>';
                            }
                        }
                ?>
            </select>
            <br>
            <div id="div_country_error" class="spanError"></div>
        </div>
        <div id="myprofile_input_right" style="display:none">
            <img alt="Full Address" src="<?echo C_IMAGE_PATH?>label/full_address_lbl.gif">
            <input type="text" class="myprofile_input"  maxlength="100" name="txtAddress" id="txtAddress" value="<?php print htmlspecialchars($current_user->profile_address)?>">
        </div>
        <div id="myprofile_input_right" style="display:none">
            <input type="text" class="myprofile_input"  maxlength="100" name="txtAddress1" id="txtAddress1" value="<?php print htmlspecialchars($current_user->profile_address_1)?>">
        </div>
        <div id="myprofile_input_right">
            <img alt="City" src="<?echo C_IMAGE_PATH?>label/city_lbl.gif"><img width="5" height="17" src="<?echo C_IMAGE_PATH?>space.gif">
            <input type="text" class="myprofile_input"  maxlength="40" name="txtCity" id="txtCity" value="<?php print htmlspecialchars($current_user->profile_city)?>">
        </div>
        <div id="myprofile_input_right">
            <div id="myprofile_state">
                <img alt="State Province" src="<?echo C_IMAGE_PATH?>label/state_province_lbl.gif"><br>
                <select class="myprofile_select01"  name="ddlState" id="ddlState">
                    <?php $states = profile_location_states($current_user->profile_country);
                            foreach ($states as $key => $value) {
                                if($key == $current_user->profile_state){
                                    print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                }
                                else{
                                    print '<option value="'.$key.'">'.$value.'</option>';
                                }
                            }
                    ?>
                </select>
            </div>
            <div id="myprofile_postal" style="display:none">
                <img alt="Postal code" src="<?echo C_IMAGE_PATH?>label/postal_code_lbl.gif"><br>
                <input type="text" class="myprofile_input01"  maxlength="20" name="txtPostalCode" id="txtPostalCode" value="<?php print $current_user->profile_postal_code?>">
                <div id="div_postalcode_error" class="spanError"></div>
            </div>
        </div>
        <div id="myprofile_input_right">
            <img alt="Twitter Link" src="<?echo C_IMAGE_PATH?>label/twitter_link_lbl.gif">
            <input type="text" class="myprofile_input"  maxlength="100" name="txtTwitterLink" id="txtTwitterLink" value="<?php print htmlspecialchars($current_user->profile_twitter_link)?>">
        </div>
        <div id="myprofile_input_right">
            <img alt="Facebook Link" src="<?echo C_IMAGE_PATH?>label/fb_link_lbl.gif">
            <input type="text" class="myprofile_input"  maxlength="100" name="txtFbLink" id="txtFbLink" value="<?php print htmlspecialchars($current_user->profile_fb_link)?>">
        </div>
        <div id="myprofile_input_right">
            <img alt="Blog Link" src="<?echo C_IMAGE_PATH?>label/blog_link_lbl.gif">
            <input type="text" class="myprofile_input"  maxlength="100" name="txtBlogLink" id="txtBlogLink" value="<?php print htmlspecialchars($current_user->profile_blog_link)?>">
        </div>

        <div id="myprofile_input_right">
        </div>
    </div>
    <div id="myprofile_m_col">
        <div id="myprofile_contain001">
            <div id="dot_bg">
                <img width="1" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></div>
            <div id="saveall_btn">
                <img alt="Save profile" onclick="submitForm('account')" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/save_btn.gif"></div>
        </div>

        <div id="myprofile_contain001">
            <img alt="Account Settings" src="<?echo C_IMAGE_PATH?>label/account_settings_lbl.gif"></div>
        <div class="messages error" style="display:none" id="divError1">
            <ul></ul>
        </div>
        <div id="myprofile_contain001">
            <a rel="nofollow" href="javascript:expandDiv('img_preferences','div_preferences','<?echo C_IMAGE_PATH?>label/lbl_network_preferences_plus.gif','<?echo C_IMAGE_PATH?>label/lbl_network_preferences_subtract.gif')">
                <img alt="Network Preference" id="img_preferences" src="<?echo C_IMAGE_PATH?>label/lbl_network_preferences_plus.gif"></a>
                <br/>
            <div id="div_preferences" style="display:none">
               <div style="padding-top:10px;">
                   <span id="myprofile_header">These settings control what you would like to share with The Recipe Diva community.</span>
               </div>
               <span id="div_MP">
               <div class="admin_lbl1">My Profile Picture and Details:</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_profile_settings=="0"? 'checked="checked"' : '' ?> name="rdoMP" id="rdoMP" value="0" type="radio">
                   </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;padding-right:10px;">All users</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_profile_settings=="1"? 'checked="checked"' : '' ?> name="rdoMP" id="rdoMP" value="1" type="radio">
                       </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;">Only foodie friends</div>
                 </span>
                 <div style="clear: both; height: 0px;"></div>

               <span id="div_MR">
               <div class="admin_lbl1">My Uploaded and Reviewed Recipes:</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_recipe_settings=="0"? 'checked="checked"' : '' ?> name="rdoMR" id="rdoMR" value="0" type="radio">
                   </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;padding-right:10px;">All users</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_recipe_settings=="1"? 'checked="checked"' : '' ?> name="rdoMR" id="rdoMR" value="1" type="radio">
                       </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;">Only foodie friends</div>
                 </span>
                 <div style="clear: both; height: 0px;"></div>

                <span id="div_PS">
               <div class="admin_lbl1">My Foodies:</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_privacy_settings=="0"? 'checked="checked"' : '' ?> name="rdoPS" id="rdoPS" value="0" type="radio">
                   </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;padding-right:10px;">All users</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_privacy_settings=="1"? 'checked="checked"' : '' ?> name="rdoPS" id="rdoPS" value="1" type="radio">
                       </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;">Only foodie friends</div>
                 </span>
                 <div style="clear: both; height: 0px;"></div>

                <span id="div_AS">
               <div class="admin_lbl1">My Activity on the Site:</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_activity_settings=="0"? 'checked="checked"' : '' ?> name="rdoAS" id="rdoAS" value="0" type="radio">
                   </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;padding-right:10px;">All users</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_activity_settings=="1"? 'checked="checked"' : '' ?> name="rdoAS" id="rdoAS" value="1" type="radio">
                       </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;">Only foodie friends</div>
                 </span>
                 <div style="clear: both; height: 0px;"></div>

                 <div style="padding-top:10px;">
                   <span id="myprofile_header">This setting controls what you would like to see on your personal live feed.</span>
               </div>
                 <span id="div_AAS">
               <div class="admin_lbl1">All Activity on the Site:</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_all_activity_settings=="0"? 'checked="checked"' : '' ?> name="rdoAAS" id="rdoAAS" value="0" type="radio">
                   </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;padding-right:10px;">All users</div>
               <div class="admin_lbl0" style="padding-top:5px;">
                       <label class="label_radio">
                           <input <?php print $current_user->profile_all_activity_settings=="1"? 'checked="checked"' : '' ?> name="rdoAAS" id="rdoAAS" value="1" type="radio">
                       </label>
                </div>
                 <div class="admin_lbl0" style="padding-top:8px;">Only foodie friends</div>
                 </span>
                 <div style="clear: both; height: 0px;"></div>

            </div>
        </div>
        <div id="myprofile_contain001">
            <div class="admin_require_field"><a rel="nofollow" href="javascript:expandDiv('img_mail','div_mail','<?echo C_IMAGE_PATH?>label/plus_email_address_lbl.gif','<?echo C_IMAGE_PATH?>label/plus_email_address_lbl0.gif')">
            <img alt="Email Address" id="img_mail" src="<?echo C_IMAGE_PATH?>label/plus_email_address_lbl.gif"></a></div>
            <div class="admin_require_field"></div>
            <div id="div_mail">
                 <div style="padding-top:10px;">
                   <span id="myprofile_header">These settings control what communications you would like to receive from The Recipe Diva</span>
                </div>
                <div class="admin_lbl0" id="div_RNL" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRNL" id="chkRNL" value="1" <?php print $current_user->profile_newsletters=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> I would like to receive the monthly 'On the Menu' newsletter and communications sent<br> by The Recipe Diva</div>
                <div style="clear: both; height: 0px;"></div>

               <!--<div class="admin_lbl0" id="div_RE" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRE" id="chkRE" value="1" <?php print $current_user->profile_receive_email=="1"? 'checked="checked"' : '' ?>>
                   </label>
                </div>
                <div class="admin_lbl2">I would like to receive notification emails when I have received a new foodie request or<br> a new message in my inbox</div>
                -->

                <div style="padding-top:10px;">
                   <span id="myprofile_header">Send a notification when:</span>
                </div>
                <div>
                <div class="admin_lbl0" id="div_RM" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRM" id="chkRM" value="1" <?php print $current_user->profile_receive_message=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> I have a new message from the user</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_RRQ" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRRQ" id="chkRRQ" value="1" <?php print $current_user->profile_receive_request=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> A user has requested to follow me</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_RQA" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRQA" id="chkRQA" value="1" <?php print $current_user->profile_request_accepted=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> A user accepts my request to follow them</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_RA" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRA" id="chkRA" value="1" <?php print $current_user->profile_recipe_approved=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> My recipe has been approved</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_RR" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRR" id="chkRR" value="1" <?php print $current_user->profile_recipe_reviewed=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> My recipe has been reviewed</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_RP" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkRP" id="chkRP" value="1" <?php print $current_user->profile_post_in_active_thread=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> There is a new post in a thread which I have been active</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_FART" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkFART" id="chkFART" value="1" <?php print $current_user->profile_friend_add_recipetip=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> A friend adds a new recipe or tip</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_FRRT" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkFRRT" id="chkFRRT" value="1" <?php print $current_user->profile_friend_review_recipetip=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> A friend reviews a recipe or tip</div>
                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_FP" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkFP" id="chkFP" value="1" <?php print $current_user->profile_friend_post=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> A friend posts in the Diva Gossip forum</div>



                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_AUAR" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkAUAR" id="chkAUAR" value="1" <?php print $current_user->profile_any_user_upload_recipe=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> Receive an email when ANY user uploads a recipes</div>

                <div style="clear: both; height: 0px;"></div>

                <div class="admin_lbl0" id="div_AUMP" style="padding-top:5px;">
                   <label class="label_radio">
                           <input type="checkbox" name="chkAUMP" id="chkAUMP" value="1" <?php print $current_user->profile_any_user_make_post=="1"? 'checked="checked"' : '' ?>>
                   </label>
                 </div>
                <div class="admin_lbl2"> Receive an email when ANY user makes a post</div>



                <div id="div_password_error" class="spanError"></div>
                </div>
                <div style="clear: both; height: 0px;"></div>
                <div style="padding-top:10px;padding-bottom:5px;">
                   <span id="myprofile_header"> Email Address:</span>
                </div>
                 <input type="text" class="myprofile_input" value="<?php print $current_user->mail?>" id="txtMail" name="txtMail" maxlength="64">
                 <div class="description">
                     <span id="myprofile_warning">A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail
                     </span>
                 </div>
                  <div id="div_mail_error" class="spanError"></div>
            </div>
        </div>
        <div id="myprofile_contain001">
            <a rel="nofollow" href="javascript:expandDiv('img_pass','div_pass','<?echo C_IMAGE_PATH?>label/plus_account_settings_lbl.gif','<?echo C_IMAGE_PATH?>label/plus_account_settings_lbl0.gif')">
                <img alt="Show account setting" id="img_pass" src="<?echo C_IMAGE_PATH?>label/plus_account_settings_lbl.gif"></a>
                <br/>
            <div id="div_pass" style="display:none">
                <img alt="Password" src="<?echo C_IMAGE_PATH?>label/password_lbl.gif">
                <br/>
                <input type="password" class="myprofile_input" name="txtPassword" id="txtPassword" maxlength="64">
                <br/>
                <img alt="Confirm Password" src="<?echo C_IMAGE_PATH?>label/confirm_password_lbl.gif">
                <br/>
                <input type="password" class="myprofile_input" name="txtConfirmPassword" id="txtConfirmPassword" maxlength="64">
                <div class="description"><span id="myprofile_warning">To change the current user password, enter the new password in both fields.</span></div>
            </div>
        </div>
        <div id="myprofile_contain001">
            <a rel="nofollow" href="javascript:expandDiv('img_timezone','div_timezone','<?echo C_IMAGE_PATH?>label/plus_locale_settings_lbl.gif','<?echo C_IMAGE_PATH?>label/plus_locale_settings_lbl0.gif')">
                <img alt="Locale Settings" id="img_timezone" src="<?echo C_IMAGE_PATH?>label/plus_locale_settings_lbl.gif"></a>
                <br/>
            <div id="div_timezone" style="display:none">
                <?php print drupal_render($form['timezone'])?>
            </div>
        </div>
        <div id="myprofile_contain001">
            <div id="dot_bg">
                <img width="1" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></div>
            <div id="saveall_btn">
               <img alt="Save profile" onclick="submitForm('account')" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/save_btn.gif">
               <br/>
            </div>
        </div>
    </div>
</div>
<div id="dialogConfirmBecomePrimium" style="display:none;text-align:center;" title="<?php print INFO_MSG_PREMIUM_FUNCTION?>">
    <div style="margin:7px 0px 0px 5px;text-align:center;">
        <input type="button" value="Yes, sign me up!" style="width:110px;" onclick="document.location.href='<?php print C_BASE_PATH?>become_premium'">
        <input type="button" value="No thanks" style="width:110px;" onclick="closeDialogConfirmBecomePrimium();">
    </div>
</div>
<script>
    $(document).ready(function(){
        onload_radio_checkbox('div_gender');
        onload_radio_checkbox('div_RNL');
        onload_radio_checkbox('div_RE');
        onload_radio_checkbox('div_PS');
        onload_radio_checkbox('div_MP');
        onload_radio_checkbox('div_MR');
        onload_radio_checkbox('div_AS');
        onload_radio_checkbox('div_AAS');

        onload_radio_checkbox('div_RM');
        onload_radio_checkbox('div_RRQ');
        onload_radio_checkbox('div_RQA');
        onload_radio_checkbox('div_RA');
        onload_radio_checkbox('div_RR');
        onload_radio_checkbox('div_RP');
        onload_radio_checkbox('div_FART');
        onload_radio_checkbox('div_FRRT');
        onload_radio_checkbox('div_FP');

        onload_radio_checkbox('div_AUAR');
        onload_radio_checkbox('div_AUMP');

        $("#uploadFile").filestyle({
             image: "<?php print C_IMAGE_PATH."button/browse_btn.gif"?>",
             imageheight : 25,
             imagewidth : 75,
             width : 170
         });

         <?php if(arg(2)=="resetpw"){?>
         expandDiv('img_pass','div_pass','<?echo C_IMAGE_PATH?>label/plus_account_settings_lbl.gif','<?echo C_IMAGE_PATH?>label/plus_account_settings_lbl0.gif');
         var x = $("#div_pass").offset().top;
         $('html,body').animate({scrollTop: x}, 500);
         <?php }else if(arg(2)=="resetnr"){?>
         //expandDiv('img_preferences','div_preferences','<?echo C_IMAGE_PATH?>label/lbl_network_preferences_plus.gif','<?echo C_IMAGE_PATH?>label/lbl_network_preferences_subtract.gif');
         //var x = $("#div_preferences").offset().top;
         //$('html,body').animate({scrollTop: x}, 500);

         expandDiv('img_mail','div_mail','<?echo C_IMAGE_PATH?>label/plus_email_address_lbl.gif','<?echo C_IMAGE_PATH?>label/plus_email_address_lbl0.gif');
         var x = $("#div_mail").offset().top;
         $('html,body').animate({scrollTop: x}, 500);
         <?php }?>

    });
    function loadState(){
        if($("#ddlCountry").val() == ""){
            //$("#div_country_error").html("+ Please input country");
        }
        else{
            //$("#div_country_error").html("");
        }

        $.get("<?php print C_BASE_PATH."mydiva/profile/load_state/"?>" + $("#ddlCountry").val(),
               function(data){
                    $("#ddlState").html(data);
               });
    }

    function showDefaultImage(selectedRadio){
        //if($("#imgNoAvartar").css("display") != "none"){
            if(selectedRadio == 0)
            {
                $("#imgNoAvartar").attr("src","<?echo C_IMAGE_PATH?>photo/male.gif");
            }
            else{
                $("#imgNoAvartar").attr("src","<?echo C_IMAGE_PATH?>photo/female.gif");
            }
        //}
    }

    function removeImage(){
        $.post("<?php print C_BASE_PATH."remove_user_image_upload/".$current_user->uid?>",
               function(data){
                   if(data == "success"){
                       $("#imgAvartar").attr("src","");
                       $("#imgAvartar").hide();
                       $("#imgNoAvartar").show();
                    $("#btnRemoveImage").hide();
                   }
               });
    }

    function submitForm(type){
//		$("#div_country_error").html("");
//		$("#div_birthday_error").html("");
//		$("#div_fullname_error").html("");
//		$("#div_postalcode_error").html("");
//		$("#div_mail_error").html("");
//		$("#div_password_error").html("");
        var errMsg = "";
        var hasError = false;
        $("#divError > ul").html("");
        $("#divError").hide();

        //Check Validate

        // User Name
        if($.trim($("#txtUserName").val()) == ""){
            //$("#div_username_error").html("+ Please input user name");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'User Name'))?></li>";
            if(!hasError){
                $("#txtUserName").focus();
            }
            hasError = true;
        }else{
            if(!/^[\s0-9a-zA-Z.,_-]+$/.test($("#txtUserName").val())){
                //$("#div_username_error").html("+ Please input a valid user name");
                errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'user name'))?></li>";
                if(!hasError){
                    $("#txtUserName").focus();
                }
                hasError = true;
            }
        }

//		if($.trim($("#txtFullName").val()) == ""){
//			//$("#div_fullname_error").html("+ Please input full name");
//			errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Full Name'))?></li>";
//			if(!hasError){
//				$("#txtFullName").focus();
//			}
//			hasError = true;
//		}
        if($.trim($("#txtBirthDay").val()) != "" && !isDate($("#txtBirthDay").val())){
            //$("#div_birthday_error").html("+ Please input a valid Birthdate");
            errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'birthdate'))?></li>";
            if(!hasError){
                $("#txtBirthDay").focus();
            }
            hasError = true;
        }
//		if($("#ddlCountry").val() == ""){
//			//$("#div_country_error").html("+ Please input country");
//			errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Country'))?></li>";
//			if(!hasError){
//				$("#ddlCountry").focus();
//			}
//			hasError = true;
//		}
        if($("#txtAboutMe").val().length > 500){
            errMsg += "<li><?php print t(ERR_MSG_ABOUTME_MAXLENGTH, array('@field_name' => 'About Me','@number' => '500'))?></li>";
            if(!hasError){
                $("#txtAboutMe").focus();
            }
            hasError = true;
        }
        if(!validatePostcode($("#txtPostalCode").val())){
            //$("#div_postalcode_error").html("+ Please input only digits");
            errMsg += "<li><?php print t(ERR_MSG_ISALPHANUMERIC, array('@field_name' => 'Postal Code'))?></li>";
            if(!hasError){
                $("#txtPostalCode").focus();
            }
            hasError = true;
        }

        if(type == "account"){
            if($.trim($("#txtMail").val()) == ""){
                //$("#div_mail_error").html("+ Please input email address");
                errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Email Address'))?></li>";
                if(!hasError){
                    $("#txtMail").focus();
                }
                hasError = true;
            }
            else if(!validateEmail($("#txtMail").val())){
                //$("#div_mail_error").html("+ Please input a valid email address");
                errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'email address'))?></li>";
                if(!hasError){
                    $("#txtMail").focus();
                }
                hasError = true;
            }

            if(!validatePassword($("#txtPassword").val()) || !validatePassword($("#txtConfirmPassword").val())){
                //$("#div_password_error").html("+ The password length at least 6 characters and not contains white-space characters");
                errMsg += "<li><?php print ERR_MSG_PASSWORD?></li>";
                if(!hasError){
                    $("#txtPassword").focus();
                }
                hasError = true;
            }
            else if($("#txtPassword").val() != $("#txtConfirmPassword").val()){
                //$("#div_password_error").html("+ Password does not match");
                errMsg += "<li><?php print ERR_MSG_PASSWORD_NOT_MATCH?></li>";
                if(!hasError){
                    $("#txtPassword").focus();
                }
                hasError = true;
            }
        }

        if(!hasError){
            $.post("<?php print C_BASE_PATH."check_user_name_email_exist"?>", { username: $("#txtUserName").val(), mail:$("#txtMail").val() },
                       function(data){
                           var json = eval("(" + data + ")");
                        if(json['username']=="true"){
                            //$("#div_username_error").html("+ User Name is exist");
                            errMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'user name'))?></li>";
                            if(!hasError){
                                $("#txtUsername").focus();
                            }
                            hasError = true;
                        }

                        if(type == "account"){
                            if(json['mail']=="true"){
                                //$("#div_mail_error").html("+ Email address is exist");
                                errMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'email address'))?></li>";
                                if(!hasError){
                                    $("#txtMail").focus();
                                }
                                hasError = true;
                            }
                        }

                        if(!hasError){
                            if($.trim($("#uploadFile").val()) != ""){
                                $("#div_upload_file_error").html("");
                                var options = {
                                                success:       function(responseText, statusText, xhr, $form)  {
                                                                    if(responseText != "success"){
                                                                        //$("#div_upload_file_error").html("+ " + responseText);
                                                                        errMsg += "<li>"+responseText+"</li>";
                                                                        $("#uploadFile").focus();
                                                                        $("#divError > ul").html(errMsg);
                                                                        $("#divError").show();
                                                                    }
                                                                    else{
                                                                        if(type == "profile"){
                                                                            $("#user-profile-form").attr("action","<?php print C_BASE_PATH."mydiva/profile/save_profile"?>");
                                                                        }else{
                                                                            $("#user-profile-form").attr("action","<?php print C_BASE_PATH."mydiva/profile/save_account"?>");
                                                                        }
                                                                        $('#user-profile-form').submit();
                                                                    }
                                                                } ,
                                                url:          '<?php print C_BASE_PATH."mydiva/profile/check_image_upload"?>'
                                                };
                                $('#user-profile-form').ajaxSubmit(options);

                            }
                            else{
                                if(type == "profile"){
                                    $("#user-profile-form").attr("action","<?php print C_BASE_PATH."mydiva/profile/save_profile"?>");
                                }else{
                                    $("#user-profile-form").attr("action","<?php print C_BASE_PATH."mydiva/profile/save_account"?>");
                                }
                                $('#user-profile-form').submit();
                            }
                        }
                        else{
                            $("#divError > ul").html(errMsg);
                            $("#divError").show();
                        }
                       });
        }
        else{
            $("#divError > ul").html(errMsg);
            $("#divError").show();
        }
    }

    function expandDiv(img_id, div_id, img_path_add, img_path_subtract){
        if($("#"+img_id).attr("src") == img_path_add){
            $("#"+img_id).attr("src",img_path_subtract);
            $("#"+div_id).fadeIn("fast");
        }
        else{
            $("#"+img_id).attr("src",img_path_add);
            $("#"+div_id).fadeOut("fast");
        }
    }
</script>
