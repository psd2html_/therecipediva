<?php
// $Id: recipe-upload.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file promotion_admin_form.tpl.php
 * Theme implementation to display the promotion information and post information for promotion.
 *
 * Available variables:
 * - $promotion: promotion information
 *
 */
 drupal_add_js('sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js');
 //<script type="text/javascript" src="/source/sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js?D"></script>

?>
<form id="promotion_upload" name="promotion_upload" accept-charset="UTF-8" method="post" enctype="multipart/form-data" action="<?php print C_BASE_PATH . "adminrecipe/promotion/save?destination=" . $_GET['destination']?>">
<div id="border_type01_content">
    <div class="registration_p_title_1">
        <?php print arg(2) == "edit"?"Edit": "Add"?> Promotion
    </div>
    <div class="messages error" style="display:none" id="divError">
        <ul><span id="message_error"></span></ul>
    </div>
    <div class="admin_border_content_1">
        <div id="recipes_upload_lbl">Name:<span id="require">*</span></div>
        <div id="recipes_upload_input">
            <input type="text" maxlength="255" name="txtTitle" id="txtTitle" size="60" value="<?php print htmlspecialchars($promotion->title);?>"/>
        </div>
        <div id="recipes_upload_lbl">Title:</div>
        <div id="recipes_upload_input_discription">
            <textarea name="description" id="description" rows="20" cols="60"><?php print $promotion->description?></textarea>
        </div>
        <div id="recipes_upload_lbl">To win:</div>
        <div id="recipes_upload_input_discription">
            <textarea name="prize" id="prize" rows="20" cols="60"><?php print $promotion->prize?></textarea>
        </div>
        <div id="recipes_upload_lbl">Url Direct:</div>
        <div id="recipes_upload_input">
            <input type="text" maxlength="100" name="url" id="url" size="60" value="<?php print empty($promotion->url)?'':$promotion->url;?>"/>
        </div>
        <div id="recipes_upload_input01_1">
            <input type="checkbox" name="chkTarget" id="chkTarget" value="1" <?php print empty($promotion->target)?'':'checked="checked"'?> />Show in new tab
        </div>
        <div id="recipes_upload_input01">
            <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
        </div>
        <?php if($promotion->banner){
            $display_input_file = 'display:none;';
        }else{
            $display_input_file = "";
        }
        ?>
        <div id="recipes_upload_input01">
            <div id="recipe_upload_lbl_1">Banner Image:<span id="require">*</span></div>
            <div id="div_recipe_upload_image_banner" style="<?php print $display_input_file;?>;height:auto;" class="recipe_upload_inputfile">
                <input type="file" name="banner" id="banner" class="myprofile_input_upload" />
                <div class="description">Image Width: <em>628 px</em><br />Maximum Filesize: <em>6 MB</em>
                <br />Allowed Extensions: <em>png gif jpg jpeg</em>
                <br />Note: <em>Upload time will be impacted by large images</em>
                </div>
            </div>
        </div>
        <?php if($promotion->banner){
            $image_path = recipe_utils::get_thumbs_image_path($promotion->banner);
            $width = recipe_utils::getImageWidthValue($image_path,138);
        ?>
        <span id="div_image_recipe_banner">
               <div id="recipes_upload_input01" class="filefield-element clear-block">
                   <div id="imgRecipe" class="widget-preview">
                       <img alt="<?php print $promotion->tittle?>" src="<?php print C_BASE_PATH . $image_path ?>" width="<?php print $width?>"/>

                </div>
                <div class="widget-edit btn_remove_image">
                    <img id="btnRemoveImage" name="btnRemoveImage" alt="Remove recipe image" src="<?echo C_IMAGE_PATH?>button/remove_btn.gif" onclick="removeImage('banner')">
                </div>
            </div>
        </span>
        <?php }?>

        <div id="recipes_upload_input01">
            <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
        </div>
        <?php if($promotion->popup){
            $display_input_file = 'display:none;';
        }else{
            $display_input_file = "";
        }
        ?>
        <div id="recipes_upload_input01">
            <div id="recipe_upload_lbl_1">Popup Background Image:</div>
            <div id="div_recipe_upload_image_popup" style="<?php print $display_input_file;?>;height:auto;" class="recipe_upload_inputfile">
                <input type="file" name="popup" id="popup" class="myprofile_input_upload" />
                <div class="description">Image Width: <em>833 px</em><br />Maximum Filesize: <em>6 MB</em>
                <br />Allowed Extensions: <em>png gif jpg jpeg</em>
                <br />Note: <em>Upload time will be impacted by large images</em>
                </div>
            </div>
        </div>
        <?php if($promotion->popup){
            $image_path = recipe_utils::get_thumbs_image_path($promotion->popup);
            $width = recipe_utils::getImageWidthValue($image_path,138);
        ?>
        <span id="div_image_recipe_popup">
               <div id="recipes_upload_input01" class="filefield-element clear-block">
                   <div id="imgRecipe" class="widget-preview">
                       <img alt="<?php print $promotion->tittle?>" src="<?php print C_BASE_PATH . $image_path ?>" width="<?php print $width?>"/>

                </div>
                <div class="widget-edit btn_remove_image">
                    <img id="btnRemoveImage" name="btnRemoveImage" alt="Remove recipe image" src="<?echo C_IMAGE_PATH?>button/remove_btn.gif" onclick="removeImage('popup')">
                </div>
            </div>
        </span>
        <?php }?>
        <div id="recipes_upload_lbl">Started Date:<span id="require">*</span></div>
        <div id="recipe_upload_input01">
            <input type="text" maxlength="50" name="txtStart" id="txtStart" size="30" value="<?php print empty($promotion->startdate)?'':date('Y-m-d', $promotion->startdate);?>"/>
        </div>
        <div id="recipes_upload_lbl">Deadline:<span id="require">*</span></div>
        <div id="recipe_upload_input01">
            <input type="text" maxlength="50" name="txtDeadline" id="txtDeadline" size="30" value="<?php print empty($promotion->deadline)?'':date('Y-m-d', $promotion->deadline);?>"/>
        </div>
        <div id="recipes_upload_lbl">Status:</div>
        <div id="recipes_upload_input01_1">
            <input type="checkbox" name="chkStatus" id="chkStatus" value="1" <?php print empty($promotion->status)?'':'checked="checked"'?> />In Use
        </div>
        <div id="recipes_upload_input01" style="padding-top:10px;">
            <input type="button" name="op" id="edit-submit" value="" onclick="return validatePromotion();"  class="recipe_save" />
        </div>
        <input type="hidden" name="pid" id="pid" value="<?php print $promotion->pid;?>"/>
         <input type="hidden" name="hdf_terms" id="hdf_terms" value="promotion"/>
         <input type="hidden" name="hdf_delete_image_banner" id="hdf_delete_image_banner" value="0"/>
         <input type="hidden" name="hdf_filepath_image_banner" id="hdf_filepath_image_banner" value="<?php print $promotion->banner?>"/>
         <input type="hidden" name="hdf_delete_image_popup" id="hdf_delete_image_popup" value="0"/>
         <input type="hidden" name="hdf_filepath_image_popup" id="hdf_filepath_image_popup" value="<?php print $promotion->popup?>"/>
    </div>
</div>
</form>

<script language="JavaScript" type="text/javascript">
$(function () {
    $("#txtStart").datepicker({ changeYear: true, yearRange: '2011:2070', dateFormat: 'yy-mm-dd' });
    $("#txtDeadline").datepicker({ changeYear: true, yearRange: '2011:2070', dateFormat: 'yy-mm-dd' });

});

function validatePromotion(){
    var errorMsg = "";
    var hasError = false;
    var errorMessage = "";
    var topPosition = $("div[class=admin_border_content_1]").offset().top;

    $("#message_error").html("");

    if (trim($("#txtTitle").val()) == ""){
        errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Title'));?>";
        errorMessage += "<li>" + errorMsg + "</li>";
        //$("#txtTitle").focus();
        $('html,body').animate({scrollTop: topPosition}, 'fast');
        hasError = true;
    }

    /*if (trim($("#description").val()) == ""){
        errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Description'));?>";
        errorMessage += "<li>" + errorMsg + "</li>";
        //$("#description").focus();
        $('html,body').animate({scrollTop: topPosition}, 'fast');
        hasError = true;
    }

    if (trim($("#prize").val()) == ""){
        errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Prize'));?>";
        errorMessage += "<li>" + errorMsg + "</li>";
        //$("#prize").focus();
        $('html,body').animate({scrollTop: topPosition}, 'fast');
        hasError = true;
    }*/

    if($.trim($("#banner").val()) != ""){
        var options = {
                        success:       function(responseText, statusText, xhr, $form)  {
                                            if(responseText != "success"){
                                                errorMessage += "<li>" + responseText + "</li>";
                                                //$("#banner").focus();
                                                $('html,body').animate({scrollTop: topPosition}, 'fast');
                                                hasError = true;
                                            }
                                        } ,
                        url:          '<?php print C_BASE_PATH."mydiva/promotion/check_image_upload/banner"?>'
                        };
        $('#promotion_upload').ajaxSubmit(options);
    }else{
        if($.trim($("#hdf_filepath_image_banner").val()) == ""){
            errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Banner Image'));?>";
            errorMessage += "<li>" + errorMsg + "</li>";
            //$("#banner").focus();
            $('html,body').animate({scrollTop: topPosition}, 'fast');
            hasError = true;
        }
    }

    if($.trim($("#popup").val()) != ""){
        var options = {
                        success:       function(responseText, statusText, xhr, $form)  {
                                            if(responseText != "success"){
                                                errorMessage += "<li>" + responseText + "</li>";
                                                //$("#popup").focus();
                                                $('html,body').animate({scrollTop: topPosition}, 'fast');
                                                hasError = true;
                                            }
                                        } ,
                        url:          '<?php print C_BASE_PATH."mydiva/promotion/check_image_upload/popup"?>'
                        };
        $('#promotion_upload').ajaxSubmit(options);
    }
    /*else{
        if($.trim($("#hdf_filepath_image_popup").val()) == ""){
            errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Background Popup Image'));?>";
            errorMessage += "<li>" + errorMsg + "</li>";
            //$("#popup").focus();
            $('html,body').animate({scrollTop: topPosition}, 'fast');
        }
    }*/

    if (trim($("#txtStart").val()) == ""){
        errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Started Date'));?>";
        errorMessage += "<li>" + errorMsg + "</li>";
        //$("#txtStart").focus();
        $('html,body').animate({scrollTop: topPosition}, 'fast');
        hasError = true;
    }else{
        if(!/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/.test($("#txtStart").val())){
            errorMsg = "<?php print t(ERR_MSG_INVALID, array('@field_name' => 'Started Date'))?>";
            errorMessage += "<li>" + errorMsg + "</li>";
            //$("#txtStart").focus();
            $('html,body').animate({scrollTop: topPosition}, 'fast');
            hasError = true;
        }
    }

    if (trim($("#txtDeadline").val()) == ""){
        errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Deadline'));?>";
        errorMessage += "<li>" + errorMsg + "</li>";
        //$("#txtDeadline").focus();
        $('html,body').animate({scrollTop: topPosition}, 'fast');
        hasError = true;
    }else{
        if(!/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/.test($("#txtDeadline").val())){
            errorMsg = "<?php print t(ERR_MSG_INVALID, array('@field_name' => 'Deadline'))?>";
            errorMessage += "<li>" + errorMsg + "</li>";
            //$("#txtDeadline").focus();
            $('html,body').animate({scrollTop: topPosition}, 'fast');
            hasError = true;
        }
    }

    if(!hasError){
        $('#promotion_upload').submit();
    }else{
        $("#message_error").html(errorMessage);
        $("#divError").show();
    }

}
function removeImage(field){
    var objDeleteImage= document.getElementById("hdf_delete_image_" + field);
    var objHdfImage = document.getElementById("hdf_filepath_image_" + field);
    objHdfImage.value = "";

    objDeleteImage.value = "1";
    $("#div_image_recipe_" + field).hide();
    $("#div_recipe_upload_image_" + field).show();

}
</script>
