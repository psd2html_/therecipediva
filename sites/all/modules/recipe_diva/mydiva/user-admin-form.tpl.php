<?php
    $txtFullName = "";
    $txtMail = "";
    $imageUrl = "";
    $rdoGender = "0";
    $txtAboutMe = "";
    $ddlRole = "";
    $chkCOW = "0";
    $status = "1";
    $txtUserName = "";
    $txtBirthDay = "";
    $txtProfession = "";
    $ddlLevel = "";
    $ddlCountry = "";
    $txtAddress = "";
    $txtAddress1 = "";
    $ddlState = "";
    $txtPostalCode = "";
    $chkRNL = "1";
    $rdoPS = "0";
    $rdoMP = "0";
    $rdoMR = "0";
    $rdoAS = "0";
    $rdoAAS = "0";
    $chkRE = "1";
    $chkRM = "1";
    $chkRRQ = "1";
    $chkRQA = "1";
    $chkRA = "1";
    $chkRR = "1";
    $chkRP = "1";
    $chkFART = "1";
    $chkFRRT = "1";
    $chkFP = "1";
    $chkAUAR = "0";
    $chkAUMP = "0";
    $txtCity = "";
    if(arg(2) == "edit"){
        $current_user = user_load($userid);
        if($current_user->uid == 1){
            drupal_goto("");
        }
        profile_load_profile($current_user);
        $txtFullName = htmlspecialchars($current_user->profile_full_name);
        $txtMail = $current_user->mail;
        $imageUrl = $current_user->picture;
        $rdoGender = $current_user->profile_gender;
        $txtAboutMe = $current_user->profile_about_me;

        $bFirst = true;
        $user_roles = $current_user->roles;
        if (sizeof($user_roles) != 1){
            foreach($user_roles as $roleItem)
            {
                if ($bFirst == true){
                    $bFirst = false;
                    continue;
                }
                $ddlRole = $roleItem;
            }
        }

        $chkCOW = $current_user->profile_cow;
        $status = $current_user->status;
        $txtUserName = $current_user->name;
        $txtBirthDay = $current_user->profile_birth_date;
        $txtProfession = htmlspecialchars($current_user->profile_profession);
        $ddlLevel = $current_user->profile_cooking_level;
        $ddlCountry = $current_user->profile_country;
        $txtAddress = htmlspecialchars($current_user->profile_address);
        $txtAddress1 = htmlspecialchars($current_user->profile_address_1);
        $ddlState = $current_user->profile_state;
        $txtPostalCode = $current_user->profile_postal_code;
        $chkRNL = $current_user->profile_newsletters;
        $rdoPS = $current_user->profile_privacy_settings;
        $rdoMP = $current_user->profile_profile_settings;
        $rdoMR = $current_user->profile_recipe_settings;
        $rdoAS = $current_user->profile_activity_settings;
        $rdoAAS = $current_user->profile_all_activity_settings;
        $chkRE = $current_user->profile_receive_email;
        $chkRM = $current_user->profile_receive_message;
        $chkRRQ = $current_user->profile_receive_request;
        $chkRQA = $current_user->profile_request_accepted;
        $chkRA = $current_user->profile_recipe_approved;
        $chkRR = $current_user->profile_recipe_reviewed;
        $chkRP = $current_user->profile_post_in_active_thread;
        $chkFART = $current_user->profile_friend_add_recipetip;
        $chkFRRT = $current_user->profile_friend_review_recipetip;
        $chkFP = $current_user->profile_friend_post;
        $chkAUAR = $current_user->profile_any_user_upload_recipe;
        $chkAUMP = $current_user->profile_any_user_make_post;
        $txtCity = htmlspecialchars($current_user->profile_city);
    }
?>

<form id="user-admin-form" method="post" accept-charset="UTF-8" action="<?echo arg(2) == "edit"? C_BASE_PATH."adminrecipe/user/edit/".$current_user->uid."?destination=".urlencode($_GET['destination']): C_BASE_PATH."adminrecipe/user/add?destination=".urlencode($_GET['destination'])?>">
<div id="border_type01_content">
    <div class="registration_p_title_1">
        <?php print arg(2) == "edit"?"Edit": "Add"?> User</div>
    <div class="messages error" style="display:none" id="divError">
        <ul></ul>
    </div>
    <!-- REGISTRATION -->
    <div id="registration_content_inner">
        <div id="myprofile_l_col">
            <div id="myprofile_input_left">
                <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/full_name_lbl.gif"></div>
                <!--<div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>-->
                <input type="text" class="myprofile_input" name="txtFullName" maxlength="128" id="txtFullName" value="<?php print $txtFullName?>">
                <div id="div_fullname_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_left">
                <div id="avatar">
                    <?php if(file_exists($imageUrl)){?>
                        <img id="imgAvartar" src="<?php print C_BASE_PATH.$imageUrl.'" '.recipe_utils::getImageWidthHeight($imageUrl,138,168) ?> />
                        <img id="imgNoAvartar" style="display:none;" width="138" height="168" src="<?echo C_IMAGE_PATH?>photo/<?echo $rdoGender=="0"? "male":"female"?>138.gif">
                    <?php }else{?>
                        <img id="imgNoAvartar" width="138" height="168" src="<?echo C_IMAGE_PATH?>photo/<?echo $rdoGender=="0"? "male":"female"?>138.gif">
                    <?php }?>
                </div>
                <br/>
                <?php if($imageUrl){?>
                    <img id="btnRemoveImage" src="<?echo C_IMAGE_PATH?>button/remove_btn.gif" onclick="removeImage()" style="cursor:pointer">
                <?php }?>
            </div>

            <div id="myprofile_input_left">
                <img src="<?echo C_IMAGE_PATH?>label/update_photo_lbl.gif">
                <input type="file" size="23" maxlength="128" class="myprofile_input_upload" name="uploadFile" id="uploadFile" />
                <br><span id="myprofile_warning">Maximum Filesize: <em>6MB</em><br>
                Allowed Extensions: <em>png gif jpg jpeg</em><br>
                Note: <em>Upload time will be impacted by large images</em></span>
                <div id="div_upload_file_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_left">
                <div id="div_gender">
                <img src="<?echo C_IMAGE_PATH?>label/gender_lbl.gif"><br>
                <div id="gender_m">
                    <img src="<?echo C_IMAGE_PATH?>label/male_lbl.gif"><br>
                    <div class="rdo_male">
                    <label class="label_radio" onclick="showDefaultImage(0)">
                        <input <?php print ($rdoGender=="0" || $rdoGender=="")? 'checked="checked"' : '' ?> name="rdoGender" id="rdoGender" value="0" type="radio" style="background: none repeat scroll 0% 0% transparent;" class="radio_check">
                    </label>
                    </div>
                </div>
                <div id="gender_f">
                    <img src="<?echo C_IMAGE_PATH?>label/female_lbl.gif"><br>
                    <div class="rdo_female"><label class="label_radio" onclick="showDefaultImage(1)">
                        <input <?php print $rdoGender=="1"? 'checked="checked"' : '' ?> name="rdoGender" id="rdoGender" value="1" type="radio" style="background: none repeat scroll 0% 0% transparent;" class="radio_check">
                    </label></div>
                    </div>
                </div>
            </div>
            <div id="myprofile_input_left">
                <img src="<?echo C_IMAGE_PATH?>label/aboutme_lbl.gif">
                <br>
                <textarea class="myprofile_textarea" maxlength="4000" name="txtAboutMe" id="txtAboutMe"><?php print $txtAboutMe?></textarea>
            </div>
            <div id="myprofile_input_left">
                <span class="admin_lbl">Role:</span>
                <span title="This field is required." class="form-required">&nbsp;*</span>
                <br>
                <select class="myprofile_select" name="ddlRole" id="ddlRole">
                    <?php 	$roles = user_roles();
                            $index = 0;
                            foreach ($roles as $key => $value) {
                                $index ++;
                                if($index > 2 && $key != 3){
                                    if($ddlRole == $value){
                                        print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                    }else{
                                        print '<option value="'.$key.'">'.$value.'</option>';
                                    }
                                }
                            }
                    ?>
                </select>
            </div>
            <div id="myprofile_input_left" style="display:none">
                <div class="admin_lbl">Cook of The Week:&nbsp;&nbsp;</div>
                <div style="margin-top:-4px;float:left">
                    <span id="div_cow">
                    <label class="label_check">
                        <input <?php print $chkCOW == "1" ? 'checked="checked"' : '' ?> name="chkCOW" id="chkCOW" value="1" type="checkbox">
                    </label>
                    </span>
                </div>
            </div>
            <div id="myprofile_input_left">
                <div><span class="admin_lbl">Status:&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                <span id="div_active">
                    <div id="gender_m">
                        Active<br>
                        <div class="padding-left:3px;"><label class="label_radio">
                        <input <?php print $status=="1" ? 'checked="checked"' : '' ?> name="rdoStatus" id="rdoStatus" value="1" type="radio" style="background: none repeat scroll 0% 0% transparent;" class="radio_check">
                        </label></div>
                        </div>
                    <div id="gender_f">
                        Block<br>
                        <div style="padding-left:3px;"><label class="label_radio">
                        <input <?php print $status=="0"? 'checked="checked"' : '' ?> name="rdoStatus" id="rdoStatus" value="0" type="radio" style="background: none repeat scroll 0% 0% transparent;" class="radio_check">
                        </label></div>
                        </div>
                </span>
            </div>
            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RNL">
                    <label class="label_check">
                        <input <?php print $chkRNL == "1" ? 'checked="checked"' : '' ?> name="chkRNL" id="chkRNL" value="1" type="checkbox">
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">I would like to receive the monthly 'On the Menu' newsletter and communications sent by The Recipe Diva</div>
            </div>

            <!--<div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RE">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRE" id="chkRE" value="1" <?php print $chkRE == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">I would like to receive notification emails when I have received a new foodie request or a new message in my inbox</div>
            </div>-->

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RM">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRM" id="chkRM" value="1" <?php print $chkRM == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">I have a new message from the user</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RRQ">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRRQ" id="chkRRQ" value="1" <?php print $chkRRQ == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">A user has requested to follow me</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RQA">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRQA" id="chkRQA" value="1" <?php print $chkRQA == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">A user accepts my request to follow them</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RA">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRA" id="chkRA" value="1" <?php print $chkRA == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">My recipe has been approved</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RR">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRR" id="chkRR" value="1" <?php print $chkRR == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">My recipe has been reviewed</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_RP">
                    <label class="label_radio">
                           <input type="checkbox" name="chkRP" id="chkRP" value="1" <?php print $chkRP == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">There is a new post in a thread which I have been active</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_FART">
                    <label class="label_radio">
                           <input type="checkbox" name="chkFART" id="chkFART" value="1" <?php print $chkFART == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">A friend adds a new recipe or tip</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_FRRT">
                    <label class="label_radio">
                           <input type="checkbox" name="chkFRRT" id="chkFRRT" value="1" <?php print $chkFRRT == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">A friend reviews a recipe or tip</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_FP">
                    <label class="label_radio">
                           <input type="checkbox" name="chkFP" id="chkFP" value="1" <?php print $chkFP == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">A friend posts in the Diva Gossip forum</div>
            </div>


            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_AUAR">
                    <label class="label_radio">
                           <input type="checkbox" name="chkAUAR" id="chkAUAR" value="1" <?php print $chkAUAR == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">Receive an email when ANY user uploads a recipes</div>
            </div>

            <div id="myprofile_input_left">
                <div style="margin-top:-4px;float:left">
                    <span id="div_AUMP">
                    <label class="label_radio">
                           <input type="checkbox" name="chkAUMP" id="chkAUMP" value="1" <?php print $chkAUMP == "1" ? 'checked="checked"' : '' ?>>
                    </label>
                    </span>
                </div>
                <div class="admin_lbl">Receive an email when ANY user makes a post</div>
            </div>

        </div>


        <div id="myprofile_r_col">
            <div id="myprofile_input_right">
                <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/username_lbl.gif"></div>
                <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                <input type="text" maxlength="30" class="myprofile_input" value="<?php print $txtUserName?>" name="txtUserName" id="txtUserName">
                <span id="myprofile_warning">Spaces are allowed; punctuation is not allowed except for periods, hyphens, and
                underscores</span>
                <div id="div_username_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_right">
                <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/password_lbl.gif"></div>
                <div class="admin_require_field"><span class="form-required" title="This field is required."><?php print arg(2) == "edit"?"": "&nbsp;*"?></span></div>
                <input type="password" class="myprofile_input" name="txtPassword" id="txtPassword" maxlength="64">
            </div>
            <div id="myprofile_input_right">
                <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/confirm_password_lbl.gif"></div>
                <div class="admin_require_field"><span class="form-required" title="This field is required."><?php print arg(2) == "edit"?"": "&nbsp;*"?></span></div>
                <input type="password" class="myprofile_input" name="txtConfirmPassword" id="txtConfirmPassword" maxlength="64">
                <div id="div_password_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_right">
                <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/email_address_lbl.gif"></div>
                <div class="admin_require_field"><span class="form-required" title="This field is required.">&nbsp;*</span></div>
                <input type="text" class="myprofile_input" value="<?php print $txtMail?>" id="txtMail" name="txtMail" maxlength="64">
                 <div id="div_mail_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_right">
                <img src="<?echo C_IMAGE_PATH?>label/birtdate_lbl.gif">
                <input type="text" class="myprofile_input" maxlength="10" name="txtBirthDay" id="txtBirthDay" value="<?php print $txtBirthDay?>">
                <span id="myprofile_warning">format MM/DD/YYYY</span>
                <div id="div_birthday_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_right">
                <img src="<?echo C_IMAGE_PATH?>label/cooking_level_lbl.gif">
                <select class="myprofile_select" name="ddlLevel" id="ddlLevel">
                    <option <?php print $ddlLevel=="-1"? 'selected="true"' : '' ?> value="-1">Not rated</option>
                    <option <?php print $ddlLevel=="0"? 'selected="true"' : '' ?> value="0">Beginner</option>
                    <option <?php print $ddlLevel=="1"? 'selected="true"' : '' ?> value="1">Intermediate</option>
                    <option <?php print $ddlLevel=="2"? 'selected="true"' : '' ?> value="2">Expert</option>
                    <option <?php print $ddlLevel=="3"? 'selected="true"' : '' ?> value="3">Professional</option>
                </select>
            </div>
            <div id="myprofile_input_right">
                <img src="<?echo C_IMAGE_PATH?>label/profession_lbl.gif">
                <input type="text" class="myprofile_input" maxlength="128" name="txtProfession" id="txtProfession" value="<?php print $txtProfession?>">
            </div>
            <div id="myprofile_input_right">
                <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/country_lbl.gif"></div>
                <!--<div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>-->
                <select class="myprofile_select" name="ddlCountry" id="ddlCountry" onchange="loadState()">
                    <option value=""></option>
                    <?php 	$countries = profile_location_countries();
                            foreach ($countries as $key => $value) {
                                if($ddlCountry == $key){
                                    print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                }else{
                                    print '<option value="'.$key.'">'.$value.'</option>';
                                }

                            }
                    ?>
                </select>
                <div id="div_country_error" class="spanError"></div>
            </div>
            <div id="myprofile_input_right" style="display:none">
                <img src="<?echo C_IMAGE_PATH?>label/full_address_lbl.gif">
                <input type="text" class="myprofile_input"  maxlength="100" name="txtAddress" id="txtAddress" value="<?php print $txtAddress?>">
            </div>
            <div id="myprofile_input_right" style="display:none">
                <input type="text" class="myprofile_input"  maxlength="100" name="txtAddress1" id="txtAddress1" value="<?php print $txtAddress1?>">
            </div>
            <div id="myprofile_input_right">
                <img src="<?echo C_IMAGE_PATH?>label/city_lbl.gif"><img width="50" height="10" src="<?echo C_IMAGE_PATH?>space.gif">
                <input type="text" class="myprofile_input"  maxlength="40" name="txtCity" id="txtCity" value="<?php print $txtCity?>">
            </div>
            <div id="myprofile_input_right">
                <div id="myprofile_state">
                    <img src="<?echo C_IMAGE_PATH?>label/state_province_lbl.gif"><br>
                    <select class="myprofile_select01"  name="ddlState" id="ddlState">
                        <?php   $states = profile_location_states($ddlCountry);
                                if($states){
                                    foreach ($states as $key => $value) {
                                        if($key == $ddlState){
                                            print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                        }
                                        else{
                                            print '<option value="'.$key.'">'.$value.'</option>';
                                        }
                                    }
                                }
                        ?>
                    </select>
                </div>
                <div id="myprofile_postal" style="display:none">
                    <img src="<?echo C_IMAGE_PATH?>label/postal_code_lbl.gif"><br>
                    <input type="text" class="myprofile_input01"  maxlength="20" name="txtPostalCode" id="txtPostalCode" value="<?php print $txtPostalCode?>">
                    <div id="div_postalcode_error" class="spanError"></div>
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/twitter_link_lbl.gif">
                    <input type="text" class="myprofile_input"  maxlength="40" name="txtTwitterLink" id="txtTwitterLink" value="<?php print htmlspecialchars($current_user->profile_twitter_link)?>">
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/fb_link_lbl.gif">
                    <input type="text" class="myprofile_input"  maxlength="40" name="txtFbLink" id="txtFbLink" value="<?php print htmlspecialchars($current_user->profile_fb_link)?>">
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/blog_link_lbl.gif">
                    <input type="text" class="myprofile_input"  maxlength="40" name="txtBlogLink" id="txtBlogLink" value="<?php print htmlspecialchars($current_user->profile_blog_link)?>">
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/promotion_age_lbl.gif" width="28" height="15" /><img width="50" height="10" src="<?echo C_IMAGE_PATH?>space.gif">
                    <input type="text" class="myprofile_input"  maxlength="40" name="txtAge" id="txtAge" value="<?php print htmlspecialchars($current_user->profile_age)?>">
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/promotion_income_lbl.gif" width="48" height="15" />
                    <select id="selIncome" name="selIncome" class="myprofile_select">
                        <option <?php print $current_user->profile_income==""? 'selected="true"' : '' ?> value="">&nbsp;</option>
                        <option <?php print $current_user->profile_income=="0"? 'selected="true"' : '' ?> value="0">Less than 30,000</option>
                        <option <?php print $current_user->profile_income=="1"? 'selected="true"' : '' ?> value="1">30,000-40,000</option>
                        <option <?php print $current_user->profile_income=="2"? 'selected="true"' : '' ?> value="2">40,000-50,000</option>
                        <option <?php print $current_user->profile_income=="3"? 'selected="true"' : '' ?> value="3">50,000-60,000</option>
                        <option <?php print $current_user->profile_income=="4"? 'selected="true"' : '' ?> value="4">60,000-70,000</option>
                        <option <?php print $current_user->profile_income=="5"? 'selected="true"' : '' ?> value="5">70,000-80,000</option>
                        <option <?php print $current_user->profile_income=="6"? 'selected="true"' : '' ?> value="6">80,000-90,000</option>
                        <option <?php print $current_user->profile_income=="7"? 'selected="true"' : '' ?> value="7">Greater than 100,000</option>
                    </select>
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/promotion_education_lbl.gif" width="68" height="15" />
                    <select id="selEdu" name="selEdu" class="myprofile_select">
                        <option <?php print $current_user->profile_education==""? 'selected="true"' : '' ?> value="">&nbsp;</option>
                        <option <?php print $current_user->profile_education=="0"? 'selected="true"' : '' ?> value="0">Less than High School</option>
                        <option <?php print $current_user->profile_education=="1"? 'selected="true"' : '' ?> value="1">High School</option>
                        <option <?php print $current_user->profile_education=="2"? 'selected="true"' : '' ?> value="2">Some College</option>
                        <option <?php print $current_user->profile_education=="3"? 'selected="true"' : '' ?> value="3">College</option>
                        <option <?php print $current_user->profile_education=="4"? 'selected="true"' : '' ?> value="4">Graduate</option>
                        <option <?php print $current_user->profile_education=="5"? 'selected="true"' : '' ?> value="5">Post Graduate</option>
                    </select>
                </div>
                <div id="myprofile_input_right">
                    <img src="<?echo C_IMAGE_PATH?>label/promotion_own_home_lbl.gif" width="135" height="15" />
                    <div id="div_own_home">
                        <div id="gender_m">
                        <span class="yes">
                            Yes<br>
                            <div class="padding-left:10px;align:center"><label class="label_radio"><input id="rdoOwnHome" name="rdoOwnHome" type="radio" <?php print $current_user->profile_own_home=="1"? 'checked="checked"' : '' ?> value="1" /></label></div>
                        </span>
                        </div>
                        <div id="gender_f">
                        <span class="no">
                            No<br>
                            <div class="padding-left:10px;align:center"><label class="label_radio"><input id="rdoOwnHome" name="rdoOwnHome" type="radio" <?php print $current_user->profile_own_home=="0"? 'checked="checked"' : '' ?> value="0" /></label></div>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myprofile_input_right">
               <div style="width: 340px; height: 20px;"><span class="admin_lbl">My Profile Picture and Details:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                  <span id="div_MP">
                    <div id="gender_m">
                        All users<br>
                        <div class="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoMP=="0"? 'checked="checked"' : '' ?> name="rdoMP" id="rdoMP" value="0" type="radio">
                        </label></div>
                        </div>
                    <div id="gender_f">
                        Only foodies<br>
                        <div style="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoMP=="1"? 'checked="checked"' : '' ?> name="rdoMP" id="rdoMP" value="1" type="radio">
                        </label></div>
                    </div>
                  </span>
            </div>

            <div id="myprofile_input_right">
               <div style="width: 340px; height: 20px;"><span class="admin_lbl">My Uploaded and Reviewed Recipes:</span></div>
                  <span id="div_MR">
                    <div id="gender_m">
                        All users<br>
                        <div class="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoMR=="0"? 'checked="checked"' : '' ?> name="rdoMR" id="rdoMR" value="0" type="radio">
                        </label></div>
                        </div>
                    <div id="gender_f">
                        Only foodies<br>
                        <div style="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoMR=="1"? 'checked="checked"' : '' ?> name="rdoMR" id="rdoMR" value="1" type="radio">
                        </label></div>
                    </div>
                  </span>
            </div>

            <div id="myprofile_input_right">
               <div style="width: 340px; height: 20px;"><span class="admin_lbl">My Foodies:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                  <span id="div_PS">
                    <div id="gender_m">
                        All users<br>
                        <div class="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoPS=="0"? 'checked="checked"' : '' ?> name="rdoPS" id="rdoPS" value="0" type="radio">
                        </label></div>
                        </div>
                    <div id="gender_f">
                        Only foodies<br>
                        <div style="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoPS=="1"? 'checked="checked"' : '' ?> name="rdoPS" id="rdoPS" value="1" type="radio">
                        </label></div>
                    </div>
                  </span>
            </div>

            <div id="myprofile_input_right">
               <div style="width: 340px; height: 20px;"><span class="admin_lbl">My Activity on the Site:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                  <span id="div_AS">
                    <div id="gender_m">
                        All users<br>
                        <div class="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoAS=="0"? 'checked="checked"' : '' ?> name="rdoAS" id="rdoAS" value="0" type="radio">
                        </label></div>
                        </div>
                    <div id="gender_f">
                        Only foodies<br>
                        <div style="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoAS=="1"? 'checked="checked"' : '' ?> name="rdoAS" id="rdoAS" value="1" type="radio">
                        </label></div>
                    </div>
                  </span>
            </div>

            <div id="myprofile_input_right">
               <div style="width: 340px; height: 20px;"><span class="admin_lbl">All Activity on the Site:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                  <span id="div_AAS">
                    <div id="gender_m">
                        All users<br>
                        <div class="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoAAS=="0"? 'checked="checked"' : '' ?> name="rdoAAS" id="rdoAAS" value="0" type="radio">
                        </label></div>
                        </div>
                    <div id="gender_f">
                        Only foodies<br>
                        <div style="padding-left:10px;align:center"><label class="label_radio">
                        <input <?php print $rdoAAS=="1"? 'checked="checked"' : '' ?> name="rdoAAS" id="rdoAAS" value="1" type="radio">
                        </label></div>
                    </div>
                  </span>
            </div>
            <div id="myprofile_input_right">
            </div>
        </div>
        <div id="myprofile_m_col">
            <img onclick="submitForm()" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/save_btn.gif">
        </div>

    </div>
    <!-- REGISTRATRION -->
</div>
</form>
<script>
    $(document).ready(function(){
        onload_radio_checkbox('div_gender');
        onload_radio_checkbox('div_cow');
        onload_radio_checkbox('div_active');
        onload_radio_checkbox('div_RNL');
        onload_radio_checkbox('div_RE');
        onload_radio_checkbox('div_PS');
        onload_radio_checkbox('div_MP');
        onload_radio_checkbox('div_MR');
        onload_radio_checkbox('div_AS');
        onload_radio_checkbox('div_AAS');
        onload_radio_checkbox('div_own_home');

        onload_radio_checkbox('div_RM');
        onload_radio_checkbox('div_RRQ');
        onload_radio_checkbox('div_RQA');
        onload_radio_checkbox('div_RA');
        onload_radio_checkbox('div_RR');
        onload_radio_checkbox('div_RP');
        onload_radio_checkbox('div_FART');
        onload_radio_checkbox('div_FRRT');
        onload_radio_checkbox('div_FP');

        onload_radio_checkbox('div_AUAR');
        onload_radio_checkbox('div_AUMP');
    });
    function loadState(){
        if($("#ddlCountry").val() == ""){
            //$("#div_country_error").html("+ Please input country");
        }
        else{
            //$("#div_country_error").html("");
        }

        $.get("<?php print C_BASE_PATH."mydiva/profile/load_state/"?>" + $("#ddlCountry").val(),
               function(data){
                    $("#ddlState").html(data);
               });
    }

    function showDefaultImage(selectedRadio){
        //if($("#imgNoAvartar").css("display") != "none"){
            if(selectedRadio == 0)
            {
                $("#imgNoAvartar").attr("src","<?echo C_IMAGE_PATH?>photo/male.gif");
            }
            else{
                $("#imgNoAvartar").attr("src","<?echo C_IMAGE_PATH?>photo/female.gif");
            }
        //}
    }

    function removeImage(){
        $.post("<?php print C_BASE_PATH."remove_user_image_upload/".$userid?>",
               function(data){
                   if(data == "success"){
                       $("#imgAvartar").attr("src","");
                       $("#imgAvartar").hide();
                       $("#imgNoAvartar").show();
                    $("#btnRemoveImage").hide();
                   }
               });
    }

    function submitForm(type){
//		$("#div_country_error").html("");
//		$("#div_username_error").html("");
//		$("#div_birthday_error").html("");
//		$("#div_fullname_error").html("");
//		$("#div_postalcode_error").html("");
//		$("#div_mail_error").html("");
//		$("#div_password_error").html("");
        var errMsg = "";
        var hasError = false;
        $("#divError > ul").html("");
        $("#divError").hide();

        //Check Validate

        // Full Name
//		if($.trim($("#txtFullName").val()) == ""){
//			//$("#div_fullname_error").html("+ Please input full name");
//			errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Full Name'))?></li>";
//			$("#txtFullName").focus();
//			hasError = true;
//		}

        // User Name
        if($.trim($("#txtUserName").val()) == ""){
            //$("#div_username_error").html("+ Please input user name");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'User Name'))?></li>";
            if(!hasError){
                $("#txtUserName").focus();
            }
            hasError = true;
        }else{
            if(!/^[\s0-9a-zA-Z.,_-]+$/.test($("#txtUserName").val())){
                //$("#div_username_error").html("+ Please input a valid user name");
                errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'user name'))?></li>";
                if(!hasError){
                    $("#txtUserName").focus();
                }
                hasError = true;
            }
        }

        // Password
        if($.trim($("#txtPassword").val()) == "" || $.trim($("#txtConfirmPassword").val()) == ""){
            <?php if(arg(2) == "add"){?>
            //$("#div_password_error").html("+ Please input password and confirm password");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Password and Confirm Password'))?></li>";
            if(!hasError){
                $("#txtPassword").focus();
            }
            hasError = true;
            <? }?>
        }
        else if(!validatePassword($("#txtPassword").val()) || !validatePassword($("#txtConfirmPassword").val())){
            //$("#div_password_error").html("+ The password length at least 6 characters and not contains white-space characters");
            errMsg += "<li><?php print ERR_MSG_PASSWORD?></li>";
            if(!hasError){
                $("#txtPassword").focus();
            }
            hasError = true;
        }
        else if($("#txtPassword").val() != $("#txtConfirmPassword").val()){
            //$("#div_password_error").html("+ Password does not match");
            errMsg += "<li><?php print ERR_MSG_PASSWORD_NOT_MATCH?></li>";
            if(!hasError){
                $("#txtPassword").focus();
            }
            hasError = true;
        }

        // Email
        if($.trim($("#txtMail").val()) == ""){
            //$("#div_mail_error").html("+ Please input email address");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Email Address'))?></li>";
            if(!hasError){
                $("#txtMail").focus();
            }
            hasError = true;
        }
        else if(!validateEmail($("#txtMail").val())){
            //$("#div_mail_error").html("+ Please input a valid email address");
            errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'email address'))?></li>";
            if(!hasError){
                $("#txtMail").focus();
            }
            hasError = true;
        }

        // Birth Day
        if($.trim($("#txtBirthDay").val()) != "" && !isDate($("#txtBirthDay").val())){
            //$("#div_birthday_error").html("+ Please input a valid Birthdate");
            errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'birthdate'))?></li>";
            if(!hasError){
                $("#txtBirthDay").focus();
            }
            hasError = true;
        }

        // Country
//		if($("#ddlCountry").val() == ""){
//			//$("#div_country_error").html("+ Please input country");
//			errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Country'))?></li>";
//			if(!hasError){
//				$("#ddlCountry").focus();
//			}
//			hasError = true;
//		}

        // About Me
        if($("#txtAboutMe").val().length > 500){
            errMsg += "<li><?php print t(ERR_MSG_ABOUTME_MAXLENGTH, array('@field_name' => 'About Me','@number' => '500'))?></li>";
            if(!hasError){
                $("#txtAboutMe").focus();
            }
            hasError = true;
        }

        // Postal Code
        if(!validatePostcode($("#txtPostalCode").val())){
            //$("#div_postalcode_error").html("+ Please input only digits");
            errMsg += "<li><?php print t(ERR_MSG_ISALPHANUMERIC, array('@field_name' => 'Postal Code'))?></li>";
            if(!hasError){
                $("#txtPostalCode").focus();
            }
            hasError = true;
        }

        if(!hasError){
            $.post("<?php print C_BASE_PATH."check_user_name_email_exist/2/".$userid?>", { username: $("#txtUserName").val(),mail:$("#txtMail").val() },
                   function(data){
                       var json = eval("(" + data + ")");
                    if(json['username']=="true"){
                        //$("#div_username_error").html("+ User name is exist");
                        errMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'user name'))?></li>";
                        if(!hasError){
                            $("#txtUserName").focus();
                        }
                        hasError = true;
                    }
                    if(json['mail']=="true"){
                        //$("#div_mail_error").html("+ Email address is exist");
                        errMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'email address'))?></li>";
                        if(!hasError){
                            $("#txtMail").focus();
                        }
                        hasError = true;
                    }

                    if(!hasError){
                        if($.trim($("#uploadFile").val()) != ""){
                            $("#div_upload_file_error").html("");
                            var options = {
                                            success:       function(responseText, statusText, xhr, $form)  {
                                                                if(responseText != "success"){
                                                                    //$("#div_upload_file_error").html("+ " + responseText);
                                                                    errMsg += "<li>"+responseText+"</li>";
                                                                    $("#uploadFile").focus();
                                                                    $("#divError > ul").html(errMsg);
                                                                    $("#divError").show();
                                                                }
                                                                else{
                                                                    $("#user-admin-form").submit();
                                                                }
                                                            } ,
                                            url:          '<?php print C_BASE_PATH."mydiva/profile/check_image_upload"?>'
                                            };
                            $('#user-admin-form').ajaxSubmit(options);

                        }
                        else{
                            $("#user-admin-form").submit();
                        }
                    }
                    else{
                        $("#divError > ul").html(errMsg);
                        $("#divError").show();
                    }
                   });
        }
        else{
            $("#divError > ul").html(errMsg);
            $("#divError").show();
        }
    }
</script>
