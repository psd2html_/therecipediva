<div id="admin_border_content">
    <form name="frmRegPromotionList" method="post" id="registry-promotion-admin-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/registrypromotion', 'txtSearch');return false;">
    <div id="admin_recipe_content">
        <div class="admin_p_title">Promotion Registration Management</div>
        <!--<div id="admin_divatips_search_form">
            <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
            <div id="searchfor_input">
                <input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
             </div>
            <div class="archive_search_btn">
                <input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/registrypromotion', 'txtSearch');return false;"/>
            </div>
        </div>-->
        <?php
            $delete_q = drupal_query_string_encode($_GET, array('q'));
            $title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
            $title_q = $title_q != "" ? $title_q.'&':"";

        ?>
        <div id="admin_recipes_content_title">
            <div id="admin_recipes_col_del" class="admin_title">
            <input type="checkbox" name="chkAll" onclick="checkAll1(document.frmRegPromotionList, 'chk_', this)" />
            </div>

              <div id="admin_home_title">
                  <a href="javascript:changePaging('registry-promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/registrypromotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="name")?"desc":"asc"?>&order=name', '0');">
                  <span class="admin_title">Name</span></a>
            </div>
              <div id="admin_home_created">
                  <a href="javascript:changePaging('registry-promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/registrypromotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
                  <span class="admin_title">Registry Date</span></a>
            </div>
              <div id="admin_home_created">
                  <a href="javascript:changePaging('registry-promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/registrypromotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="status")?"desc":"asc"?>&order=status', '0');">
                  <span class="admin_title">Status</span></a>
            </div>
            <div id="admin_home_op_link" class="admin_title">Operations</div>
          </div>
          <?php
        $id = 0;
        foreach ($regpromotionlist as $row) {
            $pLink = url("adminrecipe/user/edit/".$row->uid, array('query' => drupal_get_destination()));
        ?>
        <div id="admin_recipes_content_inner<?php print $id % 2 ? "":"01"?>">
            <div class="admin_title" id="admin_recipes_col_del">
                <input type="checkbox" onclick="checkAllChange(document.frmRegPromotionList, 'chk_', this, document.frmRegPromotionList.chkAll)" value="<?php print $row->id?>" name="chk_<?php print $id?>">
            </div>
            <div id="admin_home_title">
                <a href="<?php print $pLink?>"><?php print $row->name?></a>
            </div>
            <div id="admin_home_created">
                <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
            </div>
            <div id="admin_home_created">
                <?php print $row->status == 1 ? "Win Promotion" : "Registry Promotion"?>
            </div>
            <div class="admin_title" id="admin_home_op_link">
                <a onclick="return deleteAdminItem('frmRegPromotionList', '<?php print C_BASE_PATH."adminrecipe/registrypromotion?".$delete_q?>', '<?php print $row->id?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
                </a>
            </div>
          </div>
        <?php $id++;} ?>
          <input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/registrypromotion"?>">
          <input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
          <input type="hidden" name="op" value="">
          <input type="hidden" name="delId" value="">
    </div>

    <div id="admin_recipe_content">
        <div id="admin_divatips_delete">
              <input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmRegPromotionList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/registrypromotion?".$delete_q; ?>')" />
        </div>
        <?php print $pager ?>
    </div>
    </form>
</div>
