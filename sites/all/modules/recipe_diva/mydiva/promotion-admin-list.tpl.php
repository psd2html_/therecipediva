<div id="admin_border_content">
    <form name="frmPromotionList" method="post" id="promotion-admin-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/promotion', 'txtSearch');return false;">
    <div id="admin_recipe_content">
        <div class="admin_p_title">Promotion Management</div>
        <div id="admin_divatips_search_form">
            <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
            <div id="searchfor_input">
                <input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
             </div>
            <div class="archive_search_btn">
                <input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/promotion', 'txtSearch');return false;"/>
            </div>
        </div>
        <?php
            $delete_q = drupal_query_string_encode($_GET, array('q'));
            $title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
            $title_q = $title_q != "" ? $title_q.'&':"";

        ?>
        <div id="admin_recipes_content_title">
            <div id="admin_recipes_col_del" class="admin_title">
            <input type="checkbox" name="chkAll" onclick="checkAll1(document.frmPromotionList, 'chk_', this)" />
            </div>

              <div id="admin_home_title1">
                  <a href="javascript:changePaging('promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/promotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="title")?"desc":"asc"?>&order=title', '0');">
                  <span class="admin_title">Name</span></a>
            </div>
              <div id="admin_home_created">
                  <a href="javascript:changePaging('promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/promotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
                  <span class="admin_title">Created</span></a>
            </div>
              <div id="admin_home_created">
                  <a href="javascript:changePaging('promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/promotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="deadline")?"desc":"asc"?>&order=deadline', '0');">
                  <span class="admin_title">Deadline</span></a>
            </div>
              <div id="admin_home_created">
                  <a href="javascript:changePaging('promotion-admin-form', '<?echo C_BASE_PATH?>adminrecipe/promotion?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="status")?"desc":"asc"?>&order=status', '0');">
                  <span class="admin_title">Status</span></a>
            </div>
            <div id="admin_home_op_link" class="admin_title">Operations</div>
          </div>
          <?php
        $pid = 0;
        foreach ($promotionlist as $row) {
            $pLink = url("adminrecipe/promotion/edit/".$row->pid, array('query' => drupal_get_destination()));
        ?>
        <div id="admin_recipes_content_inner<?php print $pid % 2 ? "":"01"?>">
            <div class="admin_title" id="admin_recipes_col_del">
                <input type="checkbox" onclick="checkAllChange(document.frmPromotionList, 'chk_', this, document.frmPromotionList.chkAll)" value="<?php print $row->pid?>" name="chk_<?php print $pid?>">
            </div>
            <div id="admin_home_title1">
                <a href="<?php print $pLink?>"><?php print $row->title?></a>
            </div>
            <div id="admin_home_created">
                <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
            </div>
            <div id="admin_home_created">
                <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->deadline)?></span>
            </div>
            <div id="admin_home_created">
                <?php print $row->status==1? "In Use" : ""?>
            </div>
            <div class="admin_title" id="admin_home_op_link">
                <a onclick="return deleteAdminItem('frmPromotionList', '<?php print C_BASE_PATH."adminrecipe/promotion?".$delete_q?>', '<?php print $row->pid?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
                </a>
            </div>
          </div>
        <?php $pid++;} ?>
          <input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/promotion"?>">
          <input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
          <input type="hidden" name="op" value="">
          <input type="hidden" name="delId" value="">
    </div>

    <div id="admin_recipe_content">
        <div id="admin_divatips_delete">
              <input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmPromotionList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/promotion?".$delete_q; ?>')" />
        </div>
        <div id="admin_divatips_add">
              <input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmPromotionList', '<?php print C_BASE_PATH."adminrecipe/promotion/add?destination=adminrecipe/promotion/"; ?>');" />
        </div>
        <?php print $pager ?>
    </div>
    </form>
</div>
