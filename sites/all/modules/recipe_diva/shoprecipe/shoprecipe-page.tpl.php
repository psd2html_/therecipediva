<?php
// $Id: divatips-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_list()
 * @see theme_divatips_list()
 */
/*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
drupal_add_js(C_SCRIPT_PATH.'simplegallery.js');
?>

<div id="shop_recipe">
    <div id="shop_recipe_top">
        <div id="shop_recipe_top_title">

        </div>
    </div>
    <div id="shop_recipe_contain">
        <div id="shop_recipe_content" style="padding:0 0 0 4px;">
               <div id="shop_recipe_gallery"></div>
        </div>
    </div>
</div>

<div id="div_space">
    <img src="<?echo C_IMAGE_PATH?>space.gif" height="5" />
</div>

<div id="news_p">
    <div id="news_p_t_contain">
        <div></div>
    </div>
    <div id="news_p_b">
         <div id="news_border">

          <div id="shoprecipe_content" style="margin-top: 0px; padding-top: 0px;">
                  <div id="shoprecipe_header">
                    <div class="left_col" style="float: left">
                        <span>
                               <img alt="Newest premium recipe" src="<?echo C_IMAGE_PATH?>icon/newest_premium_recipe_icon.gif" width="42" height="29" />
                        </span>
                        <span>
                               <img alt="Newest premium recipe" src="<?echo C_IMAGE_PATH?>label/newest_premium_recipe_lbl.gif" width="203" height="17" />
                        </span>
                    </div>
                    <div class="right_col" style="float: right">
                        <div id="shoprecipe_search_for_contain">
                            <form id="shoprecipe-search-form" method="POST" accept-charset="UTF-8" action="<?php print C_BASE_PATH?>shoprecipe/search_result" onsubmit="searchPremiumRecipes('shoprecipe-search-form','<?php print C_BASE_PATH?>shoprecipe/search_result');return false;">
                            <div id="shoprecipe_searchfor_lbl">Search for:</div>
                            <div id="shoprecipe_search_for_input">
                                <input type="text" class="Archive_Search_inp" value="" size="30" id="txtKeyword" name="txtKeyword" maxlength="128">
                                <input type="hidden" value="<?php print RECIPES_SEARCH_RECIPES;?>" id="hdf_type" name="hdf_type">
                            </div>
                            <div id="shoprecipe_search_for_btn">
                                <a onclick="searchPremiumRecipes('shoprecipe-search-form','<?php print C_BASE_PATH?>shoprecipe/search_result');return false;"><img alt="Search" src="<?echo C_IMAGE_PATH?>button/search_btn.gif" width="19" height="18" /></a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="shoprecipe_content_inner">
                   <?php
                     $newest_premium_recipes = get_list_premium_recipes(NEWEST_PREMIUM_RECIPES);
                     $i = 0;
                     foreach ($newest_premium_recipes as $objRecipe){
                         if($i==NUMBERS_UPLOADED_RECIPE){
                             break;
                         }
                         $i ++;
                     ?>
                     <div id="shoprecipe_img_container">
                       <div id="img_container">
                       <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                        if(file_exists($file_path)){ ?>
                            <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                        <?php }else{?>
                            <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                        <?php }?>
                       <div class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></div>
                       </div>
                   </div>
                   <?php }?>
                   <span id="span_newest_premium_recipe">
                    </span>
              </div>
              <div id="shoprecipe_img_seeall" <?php print sizeof($newest_premium_recipes)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                 <div onclick="expandDiv('img_newest','span_newest_premium_recipe','<?echo C_IMAGE_PATH?>label/seeall_newest_premium_recipe.gif','<?echo C_IMAGE_PATH?>label/hideall_newest_premium_recipe.gif')" id="shoprecipe_seeall_txt"><img alt="See all newest premium recipe" id="img_newest" src="<?echo C_IMAGE_PATH?>label/seeall_newest_premium_recipe.gif"></div>
                 <div id="shoprecipe_seeall_btn">
                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_newest','span_newest_premium_recipe','<?echo C_IMAGE_PATH?>label/seeall_newest_premium_recipe.gif','<?echo C_IMAGE_PATH?>label/hideall_newest_premium_recipe.gif')">
                </div>
             </div>

          </div>

          <div id="shoprecipe_content">
                  <div id="shoprecipe_header">
                    <div class="left_col" style="float: left">
                        <span>
                               <img alt="Top rated" src="<?php print C_IMAGE_PATH?>icon/top_rated_icon.gif" width="46" height="17" />
                        </span>
                        <span>
                               <img alt="Top rated" src="<?php print C_IMAGE_PATH?>label/top_rated_lbl.gif" width="88" height="17" />
                        </span>
                    </div>
                </div>

                <div id="shoprecipe_content_inner">
                    <?php
                     $top_rated = get_list_premium_recipes(TOP_RATED);
                     $i = 0;
                     foreach ($top_rated as $objRecipe){
                         if($i==NUMBERS_UPLOADED_RECIPE){
                             break;
                         }
                         $i ++;
                     ?>
                    <div id="shoprecipe_img_container">
                        <div id="img_container">
                        <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                        if(file_exists($file_path)){ ?>
                            <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                        <?php }else{?>
                            <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                        <?php }?>
                        <div class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></div>
                       </div>
                   </div>
                   <?php }?>
                    <span id="span_top_rated">
                    </span>
              </div>
              <div id="shoprecipe_img_seeall" <?php print sizeof($top_rated)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                 <div onclick="expandDiv('img_toprated','span_top_rated','<?echo C_IMAGE_PATH?>label/seeall_top_rated.gif','<?echo C_IMAGE_PATH?>label/hideall_top_rated.gif')" id="shoprecipe_seeall_txt"><img alt="See all top rated" id="img_toprated" src="<?echo C_IMAGE_PATH?>label/seeall_top_rated.gif"></div>
                 <div id="shoprecipe_seeall_btn">
                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_toprated','span_top_rated','<?echo C_IMAGE_PATH?>label/seeall_top_rated.gif','<?echo C_IMAGE_PATH?>label/hideall_top_rated.gif')">
                </div>
             </div>

          </div>

        <div id="shoprecipe_content" class="bg-none">
                <div id="shoprecipe_header">
                    <div class="left_col" style="float: left">
                        <span>
                               <img alt="Diva recommendations" src="<?echo C_IMAGE_PATH?>icon/diva_recommendationas_icon.gif" width="46" height="26" />
                        </span>
                        <span>
                               <img alt="Diva recommendations" src="<?echo C_IMAGE_PATH?>label/diva_recommendationas_lbl.gif" width="192" height="17" />
                        </span>
                    </div>
                </div>

                <div id="shoprecipe_content_inner">
                     <?php
                     $diva_recommendations = get_list_premium_recipes(DIVA_RECOMMENDATIONS);
                     $i = 0;
                     foreach ($diva_recommendations as $objRecipe){
                         if($i==NUMBERS_UPLOADED_RECIPE){
                             break;
                         }
                         $i ++;
                     ?>
                     <div id="shoprecipe_img_container">
                        <div id="img_container">
                        <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                        if(file_exists($file_path)){ ?>
                            <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                        <?php }else{?>
                            <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                        <?php }?>
                        <div class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></div>
                       </div>
                   </div>
                    <?php }?>
                    <span id="span_diva_recommendations">
                    </span>
               </div>
               <div id="shoprecipe_img_seeall" <?php print sizeof($diva_recommendations)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                 <div onclick="expandDiv('img_diva_recommendations','span_diva_recommendations','<?echo C_IMAGE_PATH?>label/seeall_diva_recommendationas.gif','<?echo C_IMAGE_PATH?>label/hideall_diva_recommendationas.gif')" id="shoprecipe_seeall_txt"><img alt="See all diva recommendations" id="img_diva_recommendations" src="<?echo C_IMAGE_PATH?>label/seeall_diva_recommendationas.gif"></div>
                 <div id="shoprecipe_seeall_btn">
                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_diva_recommendations','span_diva_recommendations','<?echo C_IMAGE_PATH?>label/seeall_diva_recommendationas.gif','<?echo C_IMAGE_PATH?>label/hideall_diva_recommendationas.gif')">
                </div>
             </div>
          </div>
     </div>
  </div>
</div>

<!-- END SHOP RECIPE -->

<script>
    function searchPremiumRecipes(frmName, url){
        var frm = document.forms[frmName];
        url = url + "?type=" + document.getElementById('hdf_type').value;
        url = url + "&kw=" + encodeURIComponent(document.getElementById('txtKeyword').value);
        window.location = url;
        return false;
    }
    function expandDiv(img_id, div_id, img_path_add, img_path_subtract){
        if($.trim($("#"+div_id).html()) == ""){
            var url = "<?php print C_BASE_PATH."getallpremiumrecipe/"?>";
            if(img_id == "img_newest" ){
                url += "1";
            }else if(img_id == "img_toprated" ){
                url += "2";
            }else{
                url += "3";
            }
            url += "/1/";
            $.get(url, function(data){
                        $("#"+div_id).html(data);
                   },"text");
        }

        if($("#"+img_id).attr("src") == img_path_add){
            $("#"+img_id).attr("src",img_path_subtract);
            $("#"+div_id).show();
        }
        else{
            $("#"+img_id).attr("src",img_path_add);
            $("#"+div_id).hide();
        }

    }
</script>

<script type="text/javascript">
    var recipe_des = [<?php print $recipe_img?>]

    var simpleGallery_navpanel={
        panel: {height:'0px', opacity:0, paddingTop:'0px', fontStyle:'bold 11px Verdana'}, //customize nav panel container
        images: ['<?echo C_IMAGE_PATH?>previous.png', '<?echo C_IMAGE_PATH?>next.png', '<?echo C_IMAGE_PATH?>circle_r.png', '<?echo C_IMAGE_PATH?>circle.png'], //nav panel images (in that order)
        imageSpacing: {offsetTop:[2, 2, 2 ,2, 2, 2, 2], spacing:5}, //top offset of left, play, and right images, PLUS spacing between the 3 images
        slideduration: 50 //duration of slide up animation to reveal panel
    }
    var mygallery = new simpleGallery({
        wrapperid: "shop_recipe_gallery", //ID of main gallery container,
        dimensions: [546, 322], //width/height of gallery in pixels. Should reflect dimensions of the images exactly
        //imagearray: [<?php print $recipe_img?>],
        imagearray: [["<? print C_IMAGE_PATH ?>photo/shop_recipe_banner.jpg", "", "", ""], ["<? print C_IMAGE_PATH ?>photo/shop_recipe_banner1.jpg", "", "", ""]],
        autoplay: [false, 5000, 5], //[auto_play_boolean, delay_btw_slide_millisec, cycles_before_stopping_int]
        persist: false, //remember last viewed slide and recall within same session?
        fadeduration: 500, //transition duration (milliseconds)
        navpanelheight:0,
        oninit: function() { //event that fires when gallery has initialized/ ready to run

            //Keyword "this": references current gallery instance (ie: try this.navigate("play/pause"))
            $("img[id^=navImage]").css("filter", "");
            var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
            var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
            if (jQuery.browser.msie && (ie55 || ie6)) {
                $("img[id^=navImage]").attr("style","");
                $("div.navpanellayer").pngFix();
                $("span[id^=navImage]").css("cursor","pointer");
                $("span[id^=navImage]").css("top","6px");
                $("span[id^=navImage]").css("margin-right","5px");

                $("span[id^=navImage]").click(function(){
                    $("img[id="+this.id+"]").click();
                });
            }

            mygallery.navigate('playpause');
        },
        onslide: function(curslide, i) { //event that fires after each slide is shown
            //Keyword "this": references current gallery instance
            //curslide: returns DOM reference to current slide's DIV (ie: try alert(curslide.innerHTML)
            //i: integer reflecting current image within collection being shown (0=1st image, 1=2nd etc)
            var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
            var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
            if (jQuery.browser.msie && (ie55 || ie6)) {
                var style = "";
                for (var j = 0; j < recipe_des.length; j++) {
                    style = "cursor:pointer; top:6px; margin-right:5px; DISPLAY: inline-block; BACKGROUND: none transparent scroll repeat 0% 0%;";
                    if (j == i) {
                        $("img[id=navImage"+(j + 1)+"]").attr("src", simpleGallery_navpanel.images[2]);
                        style += " FILTER: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+simpleGallery_navpanel.images[2]+"', sizingMethod='scale');WIDTH: 11px; POSITION: relative; HEIGHT: 19px;";
                    }
                    else {
                        $("img[id=navImage"+(j + 1)+"]").attr("src", simpleGallery_navpanel.images[2]);
                        style += " FILTER: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+simpleGallery_navpanel.images[3]+"', sizingMethod='scale');WIDTH: 13px; POSITION: relative; HEIGHT: 19px;";
                    }
                    $("span[id=navImage"+(j + 1)+"]").attr("style",style);
                }
            }
            else{
                for (var j = 0; j < recipe_des.length; j++) {
                    if (j == i) {
                        $("#navImage" + (j + 1)).attr("src", simpleGallery_navpanel.images[2]);
                    }
                    else {
                        $("#navImage" + (j + 1)).attr("src", simpleGallery_navpanel.images[3]);
                    }
                }
            }
        }
    })
</script>
