<?php
    global $user;
    profile_load_profile($user);
    $amount = ($node->price > 0)?$node->price:10;
    $node->vote_average_value = getVoteByRecipeId($node->nid)->vote_average_value;
?>
<form id="shoprecipe-order-form" method="post" accept-charset="UTF-8" action="<?echo C_BASE_PATH?>shoprecipe/order/<?echo $node->nid?>">
<div id="news_p">
    <div id="news_p_t_contain">
        <div>
        </div>
    </div>
    <div id="news_p_b">
        <div id="news_border">
            <div id="pre_membership_content">
                <div class="pre_membership_p_title">
                    Purchase Premium Recipe <img width="1" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                    <img alt="Cooking Premium" width="49" height="32" src="<?echo C_IMAGE_PATH?>icon/cooking_premium.gif">
                </div>
                <div class="messages error" style="display:none" id="divError">
                    <ul></ul>
                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">

                    <?php
                        $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($node->title)."-".$node->nid;
                    ?>
                        <div>
                            <div class="admin_require_field" style="padding-top: 10px;">
                                <label class="label_check">
                                    <input id="chkPremiumRecipe" name="chkPremiumRecipe" type="checkbox" value="1">
                                </label>
                            </div>
                            <div id="img_contain">
                                <?php
                                $file_path = recipe_utils::get_thumbs_image_path($node->field_recipe_image[0]['filepath']);
                                if(file_exists($file_path)){ ?>
                                    <img alt="<?php print $node->title?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,62,63) ?> />
                                <?php }else{?>
                                    <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                                <?php }?>
                            </div>

                            <div id="divatiptop_right">
                                <div id="divatoptip_line1">
                                    <div class="divatiptop_left divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($node->title,10,false,50)?></a></div>
                                    <div class="divatiptop_left divatop_title">
                                    <?php if($node->uid!=0){?>
                                    <span class="by">by</span> <a class="by_author" href="<?php print C_BASE_PATH."user/".$node->name?>"><?php print $node->name?></a>
                                    <?php }else{
                                        print '<span class="by">by '.C_UNKNOWN_USER.'</span>';
                                    }?>
                                    </div>
                                </div>
                                <div id="divatoptip_line2">
                                    <div class="divatiptop_left"><?php print theme('fivestar_static', $node->vote_average_value, 5, 'vote');?></div>
                                </div>
                            </div>
                        </div>

                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">
                    <img alt="Bill Info" width="135" height="17" src="<?echo C_IMAGE_PATH?>label/billing_info.gif"></div>
                <div id="pre_membership_size">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">
                    <div id="pre_input_l_contain">
                        <div class="admin_require_field"><img alt="First Name" width="59" height="17" src="<?echo C_IMAGE_PATH?>label/first_name_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <input type="text" class="pre_input_type01" name="txtFirstName" id="txtFirstName" maxlength="25"><br>
                        <span id="myprofile_warning">Enter name as it appears on credit card.</span></div>
                    <div id="pre_input_m_contain">
                        <div class="admin_require_field"><img width="25" height="17" src="<?echo C_IMAGE_PATH?>label/m_i_lbl.gif"></div>
                        <input type="text" class="pre_input_type02" name="txtMiddleName" id="txtMiddleName" maxlength="10">
                    </div>
                    <div id="pre_input_r_contain">
                        <div class="admin_require_field"><img alt="Last Name" width="57" height="17" src="<?echo C_IMAGE_PATH?>label/last_name_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <input type="text" class="pre_input_type01" name="txtLastName" id="txtLastName" maxlength="25">
                    </div>
                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">
                    <div class="admin_require_field">
                        <label class="label_check">
                            <input id="chkNotUS" name="chkNotUS" type="checkbox" value="1" onclick="showAddressUSCA()">
                        </label></div>
                    <div class="admin_require_field_1">
                        <img width="3" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                           <img src="<?echo C_IMAGE_PATH?>label/lbl002.gif"></div>

                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>

                <div id="div_address_notusca" style="display:none">
                    <div id="premium_membership_content_contain">
                        <div id="pre_input_l_contain01">
                            <div class="admin_require_field"><img alt="Address" width="81" height="17" src="<?echo C_IMAGE_PATH?>label/address_lbl.gif"></div>
                            <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                            <br>
                            <textarea class="myprofile_textarea" maxlength="500" name="txtStreetAddressArea" id="txtStreetAddressArea"><?php print $user->profile_address?></textarea>
                        </div>
                    </div>
                    <div id="pre_membership_size">
                        <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                </div>

                <div id="div_address_usca" style="display:inline">
                <div id="premium_membership_content_contain">
                    <div id="pre_input_l_contain01">
                        <div class="admin_require_field"><img alt="Address" width="81" height="17" src="<?echo C_IMAGE_PATH?>label/address_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <br>
                        <input type="text" class="pre_input_type03"  name="txtStreetAddress" id="txtStreetAddress" maxlength="100" value="<?php print htmlspecialchars($user->profile_address)?>">
                    </div>
                    <div id="pre_input_m_contain01">
                        <div class="admin_require_field"><img alt="Apt Suite" width="72" height="17" src="<?echo C_IMAGE_PATH?>label/apt_suite_lbl.gif"></div>
                        <br>
                        <input type="text" class="pre_input_type05" name="txtAptSuite" id="txtAptSuite" maxlength="64">
                    </div>
                </div>
                <div id="pre_membership_size">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">
                    <div id="pre_input_l_contain01">
                        <div class="admin_require_field"><img alt="City" width="27" height="17" src="<?echo C_IMAGE_PATH?>label/city_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <br>
                        <input type="text" class="pre_input_type03" name="txtCity" id="txtCity" maxlength="40" value="<?php print htmlspecialchars($user->profile_city)?>">
                    </div>
                    <div id="pre_input_m_contain01">
                        <div class="admin_require_field"><img alt="State" width="82" height="17" src="<?echo C_IMAGE_PATH?>label/state_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <select class="pre_select_type01"  name="ddlState" id="ddlState">
                            <option value=""></option>
                            <?php   $statesUS = profile_location_states("US");
                                    $statesCA = profile_location_states("CA");
                                    $states = array_merge($statesUS, $statesCA);
                                    //sort($states,SORT_REGULAR);
                                    foreach ($states as $key => $value) {
                                        if($key == $user->profile_state){
                                            print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                        }
                                        else{
                                            print '<option value="'.$key.'">'.$value.'</option>';
                                        }
                                    }
                            ?>
                        </select>
                    </div>
                    <div id="pre_input_r_contain01">
                        <div class="admin_require_field"><img alt="Zipcode" width="53" height="17" src="<?echo C_IMAGE_PATH?>label/zipcode_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <br>
                        <input type="text" class="pre_input_type04"  maxlength="20" name="txtPostalCode" id="txtPostalCode" value="<?php print $user->profile_postal_code?>">
                    </div>
                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                </div>
                <div id="premium_membership_content_contain">
                    <div id="pre_input_m_contain02">
                        <div class="admin_require_field"><img alt="Credit card" width="93" height="17" src="<?echo C_IMAGE_PATH?>label/credit_card_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <select class="pre_select_type01" name="ddlCreditCardType" id="ddlCreditCardType">
                            <option value="0">Select Card</option>
                            <option value="Visa">Visa</option>
                            <option value="Amex">American Express</option>
                            <option value="Discover">Discover</option>
                            <option value="MasterCard">MasterCard</option>
                        </select>
                    </div>
                    <div id="pre_input_m_contain02">
                        <div class="admin_require_field"><img width="92" height="18" src="<?echo C_IMAGE_PATH?>space.gif"></div>
                        <img alt="Card Type" width="96" height="17" src="<?echo C_IMAGE_PATH?>icon/cardtype.jpg">
                    </div>
                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">
                    <div class="admin_require_field"><img alt="Card Number" width="78" height="17" src="<?echo C_IMAGE_PATH?>label/card_number_lbl.gif"></div>
                    <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                    <br>
                    <div class="admin_require_field" style="width:530px"><input type="text" style="width: 340px;" maxlength="128" name="txtCardNumber" id="txtCardNumber" value="">
                    <br><span id="myprofile_warning">Enter card number as it appears on credit card.</span></div>
                </div>
                <div id="div_space">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">
                    <div id="pre_input_m_contain01">
                        <div class="admin_require_field"><img alt="Expire Date" width="85" height="17" src="<?echo C_IMAGE_PATH?>label/exp_date_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <br>
                        <select class="pre_select_type01" name="ddlExpireMonth" id="ddlExpireMonth">
                            <option value="">Month</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div id="pre_input_m_contain01">
                        <div class="admin_require_field"><img width="85" height="17" src="<?echo C_IMAGE_PATH?>space.gif"></div>
                        <br>
                        <select class="pre_select_type01" name="ddlExpireYear" id="ddlExpireYear">
                            <option value="">Year</option>
                            <?php $cur_year = date("Y");
                                  for($i = 0; $i < 21; $i++){?>
                                <option value="<?print $i + $cur_year?>"><?print $i + $cur_year?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div id="pre_input_m_contain01">
                        <div class="admin_require_field"><img alt="Security" width="77" height="17" src="<?echo C_IMAGE_PATH?>label/security_lbl.gif"></div>
                        <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                        <br>
                        <input type="text" class="pre_input_type04" maxlength="20" name="txtSecurityCode" id="txtSecurityCode" value="">
                           <span id="myprofile_warning" onclick="showSecurityCode()" style="cursor:pointer">What this?</span>
                    </div>
                </div>
                <div id="pre_membership_size">
                    <img alt="Continue" src="<?echo C_IMAGE_PATH?>button/continue_btn.gif" onclick="checkValidate()" style="cursor:pointer"></div>
                <div id="pre_membership_size">
                    <img width="1" height="13" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
            </div>

            <!-- Confirm -->

            <div id="pre_membership_confirmed_content" style="display:none;">
                <div class="pre_membership_p_title">
                    Purchase Premium Recipe <img width="1" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                    <img alt="Cooking premium" width="49" height="32" src="<?echo C_IMAGE_PATH?>icon/cooking_premium.gif">
                </div>
                <div>
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div>
                    <img alt="Membership Info" src="<?echo C_IMAGE_PATH?>label/membership_info_lbl.gif">
                </div>
                <div>
                    <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div style="padding: 0pt 0pt 0pt 25px;">
                    <div>
                        Username:<img width="10" height="1" src="<?echo C_IMAGE_PATH?>space.gif"><?php print $user->name?>
                    </div>
                    <div>
                        <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                    <div>
                        Email Address:<img width="10" height="1" src="<?echo C_IMAGE_PATH?>space.gif"><?php print $user->mail?>
                    </div>
                </div>
                <div>
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>

                <div>
                    <img alt="Premium recipe purchase" src="<?echo C_IMAGE_PATH?>label/premium_recipe_purchase_lbl.gif">
                </div>

                <div>
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div id="premium_membership_content_contain">

                    <?php
                        $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($node->title)."-".$node->nid;
                    ?>
                        <div>

                            <div id="img_contain">
                                <?php
                                $file_path = recipe_utils::get_thumbs_image_path($node->field_recipe_image[0]['filepath']);
                                if(file_exists($file_path)){ ?>
                                    <img alt="<?php print $node->title?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,62,63) ?> />
                                <?php }else{?>
                                    <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                                <?php }?>
                            </div>

                            <div id="divatiptop_right">
                                <div id="divatoptip_line1">
                                    <div class="divatiptop_left divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($node->title,10,false,50)?></a></div>
                                    <div class="divatiptop_left divatop_title">
                                    <?php if($node->uid!=0){?>
                                    <span class="by">by</span> <a class="by_author" href="<?php print C_BASE_PATH."user/".$node->name?>"><?php print $node->name?></a>
                                    <?php }else{
                                        print '<span class="by">by '.C_UNKNOWN_USER.'</span>';
                                    }?>
                                    </div>
                                </div>
                                <div id="divatoptip_line2">
                                    <div class="divatiptop_left"><?php print theme('fivestar_static', $node->vote_average_value, 5, 'vote');?></div>
                                </div>
                            </div>
                        </div>

                </div>

                 <div>
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <!--
                <div>
                    <img src="<?echo C_IMAGE_PATH?>label/membership_option_lbl.gif">
                </div>
                <div style="padding: 0pt 0pt 0pt 25px;">
                    <div>
                        <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                    <div id="div_recipe_info">
                        <?echo $node->title?><img width="20" height="1" src="<?echo C_IMAGE_PATH?>space.gif"> $<?echo $amount?>
                    </div>
                    <div>
                        <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                    <div>
                        Expiration:<img width="10" height="1" src="<?echo C_IMAGE_PATH?>space.gif"><span id="div_Expire"></span>
                    </div>
                    <div>
                        <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                    <div>
                        Name:<img width="10" height="1" src="<?echo C_IMAGE_PATH?>space.gif"><span id="div_FirstName"></span>
                            <img width="5" height="1" src="<?echo C_IMAGE_PATH?>space.gif"><span id="div_LastName"></span>
                    </div>
                    <div>
                        <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                    <div id="div_space">
                        <div id="address_lbl_contain">
                            Address:
                        </div>
                        <div id="pre_membership_confirm_contain">
                            <div id="div_Street"></div>
                            <div id="div_CityZip"></div>
                            <div id="div_Country"><?php print profile_location_get_country($user->profile_country)?></div>
                        </div>
                    </div>
                </div>
                <div id="div_space">
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                -->
                <div id="div_space">
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                    <img alt="Edit" onclick="showConfirm(false)" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/edit_btn.gif"></a>
                </div>
                <div id="div_space">
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
                <div>
                    <img alt="Complete purchase" onclick="submitForm()" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/complete_purchase_btn.gif">
                </div>
                <div>
                    <img width="1" height="15" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<div id="dialogSecurityCode" style="display:none" title="Security Code">
<div style="width: 272px;">
    <div style="width: 262px; text-align:center;padding-left:13px;">
        Found on the back right of your VISA, MasterCard, or Discover card, or on the front right of your American Express, the CVN (Card Verification Code) is a security feature that protects cardholders against credit card fraud. For your protection, we request the CVN  when a card is not physically present at a transaction.
    </div>
</div>
</div>
<script>
    $(document).ready(function(){onload_radio_checkbox()});

    function submitForm(){
        var options = {
                        success:       function(responseText, statusText, xhr, $form)  {

                                            if(responseText != "success"){
                                                showInfoMessage(responseText);
                                                showConfirm(false);
                                            }
                                            else{
                                                showInfoMessage("The <?echo $node->title?> was added to your recipe box.");
                                                window.location = "<?php print C_BASE_PATH?>mydiva/recipebox";
                                            }
                                        },
                        url:          '<?echo C_BASE_PATH?>shoprecipe/order/<?echo $node->nid?>'
                        };
        $('#shoprecipe-order-form').ajaxSubmit(options);
    }
    function checkValidate(){
        var errMsg = "";
        var hasError = false;
        $("#divError > ul").html("");
        $("#divError").hide();

        //Check Validate

        if(!$("#chkPremiumRecipe")[0].checked){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Recipe'))?></li>";
            hasError = true;
        }

        if($.trim($("#txtFirstName").val()) == ""){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'First Name'))?></li>";
            if(!hasError){
                    $("#txtFirstName").focus();
                }
            hasError = true;
        }

        if($.trim($("#txtLastName").val()) == ""){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Last Name'))?></li>";
            if(!hasError){
                $("#txtLastName").focus();
            }
            hasError = true;
        }



        if(!$("#chkNotUS")[0].checked){
            if($.trim($("#txtStreetAddress").val()) == ""){
                errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Street Address'))?></li>";
                if(!hasError){
                    $("#txtStreetAddress").focus();
                }
                hasError = true;
            }

            if($.trim($("#txtCity").val()) == ""){
                errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'City'))?></li>";
                if(!hasError){
                    $("#txtCity").focus();
                }
                hasError = true;
            }

            if($.trim($("#ddlState").val()) == ""){
                errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'State/Province'))?></li>";
                if(!hasError){
                    $("#ddlState").focus();
                }
                hasError = true;
            }

            if($.trim($("#txtPostalCode").val()) == ""){
                errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Zip Code'))?></li>";
                if(!hasError){
                    $("#txtPostalCode").focus();
                }
                hasError = true;
            }
            else if(!validatePostcode($("#txtPostalCode").val())){
                errMsg += "<li><?php print t(ERR_MSG_ISALPHANUMERIC, array('@field_name' => 'Zip Code'))?></li>";
                if(!hasError){
                    $("#txtPostalCode").focus();
                }
                hasError = true;
            }
        }
        else
        {
            if($.trim($("#txtStreetAddressArea").val()) == ""){
                errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Street Address'))?></li>";
                if(!hasError){
                    $("#txtStreetAddressArea").focus();
                }
                hasError = true;
            }
            else if($("#txtStreetAddressArea").val().length > 500){
                errMsg += "<li><?php print t(ERR_MSG_ABOUTME_MAXLENGTH, array('@field_name' => 'Street Address','@number' => '500'))?></li>";
                if(!hasError){
                    $("#txtStreetAddressArea").focus();
                }
                hasError = true;
            }
        }


        if($.trim($("#ddlCreditCardType").val()) == "0"){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Credit Card Type'))?></li>";
            if(!hasError){
                $("#ddlCreditCardType").focus();
            }
            hasError = true;
        }

        if($.trim($("#txtCardNumber").val()) == ""){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Card Number'))?></li>";
            if(!hasError){
                $("#txtCardNumber").focus();
            }
            hasError = true;
        }
        else if($.trim($("#txtCardNumber").val()) != "" && !/^\d+$/.test($("#txtCardNumber").val())){
            errMsg += "<li><?php print t(ERR_MSG_ISNUMERIC, array('@field_name' => 'The credit card number'))?></li>";
            if(!hasError){
                $("#txtCardNumber").focus();
            }
            hasError = true;
        }

        if($("#ddlExpireMonth").val() == ""){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Expiration Month'))?></li>";
            if(!hasError){
                $("#ddlExpireMonth").focus();
            }
            hasError = true;
        }

        if($.trim($("#ddlExpireYear").val()) == ""){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Expiration Year'))?></li>";
            if(!hasError){
                $("#ddlExpireYear").focus();
            }
            hasError = true;
        }

        if($.trim($("#txtSecurityCode").val()) == ""){
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Security Code'))?></li>";
            if(!hasError){
                $("#txtSecurityCode").focus();
            }
            hasError = true;
        }
        else if($.trim($("#txtSecurityCode").val()) != "" && !/^\d+$/.test($("#txtSecurityCode").val())){
            errMsg += "<li><?php print t(ERR_MSG_ISNUMERIC, array('@field_name' => 'The security code'))?></li>";
            if(!hasError){
                $("#txtSecurityCode").focus();
            }
            hasError = true;
        }

        if(!hasError){
            showConfirm(true);
        }
        else{
            $("#divError > ul").html(errMsg);
            $("#divError").show();
        }
    }

    function showConfirm(hasShow) {
        if (hasShow) {
            $("#pre_membership_content").hide();
            $("#pre_membership_confirmed_content").show();
            $("#div_Expire").html($("#ddlExpireMonth option:selected").text() + "/" + $("#ddlExpireYear").val());
            $("#div_FirstName").html($("#txtFirstName").val());
            $("#div_LastName").html($("#txtLastName").val());
            if(!$("#chkNotUS")[0].checked){
                $("#div_Street").html($("#txtStreetAddress").val());
                $("#div_CityZip").html($("#txtCity").val() + ", " + $("#txtPostalCode").val());
            }
            else{
                $("#div_Street").html($("#txtStreetAddressArea").val());
            }
        }
        else {
            $("#pre_membership_content").show();
            $("#pre_membership_confirmed_content").hide();
        }
    }

    function showSecurityCode(){
        //$("#dialogSecurityCode").css("background","#fef3dd");
        $("#dialogSecurityCode").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "Close": function() {
                                $(this).dialog("close");
                            }
                }
            }
        );
    }

    function showAddressUSCA(){
        if($("#div_address_usca").css("display") == "inline"){
            $("#div_address_usca").hide();
            $("#txtCity").val("");
            $("#txtPostalCode").val("");
            $("#ddlState").val("");
            $("#txtAptSuite").val("");
            $("#txtStreetAddress").val("");
            $("#div_address_notusca").show();
        }else{
            $("#div_address_usca").show();
            $("#txtStreetAddressArea").val("");
            $("#div_address_notusca").hide();
        }
    }
</script>
