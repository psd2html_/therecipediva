<?php
      /*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
      drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
      drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
?>
<div id="border_type01">
    <div id="border_type01_t_contain">
        <div>
        </div>
    </div>
    <div id="border_type01_b">
        <div id="border_type01_border">
            <div id="border_type01_content">
                <div class="registration_p_title_1">
                    Registration</div>
                <div class="messages error" style="display:none" id="divError">
                    <ul></ul>
                </div>
                <!-- REGISTRATION -->
                <div id="registration_content_inner">
                    <div id="myprofile_l_col">
                        <div id="myprofile_input_left">
                            <div class="admin_require_field"><img alt="Full Name" src="<?echo C_IMAGE_PATH?>label/full_name_lbl.gif"></div>
                            <!--<div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>-->
                            <input type="text" maxlength="128" class="myprofile_input" name="txtFullName" id="txtFullName" value="">
                            <div id="div_fullname_error" class="spanError"></div>
                        </div>
                        <div id="myprofile_input_left">
                            <div id="gender_lbl">
                                <img alt="Gender" src="<?echo C_IMAGE_PATH?>label/gender_lbl.gif"></div>
                            <div id="avatar_container">
                                <div id="avatar_male">
                                    <img alt="Male" width="110" height="128" src="<?echo C_IMAGE_PATH?>photo/male.gif"></div>
                                <div id="avatar_female">
                                    <img alt="Female" width="110" height="127" src="<?echo C_IMAGE_PATH?>photo/female.gif"></div>
                            </div>
                            <span id="span_Gender">
                            <div id="avatar_choose_container">
                                <div id="avatar_male_choose">
                                    <img alt="Male" src="<?echo C_IMAGE_PATH?>label/male_lbl.gif">
                                    <br>
                                    <div class="rdo_male">
                                        <label class="label_radio">
                                            <input name="rdoGender" id="rdoGender" value="0" type="radio">
                                        </label>
                                    </div>
                                </div>
                                <div id="avatar_female_choose">
                                        <img alt="Female" src="<?echo C_IMAGE_PATH?>label/female_lbl.gif">
                                        <br>
                                        <div class="rdo_female">
                                            <label class="label_radio">
                                                <input name="rdoGender" id="rdoGender" value="1" type="radio" checked="checked">
                                            </label>
                                        </div>
                                 </div>
                            </div>
                            </span>
                        </div>
                        <!--
                        <div id="myprofile_input_left">
                            <img src="<?echo C_IMAGE_PATH?>label/registration_become_lbl.gif"><br>
                            <span id="span_BecomePre">
                            <label class="label_check">
                                <input type="checkbox" name="chkBecomePre" id="chkBecomePre" value="1">
                            </label>
                            </span>
                            <img style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/see_details_btn.gif"
                            onclick="window.open('<?echo C_BASE_PATH?>popup/become_premium/0', '', 'width=600,height=650,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no,resizable=no');">
                        </div>
                        -->
                        <div id="myprofile_input_left" style="display:none">
                                <div class="admin_lbl" id="div_RNL" style="padding-left:2px">
                                <label class="label_radio">
                                    <input type="checkbox" name="chkRNL" id="chkRNL" value="1" checked="checked">
                                </label>
                             </div>
                             <div class="admin_lbl" style="padding-top:3px">Receive Newsletters</div>
                        </div>
                    </div>
                    <div id="myprofile_r_col">
                        <div id="myprofile_input_right">
                            <div class="admin_require_field"><img alt="Username" src="<?echo C_IMAGE_PATH?>label/username_lbl.gif"></div>
                            <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                            <input type="text" maxlength="30" class="myprofile_input" value="" name="txtUserName" id="txtUserName">
                            <span id="myprofile_warning">Spaces are allowed; punctuation is not allowed except for periods, comma, hyphens, and
                            underscores</span>
                            <div id="div_username_error" class="spanError"></div>
                        </div>
                        <div id="myprofile_input_right">
                            <div class="admin_require_field"><img alt="Password" src="<?echo C_IMAGE_PATH?>label/password_lbl.gif"></div>
                            <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                            <input type="password" class="myprofile_input" name="txtPassword" id="txtPassword" maxlength="64">
                        </div>
                        <div id="myprofile_input_right">
                            <div class="admin_require_field"><img alt="Confirm Password" src="<?echo C_IMAGE_PATH?>label/confirm_password_lbl.gif"></div>
                            <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                            <input type="password" class="myprofile_input" name="txtConfirmPassword" id="txtConfirmPassword" maxlength="64">
                            <div id="div_password_error" class="spanError"></div>
                        </div>
                        <div id="myprofile_input_right">
                            <div class="admin_require_field"><img alt="Email Address" src="<?echo C_IMAGE_PATH?>label/email_address_lbl.gif"></div>
                            <div class="admin_require_field"><span class="form-required" title="This field is required.">&nbsp;*</span></div>
                            <input type="text" class="myprofile_input" value="" id="txtMail" name="txtMail" maxlength="64">
                             <div id="div_mail_error" class="spanError"></div>
                        </div>
                        <!--
                        <div id="myprofile_input_right">
                            <div class="admin_require_field"><img src="<?echo C_IMAGE_PATH?>label/country_lbl.gif"></div>
                            <div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>
                            <select class="myprofile_select" name="ddlCountry" id="ddlCountry" onchange="loadState()">
                                <option value=""></option>
                                <?php 	/*$countries = profile_location_countries();
                                        foreach ($countries as $key => $value) {
                                            print '<option value="'.$key.'">'.$value.'</option>';
                                        }*/
                                ?>
                            </select>
                            <div id="div_country_error" class="spanError"></div>
                        </div>
                        <div id="myprofile_input_right">
                            <img src="<?echo C_IMAGE_PATH?>label/full_address_lbl.gif">
                            <input type="text" class="myprofile_input"  maxlength="100" name="txtAddress" id="txtAddress" value="">
                        </div>
                        <div id="myprofile_input_right">
                            <input type="text" class="myprofile_input"  maxlength="100" name="txtAddress1" id="txtAddress1" value="">
                        </div>
                       <div id="myprofile_input_right">
                            <img src="<?echo C_IMAGE_PATH?>label/city_lbl.gif">
                            <input type="text" class="myprofile_input"  name="txtCity" id="txtCity" maxlength="40" value="">
                        </div>
                        <div id="myprofile_input_right">
                            <div id="myprofile_state">
                                <img src="<?echo C_IMAGE_PATH?>label/state_province_lbl.gif"><br>
                                <select class="myprofile_select01"  name="ddlState" id="ddlState">
                                    <?php /*$states = profile_location_states($current_user->profile_country);
                                            foreach ($states as $key => $value) {
                                                print '<option value="'.$key.'">'.$value.'</option>';
                                            }*/
                                    ?>
                                </select>
                            </div>
                            <div id="myprofile_postal">
                                <img src="<?echo C_IMAGE_PATH?>label/postal_code_lbl.gif"><br>
                                <input type="text" class="myprofile_input01"  maxlength="20" name="txtPostalCode" id="txtPostalCode" value="">
                                <div id="div_postalcode_error" class="spanError"></div>
                            </div>
                        </div>
                        -->
                        <div id="myprofile_input_right">
                                <div class="admin_lbl" id="div_term_cond">
                                <label class="label_radio">
                                    <input type="checkbox" name="chkTermCond" id="chkTermCond" value="1">
                                </label>
                             </div>
                             <div style="padding-top:1px;" >
                                I agree to The Recipe Diva's <a  href="javascript:openTerm()">Terms of Service</a> and <a href="javascript:openPrivacy()">Privacy Policy</a>
                             </div>
                        </div>
                    </div>
                    <div id="myprofile_m_col">
                        <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                    <div id="myprofile_m_col">
                        <div id="myprofile_contain001">
                            <div id="dot_bg">
                                <img width="1" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></div>
                            <div id="saveall_btn">
                                <a href="#">
                                    <img alt="Submit Registry" onclick="submitForm()" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/submit_btn.gif"></a></div>
                        </div>
                    </div>
                </div>
                <!-- REGISTRATRION -->
            </div>
        </div>
    </div>
</div>

<div id="div_term" style="display:none;overflow:hidden;">
    <div id="email_recipe_contain">
        <div class="email_recipe_top">
            <div>&nbsp;</div>
        </div>
        <div class="email_recipe_m">
            <div class="email_recipe_content">
                <div id="popup_left_col" style="width:440px;padding:0;">
                    <div id="div_term_content" style="height:450px;width:440px;overflow:auto;margin:5px 0 20px 5px;border:1px solid #CCCCCC">
                        <div style="margin:0 5px 0 5px;">
                        <?php print recipe_db::get_static_page_content(TERM_OF_USE_PAGE);?>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align:center;">
                <input type="button" class="btn_close_privacy_term" onclick="javascript:$('#div_term').dialog('close')"/>
            </div>
        </div>
    </div>
</div>
<div id="div_privacy" style="display:none;overflow:hidden;">
    <div id="email_recipe_contain">
        <div class="email_recipe_top">
            <div>&nbsp;</div>
        </div>
        <div class="email_recipe_m">
            <div class="email_recipe_content">
                <div id="popup_left_col" style="width:440px;padding:0;">
                    <div id="div_privacy_content" style="height:450px;width:440px;overflow:auto;margin:5px 0 20px 5px;border:1px solid #CCCCCC">
                        <div style="margin:0 5px 0 5px;">
                        <?php print recipe_db::get_static_page_content(PRIVACY_PAGE);?>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align:center;">
                <input type="button" class="btn_close_privacy_term" onclick="javascript:$('#div_privacy').dialog('close')"/>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        onload_radio_checkbox('span_Gender');
        onload_radio_checkbox('span_BecomePre');
        onload_radio_checkbox('div_term_cond');
        onload_radio_checkbox('div_RNL');
    });
    function loadState(){
        if($("#ddlCountry").val() == ""){
            //$("#div_country_error").html("+ Please input country");
        }
        else{
            //$("#div_country_error").html("");
        }

        $.get("<?php print C_BASE_PATH."mydiva/profile/load_state/"?>" + $("#ddlCountry").val(),
               function(data){
                    $("#ddlState").html(data);
               });
    }
    function submitForm(){
//		$("#div_country_error").html("");
//		$("#div_username_error").html("");
//		$("#div_fullname_error").html("");
//		$("#div_postalcode_error").html("");
//		$("#div_mail_error").html("");
//		$("#div_password_error").html("");
        var errMsg = "";
        var hasError = false;
        $("#divError > ul").html("");
        $("#divError").hide();

        //Check Validate

        // Full Name
//		if($.trim($("#txtFullName").val()) == ""){
//			//$("#div_fullname_error").html("+ Please input full name");
//			errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Full Name'))?></li>";
//			$("#txtFullName").focus();
//			hasError = true;
//		}

        // User Name
        if($.trim($("#txtUserName").val()) == ""){
            //$("#div_username_error").html("+ Please input user name");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'User Name'))?></li>";
            if(!hasError){
                $("#txtUserName").focus();
            }
            hasError = true;
        }else{
            if(!/^[\s0-9a-zA-Z.,_-]+$/.test($("#txtUserName").val())){
                //$("#div_username_error").html("+ Please input a valid user name");
                errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'user name'))?></li>";
                if(!hasError){
                    $("#txtUserName").focus();
                }
                hasError = true;
            }
        }

        // Password
        if($.trim($("#txtPassword").val()) == "" || $.trim($("#txtConfirmPassword").val()) == ""){
            //$("#div_password_error").html("+ Please input password and confirm password");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Password and Confirm Password'))?></li>";
            if(!hasError){
                $("#txtPassword").focus();
            }
            hasError = true;
        }
        else if(!validatePassword($("#txtPassword").val()) || !validatePassword($("#txtConfirmPassword").val())){
            //$("#div_password_error").html("+ The password length at least 6 characters and not contains white-space characters");
            errMsg += "<li><?php print ERR_MSG_PASSWORD?></li>";
            if(!hasError){
                $("#txtPassword").focus();
            }
            hasError = true;
        }
        else if($("#txtPassword").val() != $("#txtConfirmPassword").val()){
            //$("#div_password_error").html("+ Password does not match");
            errMsg += "<li><?php print ERR_MSG_PASSWORD_NOT_MATCH?></li>";
            if(!hasError){
                $("#txtPassword").focus();
            }
            hasError = true;
        }

        // Email
        if($.trim($("#txtMail").val()) == ""){
            //$("#div_mail_error").html("+ Please input email address");
            errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Email Address'))?></li>";
            if(!hasError){
                $("#txtMail").focus();
            }
            hasError = true;
        }
        else if(!validateEmail($("#txtMail").val())){
            //$("#div_mail_error").html("+ Please input a valid email address");
            errMsg += "<li><?php print t(ERR_MSG_INVALID, array('@field_name' => 'email address'))?></li>";
            if(!hasError){
                $("#txtMail").focus();
            }
            hasError = true;
        }

        // Country
//		if($("#ddlCountry").val() == ""){
//			//$("#div_country_error").html("+ Please input country");
//			errMsg += "<li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Country'))?></li>";
//			if(!hasError){
//				$("#ddlCountry").focus();
//			}
//			hasError = true;
//		}

        // Postal Code
        if(!validatePostcode($("#txtPostalCode").val())){
            //$("#div_postalcode_error").html("+ Please input only digits");
            errMsg += "<li><?php print t(ERR_MSG_ISALPHANUMERIC, array('@field_name' => 'Postal Code'))?></li>";
            if(!hasError){
                $("#txtPostalCode").focus();
            }
            hasError = true;
        }

        if(!hasError){
            $.post("<?php print C_BASE_PATH."check_user_name_email_exist"?>", { username: $("#txtUserName").val(),mail:$("#txtMail").val() },
                       function(data){
                           var json = eval("(" + data + ")");
                        if(json['username']=="true"){
                            //$("#div_username_error").html("+ User Name is exist");
                            errMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'user name'))?></li>";

                            if(!hasError){
                                $("#txtUsername").focus();
                            }
                            hasError = true;
                        }
                        if(json['mail']=="true"){
                            //$("#div_mail_error").html("+ Email address is exist");
                            errMsg += "<li><?php print t(ERR_MSG_EXIST, array('@field_name' => 'email address'))?></li>";
                            if(!hasError){
                                $("#txtMail").focus();
                            }
                            hasError = true;
                        }

                        if(!hasError){
                            if($("#chkTermCond")[0].checked){
                                $("#user-register").attr("action","<?php print C_BASE_PATH."register_user_submit"?>");
                                $('#user-register').submit();
                            }
                            else{
                                errMsg += "<li><?php print ERR_MSG_AGREE_TERM_COND?></li>";
                                $("#divError > ul").html(errMsg);
                                $("#divError").show();
                            }
                        }
                        else{
                            $("#divError > ul").html(errMsg);
                            $("#divError").show();
                        }
                       });
        }
        else{
            $("#divError > ul").html(errMsg);
            $("#divError").show();
        }
    }

</script>
