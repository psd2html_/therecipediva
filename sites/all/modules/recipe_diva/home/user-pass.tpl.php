<?php
global $user;
if ($user->uid) {
    drupal_goto("");
}
else{
    /*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
      drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
      drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
?>
<div id="border_type01">
    <div id="border_type01_b">
        <div id="border_type01_border">
            <div id="border_type01_content">
                <div class="registration_p_title">
                    Forgot Your Password?</div><br>
                <div id="border_type01_content_1">
                    <?php
                        $form['submit']['#attributes'] = array('class'=>'bt_email_pass');
                        $form['submit']['#value'] = "";
                        print drupal_render($form['name']);?>
                    <br>
                    <?php  print drupal_render($form['submit']);?>
                    <?php  print drupal_render($form['form_id']);?>
                    <input type="hidden" value="<?php print $_POST["hidHasError"]?>" id="hidHasError" name="hidHasError">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    if($("#hidHasError").val()=="true"){
        $("#user-login-form").find("#edit-name").removeClass("error");
    }

    $("div[class^='messages error']").insertAfter($("div[class=registration_p_title]"));
    $("div[class^='messages status']").insertAfter($("div[class=registration_p_title]"));
    $(document).ready(function(){
        $("#user-pass").find("#edit-submit").click(function(){
            $("#hidHasError").val("true");
        })	;
        if($("div[class^='messages error'] > ul").length == 0){
            $("div[class^='messages error']").html("<ul><li>"+ $("div[class^='messages error']").html() + "</li></ul>");
        }
        $("#border_type01_content_1 :text").addClass("adminrecipe_input");

        if($.trim($("div[class^='messages status']").html())=="Further instructions have been sent to your Email address."){
            $("div[class^='messages status']").hide();
            showInfoMessage("<?php print INFO_MSG_PASSWORD_SENT ?>");
        }
    });
</script>
<?php }?>
