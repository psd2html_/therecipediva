<div id="admin_border_content">
    <form name="frmMainContentList" method="post" id="maincontent-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/maincontent', 'txtSearch');return false;">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Main Content Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/maincontent', 'txtSearch');return false;"/>
            </div>
		</div>
		<?php 
		    $delete_q = drupal_query_string_encode($_GET, array('q'));	    	
    		$title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
    		$title_q = $title_q != "" ? $title_q.'&':"";
    		
		?>		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmMainContentList, 'chk_', this)" />
			</div>
      		<div id="admin_home_title1">
      			<a href="javascript:changePaging('maincontent-form', '<?echo C_BASE_PATH?>adminrecipe/maincontent?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="title")?"desc":"asc"?>&order=title', '0');">
      			<span class="admin_title">Title</span></a>
			</div>
      		<div id="admin_home_created">
      			<a href="javascript:changePaging('maincontent-form', '<?echo C_BASE_PATH?>adminrecipe/maincontent?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
      			<span class="admin_title">Created</span></a>
			</div>
      		<div id="admin_home_status">
      			<a href="javascript:changePaging('maincontent-form', '<?echo C_BASE_PATH?>adminrecipe/maincontent?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="status")?"desc":"asc"?>&order=status', '0');">
      			<span class="admin_title">Status</span></a>
			</div>
      		<div id="admin_home_status">
      			<a href="javascript:changePaging('maincontent-form', '<?echo C_BASE_PATH?>adminrecipe/maincontent?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="order")?"desc":"asc"?>&order=order', '0');">
      			<span class="admin_title">Order</span></a>
			</div>	  	
  	  	  	<div id="admin_home_op_link" class="admin_title">Operations</div>
      	</div>      	
      	<?php 
		$maincontent_id = 0;	    			
	    foreach ($maincontentlist as $row) {
	    	$nodeLink = url("adminrecipe/maincontent/".$row->nid."/edit", array('query' => drupal_get_destination()));	 
	    ?>
	    <div id="admin_recipes_content_inner<?php print $maincontent_id % 2 ? "":"01"?>">
	    	<div class="admin_title" id="admin_recipes_col_del">
	    		<input type="checkbox" onclick="checkAllChange(document.frmMainContentList, 'chk_', this, document.frmMainContentList.chkAll)" value="<?php print $row->nid?>" name="chk_<?php print $maincontent_id?>">
	    	</div>
	        <div id="admin_home_title1">
	        	<a href="<?php print $nodeLink?>"><?php print $row->title?></a>
	        </div>
	        <div id="admin_home_created">
	        	by <a class="by_author" title="View user profile." href="<?php print C_BASE_PATH."user/".$row->uid?>"><?php print $row->name?></a>
	        	<br><span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
        	</div>
	        <div id="admin_home_status">
	        	<?php print $arr_status[$row->field_maincontent_status_value]?>
	        </div>
	        <div id="admin_home_status">
	        	<?php print $row->field_maincontent_order_value?>
	        </div>	        
	        <div class="admin_title" id="admin_home_op_link">
	        	<a onclick="return deleteAdminItem('frmMainContentList', '<?php print C_BASE_PATH."adminrecipe/maincontent?".$delete_q?>', '<?php print $row->nid?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
	        	</a>
	        </div>
      	</div>
	    <?php $maincontent_id++;} ?>
      	<input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/maincontent"?>">
      	<input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmMainContentList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/maincontent?".$delete_q; ?>')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmMainContentList', '<?php print C_BASE_PATH."adminrecipe/maincontent/add/recipe-maincontent?destination=adminrecipe/maincontent/"; ?>');" />
		</div>		
    	<?php print $pager ?>
	</div>
	</form>
</div>