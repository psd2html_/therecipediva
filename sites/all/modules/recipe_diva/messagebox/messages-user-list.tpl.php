<form id="messages-user-search-form" method="post" accept-charset="UTF-8" action="<?echo C_BASE_PATH?>">
<input type="hidden" name="page" value="<?php print $_GET["page"]?>">
<div id="mydiva_profiles_content_inner">
    <div id="upload_recipe">
        <div class="request_friends_header">
            <div style="float: left; position: relative;">
                <img alt="Messages" src="<?echo C_IMAGE_PATH?>label/lbl_icon_messages.gif">
            </div>
            <div class="num_request_friends">
                <?php
                global $user;
                $type = 0;
                if(arg(1)=="newmessages"){
                    $type = 1;
                }else if(arg(1)=="sentmessages"){
                    $type = 2;
                }

                print "(". recipe_db::count_user_message($user->uid,$type). "):"?>
            </div>
        </div>
        <div class="messages_select_content">
            <div id="messages_col_btn_container">
                <div id="groceries_col06" style="text-align:left">
                    <span style="color:#DD0021;font-weight:bold">Select:&nbsp;&nbsp;</span> <a rel="nofollow" style="color:#431a15" href="javascript:checkedAllMessages(true)">All&nbsp;</a> | <a rel="nofollow" style="color:#431a15" href="javascript:checkedAllMessages(false)">&nbsp;None</a>
                </div>
                <div id="groceries_col07">
                    <?php if(arg(1) != "sentmessages" && arg(1) != "newmessages"){?>
                    <img alt="Mark unread message" style="cursor:pointer" onclick="mark_message()" src="<?echo C_IMAGE_PATH?>label/lbl_mark_as_unread.gif">
                    <img width="6" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                    <?php }?>
                    <img alt="Delete message" style="cursor:pointer" onclick="delete_message(0)" src="<?echo C_IMAGE_PATH?>button/clear_grocery_fields_btn.gif">
                </div>
            </div>
        </div>
        <?php	global $user;
        foreach ($messageslist as $objUser){
            $is_new = (arg(1) != "sentmessages" && $objUser->message_flg==1);
            if($objUser->reply_id == ""){
                $objUser->reply_id = 0;
            }

        ?>
        <div id="div_user_<?php print $objUser->mid?>" class="<?php print ($is_new)?"messages_center_container_r":"messages_center_container"?>">
            <div class="messages_chk">
                <div id="outer">
                  <div id="middle">
                    <div id="inner">
                        <label class="label_radio" name="lblMessage">
                            <input name="chkMessage" id="<?php print $objUser->mid?>" class="<?php print $objUser->reply_id?>" type="checkbox">
                        </label>
                    </div>
                  </div>
                </div>
            </div>
            <div class="messages_img">
                <?php $file_path = $objUser->picture;
                if(file_exists($file_path)){ ?>
                    <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser->name?>"><img alt="<?php print $objUser->name?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,60) ?> /></a>
                <?php }else{?>
                    <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser->name?>"><img alt="<?echo $objUser->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?echo C_IMAGE_PATH?>photo/<?echo $objUser->profile_gender=="0"? "male":"female"?>.gif"></a>
                <?php }?>
            </div>
            <div class="messages_center_content">
                <div id="outer">
                  <div id="middle">
                    <div id="inner_name">
                        <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser->name?>"><?php print $objUser->name?></a><br>
                        <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$objUser->created)?></span>
                    </div>
                  </div>
                </div>
            </div>
            <div class="messages_center_content1">
                <div id="outer">
                  <div id="middle">
                    <div id="inner">
                          <a rel="nofollow" id="subject_<?php print $objUser->mid?>" href="javascript:expand_body(<?php print $objUser->mid.",".$objUser->message_flg.",".$objUser->reply_id?>)">
                          <?php print $objUser->subject."</a>";
                          //$objUser->body = nl2br($objUser->body);
                          /*$body = $objUser->body;
                             $excerpt = trim(recipe_utils::get_excerpt($body,20,''));
                             $body = trim(recipe_utils::get_excerpt($body,25,''));
                             if($excerpt != $body){
                                 $excerpt .= "...";
                                 print "<span id='span_short_body_".$objUser->mid."'>".$excerpt."</span><span id='span_body_".$objUser->mid."' style='display:none'>".nl2br($objUser->body)."</span>";
                             }else{
                                 print $excerpt;
                             }
                             */
                          ?>&nbsp;
                    </div>
                  </div>
                </div>
            </div>
            <div class="messages_btn">
                <div id="outer">
                  <div id="middle">
                    <div id="inner">
                        <img alt="Delete Message" onclick="delete_message(<?php print $objUser->mid?>)" src="<?echo C_IMAGE_PATH?>button/btn_delete_message.gif" style="cursor: pointer;">&nbsp;&nbsp;
                        <?php if($is_new){?>
                            <img id="img_star_<?php print $objUser->mid?>" src="<?echo C_IMAGE_PATH?>icon/ico_red_star.gif">
                        <?php }else{?>
                            <img id="img_star_<?php print $objUser->mid?>" style="display:none" src="<?echo C_IMAGE_PATH?>icon/ico_red_star.gif">
                        <?php }?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <?php }?>


        <?php if (count($messageslist) > 0){ ?>

        <div id="div_space">
            <img width="1" height="5" src="<?echo C_IMAGE_PATH?>space.gif">
        </div>

        <div class="messages_select_content">
            <div id="messages_col_btn_container_bottom">
                <div id="groceries_col06" style="text-align:left">
                    <span style="color:#DD0021;font-weight:bold">Select:&nbsp;&nbsp;</span> <a rel="nofollow" style="color:#431a15" href="javascript:checkedAllMessages(true)">All&nbsp;</a> | <a rel="nofollow" style="color:#431a15" href="javascript:checkedAllMessages(false)">&nbsp;None</a>
                </div>
                <div id="groceries_col07">
                    <?php if(arg(1) != "sentmessages" && arg(1) != "newmessages"){?>
                    <img alt="Mark unread message" style="cursor:pointer" onclick="mark_message()" src="<?echo C_IMAGE_PATH?>label/lbl_mark_as_unread.gif">
                    <img width="6" height="1" src="<?echo C_IMAGE_PATH?>space.gif">
                    <?php }?>
                    <img alt="Delete Message" style="cursor:pointer" onclick="delete_message(0)" src="<?echo C_IMAGE_PATH?>button/clear_grocery_fields_btn.gif">
                </div>
            </div>
        </div>

        <?php }?>

          <div id="img_seeall">
            <?php print $paging;?>
        </div>
        <div id="div_space">
            <img width="1" height="100" src="<?echo C_IMAGE_PATH?>space.gif">
        </div>
    </div>
</div>
</form>
<script>
    $(document).ready(function(){
        onload_radio_checkbox('mydiva_profiles_content_inner');
        $("div.network_content_inner").pngFix();
    });
    function delete_message(mid) {
        var message = "<?print CONFIRM_MSG_DELETE_THIS_MESSAGE?>";

        if(mid == 0){
            var listCB = $("input[type='checkbox'][name=chkMessage]:checked");
            var listChecked = "";
            for(i=0;i<listCB.length;i++){
                listChecked += "," + listCB[i].id;
            }
            if(listCB.length > 1){
                message = "<?print CONFIRM_MSG_DELETE_THESE_MESSAGES?>";
            }
        }
        else{
            listChecked = "," + mid;
        }
        if(listChecked != ""){
            listChecked = listChecked.substring(1);
            $("#dialogConfirm").attr("title", message);
            $("#ui-dialog-title-dialogConfirm").text(message);
            $("#dialogConfirm").dialog(
                { modal: true },
                { resizable: false },
                { minHeight: 0 },
                { buttons:
                    {
                        "Yes": function() {
                                    var url = "<?php print C_BASE_PATH."mydiva/deletemessages"?>";
                                    $.post(url,
                                            { hidListID: listChecked},
                                            function(data){
                                                var json = eval("(" + data + ")");
                                                if(json['status'] == "success"){
                                                    window.location.href = window.location.href;
                                                }
                                       },"text");
                                    $(this).dialog("close");
                                },
                        "No": function() {
                                    $(this).dialog("close");
                                }
                    }
                }
            );
        }
    }

    function mark_message() {
        var listCB = $("input[type='checkbox'][name=chkMessage]:checked");
        var listChecked = "";
        for(i=0;i<listCB.length;i++){
            listChecked += "," + listCB[i].id;
        }

        if(listChecked != ""){
            listChecked = listChecked.substring(1);
            var url = "<?php print C_BASE_PATH."mydiva/markmessages"?>";
            $.post(url,
                    { hidListID: listChecked},
                    function(data){
                        var json = eval("(" + data + ")");
                        if(json['status'] == "success"){
                            for(i=0;i<listCB.length;i++){
                                listChecked += "," + listCB[i].id;
                                $("#div_user_"+listCB[i].id).attr("class","messages_center_container_r");
                                $("#subject_"+listCB[i].id).attr("href","javascript:expand_body("+listCB[i].id+",1," + $("#"+listCB[i].id).attr("class") + ")");
                                $("#img_star_"+listCB[i].id).show();
                            }

                        }
               },"text");

        }
    }

    function checkedAllMessages(ischecked){
        $("input[type='checkbox'][name=chkMessage]").attr("checked",ischecked);
        $("label[name=lblMessage]").unbind("click");
        onload_radio_checkbox('mydiva_profiles_content_inner');
    }

    function expand_body(mid,msg_flg, reply_id){
        $("#subject_"+mid).attr("href","javascript:expand_body("+mid+",0,"+reply_id+")");
        /*
        if($("#span_short_body_"+mid).length > 0){
            if($("#span_short_body_"+mid).css("display") == "none"){
                $("#span_short_body_"+mid).show();
                $("#span_body_"+mid).hide();
                $("#subject_"+mid).parents("div[id=outer_]").attr("id","outer");
                $("#subject_"+mid).parents("div[id=middle_]").attr("id","middle");
                $("#subject_"+mid).parents("div[id=inner_]").attr("id","inner");
            }else{
                $("#span_short_body_"+mid).hide();
                $("#span_body_"+mid).show();
                $("#subject_"+mid).parents("div[id=outer]").attr("id","outer_");
                $("#subject_"+mid).parents("div[id=middle]").attr("id","middle_");
                $("#subject_"+mid).parents("div[id=inner]").attr("id","inner_");
            }
        }
        if(msg_flg == 1){
            //$("#div_user_"+mid).attr("class","messages_center_container");
            //$("#img_star_"+mid).hide();
            var url = "<?php print C_BASE_PATH."mydiva/markmessages/0"?>";
            $.post(url,
                    { hidListID: mid},
                    function(data){
                        var json = eval("(" + data + ")");
                        if(json['status'] == "success"){
                            window.location = "<?php print C_BASE_PATH."mydiva/detailmessages/"?>" + mid + "/" + reply_id + "?des=<?php print arg(1)?>";
                        }
           },"text");
        }
        else{
            window.location = "<?php print C_BASE_PATH."mydiva/detailmessages/"?>" + mid + "/" + reply_id + "?des=<?php print arg(1)?>";
        }
        */
        window.location = "<?php print C_BASE_PATH."mydiva/detailmessages/"?>" + mid + "/" + reply_id + "?des=<?php print arg(1)?>&msg_flg=" + msg_flg;
    }
</script>
