<?php
    global $user;
?>

<div id="mydiva_profiles_content_inner">
    <div id="upload_recipe">
        <div class="messages_select_content">
            <div id="messages_col_btn_container">
                <div id="groceries_col06" style="text-align:left">
                    <a rel="nofollow" href="<?php print C_BASE_PATH."mydiva/".$_GET["des"]?>"><img alt="Back Messages" src="<?php echo C_IMAGE_PATH?>label/lbl_back_to_messages.gif"></a>
                </div>
            </div>
        </div>
        <div id="div_error_send_message" class="message error" style="display:none;padding-bottom:5px;">
            <ul style="margin-bottom:0px;"><span id="send_message_error"></span></ul>
        </div>
        <span style="color:#DD0021; font-size:11.5px;"><?php print $messageslist[0]->subject?></span>
        <?php	global $user;
        $admin_id = recipe_db::get_admin_id();
        $receiver_id = $admin_id;

        $is_automsg = true;
        $sub_msg = $messageslist[0]->subject;
        $rep_sub_msg = $sub_msg;
        $rep_sub_msg = str_replace('Your friend request has been accepted by ','',$rep_sub_msg);
        $rep_sub_msg = str_replace('You have a new friend request from ','',$rep_sub_msg);
        $rep_sub_msg = str_replace('There is a new post on ','',$rep_sub_msg);
        $rep_sub_msg = str_replace('Your recipe for ','',$rep_sub_msg);

        $rep_sub_msg = str_replace('Your foodie request has been accepted by','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' has accepted your foodie request','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' would like to follow you','',$rep_sub_msg);
        //$rep_sub_msg = str_replace(' has added a review for ','',$rep_sub_msg);
        //$rep_sub_msg = str_replace(' has added a recipe for ','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' has just been shared!','',$rep_sub_msg);
        //$rep_sub_msg = str_replace(' has added a post on','',$rep_sub_msg);
        //$rep_sub_msg = str_replace(' has added a tip for ','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' has been approved!','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' has a new review','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' has a new post','',$rep_sub_msg);
        $rep_sub_msg = str_replace(' has added ','',$rep_sub_msg);



        if($rep_sub_msg == $sub_msg){
            $is_automsg = false;
        }

        foreach ($messageslist as $objUser){
            if($objUser->message_flg!=2 && $_GET["des"] != "sentmessages" && $objUser->uid != $user->uid && ($objUser->uid != $admin_id || !$is_automsg)){
                $receiver_id = $objUser->uid;
                if($objUser->uid){
            ?>
            Between <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$user->name?>">You</a>
            and <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser->name?>"><?php print $objUser->name?></a>
        <?php
                }
            break;
            }
        }
        $hidListID = "";
        foreach ($messageslist as $objUser){
            $hidListID .= ",".$objUser->mid;
            $user_id = $objUser->uid;
            $name = $objUser->name;
            $gender = $objUser->profile_gender;
            $file_path = $objUser->picture;

            if($user_id != $user->uid){
                $last_subject = $objUser->subject;
            }
        ?>

        <div id="div_user_<?php print $objUser->mid?>" class="messages_center_container1">
            <div class="messages_img">
                <?php if(file_exists($file_path)){ ?>
                    <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$name?>"><img alt="<?php print $name?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,60) ?> /></a>
                <?php }else{?>
                    <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$name?>"><img alt="<?php echo $gender=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?php echo C_IMAGE_PATH?>photo/<?php echo $gender=="0"? "male":"female"?>.gif"></a>
                <?php }?>
            </div>
            <div class="messages_center_content2">
                <!--<div class="reply_message"><a rel="nofollow" id="subject_<?php print $objUser->mid?>" href="#">
                      <?php print $objUser->subject?></a>
                </div>-->
                  <div class="reply_message"><a rel="nofollow" href="<?php print C_BASE_PATH."user/".$name?>"><?php print $name?></a>
                  <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$objUser->created)?></span></div>
                  <?php print nl2br($objUser->body);?>
            </div>
        </div>
        <?php }?>
        <?php
        if($_GET["des"] != "sentmessages" && (($receiver_id && $receiver_id != $admin_id) || !$is_automsg)){ ?>
            <div class="div_reply">
                <div class="messages_img">&nbsp;</div>
                <div class="messages_center_content2">
                    <img alt="Reply" src="<?php echo C_IMAGE_PATH?>label/lbl_reply.gif">
                    <div style="padding:5px 0 10px 0"><textarea style="height:200px;width:420px;overflow:auto;" type="text" id="txtSendMessage" name="txtSendMessage"  maxlength="500" value=""></textarea></div>
                    <img alt="Reply" onclick="sendMessage()" id="btnReply" class="img_click" src="<?php echo C_IMAGE_PATH?>button/btn_reply.gif">
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script>
<?php if($_GET["des"] != "sentmessages" && arg(3) != 0 && sizeof($messageslist) > 4){?>
$(document).ready(function(){
     var x = $("#div_user_<?php print arg(2)?>").offset().top;
     $('html,body').animate({scrollTop: x}, 500);
});
<?php } ?>
function sendMessage(){
    var objMessage = document.getElementById("txtSendMessage");
    var hasError = false;
    var errorMessage = "";
    var x=0;

    if (trim(objMessage.value) == ""){
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Message'));?>" + "</li>";
        if(hasError == false){
            objMessage.focus();
        }
        hasError = true;
    }else{
        if ($.trim($("#txtSendMessage").val()).length > 1500) {
            errorMessage += "<li>" + "<?php print MESSAGE_MAXLENGTH_ERROR?>" + "</li>";
            objMessage.focus();
            hasError = true;
        }
    }
    var objError = document.getElementById("div_error_send_message");
    if(hasError){
        $("#send_message_error").html(errorMessage);
        objError.style.display = "";
        return false;
    }else{
        objError.style.display = "none";
    }
    var listChecked = "<?php print $hidListID?>";
    if(listChecked != ""){
        listChecked = listChecked.substring(1);
    }
    var subject = '<?php print $last_subject?>';
    $("#btnReply").attr("onclick","");
    $.post("<?php print C_BASE_PATH."mydiva/replymessages/".$receiver_id."/".arg(3)?>",
                   { hidListID: listChecked, txtSubjectMessage:subject , txtSendMessage:$("#txtSendMessage").val()},
                   function(data){
                        var json = eval("(" + data + ")");
                        $('#div_message').dialog('close');
                        if (json['status'] == "success") {
                            showInfoMessage("<?php print SEND_MESSAGE_SUCCESS ?>");

                            $("#dialogInfo").dialog(
                                { modal: true },
                                { resizable: false },
                                { minHeight: 0 },
                                { buttons:
                                    {
                                        "OK": function() {
                                            $(this).dialog("close");
                                            window.location.href = "<?php print C_BASE_PATH."mydiva/messages"; ?>";
                                        }
                                    }
                                }
                            );


                        }
                        else{
                            showInfoMessage("<?php print SEND_MESSAGE_FAIL ?>");
                        }
                   },"text");
}
</script>
