<?php
// $Id: feedback-admin-section-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-section-list.tpl.php
 * Theme implementation to display a list of feedback sections.
 *
 * Available variables:
 * - $section_list: The list of diva tips
 *
 * @see template_preprocess_feedback_admin_section_list()
 * @see theme_feedback_admin_section_list()
 */
drupal_set_title("Feedback Sections");
?>
<div id="admin_border_content">
    <form name="frmFeedbackSection" method="post" id="feedback-section-form" action="<?php print url("adminrecipe/feedback/sections"); ?>">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Feedback Sections Management</div>
    	<!--
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" name="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_POST['txtSearch']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="this.form.submit();"/>
            </div>
		</div>-->
		
    	<div id="admin_recipes_content_title">
    		<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmFeedbackSection, 'chk_', this)" />
			</div>
			<div id="admin_feedback_section_title" class="admin_title">Sections</div>
			<div id="admin_feedback_question_num" class="admin_title">Questions</div>
			<div id="admin_feedback_section_link" class="admin_title">Questions Link</div>
			<div id="admin_divatips_op_link" class="admin_title">Rate Link</div>
      	</div>

      	<?php
      	$index = 0;
      	foreach ($section_list as $section) {
	      	if ($index % 2) {
				$div = '<div id="admin_recipes_content_inner">';
			} else {
				$div = '<div id="admin_recipes_content_inner01">';
			}
			if ($section->parents[0] == 0) {
		?>
			<?php print $div; ?>
				<div id="admin_recipes_col_del" class="admin_title">
  					<input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $section->tid; ?>" onClick="checkAllChange(document.frmFeedbackSection, 'chk_', this, document.frmFeedbackSection.chkAll);"/>
  				</div>
		        <div id="admin_feedback_section_title">
		        	<?php print $section->name; ?>
		        </div>
		        <?php if (isset($section->childs) == false) : ?>
		        <div id="admin_feedback_question_num">
		        	<?php print $section->question_num?$section->question_num:0; ?>
		        </div>
		        <div id="admin_feedback_section_link"><a href="<?php print url('adminrecipe/feedback/questions/'.$section->tid); ?>">Questions</a></div>
		        <div id="admin_divatips_op_link">
	      			<a href="<?php print url('adminrecipe/feedback/rate/'.$section->tid); ?>">Rate</a>
	      		</div>
	      		<?php endif; ?>
	        </div>
		<?php
			}
			else {
		?>
			<?php print $div; ?>
				<div id="admin_recipes_col_del" class="admin_title">
  					<input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $section->tid; ?>" onClick="checkAllChange(document.frmFeedbackSection, 'chk_', this, document.frmFeedbackSection.chkAll);"/>
  				</div>
				<div id="admin_feedback_section_space">&nbsp;</div>
		        <div id="admin_feedback_sub_section_title">
		        	<?php print $section->name; ?>
		        </div>
		        <div id="admin_feedback_question_num">
		        	<?php print $section->question_num?$section->question_num:0; ?>
		        </div>
		        <div id="admin_feedback_section_link"><a href="<?php print url('adminrecipe/feedback/questions/'.$section->tid); ?>">Questions</a></div>
		        <div id="admin_divatips_op_link">
	      		<a href="<?php print url('adminrecipe/feedback/rate/'.$section->tid); ?>">Rate</a>
	      		</div>
	        </div>
		<?php
			}
      		$index++;
      	}
      	?>
	</div>
    <input type="hidden" name="op" value="">
	<input type="hidden" name="delId" value="">
	<!--
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmFeedbackSection', 'chk_', '')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmFeedbackSection', '<?php print url("adminrecipe/feedback/add/section"); ?>');" />
		</div>
	</div>-->
	</form>
</div>