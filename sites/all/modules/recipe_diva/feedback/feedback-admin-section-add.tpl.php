<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-section-add.tpl.php
 * Theme implementation to display a input section form
 *
 * Available variables:
 * - $node node
 * - $section_form: section form
 *
 * @see template_preprocess_feedback_admin_section_add()
 * @see theme_feedback_admin_section_add()
 */
global $user;
$page_title = "";

if (arg(2) == 'add') {
	$page_title = 'Add Section';
} else {
	$page_title = 'Edit Section';
}

// Set page title
drupal_set_title($page_title);
?>

<div id="admin_border_content">
    <div class="registration_p_title"><?php print $page_title; ?></div>
	<div id="divatips_add_content_space">&nbsp;
	</div>
    <?php print $section_form; ?>
</div>