<?php
// $Id: feedback-admin-section-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-section-list.tpl.php
 * Theme implementation to display a list of feedback sections.
 *
 * Available variables:
 * - $section_list: The list of diva tips
 *
 * @see template_preprocess_feedback_admin_section_list()
 * @see theme_feedback_admin_section_list()
 */
drupal_set_title("Feedback Answers");
$destination = drupal_get_destination();
$search_url = url($_REQUEST['q']);
?>
<div id="admin_border_content">
    <form name="frmFeedbackAnswer" method="post" id="feedback-answer-form" onSubmit="javascript:submitForm('frmFeedbackAnswer', '<?php print $search_url; ?>', 'Search');">
    <div id="admin_recipe_content">
        <div class="admin_p_title">Feedback Answers Management</div>
        <div id="admin_divatips_search_form">
            <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
            <div id="searchfor_input">
                <input type="text" name="text" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['text']); ?>"/>
             </div>
            <div class="archive_search_btn">
                <input type="button" name="btnSearch" value="" class="bt_search" onClick="javascript:submitForm('frmFeedbackAnswer', '<?php print $search_url; ?>', 'Search');"/>
            </div>
        </div>

        <div id="admin_recipes_content_title">
            <div id="admin_recipes_col_del" class="admin_title">
            <input type="checkbox" name="chkAll" onclick="checkAll1(document.frmFeedbackAnswer, 'chk_', this)" />
            </div>
            <?php print $header; ?>
            <div id="admin_divatips_op_link" class="admin_title">Operations</div>
          </div>

          <?php
          $index = 0;
          while ($answer = db_fetch_object($answer_list)) {
              if ($index % 2) {
                $div = '<div id="admin_recipes_content_inner">';
            } else {
                $div = '<div id="admin_recipes_content_inner01">';
            }
        ?>
            <?php print $div; ?>
                <div id="admin_recipes_col_del" class="admin_title">
                      <input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $answer->nid; ?>" onClick="checkAllChange(document.frmFeedbackAnswer, 'chk_', this, document.frmFeedbackAnswer.chkAll);"/>
                  </div>
                  <div id="admin_feedback_answer_title">
                    <?php print $answer->body; ?>
                </div>
                  <div id="admin_divatips_created"><?php print recipe_utils::create_profile_link($answer, 'admin', false); ?></div>
                <div id="admin_divatips_op_link">
                  <a href="javascript:deleteAdminItem('frmFeedbackAnswer', '', '<?php print $answer->nid;?>')">Delete</a></a>
                  </div>
            </div>
        <?php
              $index++;
          }
          ?>
    </div>
    <input type="hidden" name="page" value="">
    <input type="hidden" name="op" value="">
    <input type="hidden" name="delId" value="">
    <div id="admin_recipe_content">
        <div id="admin_divatips_delete">
              <input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmFeedbackAnswer', 'chk_', '')" />
        </div>

        <?php print $pager ?>
    </div>
    </form>
</div>
