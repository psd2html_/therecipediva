<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback.tpl.php
 * Theme implementation to display a survey form
 *
 * Available variables:
 * - $question_list question list
 *
 * @see template_preprocess_feedback()
 * @see theme_feedbackk()
 */
// Set page title
drupal_set_title("Feedback");
$arr_radios = array();
?>
<form name="frmFeedBack" id="feedback-form" method="post">
<input type="hidden" name="hdf_terms" id="hdf_terms" value="<?php print $terms_value;?>"/>
<div id="admin_border_content">
    <div id ="form_error" class="messages error" style="display:none;">
    </div>
    <?php
    foreach($question_list as $key => $arr_question) { // sections
        if (empty($arr_question)) {
            continue;
        }
    ?>
    <div id="section<?php print $key; ?>" style="display:none;"><!-- section start -->
    <?php
        $index = 1;
        foreach ($arr_question as $question) { // start for question
        $arr_radios[] = $key."_".$question->field_question_id_value;
    ?>
            <!-- start question -->
            <div id="feedback_p_title">
                <?php print $index; ?>. <?php print $question->title;?>
            </div>
            <div id="div_space">
                <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="10" width="1" />
            </div>


            <?php if ($question->field_question_type_value == FEEDBACK_QUESTION_TYPE_MULTIPLE) { ?>

                <div id="">
                        <div id=""><!-- div for radio -->
                <?php
                    $answers = $question->field_answer_choice_value;
                    $arrAnswers = explode(';', $answers);
                    $i = 0;
                    foreach ($arrAnswers as $answer){
                    $i++;
                ?>
                      <?php
                        $chkName = "ckbTerm_".$key."_" . $question->field_question_id_value."_".$i;
                        $uncheck = "";
                        $check  = "";
                        $checked = "";
                        $itemValue = "";
                        $sub_cat = "";
                        $answer_choice_value = pow(2,$i-1);
                        if ($question->field_feedback_rate_value & $answer_choice_value) {
                            $check = ' style="display:none;"';
                            $itemValue = $answer_choice_value;
                        }

                        if($check){
                            $uncheck = '';
                        }else{
                            $uncheck = ' style="display:none;"';
                        }
                        $sub_cat .=	'<div id="feedback_radio_contain_row_dyn">';
                        $sub_cat .=	'<div id="categorize_content_feedback">';
                        $sub_cat .= '<div id="categorize_col2_feedback"><input type="button" class="advanced_check" id="img_check_' . $chkName . '" name="img_check_' . $chkName . '" onclick="checkItemImage(document.frmFeedBack, \'' . $chkName . '\', \'ckbTerm_\', 0);return false;" ' . $uncheck .' />';
                        $sub_cat .= '<input type="button" class="advanced_uncheck" id="img_uncheck_' . $chkName . '" name="img_uncheck_' . $chkName . '" onclick="checkItemImage(document.frmFeedBack,\'' . $chkName . '\', \'ckbTerm_\', 1);return false;" ' . $check .'/>';
                        $sub_cat .='		<input type="hidden" id="hdf_' . $chkName . '" name="hdf_' . $chkName . '" value="' . $answer_choice_value . '"/>';
                        $sub_cat .='		<input type="hidden" id="' . $chkName . '" name="' . $chkName . '" value="' . $itemValue . '"/>';
                        $sub_cat .='</div><div id="categorize_col3_feedback">' . $answer .'</div>';
                        $sub_cat .='</div>';
                        $sub_cat .='</div>';

                        print $sub_cat;

                    ?>

                <?php }?>
                </div> <!-- div for radio -->
              </div>
              <div id="div_space">
                <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="10" width="1" />
            </div>

            <?php } else { ?>

            <?php if ($question->field_question_type_value == FEEDBACK_QUESTION_TYPE_NORMAL) { ?> <!-- if for question type -->
            <div id="feedback_p_content">
                <div id="feedback_radios_<?php print $key."_".$question->field_question_id_value;?>"><!-- div for radio -->
                    <?php
                    $selected = "";
                    if ($question->field_feedback_rate_value == FEEDBACK_RATE_STRONGLY_AGREE) {
                        $selected = "checked";
                    }
                    ?>
                         <div id="feedback_radio_contain_row">
                         <div id="feedback_radio_contain_ext">
                             <label class="label_feedback_radio">
                          <input type="radio" name="rdbRate_<?php print $key."_".$question->field_question_id_value; ?>" id="rate" <?php print $selected; ?> value="<?php print FEEDBACK_RATE_STRONGLY_AGREE ?>"/>&nbsp;&nbsp;Strongly Agree
                          </label>
                      </div>

                      <span id="feedback_radio_contain">
                      <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="1" width="50" />
                      </span>
                      </div>
                      <?php
                    $selected = "";
                    if ($question->field_feedback_rate_value == FEEDBACK_RATE_AGREE) {
                        $selected = "checked";
                    }
                    ?>
                      <div id="feedback_radio_contain_row">
                      <div id="feedback_radio_contain_ext">
                          <label class="label_feedback_radio">
                              <input type="radio" name="rdbRate_<?php print $key."_".$question->field_question_id_value; ?>" id="rate" <?php print $selected; ?> value="<?php print FEEDBACK_RATE_AGREE ?>"/>&nbsp;&nbsp;Agree
                          </label>
                      </div>

                      <span id="feedback_radio_contain">
                      <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="1" width="50" />
                      </span>
                      </div>

                      <?php
                    $selected = "";
                    if ($question->field_feedback_rate_value == FEEDBACK_RATE_MOSTLY_AGREE) {
                        $selected = "checked";
                    }
                    ?>
                    <div id="feedback_radio_contain_row">
                      <div id="feedback_radio_contain_ext">
                          <label class="label_feedback_radio">
                          <input type="radio" name="rdbRate_<?php print $key."_".$question->field_question_id_value; ?>" id="rate" <?php print $selected; ?> value="<?php print FEEDBACK_RATE_MOSTLY_AGREE ?>"/>&nbsp;&nbsp;Mostly Agree
                          </label>
                      </div>

                      <span id="feedback_radio_contain">
                      <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="1" width="70" />
                      </span>
                      </div>

                      <?php
                    $selected = "";
                    if ($question->field_feedback_rate_value == FEEDBACK_RATE_DISAGREE) {
                        $selected = "checked";
                    }
                    ?>
                    <div id="feedback_radio_contain_row">
                      <div id="feedback_radio_contain_ext">
                          <label class="label_feedback_radio">
                              <input type="radio" name="rdbRate_<?php print $key."_".$question->field_question_id_value; ?>" id="rate" <?php print $selected; ?> value="<?php print FEEDBACK_RATE_DISAGREE ?>" />&nbsp;&nbsp;Disagree
                          </label>
                      </div>

                      <span id="feedback_radio_contain">
                      <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="1" width="50" />
                      </span>
                      </div>

                      <?php
                    $selected = "";
                    if ($question->field_feedback_rate_value == FEEDBACK_RATE_STRONGLY_DISAGREE) {
                        $selected = "checked";
                    }
                    ?>
                    <div id="feedback_radio_contain_row">
                      <div id="feedback_radio_contain_ext">
                          <label class="label_feedback_radio">
                          <input type="radio" name="rdbRate_<?php print $key."_".$question->field_question_id_value; ?>" id="rate" <?php print $selected; ?> value="<?php print FEEDBACK_RATE_STRONGLY_DISAGREE ?>"/>&nbsp;&nbsp;Strongly Disagree
                          </label>
                      </div>
                      </div>
                  </div> <!-- div for radio -->
              </div>
              <div id="div_space">
                <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="10" width="1" />
            </div>
              <?php } ?> <!-- end if for question type -->

               <?php if ($question->field_question_type_value == FEEDBACK_QUESTION_TYPE_COMMENT) { ?>


            <div id="feedback_p_content">
                     <textarea name="txtAnswer_<?php print $key."_".$question->field_question_id_value; ?>"  id="txtAnswer_<?php print $key."_".$question->field_question_id_value; ?>" ><?php print $question->body; ?></textarea>
              </div>
                <div id="div_space">
                <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="15" width="1" />
            </div>
              <?php } ?>

              <?php } ?>

              <div id="div_space">
                <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="15" width="1" />
            </div>
            <!-- end question -->
            <?php
                $index++;
            } // end for question
            ?>

      </div> <!-- section end -->
      <?php
    } // end for section
      ?>
       <input type="hidden" name="hidSection" id="hidSectionId" value="">
       <input type="hidden" name="op" id="hidOp" value="">
      <!--<div id="div_space">
          <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="16" width="1" />
      </div>-->
      <?php if (arg(1) == '') : ?>
      <div id="feedback_p_content">
          <input type="button" name="btnSave" value=""  class="bt_saveall" onClick="return validate();" />
      </div>
      <div id="div_space">
          <img src="<?php print C_IMAGE_PATH; ?>space.gif" height="16" width="1" />
      </div>
      <?php endif; ?>
</div>
</form>
 <script type="text/javascript">
     <?php
     if ($is_save) {

     }
     ?>
     var arr_map = new Array();
    <?php if (empty($array_map) == false) {?>
    <?php foreach ($array_map as $key => $value) {
    ?>
        arr_map['<?php print $key; ?>'] = '<?php print $value; ?>';
    <?php
        }
    }?>

    var msg_recipe = '';
    var msg_tip = '';

    $(document).ready(function(){
        <?php
         if ($is_save) :
         ?>
        showInfoMessage("<?php print FEEDBACK_SAVE_SUCCESS ?>");
        <?php endif; ?>
        var divDefSection = document.getElementById("section<?php print $array_map[HOME_PAGE_FEEDBACK]; ?>");
        if (divDefSection != undefined) {
            divDefSection.style.display = '';
        }
        // load radio buttons
        <?php
        if (!empty($arr_radios)) {
            foreach($arr_radios as $value) {
        ?>
                onload_feedback_radio_checkbox('feedback_radios_<?php print $value; ?>');
        <?php
            }
        }
        ?>
    });

    function change_tab(section_name) {
        var div_menu = document.getElementById("admin-menu");
        var menu_text = div_menu.innerHTML;
        menu_text = menu_text.replace('-focus', '');

        $("#recipe_box").css("color","#bfb7b1");
        $("#my_profile").css("color","#bfb7b1");
        $("#my_tips").css("color","#bfb7b1");
        $("#my_groceries").css("color","#bfb7b1");
        $("#my_recipe").css("color","#bfb7b1");
        $("#search").css("color","#bfb7b1");
        $("#overall").css("color","#bfb7b1");

        if (section_name == '<?php print HOME_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-home', 'admin-home-focus');
            MM_showHideLayers('mydiva_menu_others','','hide');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
        } else if (section_name == '<?php print RECIPES_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-recipes', 'admin-recipes-focus');
            MM_showHideLayers('mydiva_menu_others','','hide');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
        } else if (section_name == '<?php print DIVAGOSSIP_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-divagossip', 'admin-divagossip-focus');
            MM_showHideLayers('mydiva_menu_others','','hide');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
        } else if (section_name == '<?php print DIVATIPS_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-divatips', 'admin-divatips-focus');
            MM_showHideLayers('mydiva_menu_others','','hide');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
        } else if (section_name == '<?php print MYDIVA_MYRECIPEBOX_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-mydiva', 'admin-mydiva-focus');
            MM_showHideLayers('mydiva_menu_mydiva','','show');
            MM_showHideLayers('mydiva_menu_others','','hide');
            $("#recipe_box").css("color","#FC0");
        } else if (section_name == '<?php print MYDIVA_MYPROFILE_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-mydiva', 'admin-mydiva-focus');
            MM_showHideLayers('mydiva_menu_mydiva','','show');
            MM_showHideLayers('mydiva_menu_others','','hide');
            $("#my_profile").css("color","#FC0");
        } else if (section_name == '<?php print MYDIVA_MYTIPS_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-mydiva', 'admin-mydiva-focus');
            MM_showHideLayers('mydiva_menu_mydiva','','show');
            MM_showHideLayers('mydiva_menu_others','','hide');
            $("#my_tips").css("color","#FC0");
        } else if (section_name == '<?php print MYDIVA_GROCERIES_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-mydiva', 'admin-mydiva-focus');
            MM_showHideLayers('mydiva_menu_mydiva','','show');
            MM_showHideLayers('mydiva_menu_others','','hide');
            $("#my_groceries").css("color","#FC0");
        } else if (section_name == '<?php print MYDIVA_RECIPES_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-others', 'admin-others-focus');
            MM_showHideLayers('mydiva_menu_others','','show');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
            $("#my_recipe").css("color","#FC0");
        } else if (section_name == '<?php print MYDIVA_SEARCH_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-others', 'admin-others-focus');
            MM_showHideLayers('mydiva_menu_others','','show');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
            $("#search").css("color","#FC0");
        } else if (section_name == '<?php print MYDIVA_OVERALLWEBSITE_PAGE_FEEDBACK; ?>') {
            menu_text = menu_text.replace('admin-others', 'admin-others-focus');
            MM_showHideLayers('mydiva_menu_others','','show');
            MM_showHideLayers('mydiva_menu_mydiva','','hide');
            $("#overall").css("color","#FC0");
        }
        div_menu.innerHTML = menu_text;

        <?php if (empty($array_map) == false) {?>
        <?php foreach ($array_map as $value) {?>
            var divSection<?php print $value; ?> = document.getElementById("section<?php print $value; ?>");
            if (divSection<?php print $value; ?> != undefined) {
                divSection<?php print $value; ?>.style.display = 'none';
            }
        <?php
            }
        }?>
        var divDefSection = document.getElementById('section' + arr_map[section_name]);
        if (divDefSection != undefined) {
            divDefSection.style.display = '';
        }
    }

    function validate() {
        var frm = document.frmFeedBack;
        var divErr = document.getElementById('form_error');
        divErr.style.display = 'none';
//        for (var i=0;i<frm.elements.length;i++)
//        {
//            var e = frm.elements[i];
//            if (e.type=='radio' && e.checked && (e.value == '<?php print FEEDBACK_RATE_DISAGREE; ?>' || e.value == '<?php print FEEDBACK_RATE_STRONGLY_DISAGREE; ?>') )
//            {
//                var arr_name = e.name.split('_');
//                var txtAnswer =  document.getElementById('txtAnswer_' + arr_name[1] + '_' + arr_name[2]);
//                var text = txtAnswer.value;
//                text = text.replace(/^\s+/, '').replace(/\s+$/, '');
//                if (text == '') {
//                    divErr.innerHTML = '<ul><li><?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Answer'))?></li></ul>';
//                    divErr.style.display = '';
//                    txtAnswer.style.border = '1px solid #F00';
//                    txtAnswer.focus();
//                    return false;
//                }
//            }
//        }
        submitForm('frmFeedBack', '', 'Save');
        return true;
    }
</script>
