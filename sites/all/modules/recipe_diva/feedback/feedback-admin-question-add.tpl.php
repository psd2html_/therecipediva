<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-question-add.tpl.php
 * Theme implementation to display a input question form
 *
 * Available variables:
 * - $node node
 * - $question_form question form
 *
 * @see template_preprocess_feedback_question_add()
 * @see theme_feedback_question_add()
 */
global $user;
$page_title = "";

if (arg(2) == 'add') {
    $page_title = 'Add Question';
} else {
    $page_title = 'Edit Question';
}

// Set page title
drupal_set_title($page_title);
?>

<div id="admin_border_content">
    <div class="registration_p_title"><?php print $page_title; ?></div>
    <div id="divatips_add_content_space">&nbsp;
    </div>
    <?php print $question_form; ?>
</div>
