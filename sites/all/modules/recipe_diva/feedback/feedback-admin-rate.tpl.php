<?php
// $Id: feedback-admin-section-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-section-list.tpl.php
 * Theme implementation to display a list of feedback sections.
 *
 * Available variables:
 * - $section_list: The list of diva tips
 *
 * @see template_preprocess_feedback_admin_section_list()
 * @see theme_feedback_admin_section_list()
 */
drupal_set_title("Feedback Rate By Question");
?>
<div id="admin_border_content">
    <form name="frmFeedbackQuestion" method="post" id="feedback-question-form" >
    <div id="admin_recipe_content">
        <div class="admin_p_title">Feedback Rate By Question</div>
        <!--<div id="admin_divatips_search_form">
            <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
            <div id="searchfor_input">
                <input type="text" name="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_POST['txtSearch']); ?>"/>
             </div>
            <div class="archive_search_btn">
                <input type="button" name="btnSearch" value="" class="bt_search" onClick="this.form.submit();"/>
            </div>
        </div>-->


        <?php
          $index = 0;

          $str_multiple_title = '';
          $str_multiple_data = '';
          $str_multiple = '';
          $str_normal_data = '';
          $str_normal = '';

          $maxSizeTable = 760;

          foreach($question_list as $question) {
              if($question->nid > 0) {
                if ($index % 2) {
                    $class = ' class="bg" ';
                } else {
                    $class = '';
                }
                if($question->field_question_type_value==2) {
                            $answers = $question->field_answer_choice_value;
                            $arrAnswers = explode(';', $answers);
                            $i = 0;

                            $str_multiple_title = '<tr>';
                            $str_multiple_title .= '<td id="admin_feedback_rate_dyn" class="admin_title" style="width: 200px; text-align:left !important;">Questions</td>';

                            $str_multiple_data = '<tr>';
                            $str_multiple_data .= '<td id="admin_feedback_rate_dyn" style="text-align:left !important;">'.$question->title.'</td>';

                            $sizeTable = 200;

                            foreach ($arrAnswers as $answer){
                                $i++;
                                $size = strlen($answer);

                                $width = ($size > 10)?'style="width:'.($size * 10).'px;"':'';

                                $str_multiple_title .= '<td id="admin_feedback_rate_dyn" class="admin_title" '.$width.'>'.$answer.'</td>';

                                $str_multiple_data .= '<td id="admin_feedback_rate_dyn" '.$width.'>'.(int)$question->$i.'</td>';

                                $sizeTable += $size * 10;
                            }

                            $maxSizeTable = ($sizeTable > $maxSizeTable)?$sizeTable:$maxSizeTable;

                            $str_multiple_data .= '</tr>';
                            $str_multiple_title .= '</tr>';
                            $str_multiple .= '';
                            $str_multiple .= '<table border="0" align="center" cellspacing="0" cellspading="3" class="tbl_rate">';
                            $str_multiple .= $str_multiple_title.$str_multiple_data;
                            $str_multiple .= '</table>';
                            $str_multiple .= '';

                } else {
                            $str_normal_data .= '<tr>
                                <td id="admin_feedback_rate_dyn" style="text-align:left !important;" '.$class.'>'.$question->title.'</td>
                                <td id="admin_feedback_rate_dyn" '.$class.'>'.$question->rate_strongly_agree.'</td>
                                <td id="admin_feedback_rate_dyn" '.$class.'>'.$question->rate_agree.'</td>
                                <td id="admin_feedback_rate_dyn" '.$class.'>'.$question->rate_mostly_agree.'</td>
                                <td id="admin_feedback_rate_dyn" '.$class.'>'.$question->rate_disagree.'</td>
                                <td id="admin_feedback_rate_dyn" '.$class.'>'.$question->rate_strongly_disagree.'</td>
                            </tr>';
                }

                $index++;
             }
          }

          if($str_normal_data != '')
              $str_normal = '
                                <table border="0" align="center" cellspacing="0" cellspading="3" class="tbl_rate">
                                <tr>
                                    <td id="admin_feedback_rate_dyn" class="admin_title" width="200" style="text-align:left !important;">Questions</td>
                                    <td id="admin_feedback_rate_dyn" class="admin_title" width="100">Strongly Agree</td>
                                    <td id="admin_feedback_rate_dyn" class="admin_title" width="70">Agree</td>
                                    <td id="admin_feedback_rate_dyn" class="admin_title" width="100">Mostly Agree</td>
                                    <td id="admin_feedback_rate_dyn" class="admin_title" width="80">Disagree</td>
                                    <td id="admin_feedback_rate_dyn" class="admin_title" width="120">Strongly Disagree</td>
                                </tr>' . $str_normal_data .
                                '</table>
                             ';

          print $str_normal;
          print $str_multiple;

          ?>
    </div>
    <input type="hidden" name="page" value="">
    <div id="admin_recipe_content">
        <!--<div id="admin_divatips_delete">
              <input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmFeedbackQuestion', 'chk_', '')" />
        </div>
        <div id="admin_divatips_add">
              <input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmFeedbackQuestion', '<?php print url("adminrecipe/feedback/add/question"); ?>');" />
        </div>-->
        <?php print $pager ?>
    </div>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $(".tbl_rate").width(<?php print $maxSizeTable; ?>);
    });

</script>
