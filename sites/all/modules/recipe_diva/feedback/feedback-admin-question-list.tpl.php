<?php
// $Id: feedback-admin-section-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-section-list.tpl.php
 * Theme implementation to display a list of feedback sections.
 *
 * Available variables:
 * - $section_list: The list of diva tips
 *
 * @see template_preprocess_feedback_admin_section_list()
 * @see theme_feedback_admin_section_list()
 */
drupal_set_title("Feedback Questions");
$arr_types = get_question_types();
$arr_sections = get_section_array();
$destination = drupal_get_destination();
$search_url = url("adminrecipe/feedback/questions");
?>
<div id="admin_border_content">
    <form name="frmFeedbackQuestion" method="post" id="feedback-question-form" onSubmit="javascript:submitForm('frmFeedbackQuestion', '<?php print $search_url; ?>', 'Search');">
    <div id="admin_recipe_content">
        <div class="admin_p_title">Feedback Questions Management</div>
        <div id="admin_divatips_search_form">
            <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
            <div id="searchfor_input">
                <input type="text" name="text" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['text']); ?>"/>
             </div>
            <div class="archive_search_btn">
                <input type="button" name="btnSearch" value="" class="bt_search" onClick="javascript:submitForm('frmFeedbackQuestion', '<?php print $search_url; ?>', 'Search');"/>
            </div>
        </div>

        <div id="admin_recipes_content_title">
            <div id="admin_recipes_col_del" class="admin_title">
            <input type="checkbox" name="chkAll" onclick="checkAll1(document.frmFeedbackQuestion, 'chk_', this)" />
            </div>
            <?php print $header; ?>
            <div id="admin_divatips_op_link" class="admin_title">Operations</div>
          </div>

          <?php
          $index = 0;
          while ($question = db_fetch_object($question_list)) {
              if ($index % 2) {
                $div = '<div id="admin_recipes_content_inner">';
            } else {
                $div = '<div id="admin_recipes_content_inner01">';
            }
        ?>
            <?php print $div; ?>
                <div id="admin_recipes_col_del" class="admin_title">
                      <input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $question->nid; ?>" onClick="checkAllChange(document.frmFeedbackQuestion, 'chk_', this, document.frmFeedbackQuestion.chkAll);"/>
                  </div>

                   <div id="admin_feedback_question_title">
                    <a href="<?php print url('adminrecipe/feedback/'.$question->nid.'/edit', array('query' => $destination)); ?>"><?php print $question->title; ?></a>
                  </div>
                  <div id="admin_feedback_question_section">
                    <?php //print $arr_sections[$question->field_page_section_id_value];
                        $href = ($question->sumcomment > 0)?url('adminrecipe/feedback/answers/'.$question->nid, array()):"#";
                    ?>
                    <a href="<?php print $href; ?>"><?php print (int)$question->sumcomment; ?></a>
                  </div>
                  <div id="admin_feedback_question_answer_rate">
                    <?php //print $arr_sections[$question->field_page_section_id_value];
                        $href = ($question->sumrate > 0)?url('adminrecipe/feedback/rate/'.$question->field_page_section_id_value, array()):"#";
                    ?>
                    <a href="<?php print $href; ?>"><?php print (int)$question->sumrate; ?></a>
                  </div>
                  <div id="admin_divatips_created"><?php print recipe_utils::create_profile_link($question, 'admin', false); ?></div>
                  <div id="admin_feedback_question_type"><?php print $arr_types[$question->field_question_type_value]; ?></div>
                  <div id="admin_feedback_question_order"><?php print $question->field_question_order_value; ?></div>
                <div id="admin_divatips_op_link">
                  <a href="javascript:deleteAdminItem('frmFeedbackQuestion', '', '<?php print $question->nid;?>')">Delete</a></a>
                  </div>
            </div>
        <?php
              $index++;
          }
          ?>
    </div>
    <input type="hidden" name="page" value="">
    <input type="hidden" name="op" value="">
    <input type="hidden" name="delId" value="">
    <div id="admin_recipe_content">
        <div id="admin_divatips_delete">
              <input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmFeedbackQuestion', 'chk_', '')" />
        </div>
        <div id="admin_divatips_add">
              <input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmFeedbackQuestion', '<?php print url("adminrecipe/feedback/add/question", array('query' => $destination)); ?>');" />
        </div>
        <?php print $pager ?>
    </div>
    </form>
</div>
