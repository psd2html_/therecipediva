<?php
// $Id: feedback-admin-section-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file feedback-admin-section-list.tpl.php
 * Theme implementation to display a list of feedback sections.
 *
 * Available variables:
 * - $section_list: The list of diva tips
 *
 * @see template_preprocess_feedback_admin_section_list()
 * @see theme_feedback_admin_section_list()
 */
drupal_set_title("Feedback Rate By User");
?>
<div id="admin_border_content">
    <form name="frmFeedbackUser" method="post" id="feedback-user-form" >
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Feedback Rate By User</div>
    	<!--<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" name="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_POST['txtSearch']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="this.form.submit();"/>
            </div>
		</div>-->
		
    	<div id="admin_recipes_content_title">
			<div id="admin_feedback_rate_title" class="admin_title">Users</div>
			<div id="admin_feedback_rate_ext" class="admin_title">Strongly Agree</div>
			<div id="admin_feedback_rate" class="admin_title">Agree</div>
			<div id="admin_feedback_rate_ext" class="admin_title">Mostly Agree</div>
			<div id="admin_feedback_rate" class="admin_title">Disagree</div>
			<div id="admin_feedback_rate_ext" class="admin_title">Strongly Disagree</div>
			<div id="admin_divatips_op_link" class="admin_title">Operations</div>
      	</div>
		<?php
      	$index = 0;
      	foreach($user_list as $user) {
	      	if ($user->uid == 0 || $user->name == '' || $user->name == C_UNKNOWN_USER) {
	      		continue;
	      	}
	      	if ($index % 2) {
				$div = '<div id="admin_recipes_content_inner">';
			} else {
				$div = '<div id="admin_recipes_content_inner01">';
			}
		?>
		<?php print $div; ?>
	        <div id="admin_feedback_rate_title">
	        	<a href="<?php print url("friendsandfamilyfeedback/$user->uid");?>" target="_blank"><?php print $user->name; ?></a>
	        </div>
			<div id="admin_feedback_rate_ext"><?php print $user->rate_strongly_agree; ?></div>
			<div id="admin_feedback_rate"><?php print $user->rate_agree; ?></div>
			<div id="admin_feedback_rate_ext"><?php print $user->rate_mostly_agree; ?></div>
			<div id="admin_feedback_rate"><?php print $user->rate_disagree; ?></div>
			<div id="admin_feedback_rate_ext"><?php print $user->rate_strongly_disagree; ?></div>
			<div id="admin_divatips_op_link">
			<a href="javascript:deleteAdminItem('frmFeedbackUser', '<?php print url("adminrecipe/feedback/user/delete"); ?>', '<?php print $user->uid;?>')">Delete</a>
			</div>
      	</div>
        <?php
        	$index++;
      	}
      	?>                      	
	</div>
	<input type="hidden" name="page" value="">
    <input type="hidden" name="op" value="">
	<input type="hidden" name="delId" value="">
	<div id="admin_recipe_content">
		<!--<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmFeedbackUser', 'chk_', '')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmFeedbackUser', '<?php print url("adminrecipe/feedback/add/question"); ?>');" />
		</div>-->
		<?php print $pager ?>
	</div>
	</form>
</div>