<?php
// $Id: review-add.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file review-add.tpl.php
 * Theme implementation to display the node information and post comment for node.
 *
 * Available variables:
 * - $node: node information
 *
 * @see template_preprocess_comment_add()
 * @see theme_review_add()
 */
// Get posted user
$post_user = user_load(array('uid' => $node->uid));
// Get user profile link
$user_link = "user/".$post_user->name."/".recipe_utils::removeWhiteSpace($post_user->name);
$user_link = l($post_user->name, $user_link);
if ($_POST['op'] == 'Preview') {
	drupal_set_title("Preview comment");
} else {
	if (arg(1) == 'reply') {
		drupal_set_title("New comment");
	} else {
		drupal_set_title($page_title);
	}
}
$err_mess = create_review_error();
?>
<!-- START FEATURE RECIPES -->
<div id="news_p">
	<div id="news_p_b">
		<div id="news_border">
			<div id="news_p_content" class="news_p_content_font">
  				<div id="divatips_comment_content_b">
  				<div class="divatips_p_comment_title"><? print $node->title?></div>
  				<?php if ($node->type == 'recipe_tips'){?>
				<!--<div style="float: left; width: 170px; text-align:right; margin: 13px 0 8px 0;"><img src="<? print C_IMAGE_PATH ?>button/addtomysavetips.gif" width="155" height="21" style="cursor:hand;" onclick="addFaviroteNode('<?php print C_BASE_PATH."favorite_nodes/add/". $node->nid ?> ')" /></div>-->
  				<div class="new_p_posted">by <?php print $user_link; ?> Submitted <span class="feed_date"><? print date(STANDARD_DATE_FORMAT, $node->created);//date("M Y", $node->created)?></span></div>
  				<div id="img_contain">
					<img alt="<? print $node->title?>" src="<? print C_BASE_PATH.$node->field_image[0]['filepath'] ?>" width="515" height="309"/>
					<div id="news_p_menu" style="visibility: visible;">
					</div>
				</div>
				</div>
				<div id="content_b">
				<div id="divatiptab_content" class="bg-none">
				<div id="divatiptab_content_inner">
				<?php print $description;?>
				<?php print $err_mess; ?>
				<?php print $preview_form ?>
				<div id="divatips_comment_content_contain">
					<span class="recipes_upload_lbl">Your name: </span><?php print recipe_utils::create_author_link($user->name, $user->name, 'new_divatips_author'); ?>
				</div>
				<?php print $comment_form ?>
				</div>
				</div>
				</div>
			<?}else if($node->type == 'recipe'){
				$current_rating = votingapi_select_results(array('content_id' => $node->nid, 'function' => 'average'));
			?>
			<!-- START FEATURE RECIPES -->
				<div style="float: left; width: 170px; margin: 13px 0 8px 0;">
                	<?php print theme('fivestar_static', $current_rating[0]['value'], '5');?>
                </div>
                <div class="new_p_posted">by <?php print $user_link; ?></a> Submitted <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT, $node->created);//date('M Y', $node->created)?></span></div>
                <div id="img_contain">
                	<img alt="<?php print $node->title?>" src="<?php print C_BASE_PATH . $node->field_recipe_image[0]['filepath']?>" <?php print recipe_utils::getImageWidthHeight($node->field_recipe_image[0]['filepath'],535,309)?>/>
                </div>
                </div>
                <div id="individual_p_b">
               		<div id="individual_p_content">
                		<div id="individual_l_col">
                        	<div id="individual_p_title_contain">
                            	<div id="individual_p_title" class="individual_title">Ingredients</div>
                          		<div id="add_all_ingredients">
                          		</div>
                            </div>
                            <div class="ingredients_content">
                                <?php 
                                	$strIngredients = $node->field_ingredients[0]['value'];
                                	$arrIngredients = split(',', $strIngredients);
									$strBr = "";
									$intCount = 1;
                            		foreach ($arrIngredients as $strIngredient) {
                            			
                                    	if (trim($strIngredient) != ""){
                                    		$intCount ++;
                                    		print $strBr . $strIngredient;
											$strBr = "<br/>";
                                    	}
                            		}
                                 ?>
                             </div>    
                      	</div>
                    </div>
                    <div id="divatiptab_content" class="bg-none" style="float: left; position: relative;">
                    	<div id="divatiptab_vote" class="individual_title">Method</div>
                        <div id="divatiptab_content_inner">
                          <?php print $node->field_preparation[0]['value']?>
                        </div>
                    </div>
	            </div>
	            <div id="content_b">
	            <?php print $preview_form ?>
	            <div id="divatips_comment_content_contain">
					<span class="recipes_upload_lbl">Your name: </span><?php print recipe_utils::create_author_link($user->name, $user->name, 'new_divatips_author'); ?>
				</div>
				<?php print $comment_form ?>
				</div>
			<!-- START FEATURE RECIPES -->
			<?php }?>
			</div>
		</div>
	</div>
</div>
<!-- END FEATURE RECIPES -->
