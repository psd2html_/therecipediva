<?php
// $Id: divagossip-add.tpl.php,v 1.0 2010/05/21 08:39:35 goba Exp $

/**
 * @file divagossip-post-reply.tpl.php
 * Default theme implementation to display a topic reply form
 *
 * Variables available:
 * - $topic_form
 *
 * @see template_preprocess_divagossip_post_reply()
 * @see theme_divagossip_post_reply()
 */
drupal_add_js('sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js');
// Get term object matching a term ID.
$term = $node->taxonomy[$node->forum_tid];
$topic_profile =  recipe_utils::create_profile_link($node);
// Get term image
$term_image = get_term_image_url($node->forum_tid, true);
$err_message = create_review_error();
// Get top bar image
$header_bar = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_POST_REPLY_PAGE, 1);
// Get top bar background image
$header_bar_bg = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_POST_REPLY_PAGE_BG, 1);
?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--



//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

  tinyMCE.init({
    mode : "exact",
    theme : "advanced",
    relative_urls : false,
    document_base_url : "<?php print C_BASE_PATH;?>",
    language : "en",
    safari_warning : false,
    entity_encoding : "raw",
    verify_html : false,
    preformatted : false,
    convert_fonts_to_spans : true,
    remove_linebreaks : true,
    apply_source_formatting : true,
    theme_advanced_resize_horizontal : false,
    theme_advanced_resizing_use_cookie : false,
    plugins : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_path_location : "none",
    theme_advanced_resizing : true,
    theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,numlist,bullist,indent,outdent,separator,undo,redo",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    elements : "comment"
  });

//--><!]]>
</script>
<script type="text/javascript">
    $(document).ready(function() {
            $('#news_p_menu_tip').pngFix();
            ResizeImage('topic_preview', 520);
            ResizeImage('topic_content_contain', 400);
        });
</script>

<!-- START FEATURE RECIPES -->
<div id="divagossip_p">
    <div id="divagossip_p_top" style="background: url(<?php print $header_bar ? C_BASE_PATH.$header_bar : C_IMAGE_PATH.'divagossip/announcement_top.gif'?>) no-repeat top left;">
        <div id="divagossip_p_top_contain" style="background: url(<?php print $header_bar_bg ? C_BASE_PATH.$header_bar_bg : C_IMAGE_PATH.'divagossip/announcement_top_bg.gif'?>) repeat-y;">
            <div id="divagossip_p_top_title">
                <div id="divagossip_p_top_icon"><?php print $term_image; ?></div>
                <div id="divagossip_p_top_forum_name"><a href="<?php print C_BASE_PATH."divagossip/".recipe_utils::removeWhiteSpace($term->name).'-'.$term->tid?>"><?php print $term->name; ?></a></div>
            </div>
        </div>
    </div>
    <div id="announcements_p_b">
        <div id="divagossip_p_content">
            <div id="divagossip_p_content_inner">
                <div id="TipArchive_Search_Form">
                    <div id="announcements_newtopic">
                        <?php $destination = drupal_get_destination(); ?>
                        <a href="<?php print url("divagossip/add/topic/$term->tid", array('query' => $destination)); ?>">
                          <img alt="New Topic" src="<?php print C_IMAGE_PATH ?>button/newtopic.gif" />
                          </a>
                    </div>
                      <div id="searchfor_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                    <?php print $search_form; ?>
                </div>
            </div>
              <div id="divagossip_p_content_inner">
                <div id="topic_thread_p_title_contain_bg">
                       <div id="topic_thread_p_content_inner_title">
                            <div id="post_replay_title">
                                <?php print $node->title;?>
                            </div>
                    </div>
                </div>

                <div id="topic_thread_p_content_inner_contain">
                    <div id="topic_thread_p_content_contain">
                        <div id="topic">
                             <div id="topic_b">
                                <div id="topic_content">
                                    <div id="topic_content_inner">
                                        <div id="topic_created_by"><?php print $topic_profile; ?></div>
                                        <div id="topic_content_contain"><?php print $node->body; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php print $err_message; ?>
                         <div id="topic_preview"><?php print $preview_form; ?></div>
                         <div id="topic_thread_p_content_contain">
                        <img alt="Reply" src="<?php print C_IMAGE_PATH; ?>divagossip/reply.gif" width="38" height="20" />
                    </div>
                    <div id="topic_thread_p_content_contain">
                        <div id="newtopic_title">Your name:</div>
                        <div id="newtopic_name"><?php print recipe_utils::create_author_link($user->name, $user->name, 'newtopic_name');; ?></div>
                    </div>
                         <?php print $comment_form; ?>
                </div>
              </div>
          </div>
    </div>
</div>
<!-- END FEATURE RECIPES -->
