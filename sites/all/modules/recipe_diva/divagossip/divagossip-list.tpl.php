<?php
// $Id: divagossip-list.tpl.php,v 1.0 2010/05/20 08:39:35 goba Exp $

/**
 * @file divagossip-list.tpl.php
 * Default theme implementation to display a forum which may contain forum
 * containers as well as forum topics.
 *
 * Variables available:
 * - $forums: The forums to display (as processed by forum-list.tpl.php)
 * - $tid: term id
 *
 * @see template_preprocess_divagossip_list()
 * @see theme_divagossip_list()
 */
$header_bar = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_PAGE, 1);
?>
<!-- START FEATURE RECIPES -->
<div id="divagossip_p">
    <div id="divagossip_p_main_top" style="background: url(<?php print $header_bar ? C_BASE_PATH.$header_bar : C_IMAGE_PATH.'border/divagossip_top.gif'?>) no-repeat top left;">
              <div id="divagossip_p_main_top_col_contain">
            <div id="divagossip_p_main_top_col1"><img src="<?php print C_IMAGE_PATH ?>space.gif" height="1" width="1" /></div>
            <div id="divagossip_p_main_top_col2"><img alt="Forum" src="<?php print C_IMAGE_PATH ?>label/forum_title.png" width="52" height="21" /></div>
            <div id="divagossip_p_main_top_col3"><img alt="Topic" src="<?php print C_IMAGE_PATH ?>label/topic_title.png" width="58" height="21" /></div>
            <div id="divagossip_p_main_top_col4"><img alt="Posts" src="<?php print C_IMAGE_PATH ?>label/posts_title.png" width="46" height="21" /></div>
            <div id="divagossip_p_main_top_col5"><img alt="Last posts" src="<?php print C_IMAGE_PATH ?>label/last_post_title.png" width="74" height="21" /></div>
        </div>
      </div>
    <div id="divagossip_p_b">
        <div id="divagossip_p_content">
            <!--<div id="divagossip_p_content_content_contain">-->
            <div id="divagossip_p_content_inner">
                <div id="TipArchive_Search_Form">
                    <div id="announcements_newtopic">
                        <?php $destination = drupal_get_destination(); ?>
                        <a href="<?php print url("divagossip/add/topic", array('query' => 'destination=divagossip')); ?>">
                          <img alt="New topic" src="<?php print C_IMAGE_PATH ?>button/newtopic.gif" />
                          </a>
                    </div>
                    <div id="searchfor_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                    <?php print $search_form; ?>
                </div>

            </div>
            <!--</div>-->
            <!-- start item -->
            <?php
            $index = 0;
            if ($tid == 0) {
                $forum_count = 0;
            } else {
                $forum_count = $count_forum_tree;
            }
            foreach ($forums as $child_id => $forum):
                $div_style = "";
                if ($index == ($forum_count - 1)) {
                    $div_style = 'style="background: none;"';
                }
                $index = $index + 1;
            ?>
            <?php

            $last_post = $forum->last_post;
            $str_last_post = recipe_utils::create_profile_link($last_post);
            ?>
            <!--<div id="divagossip_p_content_content_contain">-->
              <!--<div id="divagossip_p_content_inner">-->
                  <?php
                if (!empty($forum->container)) :
                    $index = 0;
                    $forum_count = recipe_db::get_forum_count($forum->vid, $forum->tid);
                      // None display container have no forum
                      if ($forum_count > 0) :
                ?>
                <div id="divagossip_p_content_inner">
                  <div id="divagossip_p_content_inner_title">

                        <?php if (file_exists($forum->image)){?>
                            <h1><img alt="<?php print $forum->name; ?>" src="<?php print $forum->image; ?>" /></h1>
                        <?php }else{ ?>
                            &nbsp;
                           <?php } ?>

                </div>
                </div>
                <?php endif;
                else:  ?>
                <div id="divagossip_p_content_inner">
                <div id="divagossip_p_content_inner_contain">
                    <div id="divagossip_p_content_contain" <?php print $div_style ?>>
                        <?php if (file_exists($forum->image)) :
                            //$img_width = recipe_utils::getImageWidthValue($forum->image, 70);
                        ?>
                        <div id="divagossip_p_pic">
                            <div id="outer2">
                              <div id="middle">
                                <div id="inner">
                                    <img src="<?php print $forum->image; ?>" />
                                </div>
                              </div>
                            </div>
                        </div>
                        <?php else : ?>
                        <div id="divagossip_p_pic">&nbsp;</div>
                        <?php endif; ?>
                        <div id="divagossip_p_forum">
                            <a href="<?php print url("divagossip/" . recipe_utils::removeWhiteSpace($forum->name) . "-" . $forum->tid); ?>"><?php print $forum->name; ?></a>
                          </div>
                          <div id="divagossip_p_topics">
                            <?php print $forum->num_topics ?>
                          </div>
                          <div id="divagossip_p_posts">
                            <?php print $forum->num_posts ?>
                          </div>
                          <div id="divagossip_p_last_post">
                              <?php print $str_last_post; ?>
                          </div>
                    </div>
                </div>
                </div>
                <?php endif; ?>
              <!--</div>-->
              <!--</div>-->
              <?php endforeach; ?>
            <!-- end item -->
          </div>
    </div>
</div>
<?php  //print "end tpl: ".date("Y/M/D H:i:s", time())."<br>"; ?>
<!-- END FEATURE RECIPES -->
