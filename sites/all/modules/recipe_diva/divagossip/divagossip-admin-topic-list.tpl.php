<?php
// $Id: divagossip-admin-topic-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divagossip-admin-topic-list.tpl.php
 * Theme implementation to display a list of forum topics.
 *
 * Available variables:
 * - $header: The table header. This is pre-generated with click-sorting
 *   information. If you need to change this, @see template_preprocess_forum_topic_list().
 * - $pager: The pager to display beneath the table.
 * - $topics: An array of topics to be displayed.
 * - $topic_id: Numeric id for the current forum topic.
 *
 * Each $topic in $topics contains:
 * - $topic->icon: The icon to display.
 * - $topic->moved: A flag to indicate whether the topic has been moved to
 *   another forum.
 * - $topic->title: The title of the topic. Safe to output.
 * - $topic->message: If the topic has been moved, this contains an
 *   explanation and a link.
 * - $topic->num_comments: The number of replies on this topic.
 * - $topic->new_replies: A flag to indicate whether there are unread comments.
 * - $topic->new_url: If there are unread replies, this is a link to them.
 * - $topic->new_text: Text containing the translated, properly pluralized count.
 * - $topic->created: An outputtable string represented when the topic was posted.
 * - $topic->last_reply: An outputtable string representing when the topic was
 *   last replied to.
 * - $topic->timestamp: The raw timestamp this topic was posted.
 *
 * @see template_preprocess_divagossip_admin_topic_list()
 * @see theme_divagossip_admin_topic_list()
 */
$add_destination = 'destination='.$_GET['q'];
$edit_destination = drupal_get_destination();
//$arr_status = recipe_db::get_list_recipe_content_status();
drupal_set_title("Forum Topics");
$search_url = url("adminrecipe/divagossip/topics");
?>
<div id="admin_border_content">
    <form name="frmTopicList" method="post" id="topic-form" onSubmit="javascript:submitForm('frmTopicList', '<?php print $search_url; ?>', 'Search');">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Diva Gossip Topics Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" name="text" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['text']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="javascript:submitForm('frmTopicList', '<?php print $search_url; ?>', 'Search');"/>
            </div>
		</div>
		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmTopicList, 'chk_', this)" />
			</div>
			<?php print $header; ?>
			<!--
			<?php if (arg(2) == 'topics') { ?>
      		<div id="admin_topics_title" class="admin_title">Topics</div>
      		<div id="admin_topics_forum_title" class="admin_title">Forum</div>
      		<?php 
      		} else {
      		?>
      		<div id="admin_topics_title_ext" class="admin_title">Topics</div>
      		<?php 
      		} 
      		?>
      		<div id="admin_topics_created" class="admin_title">Created</div>
            <div id="admin_topics_replies" class="admin_title">Replies</div>-->
 	  	  	<!--<div id="admin_topics_lastreply" class="admin_title">Last Reply</div>-->
  	  	  	<div id="admin_divatips_op_link" class="admin_title">Operations</div>
      	</div>
      	<?php if (is_array($topics)): ?>
      	<?php
      	$index = 0;
      	foreach ($topics as $topic):
	      	if ($index % 2) {
	      		$div_style = 'admin_recipes_content_inner';
	      	} else {
	      		$div_style = 'admin_recipes_content_inner01';
	      	}
	      	$index = $index + 1;
			// Get information of created topic author
			$str_created =  recipe_utils::create_profile_link($topic, 'admin', false);
			// Get information of last reply author
			$last_reply = $topic->last_reply;
			//$str_last_reply =  recipe_utils::create_profile_link($last_reply);
      	?>
      	<div id="<?php print $div_style; ?>">
      		<div id="admin_recipes_col_del" class="admin_title"><input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $topic->nid; ?>" onclick="checkAllChange(document.frmTopicList, 'chk_', this, document.frmTopicList.chkAll);"/></div>
      		<?php if (arg(2) == 'topics') { ?>
        	<div id="admin_topics_title"><a href="<?php print url("adminrecipe/divagossip/$topic->nid/edit", array('query' => $edit_destination));?>"><?php print $topic->title; ?></a></div>
        	<div id="admin_topics_forum_title"><?php print $topic->forum; ?></div>
        	<?php 
      		} else {
      		?>
      		<div id="admin_topics_title_ext"><a href="<?php print url("adminrecipe/divagossip/$topic->nid/edit", array('query' => $edit_destination));?>"><?php print $topic->title; ?></a></div>
      		<?php
      		}
      		?>
      		<div id="admin_topics_created"><?php print $str_created; ?></div>
        	<div id="admin_topics_replies"><?php print $topic->num_comments; ?></div>
        	<!--<div id="admin_topics_lastreply"><?php print $str_last_reply; ?></div>-->
        	<div id="admin_divatips_op_link">
        		<a href="javascript:deleteAdminItem('frmTopicList', '', '<?php print $topic->nid;?>')">Delete</a>
			</div>
      	</div>
      	<?php endforeach; ?>
      	<?php endif; ?>
      	<input type="hidden" name="page" value="<?php print $_POST['page']; ?>">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmTopicList', 'chk_', '')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmTopicList', '<?php print url("adminrecipe/divagossip/add/topic/$tid", array('query' => $add_destination)); ?>');" />
		</div>
    	<?php print $pager ?>
	</div>
	</form>
</div>