<?php
// $Id: divagossip-topic-list.tpl.php,v 1.0 2010/05/21 12:00:00 dries Exp $

/**
 * @file divagossip-topic-list.tpl.php
 * Theme implementation to display a list of forum topics.
 *
 * Available variables:
 * - $header: The table header. This is pre-generated with click-sorting
 *   information. If you need to change this, @see template_preprocess_forum_topic_list().
 * - $pager: The pager to display beneath the table.
 * - $topics: An array of topics to be displayed.
 * - $topic_id: Numeric id for the current forum topic.
 *
 * Each $topic in $topics contains:
 * - $topic->icon: The icon to display.
 * - $topic->moved: A flag to indicate whether the topic has been moved to
 *   another forum.
 * - $topic->title: The title of the topic. Safe to output.
 * - $topic->message: If the topic has been moved, this contains an
 *   explanation and a link.
 * - $topic->num_comments: The number of replies on this topic.
 * - $topic->new_replies: A flag to indicate whether there are unread comments.
 * - $topic->new_url: If there are unread replies, this is a link to them.
 * - $topic->new_text: Text containing the translated, properly pluralized count.
 * - $topic->created: An outputtable string represented when the topic was posted.
 * - $topic->last_reply: An outputtable string representing when the topic was
 *   last replied to.
 * - $topic->timestamp: The raw timestamp this topic was posted.
 *
 * @see template_preprocess_divagossip_topic_list()
 * @see theme_divagossip_topic_list()
 */
// Get term object matching a term ID.
$term = taxonomy_get_term($tid);
$urlCur = "divagossip/".arg(1);
$urlBack = "divagossip/".strtolower(recipe_utils::removeWhiteSpace($term->name))."-".$term->tid;
if($urlCur != $urlBack) {
    //drupal_goto($urlBack, NULL, NULL, 301);
}
// Get term image
$term_image = get_term_image_url($tid, true);
// Get top bar image
$header_bar = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_LIST_PAGE, 1);
// Get top bar background image
$header_bar_bg = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_LIST_PAGE_BG, 1);

// set breadcrumb
$site_url = C_SITE_URL.C_BASE_PATH;
$breadcrumb[] = l(t('TheRecipeDiva'), $site_url);
$breadcrumb[] = l(t('Diva Gossip'), 'divagossip');
$breadcrumb[] = '<span>' . t($term->name) . '</span>';
drupal_set_breadcrumb($breadcrumb);
?>
<!-- START FEATURE RECIPES -->
<div id="divagossip_p">
      <div id="divagossip_p_top" style="background: url(<?php print $header_bar ? C_BASE_PATH.$header_bar : C_IMAGE_PATH.'divagossip/announcement_top.gif'?>) no-repeat top left;">
        <div id="divagossip_p_top_contain" style="background: url(<?php print $header_bar_bg ? C_BASE_PATH.$header_bar_bg : C_IMAGE_PATH.'divagossip/announcement_top_bg.gif'?>) repeat-y;">
            <div id="divagossip_p_top_title">
                <div id="divagossip_p_top_icon"><?php print $term_image; ?></div>
                <h1 id="divagossip_p_top_forum_name"><?php print $term->name; ?></h1>
            </div>
        </div>
      </div>
    <div id="announcements_p_b">
        <div id="divagossip_p_content">
            <div id="divagossip_p_content_inner">
                <div id="TipArchive_Search_Form">
                    <div id="announcements_newtopic">
                        <?php $destination = drupal_get_destination(); ?>
                        <a href="<?php print url("divagossip/add/topic/$tid", array('query' => $destination)); ?>">
                          <img alt="New topic" src="<?php print C_IMAGE_PATH ?>button/newtopic.gif"/>
                          </a>
                    </div>
                    <div id="searchfor_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                    <?php print $search_form; ?>
                </div>
            </div>
              <div id="divagossip_p_content_inner">
                <div id="announcements_p_content_inner_title">
                    <!--<div id="announcements_p_title_contain">-->
                         <?php print $header ?>
                         <!--</div>-->
                </div>

               <?php
            $i = 0;
            $topic_count = count($topics);
            ?>
            <?php foreach ($topics as $topic): ?>
            <?php
                $div_style = "";
                if ($index == ($topic_count - 1)) {
                    $div_style = 'style="background: none;"';
                }
                $index = $index + 1;
                // Get information of created topic author
                $str_created =  recipe_utils::create_profile_link($topic);
                // Get information of last reply author
                $last_reply = $topic->last_reply;
                $str_last_reply =  recipe_utils::create_profile_link($last_reply);
            ?>
                <div id="announcements_p_content_inner_contain">
                    <div id="announcements_p_content_contain" <?php print $div_style ?>>
                        <div id="announcements_p_topic">
                        <a href="<?php print url("divagossip/topic/".recipe_utils::removeWhiteSpace($topic->title).'-'.$topic->nid);?>">
                        <span class="announcements_p_topic"><?php print $topic->title; ?></span>
                        </a>
                        </div>
                        <div id="announcements_p_replies">
                        <?php print $topic->num_comments; ?>
                        <!--<?php if ($topic->new_replies): ?>
                            <br />
                              <a href="<?php print $topic->new_url; ?>"><?php print $topic->new_text; ?></a>
                        <?php endif; ?>-->
                        </div>
                        <div id="announcements_p_created">
                        <?php print $str_created; ?>
                        </div>
                        <div id="announcements_p_lastreply"><?php print $str_last_reply; ?></div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php print $pager; ?>
              </div>
          </div>
    </div>
</div>
<!-- END FEATURE RECIPES -->
