<?php
// $Id: divagossip-admin-topic-add.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divagossip-admin-topic-add.tpl.php
 * Theme implementation to display a topic form
 *
 * Available variables:
 * - $node: forum information
 * - $topic_form topic form
 *
 * @see template_preprocess_divatips_admin_topic_add()
 * @see theme_divatips_admin_topic_add()
 */
global $user;
$page_title  = "Edit Topic";
if (arg(2) == 'add') {
	$page_title = "Add New Topic";
}
drupal_set_title($page_title);
?>
<div id="admin_border_content">
	<div class="registration_p_title"><?php print $page_title; ?></div>
	<div id="divatips_add_content_space">&nbsp;
	</div>
	<!--
    <div id="divatips_add_content_inner" style = "padding-bottom:5px;">
		<span class="recipes_upload_lbl">Your name: </span><?php print recipe_utils::create_author_link($user->name, $user->name, 'new_divatips_author'); ?>
	</div>-->
    <?php print $topic_form; ?>
</div>
