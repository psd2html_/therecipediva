<?php
// $Id: divagossip-admin-forum-add.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divagossip-admin-forum-add.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $node: forum information
 * - $forum_form forum form
 *
 * @see template_preprocess_divatips_admin_forum_add()
 * @see theme_divatips_admin_forum_add()
 */
global $user;
$page_title  = "Add Forum";
if (arg(2) == 'edit') {
	$page_title = "Edit Forum";
}
drupal_set_title($page_title);

//if (!empty($_POST)) {
//	$op = $_POST['op'];
//	if ($op == 'Save') {
//		$arr_messages = array();
//		$forum_name = $_POST['name'];
//		$weight_num = $_POST['weight'];
//		// Forum name is required
//		if ($forum_name == '') {
//			$arr_messages[] = t(ERR_MSG_REQUIRED, array('@field_name' => 'Forum name'));
//		}
//		if ($weight_num != '') {
//			if (!is_numeric($weight_num) == false) {
//				$arr_messages[] = t(ERR_MSG_ISNUMERIC, array('@field_name' => 'Weight'));
//			} else if ($weight_num < MIN_FORUM_WEIGHT || $weight_num > MAX_FORUM_WEIGHT) {
//				$arr_messages[] = t(ERR_MSG_NUMERIC_RANGE, array('@field_name' => 'Weight', '@range1' => MIN_FORUM_WEIGHT, '@range2' => MAX_FORUM_WEIGHT));
//			}
//		} else {
//			$arr_messages[] = t(ERR_MSG_REQUIRED, array('@field_name' => 'Weight'));
//		}
//		
//		$err_mess = recipe_utils::create_error_message($arr_messages);
//	}
//}
?>

<div id="admin_border_content">
	<div class="registration_p_title"><?php print $page_title; ?></div>
	<div id="divatips_add_content_space">&nbsp;
	</div>
	<!--
    <div id="divatips_add_content_inner" style = "padding-bottom:5px;">
		<span class="recipes_upload_lbl">Your name: </span><?php print recipe_utils::create_author_link($user->name, $user->name, 'new_divatips_author'); ?>
	</div>-->
	<div id="divatips_add_content_inner">
    <?php print $forum_form; ?>
    </div>
</div>

