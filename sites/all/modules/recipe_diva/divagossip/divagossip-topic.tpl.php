<?php
// $Id: divagossip-topic.tpl.php,v 1.0 2010/05/21 08:39:35 goba Exp $

/**
 * @file divagossip-topic.tpl.php
 * Default theme implementation to display a topic which containing comments
 *
 * Variables available:
 * - $node
 * - $comment
 * - $paging
 *
 * @see template_preprocess_divagossip_topic()
 * @see theme_divagossip_topic()
 */
$urlCur = "divagossip/topic/".arg(2);
$urlBack = "divagossip/topic/".strtolower(recipe_utils::removeWhiteSpace($node->title))."-".$node->nid;
if($urlCur != $urlBack) {
    //drupal_goto($urlBack, NULL, NULL, 301);
}
/*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
$topic_profile =  recipe_utils::create_author_info($node->uid, $node->name, $node->created, STANDARD_DATE_FORMAT);
// Get term object matching a term ID.
$term = $node->taxonomy[$node->forum_tid];
$term_image = get_term_image_url($node->forum_tid, true);
// Get top bar image
$header_bar = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_TOPIC_PAGE, 1);
// Get top bar background image
$header_bar_bg = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_TOPIC_PAGE_BG, 1);

// set breadcrumb
$site_url = C_SITE_URL.C_BASE_PATH;
$breadcrumb[] = l(t('TheRecipeDiva'), $site_url);
$breadcrumb[] = l(t('Diva Gossip'), 'divagossip');
$breadcrumb[] = l(t($term->name), 'divagossip/'.recipe_utils::removeWhiteSpace($term->name).'-'.$term->tid);
$breadcrumb[] = '<span>' . t($node->title) . '</span>';
drupal_set_breadcrumb($breadcrumb);
?>
<script type="text/javascript">
    $(document).ready(function() {
            ResizeImage('topic_thread_p_content_comment', 400);
            ResizeImage('topic_body', 520);
        });
</script>
<!-- START FEATURE RECIPES -->
<!-- create delete confirmation form -->
<?php print recipe_utils::create_delete_confirm_form($node->nid, $node->title); ?>
<div id="divagossip_p">
    <div id="divagossip_p_top" style="background: url(<?php print $header_bar ? C_BASE_PATH.$header_bar : C_IMAGE_PATH.'divagossip/announcement_top.gif'?>) no-repeat top left;">
        <div id="divagossip_p_top_contain" style="background: url(<?php print $header_bar_bg ? C_BASE_PATH.$header_bar_bg : C_IMAGE_PATH.'divagossip/announcement_top_bg.gif'?>) repeat-y;">
            <div id="divagossip_p_top_title">
                <div id="divagossip_p_top_icon"><?php print $term_image; ?></div>
                <div id="divagossip_p_top_forum_name"><a href="<?php print C_BASE_PATH."divagossip/".recipe_utils::removeWhiteSpace($term->name).'-'.$term->tid?>"><?php print $term->name; ?></a></div>
            </div>
        </div>
    </div>
    <div id="announcements_p_b">
        <div id="divagossip_p_content">
            <div id="divagossip_p_content_inner">
                <div id="TipArchive_Search_Form">

                    <div id="announcements_newtopic">
                        <a href="<?php print url("divagossip/reply/$node->nid");?>">
                        <img alt="Post Reply" src="<?php print C_IMAGE_PATH ?>button/post_replay_btn.gif" />
                        </a>
                    </div>

                    <div id="searchfor_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                        <?php print $search_form; ?>
                </div>
            </div>


              <div id="divagossip_p_content_inner">
                <div id="topic_thread_p_title_contain_bg">
                       <div id="topic_thread_p_content_inner_title">
                            <div id="post_replay_title">
                                <?php print $node->title;?>
                            </div>
                    </div>
                </div>
                <div id="topic_thread_p_content_inner_contain">
                    <div id="topic_thread_p_content_contain">
                        <div class="topic_info"><?php print $topic_profile; ?></div>
                        <div id="topic_body" class="topic_info">
                            <div><?php print $node->body; ?></div>
                            <?php if (check_comment_permission($node) != 0) :?>
                               <div id="divagossip_update_topic_link">
                                   <form name="frmTopic" method="post">
                                    <div id="divagossip_update_topic_link_detail">
                                        <a href="javascript:deletePageItem('frmTopic', '', '<?php print $node->nid;?>', '<?php print CONF_MSG_TOPIC_DEL; ?>')"><img alt="Delete" src="<?php print C_IMAGE_PATH; ?>button/delete_icon_btn.gif"/></a>
                                    </div>
                                    <div id="divagossip_update_topic_link_detail">
                                        <?php
                                        $destination = drupal_get_destination();
                                        $image = '<img alt="Edit" src="'.C_IMAGE_PATH.'button/edit_icon_btn.gif"/>';
                                        ?>
                                        <a href="<?php print url("divagossip/$node->nid/edit"); ?>"><img alt="Edit" src="<?php print C_IMAGE_PATH; ?>button/edit_icon_btn.gif"/></a>
                                    </div>
                                    <input type="hidden" name="op" value="">
                                    <input type="hidden" name="delId" value="">
                                    <input type="hidden" name="termId" value="<?php print $term->tid; ?>">
                                    <input type="hidden" name="termName" value="<?php print $term->name; ?>">
                                </form>
                            </div>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div id="topic_thread_p_content_comment">
                    <?php
                    $comment_content = "";
                    // Get comment list
                    while ($comment = db_fetch_object($comments)) {
                        $comment_profile =  recipe_utils::create_profile_link($comment);
                        $links = create_comment_update_links($comment, 'divagossip/reply/edit/');
                    ?>
                        <div id="topic_thread_p_content_contain">
                            <div id="topic">
                                <div id="topic_b">
                                    <div id="topic_content">
                                        <div id="topic_content_inner">
                                            <div id="topic_created_by"><?php print $comment_profile; ?></div>
                                            <div id="topic_content_contain">
                                                <div><?php print $comment->comment; ?></div>
                                                <?php print $links; ?>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    <div id="announcements_newtopic" style="padding-top: 10px;">
                        <a href="<?php print url("divagossip/reply/$node->nid");?>">
                        <img alt="Post Reply" src="<?php print C_IMAGE_PATH ?>button/post_replay_btn.gif" />
                        </a>
                    </div>
                    <?php print $paging; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END FEATURE RECIPES -->
