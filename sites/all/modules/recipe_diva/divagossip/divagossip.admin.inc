<?php
// $Id: divagossip.module,v 1.0 2010/06/09 01:31:26 drumm Exp $

/**
 * Call back function
 * 
 * @param $nid node id
 * 
 */
function divagossip_list($tid = 0){
	$op = $_POST['op'];
	if ($op == 'Delete') {
		// Delete selected items
		if ($tid == 0) {
			divagossip_delete($_POST);
		} else {
			adminrecipe_delete($_POST);
		}
	} else if ($op == 'DeleteItem') {
		$term_id = $_POST['delId'];
		if ($tid == 0) {
			taxonomy_del_term($term_id);
		} else {
			node_delete($term_id);
		}
	}
	return divagossip_page($tid);
}

/**
 * Menu callback; prints a forum listing.
 */
function divagossip_admin_topic_list() {
  	$op = $_POST['op'];
	divagossip_delete_items();
	// Collect the posted search query
	$q = trim($_POST['text']);
	if ($q != '' && $op != 'Delete' && $op != 'DeleteItem') {
	   	$q = preg_replace('/\s+/',' ', $q);
	   	drupal_goto("adminrecipe/divagossip/topics/search", 'text='.urlencode($q).'&page=0');
	   	return;
	}
	
  	$topics = '';
  	$forum_per_page = ADMIN_TOPIC_THREAD_PER_PAGE;
  	$sortby = variable_get('forum_order', 1);
  	$tid = 0;
	$topics = forum_get_topics($tid, $sortby, $forum_per_page, '');
	return theme('divagossip_admin_topic_list', $tid, $topics, $sortby, $forum_per_page, NULL);
}

/**
 * Menu callback; prints a forum listing.
 */
function divagossip_admin_topic_search_list() {
  	divagossip_delete_items();
  	$topics = '';
  	$forum_per_page = ADMIN_TOPIC_THREAD_PER_PAGE;
  	$sortby = variable_get('forum_order', 1);
  	$str_search = '';
  	if ($_GET['text'] != '') {
  		$str_search = urldecode($_GET['text']);
  	}
  	$tid = 0;
	$topics = forum_get_topics($tid, $sortby, $forum_per_page, $str_search);
	return theme('divagossip_admin_topic_list', $tid, $topics, $sortby, $forum_per_page, NULL);
}

/**
 * Delete divagossip
 */
function divagossip_delete_items() {
	$op = $_POST['op'];
	if ($op == 'Delete') {
		// Delete selected items
		adminrecipe_delete($_POST);
	} else if ($op == 'DeleteItem') {
		$node_id = $_POST['delId'];
		node_delete($node_id);
	}
}

/**
 * Delete selected forums
 * 
 * @param $post_param post parameters
 */
function divagossip_delete($post_param) {
	if (empty($post_param) == false) {
		foreach ($post_param AS $key => $value) {
			$pos = strpos($key, "chk_");
			if ($pos !== false) {
				taxonomy_del_term($value);
			}
		}
	}
}
?>
