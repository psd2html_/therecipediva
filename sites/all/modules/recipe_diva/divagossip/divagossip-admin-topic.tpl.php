<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-admin-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_admin_list()
 * @see theme_divatips_admin_list()
 */
?>
<div id="admin_border_content">
    <form name="frmDivatipList" method="post" id="divatips-form">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Topic Replies Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" name="txtSearch" class="Archive_Search_inp" value="<?php print $_POST['txtSearch']; ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="this.form.submit();"/>
            </div>
		</div>
		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmDivatipList, 'chk_', this)" />
			</div>
      		<div id="admin_divatips_title" class="admin_title">Title</div>
            <div id="admin_divatips_created" class="admin_title">Created</div>
  	  	  	<div id="admin_divatips_status" class="admin_title">Status</div>
  	  	  	<div id="admin_divatips_content_status" class="admin_title">Content Status</div>
  	  	  	<div id="admin_divatips_op_link" class="admin_title">Operations</div>
      	</div>
      	<?php print $tip_list; ?>
      	<input type="hidden" name="hidPageBack" value="adminrecipe/divatips">
      	<input type="hidden" name="page" value="<?php print $_POST['page']; ?>">
      	<input type="hidden" name="op" value="">
      	<input type="hidden" name="delId" value="">
	</div>
    
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmDivatipList', 'chk_', '<?php print url("adminrecipe/divatips"); ?>')" />
		</div>
		<div id="admin_divatips_add">
    	  	<input type="button" name="btnAdd" value=""  class="admin_bt_add" onClick="submitForm('frmDivatipList', '<?php print url("adminrecipe/divatips/add"); ?>');" />
		</div>
		<!--
		<div id="admin_divatips_status_lbl">Update options:</div>
		<div id="admin_divatips_status_input">
    		<select name="cbbStatus">
    			<?php 
    			$sel_status = $_POST['cbbStatus'];
    			$selected = "";
    			foreach($arr_status as $key => $value)  :
					if ($key == $sel_status) {
						$selected = "selected";
					} else {
						$selected = "";
					}
    			?>
    			<option value="<?php print $key; ?>" <?php print $selected; ?>><?php print $value; ?></option>
    			<?php endforeach; ?>
    		</select>
  	  	</div>
    	<div class="admin_divatips_status_btn">
			<input type="button" name="btnChangeStatus" value="Update" onClick="changeAdminStatus('frmDivatipList', 'chk_', '')">
    	</div>
    	-->
    	<?php print $pager ?>
	</div>
	</form>
</div>