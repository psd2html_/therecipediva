<?php
// $Id: divagossip-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divagossip-admin-list.tpl.php
 * Default theme implementation to display a forum which may contain forum
 * containers as well as forum topics.
 *
 * Variables available:
 * - $forums: The forums to display (as processed by forum-list.tpl.php)
 * - $tid: term id
 *
 * @see template_preprocess_divagossip_admin_list()
 * @see theme_divagossip_admin_list()
 */
$main_action_url = url("adminrecipe/divagossip");
?>
<div id="admin_border_content">
    <form name="frmDivagossipList" method="post" id="divatips-form" action="<?php print url("adminrecipe/divagossip/presearch"); ?>">
    <div id="admin_recipe_content">
    	<div class="admin_p_title">Diva Gossip Management</div>
    	<div id="admin_divatips_search_form">
    		<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
			<div id="searchfor_input">
            	<input type="text" name="text" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['text']); ?>"/>
 			</div>
            <div class="archive_search_btn">
            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="javascript:submitForm('frmDivagossipList', '', 'Search');"/>
            </div>
		</div>
		
    	<div id="admin_recipes_content_title">
        	<div id="admin_recipes_col_del" class="admin_title">
			<input type="checkbox" name="chkAll" onclick="checkAll1(document.frmDivagossipList, 'chk_', this)" />
			</div>
			<div id="admin_divagossip_forum_space"><img src="<?php print C_IMAGE_PATH ?>space.gif" height="1" width="1" /></div>
      		<div id="admin_divagossip_title" class="admin_title">Forum</div>
            <div id="admin_divagossip_topics" class="admin_title">Topics</div>
  	  	  	<div id="admin_divagossip_posts" class="admin_title">Posts</div>
  	  	  	<div id="admin_divagossip_lastpost" class="admin_title">Topics Link</div>
  	  	  	<div id="admin_divatips_op_link" class="admin_title">Operations</div>
      	</div>

      	
      	<?php
      		$index = 0;
      		foreach ($forums as $child_id => $forum): 
      			if ($index % 2) {
      				$div_style = 'admin_recipes_content_inner';
      			} else {
      				$div_style = 'admin_recipes_content_inner01';
      			}
      			$index = $index + 1;
      	?>
      	<?php 
      		$last_post = $forum->last_post;
			$str_last_post = recipe_utils::create_profile_link($last_post, "admin");
		?>
      	<?php
      		if (!empty($forum->container)) :
				$forum_count = recipe_db::get_forum_count($forum->vid, $forum->tid);
				if ($forum_count == 0 && $op == 'Search') {
					continue;
				}
      	?>
      		<div id="<?php print $div_style; ?>">
	      		<div id="admin_recipes_col_del" class="admin_title">
					<input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $forum->tid; ?>" onclick="checkAllChange(document.frmDivagossipList, 'chk_', this, document.frmDivagossipList.chkAll);" />
				</div>
	      		<div id="admin_divagossip_container_title">
	      			<a href="<?php print url('adminrecipe/divagossip/edit/container/'.$forum->tid); ?>"><span class="admin_container_title"><?php print $forum->name; ?></span></a>
	      		</div>
	      		<div id="admin_divatips_op_link">
	      		<a href="javascript:deleteAdminItem('frmDivagossipList', '<?php print $main_action_url; ?>', '<?php print $forum->tid;?>')">Delete</a></a>
	      		</div>
      		</div>
      	<?php else : ?>
      	<div id="<?php print $div_style; ?>">
      		<div id="admin_recipes_col_del" class="admin_title">
      			<input type="checkbox" name="chk_<?php print $index; ?>" value="<?php print $forum->tid; ?>" onClick="checkAllChange(document.frmDivagossipList, 'chk_', this, document.frmDivagossipList.chkAll);"/>
      		</div>
      		<div id="admin_divagossip_forum_space"><img src="<?php print C_IMAGE_PATH ?>space.gif" height="1" width="1" /></div>
      		<?php $forum_url = url("adminrecipe/divagossip/edit/forum/$forum->tid", array('query' => drupal_get_destination())); ?>
	        <div id="admin_divagossip_title"><a href="<?php print $forum_url; ?>"><?php print $forum->name; ?></a></div>
	        <div id="admin_divagossip_topics"><?php print $forum->num_topics; ?></div>
	        <div id="admin_divagossip_posts"><?php print $forum->num_posts ?></div>
	        <div id="admin_divagossip_lastpost"><a href="<?php print url("adminrecipe/divagossip/$forum->tid");?>">Topics</a></div>
	        <div id="admin_divatips_op_link">
	        <a href="javascript:deleteAdminItem('frmDivagossipList', '<?php print $main_action_url; ?>', '<?php print $forum->tid;?>')">Delete</a>
	        </div>
      	</div>
      	<?php endif; ?>
      	<?php endforeach; ?>
	</div>
	<div id="admin_recipe_content">
		<div id="admin_divatips_delete">
    	  	<input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmDivagossipList', 'chk_', '<?php print $main_action_url; ?>')" />
		</div>
		<div id="admin_divagossip_add_container">
    	  	<input type="button" name="btnAddContainer" value=""  class="admin_bt_add_container" onClick="submitForm('frmDivagossipList', '<?php print url("adminrecipe/divagossip/add/container"); ?>');" />
		</div>
		<div id="admin_divagossip_add_forum">
    	  	<?php $vid = variable_get('forum_nav_vocabulary', ''); ?>
    	  	<input type="button" name="btnAddForum" value=""  class="admin_bt_add_forum" onClick="submitForm('frmDivagossipList', '<?php print url("adminrecipe/divagossip/$vid/add/forum"); ?>');" />
		</div>
	</div>
	<input type="hidden" name="op" value="">
	<input type="hidden" name="delId" value="">
	</form>
</div>