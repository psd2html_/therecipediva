<?php
// $Id: divatips-admin-list.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file divatips-admin-list.tpl.php
 * Theme implementation to display a list of diva tips.
 *
 * Available variables:
 * - $tip_list: The list of diva tips
 *
 * @see template_preprocess_divatips_admin_list()
 * @see theme_divatips_admin_list()
 */
global $user;
$page_title = "";

if (!empty($node)) {
	$page_title = 'Edit Container';
} else {
	$page_title = 'Add Container';
}

// Set page title
drupal_set_title($page_title);
?>

<div id="admin_border_content">
    <div class="registration_p_title"><?php print $page_title; ?></div>
	<div id="divatips_add_content_space">&nbsp;
	</div>
    <!--<div id="divatips_add_content_inner">
		<span class="recipes_upload_lbl">Your name: </span><?php print recipe_utils::create_author_link($user->name, $user->name, 'new_divatips_author'); ?>
	</div>-->
    <?php print $container_form; ?>
</div>