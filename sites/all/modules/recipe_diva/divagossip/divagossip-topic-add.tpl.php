<?php
// $Id: divagossip-add.tpl.php,v 1.0 2010/05/21 08:39:35 goba Exp $

/**
 * @file divagossip-topic-add.tpl.php
 * Default theme implementation to display a topic form
 *
 * Variables available:
 * - $node
 * - $topic_form
 *
 * @see template_preprocess_divagossip_topic_add()
 * @see theme_divagossip_topic_add()
 */
global $user;
drupal_add_js('sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js');
$topic_profile = recipe_utils::create_author_link($user->name, $user->name);
$err_message = create_divagossip_error();

if (arg(2) == 'edit') {
    $page_title = "Edit Topic";
    $header_bar = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_EDIT_TOPIC_PAGE, 1);
    $default_title_img = 'divagossip/edit_topic_top.gif';
} else {
    $page_title = "Add New Topic";
    $header_bar = recipe_db::get_admin_upload_image_url(DIVA_GOSSIP_NEW_TOPIC_PAGE, 1);
    $default_title_img = 'divagossip/add_new_topic_top.gif';
}
drupal_set_title($page_title);
?>
  <!-- START FEATURE RECIPES -->
    <div id="divagossip_p">
        <div id="add_new_topic_p_top" style="background: url(<?php print $header_bar ? C_BASE_PATH.$header_bar : C_IMAGE_PATH.$default_title_img; ?>) no-repeat top left;">
              <div></div>
           </div>
        <div id="newtopic_p_b">
            <div id="divagossip_p_content">
                <?php if (arg(1) == 'add') :?>
                <div id="newtopic_p_content_inner">
                     Post a new topic in the forum category selected below. Your topic will be posted from <?php print $topic_profile; ?>. If you would like to change your user name, please edit your profile.
                     </div>
                <?php endif; ?>
                <?php print $err_message; ?>
               <?php print $topic_form; ?>
          </div>
        </div>
    </div>
<!-- END FEATURE RECIPES -->
<script type="text/javascript">
    $(document).ready(function() {
        ResizeImage('divagossip_p', 520);
    });
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--



//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

  tinyMCE.init({
    //mode : "exact",
    mode : "textareas",
    theme : "advanced",
    relative_urls : false,
    document_base_url : "<?php print C_BASE_PATH;?>",
    language : "en",
    safari_warning : false,
    entity_encoding : "raw",
    verify_html : false,
    preformatted : false,
    convert_fonts_to_spans : true,
    remove_linebreaks : true,
    apply_source_formatting : true,
    theme_advanced_resize_horizontal : false,
    theme_advanced_resizing_use_cookie : false,
    plugins : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_path_location : "none",
    theme_advanced_resizing : true,
    theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,numlist,bullist,indent,outdent,separator,undo,redo",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : ""
  });

//--><!]]>
</script>
