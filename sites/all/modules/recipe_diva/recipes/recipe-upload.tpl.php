<?php
// $Id: recipe-upload.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipe-upload.tpl.php
 * Theme implementation to display the node information and post information for node.
 *
 * Available variables:
 * - $node: node information
 *
 * @see theme_recipe_upload()
 */
 drupal_add_js('sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js');
 //<script type="text/javascript" src="/source/sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js?D"></script>

?>
<?php
    $terms = recipe_db::load_master_category_recipe_upload();
    $arrTimeUnit = recipe_db::get_list_duration_time();

    $terms_value = "";
    $commas = "";
    if (sizeof($node->taxonomy)>0){
        foreach ($node->taxonomy as $key=>$value){
            $terms_value .= $commas . $key;
            $commas = ",";
        }
    }

    // print '<pre>'.check_plain(print_r($node,1)).'</pre>';
    //<!--<?php if (recipe_utils::getUserRole() == C_ADMIN_USER || empty($node->nid) || (!empty($node->nid) && $node->field_content_status[0]['value']) != "1"){-->
    if (arg(0) == "adminrecipe" && arg(3)== "edit"){
        //	load grocerie items
        $arrGroceries = load_grocery_by_recipe($node->nid);
        $strIngredients = $node->field_ingredients[0][value];
        $strIngredientsNew = '';
        $arrIngredients = explode(BREAK_LINE , $strIngredients);
        $groceries = array();

        if (is_array($arrIngredients))
        {
            foreach($arrIngredients as $strIngItem)
            {
                $arrGroceryItem = array();
                $objIngredient = new stdClass();

                foreach ($arrGroceries as $objGrocery)
                {
                    if (trim($objGrocery->title)==trim(strip_tags($strIngItem)))
                    {
                        $objIngredient->value = $strIngItem;
                        $objIngredient->aisle = $objGrocery->aisle_id;
                    }
                }
                if(!$objIngredient->value){
                    $objIngredient->value = $strIngItem;
                    $objIngredient->aisle = 0;
                }
                $groceries[] = $objIngredient;
            }
        }

        $aisle = recipe_db::load_master_db('recipe_aisle','object',true);
        $aisleOption = '<select name="%aisleid%" id="%aisleid%"><option value="0"></option>';
        foreach ($aisle as $key => $value) {
            $aisleOption.='<option value="'.$key.'">'.$value.'</option>';
        }
        $aisleOption .= '</select>';
    }
?>

<script type="text/javascript">
<!--//--><![CDATA[//><!--

  tinyMCE.init({
    mode : "exact",
    theme : "advanced",
    relative_urls : false,
    document_base_url : "<?php print C_BASE_PATH;?>",
    language : "en",
    safari_warning : false,
    entity_encoding : "raw",
    verify_html : false,
    preformatted : false,
    convert_fonts_to_spans : true,
    remove_linebreaks : true,
    apply_source_formatting : true,
    theme_advanced_resize_horizontal : false,
    theme_advanced_resizing_use_cookie : false,
    plugins : "",
    force_br_newlines : true,
    force_p_newlines : false,
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_path_location : "none",
    theme_advanced_resizing : true,
    theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,numlist,bullist,indent,outdent,separator,undo,redo,separator,image,removeformat,separator,link,unlink,code",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    elements : "preparation,ingredients"
  });

//--><!]]>
</script>

<!-- START COOK OF WEEK -->
<script language="javascript" type="text/javascript">
        <?php if (arg(0) == "adminrecipe" && arg(3)== "edit"){?>
        var nid = <?php print sizeof($groceries)?>;

        function reorderGroceryID(){
            var list_li = $("#sortable li");
            for(i = 0 ;i<list_li.length;i++){
                $("#"+list_li[i].id + " input[name^=txtGroceriesValue_]").attr("name","txtGroceriesValue_"+i);
                $("#"+list_li[i].id + " select[name^=ddlAisle_]").attr("name","ddlAisle_"+i).attr("id","ddlAisle_"+i);
            }
        }

        function clearGroceryFields(){
            $("#txtGroceriesValue").val("");
            $("#ddlAisle").val(0);
        }
        function deleteGrocery(nid) {
            $("#dialogConfirm").attr("title", "<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => "item"))?>");
            $("#ui-dialog-title-dialogConfirm").text("<?php print t(CONF_MSG_RECIPE_TIP_DEL, array('@filename' => "item"))?>");
            $("#dialogConfirm").dialog(
                { modal: true },
                { resizable: false },
                { minHeight: 0 },
                { buttons:
                    {
                        "Yes": function() {
                            $("#groceries_content_" + nid).remove();
                            $(this).dialog("close");
                        },
                        "No": function() {
                            $(this).dialog("close");
                        }
                    }
                }
            );
        }

        function addGrocery() {
            nid++;
            $("#hidgid").val(nid);
            var unit_aisle = '<?print str_replace('"','\"', $aisleOption)?>';
            unit_aisle = unit_aisle.replace(/%aisleid%/g,"ddlAisle_" + nid);
            unit_aisle = unit_aisle.replace("<option value=\"" + $("#ddlAisle").val() + "\">","<option selected=\"true\" value=\"" + $("#ddlAisle").val() + "\">");
            var divGro = "<li id=\"groceries_content_"+nid+"\"><div class=\"groceries_content\">";
                divGro += "<div id=\"groceries_col_container\">";
                divGro += "	<div id=\"recipe_groceries_col01\">";
                divGro += "	 <input type=\"text\" value=\"" + $("#txtGroceriesValue").val().replace(/\"/g,'&quot;') + "\" name=\"txtGroceriesValue_" + nid + "\">";
                divGro += "	</div>";
                divGro += "	<div id=\"recipe_groceries_col03\">";
                divGro +=   unit_aisle;
                divGro += " </div>";
                divGro += " <div id=\"groceries_col04\">";
                divGro += "  <img alt=\"Delete Grocery\" style=\"cursor:pointer\" onclick=\"deleteGrocery('" + nid + "')\" src=\"<?echo C_IMAGE_PATH?>button/groceries_del_btn.gif\"></div>";
                divGro += "	</div>";
                divGro += "</div></li>";
            $("#sortable").append(divGro);
            clearGroceryFields();
        }
        <?php }?>
        function changeRecipeType(obj){
            if($(obj).val() == 1){
                $("#div_recipes_upload").hide();
            }else{
                $("#div_recipes_upload").show();
            }
        }
        function validateRecipe(){
            document.forms['recipe_upload'].action =  document.getElementById("hdf_action").value;
            document.forms['recipe_upload'].target="";

            var errorMsg = "";
            var hasError = false;
            var errorMessage = "";
            var objTitle = document.getElementById("txtTitle");
            var objIngredient = document.getElementById("ingredients");
            var objPreparation = document.getElementById("preparation");
            var objPreTime = document.getElementById("txtPreTime");
            var objCookTime = document.getElementById("txtCookingTime");
            var objInactiveTime = document.getElementById("txtInactiveTime");
            var objYieldQuality = document.getElementById("txtYieldQuantity");
            var objYieldUnit = document.getElementById("txtYieldUnit");
            var objPrice = document.getElementById("txtPrice");

            var objTerms= document.getElementById("hdf_terms");
            var topPosition = $("div[class=registration_p_title]").offset().top;

            $("#message_error").html("");

            if (trim(objTitle.value) == ""){
                errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Title'));?>";
                errorMessage = "<li>" + errorMsg + "</li>";
                objTitle.focus();
                $('html,body').animate({scrollTop: topPosition}, 'fast');
                hasError = true;
            }
            //var description=tinyMCE.get('description').getContent();
            //description = strip_tags(description);
            var objDescription = document.getElementById("description");
            if (objDescription.value.length > 600){
                errorMsg = "<?php print RECIPE_UPLOAD_DESCRIPTION_MAXLENGTH_ERROR?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    objDescription.focus();
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }

            <?php if (arg(0) == "adminrecipe" && arg(3)== "edit"){?>
            var ingredient_value = "";
            $("input[name^=txtGroceriesValue_]").each(function(){
                if($.trim($(this).val()) != ""){
                    ingredient_value += $(this).val() + "\n";
                }
            });
            if (ingredient_value == ""){
                errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Ingredients'));?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    $("#txtGroceriesValue").focus();
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }else{
                if(ingredient_value.length > 1800){
                    errorMsg = "<?php print RECIPE_UPLOAD_INGREDIENTS_MAXLENGTH_ERROR?>";
                    errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                    if (!hasError){
                        $("#txtGroceriesValue").focus();
                        $('html,body').animate({scrollTop: topPosition}, 'fast');
                    }
                    hasError = true;
                }
            }
            <?php }else{?>
            var ingredient=tinyMCE.get('ingredients').getContent();
            ingredient = strip_tags(ingredient);
            if (trim(ingredient) == ""){
                errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Ingredients'));?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    tinyMCE.execCommand('mceFocus',false,'ingredients');
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }else{
                var ingre = trim(ingredient);
                if(ingre.length > 1800){
                    errorMsg = "<?php print RECIPE_UPLOAD_INGREDIENTS_MAXLENGTH_ERROR?>";
                    errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                    if (!hasError){
                        tinyMCE.execCommand('mceFocus',false,'ingredients');
                        $('html,body').animate({scrollTop: topPosition}, 'fast');
                    }
                    hasError = true;
                }
            }
            <?php }?>


            var preparation=tinyMCE.get('preparation').getContent();
            preparation = strip_tags(preparation);
            if (preparation == ""){
                errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Directions'));?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    //bookmark_us('<?php print $_GET['q'];?>', 'preparation');
                    tinyMCE.execCommand('mceFocus',false,'preparation');
                     $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }else if(preparation.length > 5000){
                errorMsg = "<?php print RECIPE_UPLOAD_DIRECTIONS_MAXLENGTH_ERROR?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    //tinyMCE.execInstanceCommand("preparation", "mceFocus");
                    //bookmark_us('<?php print $_GET['q'];?>', 'preparation');
                    tinyMCE.execCommand('mceFocus',false,'preparation');
                     $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }
            if (objYieldQuality.value != ""){
                if(!IsNumeric(objYieldQuality.value, "0123456789.-/ ")){
                    errorMsg = "<?php print RECIPE_UPLOAD_YIELD_INVALID_ERROR?>";
                    //errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Directions'));?>";
                    errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                    if (!hasError){
                        objYieldQuality.focus();
                        $('html,body').animate({scrollTop: topPosition}, 'fast');
                    }
                    hasError = true;
                }
            }
            if (objPreTime.value == ""){
                errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Preparation Time'));?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    objPreTime.focus();
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }else{
                if(!IsNumeric(objPreTime.value, "0123456789./") || !checkNumberTime(objPreTime.value)){
                    errorMsg = "<?php print t(ERR_MSG_ISNUMERIC, array('@field_name' => 'Preparation Time'));?>";
                    errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                    if (!hasError){
                        objPreTime.focus();
                        $('html,body').animate({scrollTop: topPosition}, 'fast');
                    }
                    hasError = true;
                }
            }
            if (objCookTime.value == ""){
                errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Cooking Time'));?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    objCookTime.focus();
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }else{
                if(!IsNumeric(objCookTime.value, "0123456789./") || !checkNumberTime(objCookTime.value)){
                    errorMsg = "<?php print t(ERR_MSG_ISNUMERIC, array('@field_name' => 'Cooking Time'));?>";
                    errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                    if (!hasError){
                        objCookTime.focus();
                        $('html,body').animate({scrollTop: topPosition}, 'fast');
                    }
                    hasError = true;
                }
            }

            if(!IsNumeric(objInactiveTime.value, "0123456789./") || !checkNumberTime(objInactiveTime.value)){
                errorMsg = "<?php print t(ERR_MSG_ISNUMERIC, array('@field_name' => 'Inactive Time'));?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    objInactiveTime.focus();
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }
                hasError = true;
            }

            var flag = false;
            <?php if ((arg(0) != "adminrecipe" && recipe_utils::getUserRole() == C_ADMIN_USER) || recipe_utils::getUserRole() == C_PREMIUM_USER){?>

                        if(document.forms['recipe_upload'].recipe_type.checked == true){
                            flag = true;
                        }
            <?php }else if((arg(0) == "adminrecipe" && (empty($node->nid) || get_user_role_name($node->uid) == C_ADMIN_USER || get_user_role_name($node->uid) == C_PREMIUM_USER))){?>
                        if(document.forms['recipe_upload'].recipe_type[0].checked == true){
                            flag = true;
                        }
            <?php }?>
            if(flag){
                var user_input = '';
                for (i = 0; i < document.forms['recipe_upload'].recipe_price.length; i++) {
                    if (document.forms['recipe_upload'].recipe_price[i].checked) {
                        user_input = document.forms['recipe_upload'].recipe_price[i].value;
                    }
                }

                if(user_input == ''){
                    errorMsg = "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Recipe Price'));?>";
                    errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                    if (!hasError){
                        $('html,body').animate({scrollTop: topPosition}, 'fast');
                    }
                    hasError = true;
                }
            }

            /*if(trim(objTerms.value) == ""){
                errorMsg = "<?php print RECIPE_UPLOAD_CATEGORY_REQUIRED_ERROR?>";
                errorMessage = errorMessage + "<li>" + errorMsg + "</li>";
                if (!hasError){
                    var objCat= document.getElementById("border_categorize");
                    objCat.focus();
                    $('html,body').animate({scrollTop: topPosition}, 'fast');
                }

                hasError = true;
            }*/
            var objError = document.getElementById("div_error");
            if(hasError){
                $("#message_error").html(errorMessage);
                objError.style.display = "";
                return false;
            }else{
                objError.style.display = "none";
            }
            if(!hasError){

                showInfoMessage("Recipe is being saved. Please wait...");

                if($.trim($("#recipe_upload_image").val()) != ""){
                    $("#message_error").html("");
                    var options = {
                                    success:       function(responseText, statusText, xhr, $form)  {
                                                        if(responseText != "success"){
                                                            $("#message_error").html("<li>" + responseText + "</li>");
                                                            objError.style.display = "";
                                                            $("#recipe_upload_image").focus();
                                                            $('html,body').animate({scrollTop: topPosition}, 'fast');
                                                            return false;
                                                        }
                                                        else{
                                                            <?php if (arg(0) == "adminrecipe" && arg(3)== "edit"){?>
                                                                reorderGroceryID();
                                                            <?php }?>
                                                            $('#recipe_upload').submit();
                                                        }
                                                    } ,
                                    url:          '<?php print C_BASE_PATH."recipes/upload/check_image_upload"?>'
                                    };
                    $('#recipe_upload').ajaxSubmit(options);
                }
                else{
                    <?php if (arg(0) == "adminrecipe" && arg(3)== "edit"){?>
                        reorderGroceryID();
                    <?php }?>
                    $('#recipe_upload').submit();
                }
            }

        }

        /**Check inputting time
         *
         * return String
         **/
        function checkNumberTime(strNumber){
            var bChecked = false;
            var arrNumber = strNumber.split(" ");
            for (x in arrNumber){
                var number = trim(arrNumber[x]);
                if(number != "" && number.indexOf("/") >= 0){
                    var arrNew = number.split("/");
                    if(arrNew.length < 2){
                        return false;
                    }
                    if(arrNew[1]<=0 || arrNew[0] <= 0) return false;
                }
            }
            return true;
        }

        /*function validateIngredient(){
            var errorMessage = "";
            var x;
            var objIngredient = document.getElementById("ingredients");

            var value = trim(objIngredient.value);
            var arrIngredients = value.split(/[\n\r]+/);

            for (x in arrIngredients){
                var strIng = arrIngredients[x];
                if (strIng != ""){
                    var bError = false;
                    var arrIng = strIng.split(" ");
                    if (arrIng.length > 0){
                        var quality = parseFloat(arrIng[0]);
                        if (isNaN(quality)){
                            return false;
                        }
                    }
                }
            }
            return true;
        }*/

        /**
         * Remove tag html
         *
         * return String
         **/
        function strip_tags(str) {
             //str = str.replace(/<.[^<>]*?>/g, "");
               //str = str.replace(/&lt;\/?[^&gt;]+&gt;/gi, "");

             document.getElementById("hdf_content").innerHTML = str;
            if(document.all){
                str = document.getElementById('hdf_content').innerText;
            }else{
                str = document.getElementById('hdf_content').textContent;
            }
               return str;
        }
        /**
         * Replace measurement of ingredient to special words
         *
         * return String
         **/
        /*function replace_measurement_units(ingredient, idx){
            ingredient = " " + trim(ingredient) + " ";
            var strReturn = "";
            var x;
            var idy = 0;
            var len = arrMeasurement.length;
            var ogrIngredient = ingredient;
            for (idy=0; idy<len; idy++){
                var arr = arrMeasurement[idy];
                if ((idx < 2) && (arr.length>0)){
                    for (x in arr){
                        var strReplace =  arrReplace[idx][idy];
                        ingredient = ingredient.replace(arr[x], strReplace);
                        if (ingredient != ogrIngredient){
                            return ingredient;
                        }
                    }
                }
            }
            return ingredient;
        }*/

        function checkItem(idTerm) {
            var objTerm= document.getElementById(idTerm);
            var objTerms= document.getElementById("hdf_terms");
            var arrTerms = new Array();
            var arrData = new Array();
            var x;

            if (objTerms.value != ""){
                arrTerms = objTerms.value.split(",");
            }
            if (objTerm != null){
                if (objTerm.checked == true){
                    for (x in arrTerms){
                        if (arrTerms[x] == objTerm.value){
                            return false;
                        }
                    }
                    arrTerms.push(objTerm.value);
                }else{
                    var idx = 0;
                    for (x in arrTerms){
                        if (arrTerms[x] == objTerm.value){
                            arrTerms[x] = 0;
                        }else{
                            arrData[idx] = arrTerms[x];
                            idx ++;
                        }
                    }
                    arrTerms = arrData;
                }
            }
            objTerms.value = arrTerms.join(",");
        }
        function confirmDelete(optionSubmitted){
            if (confirm('<?php print RECIPE_DELETE_CONFIRM?>')){
                var objOption= document.getElementById(optionSubmitted);
                objOption.value = "delete";
                $('#recipe_upload').submit();
                return true;
            }
            return false;
        }
        function bookmark_us(url, title){
//			if (window.sidebar){ // firefox
//			    window.sidebar.addPanel(title, url, "");
//			}else if(window.opera && window.print){ // opera
//			    var elem = document.createElement('a');
//			    elem.setAttribute('href',url);
//			    elem.setAttribute('title',title);
//			    elem.setAttribute('rel','sidebar');
//			    elem.click();
//			}else if(document.all){// ie
//			    window.external.addFavorite(url, title);
//			}
        }
        function removeImage(){
            var objDeleteImage= document.getElementById("hdf_delete_image");
            var objHdfImage = document.getElementById("hdf_filepath_image");
            objHdfImage.value = "";

            objDeleteImage.value = "1";
            $("#div_image_recipe").hide();
            $("#imgRecipe").hide();
            $("#btnRemoveImage").hide();
            $("#div_recipe_upload_image").show();

        }

        /* preview recipe*/
        function previewRecipe(){
            if($.trim($("#recipe_upload_image").val()) != ""){
                $("#message_error").html("");
                var options = {
                                success:       function(responseText, statusText, xhr, $form)  {
                                                    if(responseText != "false"){
                                                        var pathfile = "";
                                                        if(responseText != ""){
                                                            pathfile = pathfile + responseText;
                                                            var objImage = document.getElementById("hdf_filepath_image");
                                                            objImage.value = pathfile;

                                                            var myform = document.forms['recipe_upload'];
                                                            var windowname = "preview";
                                                            myform.action = '<?php print C_BASE_PATH?>'+'recipe_preview';
                                                            window.open('', windowname, 'height=1200,width=1200,scrollbars=yes');
                                                            myform.target=windowname;
                                                            myform.submit();
                                                        }
                                                    }
                                                } ,
                                url:          '<?php print C_BASE_PATH."recipes/upload/check_image_upload/1"?>'
                                };
                $('#recipe_upload').ajaxSubmit(options);
            }else{
                var myform = document.forms['recipe_upload'];
                var windowname = "preview";
                myform.action = '<?php print C_BASE_PATH?>'+'recipe_preview';
                window.open('', windowname, 'height=1200,width=1200,scrollbars=yes');
                myform.target=windowname;
                myform.submit();
            }
        }

        $(document).ready(function(){
<?php if (arg(0) == "adminrecipe" && arg(3)== "edit"){?>
            $("#sortable").sortable();
<?php }?>
            onload_radio_checkbox('recipe_difficulty_level_radio');
            onload_radio_checkbox('recipe_type_checkbox');
            onload_radio_checkbox('recipe_price_radio');
<?php if (arg(0) == "recipes"){?>
            $("#recipe_upload_image").filestyle({
                 image: "<?php print C_IMAGE_PATH."button/browse_btn.gif"?>",
                 imageheight : 25,
                 imagewidth : 75,
                 width : 170
             });
<?php }?>
        });
</script>

<?php if (arg(0) == "recipes"){
    //drupal_add_js(C_SCRIPT_PATH.'jquery.filestyle.js');
?>
<div id="recipes_upload">
    <div id="recipes_upload_b">
        <div id="recipes_upload_container">
            <div id="recipes_upload_contain">
                <form action="<?php print C_BASE_PATH . "recipes/save".(arg(3)=="org"?"/org":"")."?destination=" .urlencode($_GET['destination'])?>" name="recipe_upload" id="recipe_upload" accept-charset="UTF-8" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="hdf_action" id="hdf_action" value="<?php print C_BASE_PATH . "recipes/save".(arg(3)=="org"?"/org":"")."?destination=" . urlencode($_GET['destination'])?>"/>
<?php }else{
    if (arg(0) == "adminrecipe"){
        $query_back =  "?page=" . ($_GET['page']? $_GET['page']: 0);
        $query_back .= recipe_utils::get_url_adminrecipe();
    }
    ?>
<div id="admin_border_content">
    <form action="<?php print C_BASE_PATH . "adminrecipe/recipes/save?destination=" . $_GET['destination']?>" name="recipe_upload" id="recipe_upload" accept-charset="UTF-8" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="hdf_action" id="hdf_action" value="<?php print C_BASE_PATH . "adminrecipe/recipes/save" . $query_back?>"/>
    <div id="admin_border_content">
<?php }
if (empty($node->nid) || recipe_utils::getUserRole() == C_ADMIN_USER || recipe_utils::getUserRole() == C_PREMIUM_USER || recipe_utils::getUserRole() == C_REGISTER_USER){?>
                         <input type="hidden" name="nid" id="nid" value="<?php print $node->nid;?>"/>
                         <input type="hidden" name="hdf_terms" id="hdf_terms" value="<?php print $terms_value;?>"/>
                         <input type="hidden" name="hdf_delete_image" id="hdf_delete_image" value="0"/>
                         <input type="hidden" name="hdf_filepath_image" id="hdf_filepath_image" value="<?php print $node->field_recipe_image[0]['filepath']?>"/>
                         <div id="hdf_content" style="display:none;">
                         </div>
                         <?php if (arg(0) == "adminrecipe"){?>
                             <div class="registration_p_title">Recipe Upload
                                 <div style="padding:2px 0px 0px 0px;width:310px;float:right;"><img alt="Recipe Upload" src="<? print C_IMAGE_PATH ?>icon/icon_recipe_upload.gif"></div>
                             </div>
                             <div style="padding-left: 10px;">
                         <?php }else{?>
                             <div class="registration_p_title" style="padding: 0px;"><?php print recipe_db::get_admin_enter_text(RECIPE_UPLOAD_PAGE)?>
                                 <div style="padding:2px 0px 0px 0px;width:310px;float:right;"><img alt="Recipe Upload" src="<? print C_IMAGE_PATH ?>icon/icon_recipe_upload.gif"></div>
                             </div>
                         <?php }?>
                         <div id="divatips_add_content_space">&nbsp;</div>

                         <!-- START FEATURE RECIPES -->
                         <?php if ((arg(0) != "adminrecipe" && recipe_utils::getUserRole() == C_ADMIN_USER) || recipe_utils::getUserRole() == C_PREMIUM_USER){
                                    $arrPrice = recipe_db::get_list_recipe_price();
                                    $strPrice = "";
                                      foreach($arrPrice as $key => $value){
                                          $checked = " ";
                                          if ((float)$value == $node->field_recipe_price[0]['value']){
                                              $checked = " checked ";
                                          }
                                          $strPrice .= '<span class="make_select">';
                                          $strPrice .= '<label class="label_radio"><input type="radio" name="recipe_price" id="recipe_price" value="'. $value .'"'. $checked .'/>$' . $value . '</label>';
                                          $strPrice .= '</span>';
                                      }
                                ?>
                                <div id="make_recipe_premium" style="display:none;">
                                    <div class="make_recipe_premium_content" style="height: 79px; padding-top: 100px;">
                                        <div class="make_row" style="float: left; width: 100%;">
                                            <span class="make_label">
                                                Make my recipe premium!
                                            </span>
                                            <span id="recipe_type_checkbox" class="make_select">
                                                <label class="label_radio">
                                                       <input type="checkbox" name="recipe_type" id="recipe_type" value="0" <?php print (!empty($node->nid) && $node->field_recipe_type[0]['value']==0)? 'checked="checked"' : '' ?>>
                                               </label>
                                            </span>
                                        </div>
                                        <div id="recipe_price_radio" class="make_row" style="padding-top: 20px;">
                                            <span class="make_label">
                                                My recipe should sell for:
                                            </span>
                                            <?php print $strPrice?>
                                        </div>
                                    </div>
                                </div>
                    <?php }?>
                       <!-- END FEATURE RECIPES -->
                         <div id="div_error" class="message error" style="float:left;display:none;">
                        <ul><span id="message_error"></span></ul>
                    </div>
                    <div id="recipes_upload_lbl">Title:<span id="require">*</span></div>
                    <div id="recipes_upload_input">
                        <input type="text" maxlength="255" name="txtTitle" id="txtTitle" size="60" value="<?php print htmlspecialchars($node->title);?>"/>
                    </div>
                    <div id="recipes_upload_note">(255 characters Max)</div>
                    <div id="recipe_title_collapsed" class="recipe_upload_line" onClick="expandedCategory('recipe_title_expanded', 'recipe_title_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_title_expanded" class="recipe_upload_line" style="display:none;" onClick="expandedCategory('recipe_title_collapsed', 'recipe_title_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Title
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none" >Keep the title short but unique and descriptive. <br>
                               Example: Superbowl Sunday Beef Chilli
                           </div>
                       </div>
                    <div id="recipes_upload_lbl">Description:</div>
                    <div id="recipes_upload_input_discription">
                        <textarea name="description" id="description" maxLength="600"><?php print $node->body?></textarea>
                    </div>
                    <div id="recipes_upload_note">(600 characters Max)</div>
                    <div id="recipe_description_collapsed" class="recipe_upload_line" onClick="expandedCategory('recipe_description_expanded', 'recipe_description_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_description_expanded" class="recipe_upload_line" style="display:none;" onClick="expandedCategory('recipe_description_collapsed', 'recipe_description_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Description
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                            Describe why you like the recipes, where it came from, what makes it unique, any special preparation tips or notes, etc.  <br>
                               Example: This hearty chilli is perfect for a cold day of watching football with your foodies!  The splash of hot sauce gives it a nice kick!  Serve topped with shredded cheese and cool sour cream.
                           </div>
                       </div>
                    <div id="recipes_upload_lbl">Ingredients:<span id="require">*</span></div>
                    <div id="recipes_upload_input">
                        <?php if (arg(0) == "adminrecipe" && arg(3)== "edit"){?>
                        <div id="groceries_m_col">
                            <div id="div_groceries_content">
                                <ul id="sortable" class="sortable">
                                <?php
                                    $gid = 0;
                                    foreach ($groceries as $row) {
                                    $gid++;
                                    ?>
                                    <li id="groceries_content_<?php print $gid?>">
                                        <div class="groceries_content">
                                            <div id="groceries_col_container">
                                                <div id="recipe_groceries_col01">
                                                    <input type="text" value="<?php print htmlspecialchars($row->value)?>" name="txtGroceriesValue_<?php print $gid?>">
                                                </div>
                                                <div id="recipe_groceries_col03">
                                                    <?print str_replace('<option value="'.$row->aisle.'">','<option selected="true" value="'.$row->aisle.'">', str_replace('%aisleid%','ddlAisle_'.$gid,$aisleOption))?>
                                                </div>
                                                <div id="groceries_col04">
                                                    <img alt="Delete Grocery" style="cursor:pointer" onclick="deleteGrocery('<?php print $gid?>')" src="<?echo C_IMAGE_PATH?>button/groceries_del_btn.gif">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                            <div class="recipe_groceries_title">
                                Custom
                                <br>
                                <span id="add_your_own">Add your own custom items.</span>
                                <div class="groceries_content">
                                    <div id="groceries_col_container">
                                        <div id="recipe_groceries_col01">
                                            <input type="text" id="txtGroceriesValue"/>
                                        </div>
                                        <div id="recipe_groceries_col03">
                                            <?print str_replace('%aisleid%','ddlAisle',$aisleOption)?>
                                        </div>
                                        <img alt="Add Grocery" style="cursor:pointer" onclick="addGrocery()" src="<?echo C_IMAGE_PATH?>button/add_tolist_btn.gif">
                                        <input type="hidden" value="<?php print sizeof($groceries)?>" id="hidgid" name="hidgid">
                                    </div>
                                </div>
                            </div>
                        </div>
                            <textarea style="display:none" name="ingredients" id="ingredients" maxLength="1800"><?php print nl2br($node->field_ingredients[0]['value'])?></textarea>
                        <?php }else{?>
                            <textarea name="ingredients" id="ingredients" maxLength="1800"><?php print nl2br($node->field_ingredients[0]['value'])?></textarea>
                        <?php }?>
                    </div>
                    <div id="recipes_upload_note">(Include package sizes; 1 ingredients per line, 1800 characters max)</div>
                    <div id="recipe_ingre_collapsed" class="recipe_upload_line" onClick="expandedCategory('recipe_ingre_expanded', 'recipe_ingre_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_ingre_expanded" class="recipe_upload_line" style="display:none;" onClick="expandedCategory('recipe_ingre_collapsed', 'recipe_ingre_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Ingredients
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                            List each ingredient on a new line in the order that they are used in the recipe.  Be specific and indicate if any preparation has been done to the ingredient.<br>
                            Example:   2 garlic cloves, thinly sliced
                            <div style="padding-left:53px;">1 cup fresh mint, chopped</div>
                            <!--Use [link] tag to link a text to external webpage. The syntax should be [link URL] Text to link [/link]<br>
                            Example: [link http://www.therecipediva.com/recipe/10672/Pizza-Dough] Pizza Dough [/link]
                            -->
                           </div>
                       </div>
                       <div id="recipes_upload_lbl">Directions:<span id="require">*</span></div>
                    <div id="recipes_upload_input"><textarea name="preparation" id="preparation" maxLength="5000"><?php print $node->field_preparation[0]['value']?></textarea></div>
                    <div id="recipes_upload_note">(5000 characters Max)</div>
                    <div id="recipe_pre_collapsed" class="recipe_upload_line" onClick="expandedCategory('recipe_pre_expanded', 'recipe_pre_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_pre_expanded" class="recipe_upload_line" style="display:none;" onClick="expandedCategory('recipe_pre_collapsed', 'recipe_pre_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Directions
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                            Be specific and list steps in the order that they are executed.  Remember to indicate pre-heating, marinating time, the heat at which you are cooking, etc.
                           </div>
                       </div>
                    <div id="recipes_upload_input01">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
                    </div>
                    <?php
                            $arrLevel = recipe_db::get_list_recipe_difficult_level();
                            $strLevel = "";
                            if(arg(0) == "adminrecipe"){
                                foreach($arrLevel as $key => $value){
                                      $checked = " ";
                                      if ($key == $node->field_difficulty_level[0]['value']){
                                          $checked = " checked ";
                                      }elseif($key == EASY_LEVEL_VALUE && empty($node->nid)){
                                          $checked = " checked ";
                                      }
                                      $strLevel .= '<div id="recipe_upload_input01_1">';
                                      $strLevel .= '<label class="option"><input type="radio" id="difficulty_level" name="difficulty_level" value="'. $key .'"'. $checked .'style="width:20px;"/>' . $value . '</label>';
                                      $strLevel .= '</div>';
                                  }
                            }else{
                                $valueLevel = $node->field_difficulty_level[0]['value'];
                                $valueLevel = $valueLevel? $valueLevel : EASY_LEVEL_VALUE;
                                foreach($arrLevel as $key => $value){
                                      $checked = " ";
                                      if ($key == $valueLevel){
                                          $checked = ' checked ';
                                      }
                                      $strLevel .= '<div id="recipe_upload_input01_1">';
                                      $strLevel .= '<label class="label_radio"><input type="radio" id="difficulty_level" name="difficulty_level" value="'. $key .'"'. $checked .'style="width:20px;"/>&nbsp;' . $value . '</label>';
                                      $strLevel .= '</div>';
                                  }
                                  $strLevel = '<div id="recipe_difficulty_level_radio">' . $strLevel . '</div>';
                              }

                        ?>
                    <div id="recipes_upload_input01_1">
                        <div id="recipe_upload_lbl">Difficulty Level:</div>
                        <?php print $strLevel;?>
                    </div>
                    <?php if (arg(0) == "adminrecipe" && recipe_utils::getUserRole() == C_ADMIN_USER){
                        $strType = "";
                        if (empty($node->nid) || get_user_role_name($node->uid) == C_ADMIN_USER || get_user_role_name($node->uid) == C_PREMIUM_USER){
                            $arrType = recipe_db::get_list_recipe_type();
                            foreach($arrType as $key => $value){
                                  $checked = " ";
                                  if ($key == $node->field_recipe_type[0]['value']){
                                      $checked = " checked ";
                                  }elseif($key == RECIPE_TYPE_PUBLIC_VALUE && empty($node->nid)){
                                      $checked = " checked ";
                                  }
                                  $strType .= '<div id="recipe_upload_input01_1">';
                                  $strType .= '<label class="option"><input type="radio" id="recipe_type" name="recipe_type" value="'. $key .'"'. $checked .'style="width:20px;" onclick="changeRecipeType(this)"/>' . $value . '</label>';
                                  $strType .= '</div>';
                              }
                          ?>
                    <div id="recipes_upload_input01_1">
                        <div id="recipe_upload_lbl">Recipe Type:</div>
                        <?php print $strType;?>
                    </div>

                    <div id="recipes_upload_input01_1">
                        <div id="recipe_upload_lbl_2">Not send out an email notification:</div>
                        <div id="recipe_upload_input01_1">
                            <?php
                                      $checked = "";
                                      if (1 == $node->field_not_send_mail[0]['value']){
                                          $checked = "checked";
                                      }

                              ?>
                            <label class="option"><input type="checkbox" id="recipe_not_send_mail" name="recipe_not_send_mail" <?php print $checked;?> value="1" /></label>
                        </div>
                    </div>

                    <?php }?>
                    <?php
                        $arrStatus = recipe_db::get_list_recipe_content_status();
                        $strStatus = "";
                          foreach($arrStatus as $key => $value){
                              $checked = " ";
                              if ($key == $node->field_content_status[0]['value']){
                                  $checked = " checked ";
                              }
                              $strStatus .= '<div id="recipe_upload_input01_1">';
                              $strStatus .= '<label class="option"><input type="radio" id="content_status" name="content_status" value="'. $key .'"'. $checked .'style="width:20px;"/>' . $value . '</label>';
                              $strStatus .= '</div>';
                          }
                          $strCheckTop = $node->field_top_status[0]['value'] == 0? "": " checked";
                          $strRecommended = $node->field_recipe_recommended[0]['value'] == 0? "": " checked";
                    ?>
                      <div id="recipes_upload_input01_1">
                        <div id="recipe_upload_lbl">Content Status:</div>
                        <?php print $strStatus;?>
                    </div>
                    <!--<div id="recipes_upload_input01_1">
                        <div id="recipe_upload_lbl" style="margin-right:5px;">Location:</div>
                        <input type="checkbox" id="chk_top_page" value="1" name="chk_top_page"<?php print $strCheckTop;?>> Home
                    </div>-->
                    <?php if (empty($node->nid) || get_user_role_name($node->uid) == C_ADMIN_USER || get_user_role_name($node->uid) == C_PREMIUM_USER){
                               if(empty($node->nid) || $node->field_recipe_type[0]['value'] == RECIPE_TYPE_PUBLIC_VALUE){
                                   $style = 'style="display:none;"';
                               }else{
                                   $style = '';
                               }
                               $arrPrice = recipe_db::get_list_recipe_price();
                                $strPrice = "";
                                  foreach($arrPrice as $key => $value){
                                      $checked = " ";
                                      if ($value == $node->field_recipe_price[0]['value']){
                                          $checked = " checked ";
                                      }
                                      $strPrice .= '<div id="recipe_upload_input01_1">';
                                      $strPrice .= '<label class="option"><input type="radio" name="recipe_price" id="recipe_price" value="'. $value .'"'. $checked .'style="width:20px;"/>$' . $value . '</label>';
                                      $strPrice .= '</div>';
                                  }
                               ?>
                    <div id="div_recipes_upload" <?php print $style;?>>
                    <div id="recipes_upload_input01_1">
                        <div id="recipe_upload_lbl" style="margin-right:5px;">Premium recipe:</div>
                        <input type="checkbox" id="chk_recommended" value="1" name="chk_recommended"<?php print $strRecommended;?>> Recommended
                    </div>

                    <div id="recipes_upload_input01">
                        <div id="recipe_upload_lbl">Price:</div>
                        <?php print $strPrice?>
                    </div>
                    </div>
                    <?php }
                    }?>
                    <div id="recipes_upload_input01">
                        <div id="recipe_upload_lbl">Yield:</div>
                        <div id="recipe_upload_input01"><input type="text" name="txtYieldQuantity" id="txtYieldQuantity" value="<?php print trim($node->field_recipe_yield_quantity[0]['value']);?>" maxlength="10"/></div>
                        <div id="recipe_upload_input02"><input type="text" name="txtYieldUnit" id="txtYieldUnit" value="<?php print htmlspecialchars(trim($node->field_recipe_yield_unit[0]['value']));?>" maxlength="20"/></div><div id="recipes_upload_note_auto">(20 characters Max)</div>
                    </div>
                    <div id="recipe_yield_collapsed" class="recipe_upload_line_01"  onClick="expandedCategory('recipe_yield_expanded', 'recipe_yield_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_yield_expanded" class="recipe_upload_line_01" style="display:none;" onClick="expandedCategory('recipe_yield_collapsed', 'recipe_yield_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Yield
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                            Enter the quantity or number of servings that the recipe will yield.<br>
                            Example: 12 muffins<br>
                            <div style="padding-left:50px;">4 servings</div>
                        </div>
                       </div>
                    <div id="recipes_upload_input01">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
                    </div>
                    <?php $unit_time_value = ($node->field_preparation_unit[0]['value'] != "")? $node->field_preparation_unit[0]['value']: DURATION_TIME_MINUTES_VALUE;
                    ?>
                    <div id="recipes_upload_input01">
                        <div id="recipe_upload_lbl" style="margin:0px;">Preparation Time:<span id="require">*</span></div>
                        <div id="recipe_upload_input01"><input type="text" name="txtPreTime" id="txtPreTime" value="<?php print $node->field_preparation_time[0]['value'];?>" maxlength="10"/></div>
                          <div id="recipe_upload_input02">
                              <select name="cbbPreTimeUnit" id="cbbPreTimeUnit">
                                  <?php

                                  foreach ($arrTimeUnit as $key => $value){
                                      $selected = "";
                                      if ($key == $unit_time_value){
                                          $selected = ' selected="selected"';
                                      }
                                      print '<option value="' . $key .'"' . $selected .'>' . $value .'</option>';
                                  }?>
                            </select>
                          </div>
                       </div>
                       <div id="recipe_prepare_collapsed" class="recipe_upload_line_01"  onClick="expandedCategory('recipe_prepare_expanded', 'recipe_prepare_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                        </div>
                       </div>
                       <div id="recipe_prepare_expanded" class="recipe_upload_line_01" style="display:none;" onClick="expandedCategory('recipe_prepare_collapsed', 'recipe_prepare_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Preparation Time
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                               Enter the time it takes to prepare the ingredients - peeling, slicing, chopping, etc.
                        </div>
                       </div>
                    <div id="recipes_upload_input01">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
                    </div>
                    <?php $unit_time_value = ($node->field_cook_unit[0]['value'] != "")? $node->field_cook_unit[0]['value']: DURATION_TIME_MINUTES_VALUE;
                    ?>
                    <div id="recipes_upload_input01">
                        <div id="recipe_upload_lbl" style="margin:0px;">Cooking Time:<span id="require">*</span></div>
                        <div id="recipe_upload_input01"><input type="text" name="txtCookingTime" id="txtCookingTime" value="<?php print $node->field_cook_time[0]['value'];?>" maxlength="10"/></div>
                        <div id="recipe_upload_input02">
                              <select name="cbbCookTimeUnit" id="cbbCookTimeUnit">
                                <?php

                                foreach ($arrTimeUnit as $key => $value){
                                      $selected = "";
                                      if ($key == $unit_time_value){
                                          $selected = ' selected="selected"';
                                      }
                                      print '<option value="' . $key .'"' . $selected .'>' . $value .'</option>';
                                  }?>
                            </select>
                        </div>
                    </div>
                    <div id="recipe_cooking_collapsed" class="recipe_upload_line_01"  onClick="expandedCategory('recipe_cooking_expanded', 'recipe_cooking_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_cooking_expanded" class="recipe_upload_line_01" style="display:none;" onClick="expandedCategory('recipe_cooking_collapsed', 'recipe_cooking_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Cooking Time
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                            Enter the time it takes to do the actual cooking, in the oven, on the grill, on the stove, etc.
                        </div>
                       </div>
                    <div id="recipes_upload_input01">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
                    </div>
                    <?php $unit_time_value = ($node->field_inactive_unit[0]['value'] != "")? $node->field_inactive_unit[0]['value']: DURATION_TIME_MINUTES_VALUE;
                    ?>
                    <div id="recipes_upload_input01">
                        <div id="recipe_upload_lbl" style="margin:0px;">Inactive Time:</div>
                        <div id="recipe_upload_input01"><input type="text" name="txtInactiveTime" id="txtInactiveTime" value="<?php print $node->field_inactive_time[0]['value'];?>" maxlength="10"/></div>
                        <div id="recipe_upload_input02">
                              <select name="cbbInactiveTimeUnit" id="cbbInactiveTimeUnit">
                                <?php

                                foreach ($arrTimeUnit as $key => $value){
                                      $selected = "";
                                      if ($key == $unit_time_value){
                                          $selected = ' selected="selected"';
                                      }
                                      print '<option value="' . $key .'"' . $selected .'>' . $value .'</option>';
                                  }?>
                            </select>
                        </div>
                    </div>
                    <div id="recipe_inactive_collapsed" class="recipe_upload_line_01"  onClick="expandedCategory('recipe_inactive_expanded', 'recipe_inactive_collapsed');" >
                           <div class="recipe_upload_line_content" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Describe field" src="<? print C_IMAGE_PATH ?>icon/plus_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Diva Tip for Recipe Writing
                               </div>
                           </div>
                       </div>
                       <div id="recipe_inactive_expanded" class="recipe_upload_line_01" style="display:none;" onClick="expandedCategory('recipe_inactive_collapsed', 'recipe_inactive_expanded');" >
                           <div class="recipe_upload_line_content_expanded" style="cursor:pointer; cursor:hand;">
                               &nbsp;
                               <div style="float: left;">
                                <img alt="Hide describe field" src="<? print C_IMAGE_PATH ?>icon/subtract_ico.gif" hspace="5" />
                            </div>
                               &nbsp;
                               <div style="float: left;">
                                   Inactive Time
                               </div>
                           </div>
                           <div class="recipe_upload_line_content_none">
                            Enter the time that you are not actively preparing the dish - marinating, chilling, resting, etc.
                        </div>
                       </div>
                    <div id="recipes_upload_input01">
                        <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="1" />
                    </div>
                    <?php if($node->field_recipe_image[0]['filepath']){
                        $display_input_file = 'display:none;';
                    }else{
                        $display_input_file = "";
                    }
                    ?>
                    <div id="recipes_upload_input01">
                        <div id="recipe_upload_lbl">Recipe Image:</div>
                        <div id="div_recipe_upload_image" style="<?php print $display_input_file;?>;height:auto;" class="recipe_upload_inputfile">
                            <input type="file" name="recipe_upload_image" id="recipe_upload_image" class="myprofile_input_upload" />
                            <div class="description">Maximum Filesize: <em>6 MB</em>
                            <br />Allowed Extensions: <em>png gif jpg jpeg</em>
                            <br />Note: <em>Upload time will be impacted by large images</em>
                            </div>
                        </div>
                    </div>
                    <?php if($node->field_recipe_image[0]['filepath']){
                        $image_path = recipe_utils::get_thumbs_image_path($node->field_recipe_image[0]['filepath']);
                        $width = recipe_utils::getImageWidthValue($image_path,138);
                    ?>
                    <span id="div_image_recipe">
                           <div id="recipes_upload_input01" class="filefield-element clear-block">
                               <div id="imgRecipe" class="widget-preview">
                                   <img alt="<?php print $node->tittle?>" src="<?php print C_BASE_PATH . $image_path ?>" width="<?php print $width?>"/>

                            </div>
                            <div class="widget-edit btn_remove_image">
                                <img id="btnRemoveImage" name="btnRemoveImage" alt="Remove recipe image" src="<?echo C_IMAGE_PATH?>button/remove_btn.gif" onclick="removeImage()">
                            </div>
                        </div>
                    </span>
                    <?php }?>
                    <div id="recipes_upload_input01">
                        <div id="category_upload_seperate">&nbsp;</div>
                    </div>
                    <?php
                           if (sizeof($terms) != 0){
                               $icount = 0;
                              $intCategory = 0;
                              $intCountCat = 0;
                            $curParent = "";
            $col_left =	  	'<div id="categorize_recipe_l_col">';
            $col_middle =	'<div id="categorize_recipe_m_col">';
            $col_right =	'<div id="categorize_recipe_r_col">';
            $header = 		'	<div id="border_categorize">
                                    <div id="border_categorize_b">
                                        <div id="border_categorize_contain">
                                            <div id="border_categorize_content">';

            $space = 		'	<div id="div_space" class="bg-none">
                                      <img src="' . C_IMAGE_PATH . 'space.gif" height="15" width="1" />
                                  </div>';
            $footer = 		'				</div>
                                        </div>
                                    </div>
                                </div>';
                       ?>

                       <div id="categorize_recipe">
                        <div id="categorize_contain_content">
                            <div id="categorize_recipe_m_col01">
                                <div style="padding:0px 0px 0px 0px;width:310px;float:left;">Categorize your recipe</div>
                                <div style="padding:0px 0px 0px 0px;width:200px;float:left;"><img alt="Recipe Upload Category" src="<? print C_IMAGE_PATH ?>icon/iccon_recipe_upload_cat.gif"></div>
                            </div>
                        <?php foreach ($terms as $objTerm){

                            if ($icount == 0 || $curParent != $objTerm->parent_id){
                                $curParent = $objTerm->parent_id;
                                if ($intCategory %3 == 0){
                                    if ($intCategory != 0){
                                        $col_right .= $sub_cat;
                                        $col_right .= $footer;
                                        $col_right .= $space;
                                    }
                                    $col_left .= $header;
                                    $col_left .= '<div id="categorize_title">' . $objTerm->parent_name .'</div>';
                                }elseif ($intCategory %3 == 1){
                                    $col_left .= $sub_cat;
                                    $col_left .= $footer;
                                    $col_left .= $space;
                                    $col_middle .= $header;
                                    $col_middle .= '<div id="categorize_title">' . $objTerm->parent_name .'</div>';
                                }else{
                                    $col_middle .= $sub_cat;
                                    $col_middle .= $footer;
                                    $col_middle .= $space;
                                    $col_right .= $header;
                                    $col_right .= '<div id="categorize_title">' . $objTerm->parent_name .'</div>';
                                }
                                $intCategory++;
                                $sub_cat = "";
                            }
                            $checked = "";
                            if (sizeof($node->taxonomy) > 0){
                                foreach ($node->taxonomy as $key=>$value){
                                    if ($key == $objTerm->tid){
                                        $checked = " checked";
                                        break;
                                    }
                                }
                            }

                            $chkName = "ckbTerm_" . $objTerm->tid;
                             /*$sub_cat .=		'<div id="categorize_content">
                                                <div id="categorize_col1"><img src="' . C_IMAGE_PATH . 'icon/categorize_icon.gif" width="11" height="11" /></div>
                                                <div id="categorize_col2"><input type="checkbox" id="' . $chkName . '" name="' . $chkName . '" value="' . $objTerm->tid . '" onclick="checkItem(\'' . $chkName . '\');" ' . $checked .' /></div>
                                                <div id="categorize_col3">' . $objTerm->subcatName .'</div>
                                            </div>';*/
                            //remove the brown image next to category checkboxes, follow format of advanced search
                            /*$sub_cat .=		'<div id="categorize_content">
                                                <div id="categorize_col2"><input type="checkbox" id="' . $chkName . '" name="' . $chkName . '" value="' . $objTerm->tid . '" onclick="checkItem(\'' . $chkName . '\');" ' . $checked .' /></div>
                                                <div id="categorize_col3">' . $objTerm->subcatName .'</div>
                                            </div>';*/
                            $uncheck = "";
                            $check  = "";
                            $checked = "";
                            $itemValue = "";
                            if (sizeof($node->taxonomy) > 0){
                                foreach ($node->taxonomy as $key=>$value){
                                    if ($key == $objTerm->tid){
                                        $check = ' style="display:none;"';
                                        $itemValue = $objTerm->tid;
                                        break;
                                    }
                                }
                            }
                            if($check){
                                $uncheck = '';
                            }else{
                                $uncheck = ' style="display:none;"';
                            }
                            /*if ($_POST[$chkName] != "" || (!$post)){
                                $itemValue = $objTerm->tid;
                                $check = ' style="display:none;"';
                            }else{
                                $itemValue = "";
                                $uncheck = ' style="display:none;"';
                            }*/
                            /*$sub_cat .=		'<div id="categorize_content">
                                                <div id="categorize_col2"><input type="checkbox" id="' . $chkName . '" name="' . $chkName . '" value="' . $objTerm->tid . '" onclick="checkItem(\'' . $chkName . '\');" ' . $checked .' /></div>
                                                <div id="categorize_col3">' . $objTerm->subcatName .'</div>
                                            </div>';*/
                            $sub_cat .=	'<div id="categorize_content">';
                            $sub_cat .= '<div id="categorize_col2"><input type="button" class="advanced_check" id="img_check_' . $chkName . '" name="img_check_' . $chkName . '" onclick="checkItemImage(document.recipe_upload, \'' . $chkName . '\', \'ckbTerm_\', 0);return false;" ' . $uncheck .' />';
                            $sub_cat .= '<input type="button" class="advanced_uncheck" id="img_uncheck_' . $chkName . '" name="img_uncheck_' . $chkName . '" onclick="checkItemImage(document.recipe_upload,\'' . $chkName . '\', \'ckbTerm_\', 1);return false;" ' . $check .'/>';
                                //$strChkHtml .='		<input type="checkbox" id="' . $chkName . '" name="' . $chkName . '" value="' . $objTerm->tid . '" onclick="checkItem(document.frm_advanced_search_form, \'ckbTerm_\');" ' . $strCheck .'/>';
                            $sub_cat .='		<input type="hidden" id="hdf_' . $chkName . '" name="hdf_' . $chkName . '" value="' . $objTerm->tid . '"/>';
                            $sub_cat .='		<input type="hidden" id="' . $chkName . '" name="' . $chkName . '" value="' . $itemValue . '"/>';
                            $sub_cat .='</div><div id="categorize_col3">' . $objTerm->subcatName .'</div>';
                            $sub_cat .='</div>';

                            $icount++;
                               }
                               if ($intCategory%3 == 1){
                                   $col_left .= $sub_cat;
                                $col_left .= $footer;
                            }elseif ($intCategory %3 == 2){
                                $col_middle .= $sub_cat;
                                $col_middle .= $footer;
                            }else{
                                $col_right .= $sub_cat;
                                $col_right .= $footer;
                            }
                            print $col_left . "</div>";
                            print $col_middle. "</div>";
                            print $col_right. "</div>";
                               ?>
                           </div>
                       </div>
                       <?php }?>
                    <div id="recipes_upload_input01">
                          <img src="<? print C_IMAGE_PATH ?>space.gif" height="30" width="1" />
                    </div>
                    <input type="hidden" id="hdf_type" name="hdf_type" value="">
                    <div id="recipes_upload_input01" >
                        <?php if (arg(0) != "recipes"){?>
                            <input type="button" name="preview" id="preview" onclick="previewRecipe();"  value="" class="bt_forum_preview"/>
                        <?php }?>
                        <input type="button" name="op" id="edit-submit" value="" onclick="return validateRecipe();"  class="recipe_save" />
                        <?php
                        $urlBack = $_GET['destination'];
                        ?>
                        <input type="button" name="op" id="cancel-submit" value="" onclick="window.location.href = '<? print C_BASE_PATH.$urlBack ?>';"  class="recipe_cancel" />

                    </div>
                    <div id="recipes_upload_input01">
                          <img src="<? print C_IMAGE_PATH ?>space.gif" height="30" width="1" />
                    </div>
                    <?php }else{
                        print NOT_AUTHORIZED_TO_ACCESS_PAGE;
                    }
                    if (arg(0) == "adminrecipe"){?>
                            </div>
                        </div>
                    <?php }?>

                </form>
<?php if (arg(0) == "recipes"){?>
            </div>
           </div>
    </div>
</div>
<?php }else{ ?>
</div>
<?php }?>


<!-- END COOK OF WEEK -->
