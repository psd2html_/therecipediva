<div class="definition" id="dialogConvert" style="display:none;cursor:move">
    <div id="convert_p">
        <div id="convert_t">
            <div id="convert_t_contain">
            </div>
            <div id="convert_hr">
            </div>
        </div>
        <div id="convert_b">
            <div id="convert_content">
                <div id="convert_content_inner_textarea" onclick="$('#txtQuantity').focus();">
                	<img width="150px" height="15px" src="<? print C_IMAGE_PATH ?>space.gif">
                    <input type="text" style="font-size:11.5px !important;padding-left:3px !important;" maxlength="22" class="convert_content_inner_textarea" id="txtQuantity"></input>
                </div>
                <div id="convert_content_inner">
                	<img alt="From Unit" width="31" height="10" src="<? print C_IMAGE_PATH ?>label/from_lbl.gif">                    
                </div>
                <div id="convert_content_inner">
			        <select id="ddlFrom" onchange="load_measurement()" class="pre_select_type01">
			            <option value="w_ou">ounces</option>
			            <option value="w_po">pounds</option>            
			            <option value="w_gr">grams</option>
			            <option value="w_kg">kilograms</option>
			            <option value="v_dr">drops</option>
			            <option value="v_da">dashes</option>
			            <option value="v_pi">pinches</option>
			            <option value="v_te">teaspoons</option>
			            <option value="v_ta">tablespoons</option>
			            <option value="v_cu">cups</option>
			            <option value="v_pt">pints</option>
			            <option value="v_qu">quarts</option>
			            <option value="v_ga">gallons</option>
			            <option value="v_mi">milliliters</option>
			            <option value="v_li">liters</option>
			            <option value="t_fa">Fahrenheit</option>
			            <option value="t_ce">Celsius</option>
			        </select>
                </div>
                <div id="convert_content_inner">
                    <img alt="To Unit" width="17" height="10" src="<? print C_IMAGE_PATH ?>label/to_lbl.gif">
                </div>
                <div id="convert_content_inner">
                    <select id="ddlTo" class="pre_select_type01">
			            <option value="w_ou">ounces</option>
			            <option value="w_po">pounds</option>            
			            <option value="w_gr">grams</option>
			            <option value="w_kg">kilograms</option>           
			        </select>
                </div>
                <div id="convert_content_inner">
                    <img alt="Result Convert" width="38" height="10" src="<? print C_IMAGE_PATH ?>label/result_lbl.gif">
                    <img width="94" height="1" src="<? print C_IMAGE_PATH ?>space.gif">                    
                    <img id="btnConvert" alt="Convert" onclick="javascript:conversion();" style="cursor:pointer" src="<? print C_IMAGE_PATH ?>button/go_btn.gif">
                </div>
                <div id="convert_content_inner">
                    <input type="text" id="txtResult" style="width:163px;line-height:18px;" class="pre_select_type01">
                </div>
                <div id="convert_content_inner" style="text-align:center;padding:0">
                	<img width="150" height="8" src="<? print C_IMAGE_PATH ?>space.gif"> 
                	<img alt="Close" style="cursor:pointer" src="<? print C_IMAGE_PATH ?>button/close_btn.gif" onclick="$('#dialogConvert').dialog('close');">
                </div>
            </div>
        </div>
    </div>
</div>
<script language="JavaScript" type="text/javascript">
    var weight_m =  "<option value=\"w_ou\">ounces</option>"+
                    "<option value=\"w_po\">pounds</option>"+
                    "<option value=\"w_gr\">grams</option>"+
                    "<option value=\"w_kg\">kilograms</option>";
                    
    var volume_m =  "<option value=\"v_dr\">drops</option>"+
                    "<option value=\"v_da\">dashes</option>"+
                    "<option value=\"v_pi\">pinches</option>"+
                    "<option value=\"v_te\">teaspoons</option>"+            
                    "<option value=\"v_ta\">tablespoons</option>"+
                    "<option value=\"v_cu\">cups</option>"+
                    "<option value=\"v_pt\">pints</option>"+
                    "<option value=\"v_qu\">quarts</option>"+
                    "<option value=\"v_ga\">gallons</option>"+
                    "<option value=\"v_mi\">milliliters</option>"+
                    "<option value=\"v_li\">liters</option>";
                    
    var temp_m =    "<option value=\"t_fa\">Fahrenheit</option>"+
                    "<option value=\"t_ce\">Celsius</option>";
    
    function show_convert_tool(){
    	$("#dialogConvert").dialog(
			{ modal: true },
			{ resizable: false },
			{ height: 400 },
			{ width: 232},
			{ draggable: false},
			{ buttons:
				{ 	
					"Close": function() {																		
								$(this).dialog("close");						
							}														
				}
			},
			{ open: function(event, ui) { 
						$("div[class^=ui-dialog-titlebar]").hide();
						$("div[class^=ui-dialog-buttonpane]").hide();
						$("div[class^=ui-dialog]").removeClass("ui-widget-content");
						$("div[class^=ui-dialog]").draggable();																	
						reset_convert_tool();
					}
			},			
			{ beforeclose: function(event, ui) { 
						$("div[class^=ui-dialog-titlebar]").show();
						$("div[class^=ui-dialog-buttonpane]").show();
						$("div[class^=ui-dialog]").addClass("ui-widget-content");
					}
			}			
		);
		$("#txtQuantity").focus();
    }
    
    function load_measurement() {
        FromVal = $("#ddlFrom").val();
        ToVal = $("#ddlTo").val();
        if (FromVal.indexOf("w_") == 0 && ToVal.indexOf("w_") < 0) {
            $("#ddlTo").html(weight_m);
        }
        else if (FromVal.indexOf("v_") == 0 && ToVal.indexOf("v_") < 0) {
            $("#ddlTo").html(volume_m);
        }
        else if (FromVal.indexOf("t_") == 0 && ToVal.indexOf("t_") < 0) {
            $("#ddlTo").html(temp_m);
        }
    }

    function conversion() {
	    var convertedVal = 0;
	    var quantity = $("#txtQuantity").val();
	    var FromVal = $("#ddlFrom").val();
	    var ToVal = $("#ddlTo").val();

	    quantity = parse_quantity(quantity);

	    if (quantity == 0) {
	        //showInfoMessage("<?php print t(ERR_MSG_INVALID, array('@field_name' => "Quantity")) ?>");
	        $("#txtResult").val(0);    
	        return;
	    }
	    
	    quantity = parseFloat(quantity);	    
	    if (isNaN(quantity)) quantity= 0;

	    if (FromVal.indexOf("w_") == 0 && ToVal.indexOf("w_") == 0) {
	        convertedVal = convert_weight(quantity, FromVal, ToVal);
	    }
	    else if (FromVal.indexOf("v_") == 0 && ToVal.indexOf("v_") == 0) {
	        convertedVal = convert_volume(quantity, FromVal, ToVal);
	    }
	    else if (FromVal.indexOf("t_") == 0 && ToVal.indexOf("t_") == 0) {
	        convertedVal = convert_temperature(quantity, FromVal, ToVal);
	    }

	    $("#txtResult").val(convertedVal);
	}

	function is_number(value) {
	    if (/^\d+$/.test(value)) {
	        return true;
	    }
	    else{
	        return false;
	    }
	}

    function parse_quantity(quantity) {
        quantity = $.trim(quantity);
        var arrint = quantity.split(" ");
        var arrflt = "";
        var intnum = 0;
        var fltnum = 0;

        if (!isNaN(arrint[0])) {
            intnum = Number(arrint[0]);
        }
        else {
            arrflt = arrint[0].split("/");
            if (arrflt.length == 2) {
                if (is_number(arrflt[0]) && is_number(arrflt[1])) {
                    intnum = Number(arrflt[0]) / Number(arrflt[1]);
                }
                else {
                    //Error not number
                    return 0;
                }
            }
            else {
                //Error parse float
                return 0;
            }
        }

        for (var i = 1; i < arrint.length; i++) {
            if (arrint[i] != "") {
                if (fltnum == 0) {
                    arrflt = arrint[i].split("/");
                    if (arrflt.length == 2) {
                        if (is_number(arrflt[0]) && is_number(arrflt[1])) {
                            fltnum = Number(arrflt[0]) / Number(arrflt[1]);
                        }
                        else {
                            //Error not number
                            return 0;
                        }
                    }
                    else {
                        //Error parse float
                        return 0;
                    }
                }
                else {
                    // Error input > 3 number
                    return 0;
                }             
            }
        }

        return Math.round((intnum + fltnum) * 10000000) / 10000000;
    }
    
    function reset_convert_tool() {
      $("#txtResult").val("");
      $("#ddlTo").html(weight_m);
      $("#ddlFrom")[0].selectedIndex=0;
      $("#txtQuantity").val("");
    }

    function convert_volume(ff, from_val, to_val) {
        var v_dr = 1;
        var v_da = 12;
        var v_pi = 6;
        var v_te = 96;
        var v_ta = 288;
        var v_cu = 4608;
        var v_pt = 9216;
        var v_qu = 18432;
        var v_ga = 73728;
        var v_mi = 19.476877;
        var v_li = 19476.8770;
        var dNumber = 0;
        var rNumber = 0;
        
        // first convert to drop
        if (from_val == "v_dr") {
            dNumber = ff;
        }
        else if (from_val == "v_da") {
            dNumber = ff * v_da;
        }
        else if (from_val == "v_pi") {
            dNumber = ff * v_pi;
        }
        else if (from_val == "v_te") {
            dNumber = ff * v_te;
        }
        else if (from_val == "v_ta") {
            dNumber = ff * v_ta;
        }
        else if (from_val == "v_cu") {
            dNumber = ff * v_cu;
        }
        else if (from_val == "v_pt") {
            dNumber = ff * v_pt;
        }
        else if (from_val == "v_qu") {
            dNumber = ff * v_qu;
        }
        else if (from_val == "v_ga") {
            dNumber = ff * v_ga;
        }
        else if (from_val == "v_mi") {
            dNumber = ff * v_mi;
        }
        else if (from_val == "v_li") {
            dNumber = ff * v_li;
        }

        // now convert drop to unit
        if (to_val == "v_dr") {
            rNumber = dNumber;
        }
        else if (to_val == "v_da") {
            rNumber = dNumber / v_da;
        }
        else if (to_val == "v_pi") {
            rNumber = dNumber / v_pi;
        }
        else if (to_val == "v_te") {
            rNumber = dNumber / v_te;
        }
        else if (to_val == "v_ta") {
            rNumber = dNumber / v_ta;
        }
        else if (to_val == "v_cu") {
            rNumber = dNumber / v_cu;
        }
        else if (to_val == "v_pt") {
            rNumber = dNumber / v_pt;
        }
        else if (to_val == "v_qu") {
            rNumber = dNumber / v_qu;
        }
        else if (to_val == "v_ga") {
            rNumber = dNumber / v_ga;
        }
        else if (to_val == "v_mi") {
            rNumber = dNumber / v_mi;
        }
        else if (to_val == "v_li") {
            rNumber = dNumber / v_li;
        }

        return Math.round(rNumber * 10000000) / 10000000;
    }

    function convert_weight(ff,from_val,to_val){
	    var gValue = 1;
	    var kgValue = 1000;
	    var ounceValue = 28.3495;
	    var poundValue = 453.592;
	    var gNumber = 0;
	    var rNumber = 0;
	    // first convert to gram
	    if (from_val == "w_gr"){
		    gNumber = ff;
	    }
	    else if(from_val == "w_kg"){
		    gNumber = ff * kgValue;
	    }
	    else if(from_val == "w_ou"){
		    gNumber = ff * ounceValue;
	    }
	    else if(from_val == "w_po" ){
		    gNumber = ff * poundValue;
	    }

	    // now convert gram to unit
	    if (to_val == "w_gr"){
		    rNumber = gNumber;
	    }
	    else if(to_val == "w_kg"){
		    rNumber = gNumber / kgValue;
	    }
	    else if(to_val == "w_ou"){
		    rNumber = gNumber / ounceValue;
	    }
	    else if(to_val == "w_po" ){
		    rNumber = gNumber / poundValue;
	    }

	    return Math.round(rNumber * 10000000) / 10000000;
    }

    function convert_temperature(ff,from_val,to_val){
	    // first convert to kelvin
	    if (from_val == "t_ce"){
		    ff = ff + 273.15;
	    } else if (from_val == "t_fa"){
		    ff = ((ff - 32)/ 1.8) + 273.15;
	    } 

	    if (ff < 0){		
		    return 0;
	    }

	    // now convert kelvin to unit
	    if (to_val ==  "t_ce"){
		    ff = ff - 273.15;
	    } else if (to_val ==  "t_fa"){
	        ff = (1.8 * (ff - 273.15)) + 32;	    
	    } 

	    // round it off
	    if (Number.prototype.toFixed) {
		    ff = ff.toFixed(7);
		    ff = parseFloat(ff);
	    }
	    else {
		    var leftSide = Math.floor(ff);
		    var rightSide = ff - leftSide;
		    ff = leftSide + Math.round(rightSide *10000000)/10000000;
	    }
    	
	    return ff;
    }
</script>
