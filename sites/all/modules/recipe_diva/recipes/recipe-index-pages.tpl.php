<div id="Ingredients">
    <div id="recipes_title_contain">
        <div id="recipes_titles">Recipe Index<?php if ($page > 1) print ' Page ' . $page; ?></div>
     </div>
    <div id="Ingredients_b">
        <div id="Ingredients_content">
            <div id="TipArchive_p_content_inner">

                <?php
                    foreach ($arr_recipe as $recipe){
                        $title_link = recipe_utils::reduceString($recipe->title, 20, 200);
                        $title_link = recipe_utils::removeWhiteSpace($title_link);
                        $recipeLink = C_BASE_PATH . "recipe/" . $title_link . "-" . $recipe->nid;
                ?>
                        <div id="recipe">
                            <a href="<?php print $recipeLink ?>"><?php print $recipe->title ?></a><br />
                        </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
if($totalRecipes > RECIPE_INDEX_PER_PAGE*$page){
    $next_page = $page + 1;
?>
    <div id="recipe_number_paging">
        <div class="prev_image_paging">
            <a href="/recipe/index?page=<?php print $page; ?>"><img width="12" height="30" src="/sites/all/themes/recipe_diva/images/pasgeof_06.gif"></a>
        </div>
        <div class="nav_number_paging">
            <a href="/recipe/index?page=<?php print $page; ?>">Recipe Index Page <?php print $next_page; ?></a>
        </div>
    </div>
<?php }?>
