<?php
// $Id: recipes-admin-list.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-admin-list.tpl.php
 * Theme implementation to display list of recipes.
 *
 * Available variables:
 * - recipes: The list of recipes
 * - paging
 * - cat: Catergory
 *
 * @see theme_recipes_list()
 */
 $url = "adminrecipe/recipes";
?>
<div id="admin_border_content">
	<form name="frm_recipe_admin" id="frm_recipe_admin" method="post" onSubmit="searchData('<?php print C_BASE_PATH;?>adminrecipe/recipes', 'txtSearch');return false;">
		<div id="admin_recipe_content">
			<div class="admin_p_title">Recipes Management</div>
			<div id="admin_divatips_search_form">
				<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
				<div id="searchfor_input">
	            	<input type="text" name="txtSearch" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
	 			</div>
	            <div class="archive_search_btn">
	            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="searchData('<?php print C_BASE_PATH;?>adminrecipe/recipes', 'txtSearch');"/>
	            </div>
			</div>
			<div id="admin_recipes_content_title">
            	<?php print $header;?>
          	</div>
		<?php if (sizeof($recipes)>0){
      		$icount = 0;
      		
      		//url
  			$query_back =  "?page=" . ($_GET['page']? $_GET['page']: 0);
  			$query_back .= ($_GET['txtSearch']? "&txtSearch=" .  $_GET['txtSearch']: "");
			$query_back .= recipe_utils::get_url_adminrecipe();
			
      		foreach ($recipes as $objRecipe){
      			/*switch ($objRecipe['difficulty']){
      				case 0: $level = 'Easy';
      						break;
      				case 1: $level = 'Moderate ';
      						break;	
      				case 2: $level = 'Difficult';
      						break;	
      			}*/
      			$level = $objRecipe['difficulty'];
      			$recipe_type = $objRecipe['type_recipe'] == 0? 'Private': 'Public';
      			$check = $objRecipe['top_status'] == 0? '': 'checked';
      			
      			
      			//$arrStatus = recipe_db::get_list_recipe_content_status();		
				
				/*$strStatus = '<select name="cbb_status_' . $objRecipe['nid'] .'" id="cbb_status_' . $objRecipe['nid'] .'">';	
      			foreach($arrStatus as $key => $value){
      				$selected = "";
      				if ($key == $objRecipe['content_status']){
      					$selected = " selected ";
      				}
      				$strStatus .= '<option value="' . $key . '"' . $selected .'>' . $value .'</option>';
      			}
      			$strStatus .= "</select>";*/
      			/*foreach($arrStatus as $key => $value){
      				$selected = "";
      				if ($key == $objRecipe['content_status']){
      					$strStatus = $value;
      				}
      			}*/
      			$strStatus = $objRecipe['content_status'];
      			
      			if ($icount%2 == 0){
      				$classrow = "admin_recipes_content_inner01";
      			}else{
      				$classrow = "admin_recipes_content_inner";
      			}
      			$icount++;
      			
      			if($objRecipe['uid'] != 0){
      				$author = '<a title="View user profile." href="' . C_BASE_PATH."user/".$objRecipe['postuser'] . '"><span class="author">' . $objRecipe['postuser'] . '</span></a>';	
      			}else{
      				$author = C_UNKNOWN_USER;
      			}
      			?>
      		<div id="<?php print $classrow;?>">
          		<input type="hidden" id="hdf_recipe_nid_<?php print $objRecipe['nid']?>" name="hdf_recipe_nid_<?php print $objRecipe['nid']?>" value="<?php print $objRecipe['nid']?>">
          		<div id="admin_recipes_col_del"><input id="recipe_del_<?php print $objRecipe['nid']?>" name="recipe_del_<?php print $objRecipe['nid']?>" type="checkbox" value="<?php print $objRecipe['nid']?>" /></div>
          		<div id="admin_recipes_col_01"><a href="<?php print C_BASE_PATH;?>adminrecipe/recipes/<?php print $objRecipe['nid']?>/edit<?= $query_back;?>"><?php print $objRecipe['recipe_title']?></a></div>
                <div id="admin_recipes_col02">
                	by <?php print $author;?>
	        	<br><span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$objRecipe['created'])?></span><br>
	        	</div>
                <div id="admin_recipes_col03"><?php print $level?></div>
                <!--<div id="admin_recipes_col04"><input type="checkbox" id="chk_top_status_<?php print $objRecipe['nid']?>" name="chk_top_status_<?php print $objRecipe['nid']?>" <?php print $check;?>/></div>-->
                <!--<div id="admin_recipes_col04"><?php print $recipe_type?></div>-->
  	  	  	  	<div id="admin_recipes_col05"><?php print $strStatus;?></div>
  	  	  	  	<!--<div id="admin_recipes_col06"><a href="<?echo C_BASE_PATH?>adminrecipe/recipes/<?php print $objRecipe['nid']?>/grocery">Change</a></div>-->
  	  	  	  	<div id="admin_recipes_col07"><a href="javascript:deleteAdminItem('frm_recipe_admin', '', '<?php print $objRecipe['nid']?>', '<?php print CONFIRM_MSG_DELETE_ITEM;?>');">Delete</a></div>
          	</div>
      	<?}
      	
      	}?>	
      	<div id="admin_recipe_content">
        	<img src="<? print C_IMAGE_PATH ?>space.gif" height="10" width="1" />
        </div>
        <div id="admin_recipe_content">
        	<input type="hidden" name="op" value="">
  			<input type="hidden" name="delId" value="">
  			<input type="hidden" name="page" value="<?php print $_POST['page']; ?>">
			<input type="button" name="delete" id="delete" onclick="return deleteAdminList('frm_recipe_admin', 'recipe_del_', '', '<?php print CONFIRM_MSG_DELETE_ITEMS;?>');"  class="admin_bt_delete" />
        	<input type="button" name="add" id="add" value="" class="admin_bt_add" onclick="addNewItemAmin(document.frm_recipe_admin, '<?echo C_BASE_PATH?>adminrecipe/recipes/add?destination=adminrecipe/recipes');"/>
        	<!--<input type="button" name="save" id="save" onclick="return validateRecipeSave('hdf_type');" class="admin_recipes_saveall" />-->
        </div>
        <?php print $paging;?>
     
	</form>   
</div>   	
 



          	

