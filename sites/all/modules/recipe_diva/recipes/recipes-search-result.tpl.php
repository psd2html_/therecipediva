<?php
// $Id: recipes-search-result.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-search-result.tpl.php
 * Theme implementation to display list of recipes corresponding the information seach.
 *
 * Available variables:
 * - $recipes: The list of recipes
 * - $form: sort form
 * - paging
 *
 * @see theme_recipes_result()
 */
?>
    <?php if (sizeof($recipes)>0){?>
    <div id="border_type002">
        <div id="border_type002_top">
            <div id="border_type002_titles">
                <img alt="Search Result" src="<? print C_IMAGE_PATH ?>label/search_result_title_lbl.gif" />
            </div>
        </div>
        <div id="border_type002_b">
            <div id="border_type002_content">
                <div id="search_result_contain">
                    <div id="search_result_contain_inner" class="bg-none" style="width:390px;">
                        <div id="show_result_num">Showing <?php print $_GET["page"]*ARCHIVED_RECIPE_PER_PAGE+1?>-<?php print (($_GET["page"]+1)*ARCHIVED_RECIPE_PER_PAGE < $totalRecipes) ? ($_GET["page"]+1)*ARCHIVED_RECIPE_PER_PAGE : $totalRecipes?> of <?php print $totalRecipes?></div>
                        <div id="show_sort_form"><?php print $form?></div>
                    </div>
                    <?php foreach ($recipes as $objRecipe){
                            if($objRecipe['uid'] != 0){
                                  $author = '<a title="View user profile." href="' . C_BASE_PATH."user/".$objRecipe['postuser'] . '"><span class="author">' . $objRecipe['postuser'] . '</span></a>';
                              }else{
                                  $author = '<span class="by">' . C_UNKNOWN_USER . "</span>";
                              }
                            //$objRecipe['image'] = $objRecipe['image'] != ""? $objRecipe['image']: (file_directory_path()."/nophoto.jpg");
                        ?>
                        <div id="search_result_contain_inner">
                            <?php
                            $image_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                              $width = recipe_utils::getImageWidthValue($image_path,60);
                              $class = "search_result_center_content1";
                              if ($width){
                              $class = "search_result_center_content";?>
                                <div id="img_contain">
                                    <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH . $image_path?>" width="<?php print $width;?>"/>
                                </div>
                            <?php }else{
                                $class = "search_result_center_content";?>
                                <div id="img_contain">
                                    <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif"/>
                                </div>
                            <?php } ?>
                            <div id="<?php print $class;?>">
                                <span class="divatop_title">
                                    <a href="<?php print $objRecipe['recipe_url']?>"><?php print recipe_utils::reduceString($objRecipe['recipe_title'], 10, 120)?></a><br />
                                    <span class="by">by</span>&nbsp;<?php print $author?>
                                </span>
                                <div id="vote_contain">
                                <?php print $objRecipe['voting']?>
                                </div>
                            </div>
                        </div>
                        <?php }
//	                    $terms = show_filter();
//	                    if($terms != ' ' && sizeof($terms)){
//		              		print '<div class="filter_showall" style="float:left;width:80px;padding-top:9px;"><a href="javascript:filter(0, 0);">Remove filter</a></div>';
//		              	}
                        print $paging;?>
                </div>
            </div>
         </div>
    </div>
    <?php }else{ ?>
    <div id="border_type002">
        <div id="border_type002_top">
            <div id="border_type002_titles">
                <img alt="Search Result" src="<? print C_IMAGE_PATH ?>label/search_result_title_lbl.gif" />
            </div>
        </div>
       <div id="border_type002_b">
            <div id="border_type002_content">
                <div id="search_result_contain">
                    We're sorry, we could not find any recipes containing: "<?php print $form;?>"
                    <form id="search-sort-form" name="frm_search" method="post" accept-charset="UTF-8" onSubmit="javascript:return searchRecipes('frm_search', '<?echo C_BASE_PATH?>recipes');" action="<?echo C_BASE_PATH?>recipes">
                        <input type="hidden" name="hdf_type" id="hdf_type" value="<?php print RECIPES_SEARCH_RECIPES;?>"/>
                        <div id="border_type002_searchfor_contain" style="padding-top:3px;">
                            <div id="border_type002_searchfor_inp">
                                <div id="border_type002_Search_Form">
                                      <div id="searchfor_lbl" style="width:85px;padding-top:1px;">Search Again:</div>
                                      <div id="searchfor_input">
                                        <input type="text" name="txtKeyword" id="txtKeyword" value="<?php print $_POST['txtKeyword'];?>" class="form-text Archive_Search_inp" />
                                      </div>
                                    <div class="archive_search_btn">
                                        <input type="button" name="op" id="submit" onClick="searchRecipes('frm_search', '<?echo C_BASE_PATH?>recipes');" value="" class="advanced_search" />
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                    <?php print theme('recipe_not_found');?>
                </div>
            </div>
         </div>
    </div>
    <?php }?>
<script>
    function searchRecipes(frmName, url){
        var frm = document.forms[frmName];
        url = url + "?type=" + document.getElementById('hdf_type').value;
        url = url + "&kw=" + encodeURIComponent(document.getElementById('txtKeyword').value);
        window.location = url;
        return false;
    }
    function sortRecipes(frmName, url){
        url = url + '\/' + document.getElementById('edit-hdf-tid').value;
        url = url + "?type=" + document.getElementById('edit-hdf-type').value;
        url = url + "&sort=" + document.getElementById('edit-cbbSort').value;
        url = url + "&kw=" +  encodeURIComponent(document.getElementById('edit-txtKeyword').value);
        url = url + "&terms=" + encodeURIComponent(document.getElementById('edit-hdf-terms').value);
        for(var idx=1; idx <6; idx++){
            var strKey = encodeURIComponent(document.getElementById("edit-txtSearchIn-" + idx).value);
            if(trim(strKey) != ""){
                url = url + "&In_" + idx + "=" + strKey;
            }
            var strNotKey = encodeURIComponent(document.getElementById("edit-txtSearchEx-" + idx).value);
            if(trim(strNotKey) != ""){
                url = url + "&Ex_" + idx + "=" + strNotKey;
            }
        }
        window.location = url.replace(/\\/g,'');
    }
</script>







