<?php
// $Id: advanced-search.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file advanced-search.tpl.php
 * Theme implementation to display advanced search page.
 *
 * @see theme_advanced_search()
 */
?>
<?php
$post = $backpage;
if(!$post){

    $arr = array();
    foreach ($terms as $objTerm){
        $arr[] = $objTerm->tid;
    }
    $_POST['hdf_terms'] = implode(",", $arr);
}
?>
<script>

        /**
          * check all of check box when click check all
          * @param: form, fieldcheck, field name
          * @return none
          * **/
        function checkAllTermsImage(frm, fieldNameCheck,  fieldCheckAll, fieldNameCheck1, value) {
            var objTerms= document.getElementById(fieldCheckAll);
            var objImgItemCheck = document.getElementById("img_check_" + fieldCheckAll);
            var objImgItemUnCheck = document.getElementById("img_uncheck_" + fieldCheckAll);

            if (value == 0){
                objTerms.value = "";
                objImgItemCheck.style.display = "none";
                objImgItemUnCheck.style.display = "";
            }else{
                objTerms.value = "1";
                objImgItemCheck.style.display = "";
                objImgItemUnCheck.style.display = "none";
            }
            checkAllImage(frm, fieldNameCheck, value);
            getTerm(frm, fieldNameCheck1);
        }

    function checkItemImageAdvanced(frm, fieldNameCheck, fieldNameCheck1, value, parent_id) {
        // check image
        checkItemImage(frm, fieldNameCheck, fieldNameCheck1, value);

        var check_id = "img_check_chk_all_" + parent_id;
        var uncheck_id = "img_uncheck_chk_all_" + parent_id;
        var objImgItemCheck = document.getElementById(check_id);
        var objImgItemUnCheck = document.getElementById(uncheck_id);

        // when check box isn't checked, check all button isn't checked.
        if (value == 0){
            objImgItemUnCheck.style.display = "";
            objImgItemCheck.style.display = "none";
        }else{
            // when all of check box is checked, check all button is checked.
            var preName = "ckbTerm_" + parent_id;

            for (var i=0;i<frm.elements.length;i++)
            {
                var e=frm.elements[i];
                if (e.type =='hidden' && e.name.indexOf(preName)>=0 && !e.disabled)
                {
                    if ((e.name.indexOf("hdf_")< 0) && e.value == ""){
                        return false;
                    }
                }
            }
            objImgItemUnCheck.style.display = "none";
            objImgItemCheck.style.display = "";
        }
    }

    function searchRecipes(frmName, url){
        url = url + "?type=" + document.getElementById('hdf_type').value;
        url = url + "&kw=" + encodeURIComponent(document.getElementById('txtKeyword').value);
        url = url + "&terms=" + encodeURIComponent(document.getElementById('hdf_terms').value);
        window.location = url;
        return false;
    }
</script>
<div id="body_right_of_left_col">
    <?php
        $title =  recipe_db::get_admin_enter_text(ADVANCED_SEARCH_PAGE);
        print (empty($title)? "": '<div class="registration_p_title_small">' . $title . '</div>')
    ?>
              <!-- START COOK OF WEEK -->
    <div id="border_type002">
        <div id="border_type002_top">
            <div id="border_type002_titles">
                <img alt="Advance Search" src="<? print C_IMAGE_PATH ?>label/AdvancedSearch_title_lbl.gif" />
            </div>
        </div>
        <div id="border_type002_b">
                 <div id="border_type002_content">
                <div id="border_type002_contain">
                    <img src="<? print C_IMAGE_PATH ?>space.gif" height="12" width="1" />
                </div>
                <form id="frm_advanced_search_form" name="frm_advanced_search_form" method="GET" onSubmit="javascript:return searchRecipes('frm_advanced_search_form', '<?echo C_BASE_PATH?>recipes');" accept-charset="UTF-8" action="<? print C_BASE_PATH ?>recipes">
                    <input type="hidden" name="hdf_type" id="hdf_type" value="<?php print RECIPES_SEARCH_ADVANCED;?>"/>
                    <input type="hidden" name="hdf_terms" id="hdf_terms" value="<?php print $_POST['hdf_terms'];?>"/>
                    <div id="border_type002_searchfor_contain">
                        <div id="border_type002_searchfor_inp">
                            <div id="border_type002_Search_Form">
                                  <div id="searchfor_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                                  <div id="searchfor_input">
                                    <input type="text" name="txtKeyword" id="txtKeyword" value="<?php print $_POST['txtKeyword'];?>" class="Archive_Search_inp" />
                                  </div>
                                <div class="archive_search_btn">
                                    <input type="button" name="op" id="submit" onClick="searchRecipes('frm_advanced_search_form', '<?echo C_BASE_PATH?>recipes');" value="" class="advanced_search" />
                                </div>
                            </div>
                        </div>
                     </div>

                     <div class="border_type002_contain">
                        <div id="border_type002_inp">
                            If you would like to narrow your search, check one or more values from the categorives blow.
                        </div>
                    </div>
                    <?php
                          if (sizeof($terms) != 0){
                          $icount = 0;
                          $intCategory = 0;
                          $intCountCat = 0;
                        $curParent = "";
                        $filter_none = "";
                        $expandedDisplay = "";
                        $preParent = "";
                        $collapseDisplay = 'style="display:none;"';
                        foreach ($terms as $objTerm){
                              if ($icount == 0 || $curParent != $objTerm->parent_id){
                                $curParent = $objTerm->parent_id;
                                if ($intCategory%2 == 1){
                                    print "</div>";
                                }

                                $intCategory = 1;
                                $chkAllName = "chk_all_" . $curParent;

                                $check = '';
                                $uncheck = '';
                                if (($_POST[$chkAllName] != "" || (!$post))){
                                    $check = '';
                                    $uncheck = ' style="display:none;"';
                                    $valueParent = '1';
                                }else{
                                    $check =  ' style="display:none;"';
                                    $uncheck = '';
                                    $valueParent = '';
                                }
                                $strAllChkHtml = '<div id="border_check_contain">';
                                $strAllChkHtml .= '<input type="button" class="advanced_check" id="img_check_' . $chkAllName . '"  name="img_check_' . $chkAllName . '" onclick="checkAllTermsImage(document.frm_advanced_search_form, \'ckbTerm_' . $curParent .'_\', \'' . $chkAllName.'\', \'ckbTerm_\', 0);return false;" ' .  $check . ' />';
                                $strAllChkHtml .= '<input type="button" class="advanced_uncheck"  id="img_uncheck_' . $chkAllName . '" name="img_uncheck_' . $chkAllName . '" onclick="checkAllTermsImage(document.frm_advanced_search_form, \'ckbTerm_' . $curParent .'_\', \'' . $chkAllName.'\', \'ckbTerm_\', 1);return false;" ' . $uncheck . '/>';
                                //$strAllChkHtml .= '<input type="checkbox" id="' . $chkAllName . '" name="' . $chkAllName . '" onclick="checkAllTerms(document.frm_advanced_search_form, \'ckbTerm_' . $curParent .'\', document.frm_advanced_search_form.' . $chkAllName.', \'ckbTerm_\');" ' . $strAllCheck .'>';
                                $strAllChkHtml .= '<input type="hidden" id="' . $chkAllName . '" name="' . $chkAllName . '" value="' . $valueParent . '">';
                                $strAllChkHtml .='</div>';
                                $strAllChkHtml .='<div id="border_check_label_contain">All ' . $objTerm->plural_parent_name . '</div>';
                                $filter = "filters_" . $curParent;
                                $idExpanded = 'expanded_category_' . $curParent;
                                $idCollapsed = 'collapsed_category_' . $curParent;
                                if ($intCountCat != 0){
                                    $expandedDisplay = 'style="display:none;"';
                                    $collapseDisplay = '';
                                ?>
                                        </div>
                                          </div>
                                   </div>
                               </div>
                           </div>
                           <?php }
                                   $preParent = $curParent;

                                   $imageSrc = "";
                                $advanced_line = "";
                                if ($intCountCat %2 == 0){
                                    $advanced_line = "advanced_lineL00";
                                    $imageSrc = C_IMAGE_PATH . 'icon/plus_icon_white.gif';
                                }else{
                                    $advanced_line = "advanced_lineL01";
                                    $imageSrc = C_IMAGE_PATH . 'icon/plus_icon_pink.gif';
                                }
                                $filter_none = '<div class="border_type002_contain">';
                                $filter_none .= '<div id="' . $idCollapsed .'" class="' . $advanced_line . '" onClick="expandedCategory(\'' . $idExpanded . '\', \'' . $idCollapsed . '\');" ' . $collapseDisplay . '>';
                                  $filter_none .= '	<div id="plus_icon_contain"><img src="' . $imageSrc .  '" width="20"/></div>';
                                $filter_none .= '	<div id="advanced_line_content" style="cursor:pointer; cursor:hand;"><span style="padding:0 0 0 5px;">' . $objTerm->parent_name .'</span></div>';
                                  $filter_none .= '</div>';
                                  $filter_none .= '</div>';
                                  print ($filter_none);
                           ?>

                           <div id="<?php print $idExpanded;?>" class="border_type002_contain" <?php print $expandedDisplay;?>>
                            <div id="advanced_line_content" style="margin-bottom:12px;">
                                <div id="border_line">
                                    <div id="border_expand_contain" onClick="expandedCategory('<?php print $idCollapsed;?>', '<?php print $idExpanded;?>');">
                                        <div id="expand_col01"><img src="<? print C_IMAGE_PATH ?>space.gif" height="1" width="8" /></div>
                                        <div id="expand_col02"><img src="<? print C_IMAGE_PATH ?>icon/up_icon.gif" width="20"/></div>
                                        <div id="expand_col03"><span style="padding:0 0 0 5px;"><?php print $objTerm->parent_name;?></span></div>
                                        <div id="expand_col04"><img src="<? print C_IMAGE_PATH ?>space.gif" height="1" width="8" /></div>
                                    </div>
                                    <div id="border_line_b">
                                        <div id="border_line_content">
                       <?php $intCountCat++;
                               }

                                $chkName = "ckbTerm_" . $curParent . "_". $objTerm->tid;

                                $uncheck = "";
                                $check  = "";
                                if (($_POST[$chkName] != "" || (!$post))){
                                    $itemValue = $objTerm->tid;
                                    $check = ' style="display:none;"';
                                }else{
                                    $itemValue = "";
                                    $uncheck = ' style="display:none;"';
                                }
                                $strChkHtml = '<div id="border_check_contain">';
                                $strChkHtml .= '<input type="button" class="advanced_check" id="img_check_' . $chkName . '" name="img_check_' . $chkName . '" onclick="checkItemImageAdvanced(document.frm_advanced_search_form, \'' . $chkName . '\', \'ckbTerm_\', 0, \'' . $curParent . '\');return false;" ' . $uncheck .' />';
                                $strChkHtml .= '<input type="button" class="advanced_uncheck" id="img_uncheck_' . $chkName . '" name="img_uncheck_' . $chkName . '" onclick="checkItemImageAdvanced(document.frm_advanced_search_form,\'' . $chkName . '\', \'ckbTerm_\', 1, \'' . $curParent . '\');return false;" ' . $check .'/>';
                                //$strChkHtml .='		<input type="checkbox" id="' . $chkName . '" name="' . $chkName . '" value="' . $objTerm->tid . '" onclick="checkItem(document.frm_advanced_search_form, \'ckbTerm_\');" ' . $strCheck .'/>';
                                $strChkHtml .='		<input type="hidden" id="hdf_' . $chkName . '" name="hdf_' . $chkName . '" value="' . $objTerm->tid . '"/>';
                                $strChkHtml .='		<input type="hidden" id="' . $chkName . '" name="' . $chkName . '" value="' . $itemValue . '"/>';
                                $strChkHtml .='</div>';
                                $strChkHtml .='<div id="border_check_label_contain">' . $objTerm->subcatName . '</div>';
                                if ($intCategory%2 == 1 && $intCategory == 1){?>
                                <div id="border_line_content_inner">
                                    <div id="border_content_l_col">
                                        <?print $strAllChkHtml;?>
                                    </div>
                                <?php }
                                if ($intCategory%2 == 0){?>
                                <div id="border_line_content_inner">
                                    <div id="border_content_l_col">
                                        <?print $strChkHtml;?>
                                    </div>
                            <?php }else {?>
                                      <div id="border_content_r_col">
                                        <?print $strChkHtml;?>
                                    </div>
                                </div>
                            <?php }
                                $icount ++;
                                $intCategory ++;
                            }
                            if ($intCategory%2 == 1){
                                print "</div>";
                            }
                            ?>
                                         </div>
                                   </div>
                                   </div>
                               </div>
                          </div>
                          <div id="ingredients_contain">
                               <div id="ingredients_search_btn_contain">
                                   <input type="button" name="op" id="submit" value="" class="bt_ingredients_search" onClick="searchRecipes('frm_advanced_search_form', '<?echo C_BASE_PATH?>recipes');" />
                            </div>
                        </div>
                          <?php }?>
                </form>
            </div>
        </div>
    </div>
              <!-- END COOK OF WEEK -->
</div>
