<?php
// $Id: ingredients-search.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file ingredients-search.tpl.php
 * Theme implementation to display ingredients search page.
 *
 * @see theme_ingredients_search()
 */
?>
<div id="body_right_of_left_col">
<!--Ingredients Search Start-->
	<div id="border_type002">
		<?php
		$title =  recipe_db::get_admin_enter_text(INGREDIENTS_SEARCH_PAGE);
		print (empty($title)? "": '<div class="registration_p_title_small">' . $title . '</div>')?>
		<div id="border_type002_top">
			<div id="border_type002_titles">
            	<img alt="Ingredients Search" src="<? print C_IMAGE_PATH ?>label/IngredientsSearch_title_lbl.gif" />
            </div>
        </div>
        <div id="border_type002_b">
        	<div id="border_type002_content">
            	<div id="ingredients_contain">    
                	<div id="ingredients_help_text">
        		        Enter one or more ingredients that you would like to use and/or any ingredients that you would not like in the recipe.
	            	</div>
	            </div>
				<?php print $form;?>
			</div>
		</div>
	</div>
<!--Ingredients Search End-->
</div>
<script>
	function searchRecipes(frmName, url){
		url = url + "?type=" + document.getElementById('edit-hdf-type').value;
		for(var idx=1; idx <6; idx++){
			var strKey = encodeURIComponent(document.getElementById("edit-txtSearchIn-" + idx).value);
			if(trim(strKey) != ""){
				url = url + "&In_" + idx + "=" + strKey;
			}
			var strNotKey = encodeURIComponent(document.getElementById("edit-txtSearchEx-" + idx).value);
			if(trim(strNotKey) != ""){
				url = url + "&Ex_" + idx + "=" + strNotKey;
			}
		}
		
		window.location = url;
	}
</script>