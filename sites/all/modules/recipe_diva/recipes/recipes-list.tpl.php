<?php
// $Id: recipes-list.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-list.tpl.php
 * Theme implementation to display list of recipes.
 *
 * Available variables:
 * - recipes: The list of recipes
 * - paging
 * - cat: Catergory
 *
 * @see theme_recipes_list()
 */
?>
<div id="Ingredients">
    <div id="recipes_title_contain">
        <div id="recipes_titles"><?php if ($cat != null) print $cat->category?></div>
     </div>
     <?php /*if (sizeof($recipes)>0){
         $cat_image_path = C_BASE_PATH . $recipes[0]['cat_image'];
         print '<div style="float:left;position: relative;">
                <img src="' . $cat_image_path .'" width="554px"/>
               </div>';
     }*/?>
    <div id="Ingredients_b">
        <div id="Ingredients_content">
            <div id="TipArchive_p_content_inner">
             <?php
                if (sizeof($recipes)>0){
                    foreach ($recipes as $objRecipe){
                        $objRecipe['image'] = $objRecipe['image'] != ""? $objRecipe['image']: (file_directory_path()."/nophoto.jpg");
                    ?>
                <div id="TipArchive_content_contain">
                    <div id="TipArchive_content_contain2">
                           <div class="archive_row_2">
                            <div id="img_contain"><img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH . $objRecipe['image']?>" width="62"/></div>
                            <div class="archive_title">
                                <a href="<?php print $objRecipe['recipe_url']?>"><?php print recipe_utils::reduceString($objRecipe['recipe_title'], 10, 130)?></a><br />
                                <span class="by">by</span> <a href="<?php print C_BASE_PATH . 'user/'. $objRecipe['postuser']?>"><?php print $objRecipe['postuser']?></a>
                            </span>
                            <br><?php print $objRecipe['voting']?>
                               </div>

                        </div>
                    </div>
                </div>
                    <?php }
                    print $paging;
                }else{ ?>
                <div style="text-align:center;">
                            Data not fournd.
                </div>
                <?php }?>
            </div>
        </div>
      </div>
</div>









