<div id="dialogConfirm" style="display:none" title="Are you sure you want to delete?">
</div>

<?php
//	$unitOption = '<select name="%unitid%" id="%unitid%" disabled="true">';
//	foreach ($unit as $row) {
//		$unitOption.='<option value="'.$row->id.'">'.$row->name.'</option>';
//	}			   
//	$unitOption .= '</select>';
	$aisleOption = '<select name="%aisleid%" id="%aisleid%">';
	
	foreach ($aisle as $row) {
		$aisleOption.='<option value="'.$row->id.'">'.$row->name.'</option>';
	}			   
	$aisleOption .= '</select>';	
?>

<form id="recipes-groceries-form" name="recipes-groceries-form" method="post" accept-charset="UTF-8" action="<?echo C_BASE_PATH?>adminrecipe/recipes/grocery/save">
	<div id="admin_border_content">
	    <div id="admin_recipe_content">
            <div id="admin_recipes_content_title">
	            <div class="admin_groceries_col_type" id="admin_groceries_col01_title">
	                Title
	            </div>
	            <div class="admin_groceries_col_type" id="admin_groceries_col02_title">
	                Quantity
	            </div>
	            <div class="admin_groceries_col_type" id="admin_groceries_col03_title">
	                Unit
	            </div>
	            <div class="admin_groceries_col_type" id="admin_groceries_col04_title">
	                Aisle
	            </div>
	            
	        </div>
	    </div>
	    <div class="messages error" style="display:none" id="divError">
			<ul></ul>
		</div>
   		<div>
            <div id="spanError"></div>
        </div>
	    <div id="admin_recipe_content">
	    	<div id="div_groceries_content">
    		<?php
    			$aisle_id = -1;	    			
    			foreach ($groceries as $row) { 
    				?>
    				<div id="admin_groceries_content_<?php print $row->nid?>">			        
				        <div id="admin_groceries_col_container">
			                <div id="admin_groceries_col01">
			                    <input type="text" value="<?php print recipe_utils::display_grocery_title($row->title)?>" name="txtTitle_<?php print $row->nid?>" READONLY>
			                </div>
			                <div id="admin_groceries_col02">
			                    <input type="text" value="<?php print $row->quantity?>" name="txtQuantity_<?php print $row->nid?>" READONLY>
			                </div>
			                <?php
			                	$unit_name = "";
			                	$quality = recipe_utils::convert_to_numeric($row->quantity); 
			                	foreach ($unit as $objUnit) {
									if($row->unit == $objUnit->id){
										if($quality > 1){
											$unit_name = $objUnit->plural_name;
										}else{
											$unit_name = $objUnit->name;
										}
										break;
									}
								}	
							
							?>
			                <div id="admin_groceries_col03">
			                	<input type="text" value="<?php print $unit_name?>" name="ddlUnit_<?php print $row->nid?>" READONLY>
			                </div>
			                <div id="admin_groceries_col04">
			                	<?print str_replace('<option value="'.$row->aisle_id.'">','<option selected="true" value="'.$row->aisle_id.'">', str_replace('%aisleid%','ddlAisle_'.$row->nid,$aisleOption))?>
			                </div>
			           </div> 
			         </div>           
			<?php } ?>	
	        </div>	        
	    </div>
	    <div id="admin_recipe_content">
		    <div id="admin_recipe_content">
	        	<input type="button" name="op" id="edit-submit" value=""  onclick="return saveAllGrocery();" class="recipe_save" />
	        </div>   
	        <div id="admin_recipe_content">
	            <img width="12" height="100" src="<?echo C_IMAGE_PATH?>space.gif">
	        </div>
		</div>
	 </div>
	 <input type="hidden" value="" id="hidListnid" name="hidListnid">
</form>
<script>
	
	function saveAllGrocery(){
		var groceryArray = $("#div_groceries_content").find("div[id^=admin_groceries_content_]");
		var nid = "";
		
		// Check required
    	for (i = 0; i < groceryArray.length; i++) {
    		nid = groceryArray[i].id.replace("admin_groceries_content_","");
    		$("#hidListnid").val("," + nid + $("#hidListnid").val());
    	}
    	
    	if($("#hidListnid").val() != ""){
    		$("#hidListnid").val($("#hidListnid").val().substring(1));
			$("#recipes-groceries-form").submit();
		}
	}
</script>
