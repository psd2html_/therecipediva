<?php
// $Id: recipe.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file recipe.tpl.php
 * Theme implementation to display information of the recipe.
 *
 * Available variables:
 * - $recipe: recipe information
 *
 * @see theme_recipe_page()
 */
if(!isset($_GET["cid"])){
    $urlCur = "recipe/".arg(1);
    $urlBack = "recipe/".strtolower(recipe_utils::removeWhiteSpace($node->title))."-".$node->nid;
    if($urlCur != $urlBack) {
        //drupal_goto($urlBack, NULL, NULL, 301);
    }
}
     global $user;
    /*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
      drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
      drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
    drupal_add_js('sites/all/modules/tinymce/tinymce/jscripts/tiny_mce/tiny_mce.js');

    $hasPer = recipe_utils::getUserRole() == C_ADMIN_USER ? true : $user->uid == $node->uid? true: false;
    $description = $node->body;
    $description = nl2br(trim($description));
    $description = str_replace('"','\'',$node->title.' - '.$description);
    $title = str_replace('"','\'',$node->title);
    $site_url = C_SITE_URL.C_BASE_PATH;
    drupal_set_html_head('<meta property="og:title" content="'.$node->title.'"/>
                          <meta property="og:type" content="food"/>
                          <meta property="og:url" content="'.$site_url . "recipe/" . recipe_utils::removeWhiteSpace($node->title) . "-" . $node->nid.'"/>
                          <meta property="og:image" content="'.$site_url . (empty($node->field_recipe_image[0]['filepath']) ? 'sites/all/themes/recipe_diva/images/logo.png':$node->field_recipe_image[0]['filepath']).'"/>
                          <meta property="og:site_name" content="TheRecipeDiva"/>
                          <meta property="fb:admins" content="6230637"/>
                          <meta property="og:description" content="The Recipe Diva invites you to try this '.$title.' recipe and hundreds more recipes, reviews, and cooking tips from therecipediva.com." />
                        ');
//    if(isset($_GET['cid'])){
//        drupal_add_link(array("rel"=>"canonical", "href"=>$site_url."recipe/".recipe_utils::removeWhiteSpace($node->title)."-".$node->nid));
//    }
    // set breadcrumb
    $breadcrumb[] = l(t('TheRecipeDiva'), $site_url);
    $breadcrumb[] = l(t('Recipes'), 'recipes');
    $breadcrumb[] = '<span>' . t($node->title) . '</span>';
    drupal_set_breadcrumb($breadcrumb);

      $destination = "destination=" . "recipe/$node->nid";
    $login_url = "user/login?" . $destination;
    $flagVersion = 0;
    if ($node->field_content_status[0]['value'] == 1 || $node->field_customize_user[0]['value'] == $user->uid || $node->uid == $user->uid){
        $nid_customize = 0;
        $flag_customize = 0;
        if($user->uid){
            if($node->field_recipe_original[0]['value']){
                $node_version = node_load($node->nid);
                $node = node_load($node->field_recipe_original[0]['value']);
                $flag_customize = $node_version->field_customize[0]['value'];
                $flagVersion = 1;
            }else{
                $nid_customize = get_customize_recipe_by_user($node->nid);
                if($nid_customize){
                    $node_version = node_load($nid_customize);
                    $flag_customize = $node_version->field_customize[0]['value'];
                }
            }
        }
        $title = $title . " Recipe - Tips, Reviews";
        drupal_set_title($title);

        $display_comment_lst = "";
        $display_form = "display:none;";
        $display_myversion = "display:none;";
        $cid = $_GET['cid'];
        if ($cid > 0) {
            $display_recipe = "display:none;";
        } else if ($cid == '0') {
            $display_review = "";
            $display_recipe = "display:none;";
            $display_comment_lst = "display:none;";
            $display_form = "";
        } else {
            $display_review = "display:none;";
            $display_comment_lst = "display:none;";
            $display_form = "display:none;";
        }

        if($flag_customize && $nid_customize == 0){
            $display_title  = "display:none;";
            $display_form = "display:none;";
            $display_review = "display:none;";
            $display_comment_lst = "display:none;";
            $display_recipe = "display:none;";
            $display_myversion = "";
        }
        //get user
        $postuser = user_load(array('uid' => $node->uid));

        $author_info = recipe_utils::create_author_info($node->uid, $node->name, $node->created, STANDARD_DATE_FORMAT, 'author', 'published ');

        $review_link = url("comment/reply/".$node->nid);

        //title of recipe
        $urlTitle = recipe_utils::removeWhiteSpace($node->title);

        //voting
        $rating = recipe_db::get_average_vote_value($node->nid);
        $countrating = recipe_db::get_count_vote_value($node->nid);

        // Check permission of comment
        $edit_path = '';
        $delete_path = '';

        //comments of the recipe
        $result = recipe_db::get_node_comment($node->nid);
        $strComment = "";
        $img_review = '<img alt="Write preview" src="'.C_IMAGE_PATH.'button/writereview.gif" style="cursor:pointer;" />';
        if (user_is_logged_in()) {
            $review_link = C_BASE_PATH . "recipe/$node->nid&cid=0";
            //$strComment = '<a href="javascript:write_review();">'.$img_review.'</a>';
            $strComment = '<a href="' . $review_link . '">'.$img_review.'</a>';
        }else{
            $strComment = '<a href="' . C_BASE_PATH . $login_url . '">'.$img_review.'</a>';
        }
        $strComment = '<div id="writereview">'.$strComment.'</div>';
        $comment_count = comment_num_all($node->nid);
        $index = 0;
        while ($comment = db_fetch_object($result)){
            $links = "";
            if ($comment->uid == $user->uid || (recipe_utils::getUserRole() == C_ADMIN_USER)){
                // Create links comment
                $links = create_comment_update_links($comment, '', $node->type);
            }
            $div_vote_style = "divatiptab_vote";

            // Add style
            $div_style = "";
            if ($index == ($comment_count - 1)) {
                $div_style = 'style="background: none;"';
            }
            $fivestar_view = theme('fivestar_static', $comment->value, variable_get('fivestar_stars_'. $node->type, 5));
            $strComment .= '<div id="divatiptab_content" '.$div_style.'>';
            $strComment .= '	<div id="'.$div_vote_style.'">'.$fivestar_view .'</div>';
            $strComment .= '		<div id="divatiptab_content_inner">';
            $strComment .= '<div id="divatiptab_author">'.recipe_utils::create_author_info($comment->uid, $comment->name, $comment->timestamp).'</div>' ;
            $strComment .= '<div id="review_content">'.$comment->comment.'</div>';
            $strComment .= $links;
            $strComment .= '	</div>';
            $strComment .= '</div>';
            $index++;
        }
        if ($comment_count == 0) {
            $comment_count = "";
            $strComment .= '<div>&nbsp;</div>';
        } else {
            $comment_count = "(".$comment_count.")";
        }
        $strComment .= '<br>';
        //load grocery to display in recipe home page START
        //update $strIngredients
        //load grocerie items
        $arrGroceries = load_grocery_by_recipe($node->nid);
        $strIngredients = $node->field_ingredients[0][value];
        $strIngredientsNew = '';
        $arrIngredients = explode(BREAK_LINE , $strIngredients);
        if (is_array($arrIngredients))
        {
            $intCount = 1;

            foreach($arrIngredients as $strIngItem)
            {
                $arrGroceryItem = array();
                $strIngItemNew = strip_tags($strIngItem);
                $strReplace = $strIngItem;//recipe_utils::convertLinkToURL($strIngItem, $strIngItemNew);
                $is_ingredient = false;

                foreach ($arrGroceries as $objGrocery)
                {
                    if (trim($objGrocery->title)==trim($strIngItemNew))
                    {
                        $is_ingredient = true;

                        $arrGroceryItem = array();
                        checkIngredientItem($strIngItemNew, $arrGroceryItem);
                        $str_rep = str_replace($arrGroceryItem["quantity"].' '. $arrGroceryItem["unit"],
                                                '<span class="amount">'.$arrGroceryItem["quantity"].' '. $arrGroceryItem["unit"].'</span> <span class="name">'
                                                ,$strReplace);
                        if($str_rep != $strReplace){
                            //$strIngredientsNew .= '<span class="ingredient">'.$str_rep.'</span></span>';
                            $strIngredientsNew .= '<span class="ingredient">'.$strReplace.'</span>';
                        }
                        else{
                            $strIngredientsNew .= $strReplace;
                        }

                        $strIngredientsNew .= '&nbsp;<span class="individual_plus" id="individual_plus_' . $intCount . '">';
                        if ($user->uid){
                            $strIngredientsNew .= '<a href="javascript:addFaviroteNode_new(\'' . C_BASE_PATH . 'favorite_nodes/checked/grocery/'. $objGrocery->nid . '\', \'' . C_BASE_PATH . 'favorite_nodes/add_my_grocery/single/'. $objGrocery->nid . '\', \'' . ADD_GROCERY_COMPLETE_INFO . '\', \'' . ADD_GROCERY_ALREADY_COMPLETE_INFO . '\', \'' . ERR_MSG_ADD_FAVORITE_NODE . '\');"> [+]</a>';
                            $intCount++;
                        }else{
                            $strIngredientsNew .= '<a href="' . C_BASE_PATH . $login_url . '">[+]</a>';
                        }

                        $strIngredientsNew .= '</span><br>';
                        break;
                    }
                }
                if(!$is_ingredient){
                    //$strIngredientsNew .= $strReplace . '<br>';
                    if(trim($strReplace) == ''){
                        $strIngredientsNew .= $strReplace . '<br>';
                    }else{
                        $strIngredientsNew .= '<span class="ingredient">'.$strReplace . '</span><br>';
                    }
                }
            }
        }
        $strIngredients = $strIngredientsNew;
        //load grocery to display in recipe home page END
        // create error message
        $err_mess = create_review_error();

        // get time
        // yield
          $yieldValue = $node->field_recipe_yield_quantity[0]['value'];
          $yieldUnit = $node->field_recipe_yield_unit[0]['value'];
          // preparation time
          $preValue = recipe_utils::convert_to_numeric($node->field_preparation_time[0]['value']);
          // cooking time
          $cookValue = recipe_utils::convert_to_numeric($node->field_cook_time[0]['value']);
          // inactive time
          $inactiveValue = recipe_utils::convert_to_numeric($node->field_inactive_time[0]['value']);

          $arrTimeUnit = recipe_db::get_view_list_duration_time();

          $preUnitValue = $node->field_preparation_unit[0]['value'];
          $cookUnitValue = $node->field_cook_unit[0]['value'];

          $totalUnitValue = 0;
          $totalUnit = "";
          $totalValue = 0;

          if($inactiveValue != 0){
              $inactiveUnitValue = $node->field_inactive_unit[0]['value'];
              $inactiveValue = recipe_utils::convert_to_second($inactiveValue,$inactiveUnitValue);
              $totalUnitValue = max($preUnitValue,$cookUnitValue,$inactiveUnitValue);
              $totalValue = recipe_utils::convert_to_second($preValue,$preUnitValue) + recipe_utils::convert_to_second($cookValue,$cookUnitValue) + $inactiveValue;
          }else{
              $totalUnitValue = max($preUnitValue,$cookUnitValue);
              $totalValue = recipe_utils::convert_to_second($preValue,$preUnitValue) + recipe_utils::convert_to_second($cookValue,$cookUnitValue);
          }

          // Dung Nguyen start -------------------------
          if($totalValue > 60){
              $remainSeconds = $totalValue%60;
              if($remainSeconds <= 30){
                  $roundHalfMin = ($remainSeconds < 15) ? 0 : 0.5;
              }else{
                  $roundHalfMin = ($remainSeconds < 45) ? 0.5 : 1;
              }
              $totalValue = intval($totalValue/60);
              $totalValue = $totalValue + $roundHalfMin;

              if($totalValue > 90){
                  $totalUnitValue = DURATION_TIME_HOUR_VALUE;
                  $remainMins = $totalValue%60;
                  if($remainMins <= 30){
                      $roundHalfHour = ($remainMins < 15) ? 0 : 0.5;
                  }else{
                      $roundHalfHour = ($remainMins < 45) ? 0.5 : 1;
                  }
                  $totalValue = intval($totalValue/60);
                  $totalValue = $totalValue + $roundHalfHour;
              }else{
                  $totalUnitValue = DURATION_TIME_MINUTES_VALUE;
              }
          }else{
              $totalUnitValue = DURATION_TIME_SECONDS_VALUE;
          }
          // end -------------------------------------------

          foreach($arrTimeUnit as $key => $value){
              if ($preUnitValue == $key){
                  $preUnit = $value;
              }
              if ($cookUnitValue == $key){
                  $cookUnit = $value;
              }
              if ($totalUnitValue == $key){
                  $totalUnit = $value;
              }
          }

          /*if($totalUnitValue == DURATION_TIME_HOUR_VALUE){
              $totalValue = $totalValue/3600;
          }elseif($totalUnitValue == DURATION_TIME_MINUTES_VALUE){
              $totalValue = $totalValue/60;
          }elseif($totalUnitValue == DURATION_TIME_SECONDS_VALUE){

          }*/



          /*
          $totalUnit = $preUnit;
          if ($preUnit == $cookUnit){
              $totalValue = $preValue + $cookValue;
              $totalUnit = $preUnit;
          }elseif ($preUnitValue == DURATION_TIME_HOUR_VALUE){
            if($cookUnitValue == DURATION_TIME_MINUTES_VALUE){
                $totalValue = $preValue * 60 + $cookValue;
            }else{
                $totalValue = $preValue * 60 * 60 + $cookValue;
            }
            $totalUnit = $cookUnit;
        }elseif($preUnitValue == DURATION_TIME_MINUTES_VALUE){
            if($cookUnitValue == DURATION_TIME_HOUR_VALUE){
                $totalValue = $preValue + $cookValue * 60;
                $totalUnit = $preUnit;
            }else{
                $totalValue = $preValue * 60 + $cookValue;
                $totalUnit = $cookUnit;
            }
          }else{
              if($cookUnitValue == DURATION_TIME_HOUR_VALUE){
                  $totalValue = $preValue + $cookValue * 60 * 60;
              }else{
                  $totalValue = $preValue + $cookValue * 60;
              }
              $totalUnit = $preUnit;
          }
          */
          //difficult
          $arrDifficult = recipe_db::get_list_recipe_difficult_level();
          $strDifficult = $arrDifficult[$node->field_difficulty_level[0]['value']];
        ?>

        <script type="text/javascript">
        <!--//--><![CDATA[//><!--

          tinyMCE.init({
            mode : "exact",
            theme : "advanced",
            relative_urls : false,
            document_base_url : "<?php print C_BASE_PATH;?>",
            language : "en",
            safari_warning : false,
            entity_encoding : "raw",
            verify_html : false,
            preformatted : false,
            convert_fonts_to_spans : true,
            remove_linebreaks : true,
            apply_source_formatting : true,
            theme_advanced_resize_horizontal : false,
            theme_advanced_resizing_use_cookie : false,
            plugins : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_path_location : "none",
            theme_advanced_resizing : true,
            theme_advanced_blockformats : "p,address,pre,h1,h2,h3,h4,h5,h6",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,numlist,bullist,indent,outdent,separator,undo,redo",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            elements : "comment"
          });

        //--><!]]>
        </script>
        <script>
            $(document).ready(function() {
                <?php if ($flagVersion != 0){ ?>
                $('#news_p_menu_myversion').pngFix();
                <?php }elseif ($cid > 0 || $cid == '0'){ ?>
                $('#news_p_menu_comment').pngFix();
                <?php }else{ ?>
                $('#news_p_menu_tip').pngFix();

                <?php } ?>

                ResizeImage('div_comment', 520);
                ResizeImage('individual_r_content', 300);
                ResizeImage('divatiptab_content_inner', 520);

                <?php if ($display_myversion != ""){?>
                $("#recipes_version_img_contain").hide();
                <?php }?>

                <?php if ($node->field_recipe_type[0]['value'] == 0){?>
                $("#RecipesPage").attr('class', 'Recipes');
                $("#ShopRecipesPage").attr('class', 'ShopRecipes_bg');
                <?php }else{?>
                    $("#RecipesPage").attr('class', 'Recipes_bg');
                <?php }?>

                FB.Event.subscribe('edge.create', function(response) {
                  //alert(response);
                });

            });


            function change_tab(flag) {
                var commentRecipeTab= document.getElementById("div_comment");
                var infoRecipeTab= document.getElementById("individual_p_b");
                var infoRecipeImage = document.getElementById("news_p_menu_tip");
                var divCommentList = document.getElementById("comment_list");
                var divCommentForm = document.getElementById("comment_form");
                var commentRecipeImage = document.getElementById("news_p_menu_comment");

                var recipeTitle = document.getElementById("recipes_img_contain");
                var myversionTab = document.getElementById("news_p_menu_myversion");
                var myversionTitle = document.getElementById("recipes_version_img_contain");
                var myversionInfo = document.getElementById("recipe_myversion_p_b");

                if (flag == '1') {
                    commentRecipeTab.style.display = "none";
                    commentRecipeImage.style.display = "none";
                    infoRecipeTab.style.display = "";
                    infoRecipeImage.style.display = "";
                    recipeTitle.style.display = "";
                    divCommentList.style.display = 'none';
                    divCommentForm.style.display = 'none';
                    if(myversionTab != null){
                        myversionTab.style.display = 'none';
                        myversionTitle.style.display = 'none';
                        myversionInfo.style.display = 'none';
                    }
                    $('#news_p_menu_tip').pngFix();
                } else if (flag == '2') {
                    infoRecipeTab.style.display = "none";
                    infoRecipeImage.style.display = "none";
                    recipeTitle.style.display = "";
                    commentRecipeTab.style.display = "";
                    commentRecipeImage.style.display = "";
                    divCommentList.style.display = '';
                    divCommentForm.style.display = 'none';
                    if(myversionTab != null){
                        myversionTab.style.display = 'none';
                        myversionTitle.style.display = 'none';
                        myversionInfo.style.display = 'none';
                    }
                    $('#news_p_menu_comment').pngFix();
                }else{
                    infoRecipeTab.style.display = "none";
                    infoRecipeImage.style.display = "none";
                    commentRecipeTab.style.display = "none";
                    commentRecipeImage.style.display = "none";
                    divCommentList.style.display = 'none';
                    divCommentForm.style.display = 'none';
                    recipeTitle.style.display = "none";
                    if(myversionTab != null){
                        myversionTab.style.display = "";
                        myversionTitle.style.display = '';
                        myversionInfo.style.display = '';
                        $('#news_p_menu_myversion').pngFix();
                    }
                }
            }

            function checkCustomizeRecipe(url_check){
                $.post(url_check + "/true", function(data){
                    //Data do not exist in user's favorite
                    if(data == "false"){
                        showInfoMessage("<?php print ADD_RECIPE_FAVORITE_REQUEST_INFO ?>");
                    }else{
                        document.location.href = "<?php print C_BASE_PATH . "recipes/"?>" + data + "<?php print "/edit/" . $urlTitle . "?destination=recipe/"?>" + data;
                    }
                });
            }

            function expandDiv(img_id, div_id, img_path_add, img_path_subtract){
                if($.trim($("#"+div_id).html()) == ""){
                    var url = "<?php print C_BASE_PATH."getallpreminumrecipeofuser/".$node->uid."/1/".$node->nid."/"?>";

                    $.get(url, function(data){
                                $("#"+div_id).html(data);
                           },"text");
                }

                if($("#"+img_id).attr("src") == img_path_add){
                    $("#"+img_id).attr("src",img_path_subtract);
                    $("#"+div_id).show();
                }
                else{
                    $("#"+img_id).attr("src",img_path_add);
                    $("#"+div_id).hide();
                }

            }
        </script>
        <!-- create delete confirmation form -->
        <?php print recipe_utils::create_delete_confirm_form($node->nid); ?>

        <!-- display folder to choose for user's favorite -->
        <?php //print recipe_utils::create_foler_selection($node->type, "My Saved Recipes"); ?>
        <div id="dialogConfirmBecomePrimium" style="display:none;text-align:center;" title="<?php print INFO_MSG_PREMIUM_FUNCTION?>">
            <div style="margin:7px 0px 0px 5px;text-align:center;">
                <input type="button" style="width:110px;" value="<?php print INFO_MSG_AGREE_PRIMIUM_USERS;?>" onclick="document.location.href='<?php print C_BASE_PATH?>become_premium'">
                <input type="button" style="width:110px;" value="<?php print INFO_MSG_REFUSE_PRIMIUM_USERS;?>"  onclick="closeDialogConfirmBecomePrimium();">
            </div>
        </div>

        <?php if ($node->field_recipe_type[0]['value'] == 0){?>
        <div id="shop_recipe">
            <div id="shop_recipe_top">
                <div id="shop_recipe_top_title">

                </div>
            </div>
            <div id="shop_recipe_contain">
                <div id="shop_recipe_content" style="padding:0 0 0 4px;">
                    <div style="padding:0 0 0 6px;">
         <?php }else{?>

        <div id="news_p">
            <div id="news_p_b">
                <div id="news_border">
                    <div id="news_p_content" class="hrecipe news_p_content_font">
         <?php }?>
                        <div id="recipes_img_contain" style="<?php print $display_title?>">
                            <div id="recipe_right_content" style="width: 618px;">
                                <span>
                                    <div id="individual_r_title">
                                           <span id="nid_saved" style="display:none;"><?php print $node->nid?></span>
                                           <h1 class="fn" style="font-size: 27px; font-weight: normal;"><?php print $node->title?></h1>
                                    </div>
                                    <div id="individual_r_posted" style="padding-bottom:5px;">
                                        <div><?php print $author_info?>
                                        </div>
                                       </div>
                                   </span>
                                   <?php

                                $description = $node->body;
                                $description = nl2br(trim($description));
                                $read_more_url = "javascript:expand_description('individual_r_excerpt_desc', 'individual_r_content');";
                                     $excerpt = recipe_utils::get_excerpt($node->body, NUMBER_WORDS_DESCRIPTION, $read_more_url);
                                     if ($excerpt == '') {
                                         $excerpt = "&nbsp;";
                                     }

                                $width = recipe_utils::getImageWidthValue($node->field_recipe_image[0]['filepath'], RECIPE_IMAGE_WIDTH);
                                $style = "float: left;" ;
                                $bg_width = 613 - ($width + 5);
                                if ($width){
                                    $style = "float: left;" . " width:" . $bg_width . "px;" ;
                                ?>
                                <div id="individual_image" class="individual_r_image">
                                    <img class="photo" src="<?php print C_BASE_PATH . $node->field_recipe_image[0]['filepath']?>" alt="<?php print $node->title?>" width="<?php print $width?>"/>
                                </div>
                                    <?php }
                                    else{
                                        if(trim($description) != ""){
                                            print '	<div id="individual_image" class="individual_r_image" style="width:313px;">
                                                    <span>
                                                             <div id="individual_r_excerpt_desc" style="padding-top:0px;'.$style.'" class="summary">
                                                            '.nl2br($excerpt).'
                                                        </div>
                                                        <div id="individual_r_content" style="padding-top:0px;display:none;'.$style.'">
                                                                 '.$description.'
                                                             </div>
                                                         </span>
                                                    </div>';
                                        }
                                    }

                                      if ($rating > 0){?>

                                     <span class="review hreview-aggregate">
                                      <span class="rating">
                                         <span class="average"><span class="value-title" title="<?php print $rating/20?>"> </span></span>
                                         <span class="count"><span class="value-title" title="<?php print $countrating?>"> </span></span>
                                      </span>
                                     </span>

                                        <span>
                                            <div id="individual_r_voted">
                                            <?php print theme('fivestar_static_recipe', $rating, '5');?>
                                        </div>
                                    </span>
                                    <?php }?>
                                    <span>
                                        <div id="individual_r_voted" style="<?php print $style;?>padding-bottom:8px;">
                                            <div id="myversion_tips_bg01">
                                                <div id="myversion_tips_bg02">Difficulty: <?=$strDifficult;?></div>
                                                <div id="myversion_tips_bg03">
                                                    <div id="myversion_tips_bg03_l">
                                                        <div>Yield: <span class="yield"><?php print $yieldValue . " " . $yieldUnit?></span></div>
                                                        <div>Prep Time: <span<?php print $preValue!=0?' class="preptime"':''?> datetime="PT<?php print $preValue . ($preUnit=="Hour(s)"?"H":($preUnit=="Min(s)"?"M":"S"))?>"><?php print $preValue . " " . $preUnit?><span class="value-title" title="PT<?php print $preValue . ($preUnit=="Hour(s)"?"H":($preUnit=="Min(s)"?"M":"S"))?>"></span></span></div>
                                                    </div>
                                                    <div id="myversion_tips_bg03_r">
                                                        <div>Cook Time: <span<?php print $cookValue!=0?' class="cooktime"':''?> datetime="PT<?php print $cookValue . ($cookUnit=="Hour(s)"?"H":($cookUnit=="Min(s)"?"M":"S"))?>"><?php print $cookValue . " " . $cookUnit?><span class="value-title" title="PT<?php print $cookValue . ($cookUnit=="Hour(s)"?"H":($cookUnit=="Min(s)"?"M":"S"))?>"></span></span></div>
                                                        <div>Ready In: <span<?php print $totalValue!=0?' class="duration"':''?> datetime="PT<?php print $totalValue . ($totalUnit=="Hour(s)"?"H":($totalUnit=="Min(s)"?"M":"S"))?>"><?php print $totalValue . " " . $totalUnit?><span class="value-title" title="PT<?php print $totalValue . ($totalUnit=="Hour(s)"?"H":($totalUnit=="Min(s)"?"M":"S"))?>"></span></span></div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div id="individual_r_excerpt_desc" style="<?php print $style;?>" class="summary">
                                                <?php if ($width){
                                                    print nl2br($excerpt);
                                                }?>
                                            </div>
                                            <div id="individual_r_content" style="display:none;<?php print $style;?>">
                                                 <?php if ($width){
                                                     print $description;
                                                 }?>
                                             </div>



                                             <div id="individual_r_content" style="<?php print $style;?>">
                                                 <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
                                                 <fb:like href="<?php print $site_url . "recipe/" . recipe_utils::removeWhiteSpace($node->title) . "-" . $node->nid?>" send="true" width="295" show_faces="false" font=""></fb:like>
                                             </div>
                                        </div>
                                    </span>


                               </div>

                        <?php
                        if ($node->field_recipe_type[0]['value'] == 0){
                        $uploaded_recipes = load_user_premium_recipes_list($node->uid);
                        if(sizeof($uploaded_recipes)>1){
                        ?>
                        <div id="shoprecipe_upload_recipe">
                            <div style="float: left; position: relative; width:535px;">
                                <img alt="Make premium recipe" src="<?echo C_IMAGE_PATH?>icon/reg_comform_pre_icon.gif">
                                <span style="padding-left:10px; vertical-align:top; color:#71534F; font-size:18px; font-weight:normal; word-wrap:break-word;">
                                    <?php
                                    if($node->name){
                                        print $node->name."'s ";
                                    }?>
                                    Premium Recipes:
                                </span>
                            </div>

                             <div id="shoprecipe_content_inner">
                                     <?php
                                     $i = 0;
                                     foreach ($uploaded_recipes as $objRecipe){
                                         if($i==NUMBERS_UPLOADED_RECIPE){
                                             break;
                                         }else if($objRecipe['nid'] == $node->nid){
                                             continue;
                                         }
                                         $i ++;
                                     ?>
                                     <div id="shoprecipe_img_container">
                                        <div id="img_container">
                                            <div>

                                            <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                                            if(file_exists($file_path)){ ?>
                                                <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                                            <?php }else{?>
                                                <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                                            <?php }?>
                                             </div>
                                           <span class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></span>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <span id="span_uploaded_recipe">
                                    </span>
                               </div>
                               <div class="img_seeall" <?php print sizeof($uploaded_recipes)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                                 <div onclick="expandDiv('img_uploaded','span_uploaded_recipe','<?echo C_IMAGE_PATH?>label/seeall_this_user_premium_recipe_lbl.gif','<?echo C_IMAGE_PATH?>label/hideall_this_user_premium_recipe_lbl.gif')" class="see_all_txt"><img id="img_uploaded" alt="See all premium recipe" src="<?echo C_IMAGE_PATH?>label/seeall_this_user_premium_recipe_lbl.gif"></div>
                                 <div id="see_all_btn">
                                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_uploaded','span_uploaded_recipe','<?echo C_IMAGE_PATH?>label/seeall_this_user_premium_recipe_lbl.gif','<?echo C_IMAGE_PATH?>label/hideall_this_user_premium_recipe_lbl.gif')">
                                </div>
                             </div>
                          </div>
                        <?php }
                        }?>
                        </div>

                        <?php if($flag_customize){
                            // yield
                              $yieldValue = $node_version->field_recipe_yield_quantity[0]['value'];
                              $yieldUnit = $node_version->field_recipe_yield_unit[0]['value'];
                              // preparation time
                              $preValue = recipe_utils::convert_to_numeric($node_version->field_preparation_time[0]['value']);
                              // cooking time
                              $cookValue = recipe_utils::convert_to_numeric($node_version->field_cook_time[0]['value']);
                              // inactive time
                              $inactiveValue = recipe_utils::convert_to_numeric($node_version->field_inactive_time[0]['value']);

                              $preUnitValue = $node_version->field_preparation_unit[0]['value'];
                              $cookUnitValue = $node_version->field_cook_unit[0]['value'];

                              $totalUnitValue = 0;
                              $totalUnit = "";
                              $totalValue = 0;

                              if($inactiveValue != 0){
                                  $inactiveUnitValue = $node_version->field_inactive_unit[0]['value'];
                                  $inactiveValue = recipe_utils::convert_to_second($inactiveValue,$inactiveUnitValue);
                                  $totalUnitValue = max($preUnitValue,$cookUnitValue,$inactiveUnitValue);
                                  $totalValue = recipe_utils::convert_to_second($preValue,$preUnitValue) + recipe_utils::convert_to_second($cookValue,$cookUnitValue) + $inactiveValue;
                              }else{
                                  $totalUnitValue = max($preUnitValue,$cookUnitValue);
                                  $totalValue = recipe_utils::convert_to_second($preValue,$preUnitValue) + recipe_utils::convert_to_second($cookValue,$cookUnitValue);
                              }

                              // Dung Nguyen start -------------------------
                              if($totalValue > 60){
                                  $remainSeconds = $totalValue%60;
                                  if($remainSeconds <= 30){
                                      $roundHalfMin = ($remainSeconds < 15) ? 0 : 0.5;
                                  }else{
                                      $roundHalfMin = ($remainSeconds < 45) ? 0.5 : 1;
                                  }
                                  $totalValue = intval($totalValue/60);
                                  $totalValue = $totalValue + $roundHalfMin;

                                  if($totalValue > 90){
                                      $totalUnitValue = DURATION_TIME_HOUR_VALUE;
                                      $remainMins = $totalValue%60;
                                      if($remainMins <= 30){
                                          $roundHalfHour = ($remainMins < 15) ? 0 : 0.5;
                                      }else{
                                          $roundHalfHour = ($remainMins < 45) ? 0.5 : 1;
                                      }
                                      $totalValue = intval($totalValue/60);
                                      $totalValue = $totalValue + $roundHalfHour;
                                  }else{
                                      $totalUnitValue = DURATION_TIME_MINUTES_VALUE;
                                  }
                              }else{
                                  $totalUnitValue = DURATION_TIME_SECONDS_VALUE;
                              }
                              // end -------------------------------------------

                              foreach($arrTimeUnit as $key => $value){
                                  if ($preUnitValue == $key){
                                      $preUnit = $value;
                                  }
                                  if ($cookUnitValue == $key){
                                      $cookUnit = $value;
                                  }
                                  if ($totalUnitValue == $key){
                                      $totalUnit = $value;
                                  }
                              }

                              /*if($totalUnitValue == DURATION_TIME_HOUR_VALUE){
                                  $totalValue = $totalValue/3600;
                              }elseif($totalUnitValue == DURATION_TIME_MINUTES_VALUE){
                                  $totalValue = $totalValue/60;
                              }elseif($totalUnitValue == DURATION_TIME_SECONDS_VALUE){

                              }*/

                              /*
                              $totalValue = 0;
                              $totalUnit = $preUnit;
                              if ($preUnit == $cookUnit){
                                  $totalValue = $preValue + $cookValue;
                                  $totalUnit = $preUnit;
                              }elseif ($preUnitValue == DURATION_TIME_HOUR_VALUE){
                                  if($cookUnitValue == DURATION_TIME_MINUTES_VALUE){
                                      $totalValue = $preValue * 60 + $cookValue;
                                  }else{
                                      $totalValue = $preValue * 60 * 60 + $cookValue;
                                  }
                                  $totalUnit = $cookUnit;
                              }elseif($preUnitValue == DURATION_TIME_MINUTES_VALUE){
                                  if($cookUnitValue == DURATION_TIME_HOUR_VALUE){
                                      $totalValue = $preValue + $cookValue * 60;
                                      $totalUnit = $preUnit;
                                  }else{
                                      $totalValue = $preValue * 60 + $cookValue;
                                      $totalUnit = $cookUnit;
                                  }
                              }else{
                                  if($cookUnitValue == DURATION_TIME_HOUR_VALUE){
                                      $totalValue = $preValue + $cookValue * 60 * 60;
                                  }else{
                                      $totalValue = $preValue + $cookValue * 60;
                                  }
                                  $totalUnit = $preUnit;
                              }
                              */
                              $strDifficult = $arrDifficult[$node_version->field_difficulty_level[0]['value']];
                          ?>
                                 <div id="recipes_version_img_contain">
                                <div id="recipe_right_content" style="width: 535px;">
                                    <span>
                                        <div id="individual_r_title">
                                               <?php print $node_version->title?>
                                        </div>
                                        <div id="individual_r_posted" style="padding-bottom:5px;">
                                            <div><?php print $author_info?>
                                            </div>
                                           </div>
                                       </span>
                                       <?php
                                         $description = $node_version->body;
                                    $description = nl2br(trim($description));
                                    $read_more_url = "javascript:expand_description('individual_r_excerpt_desc_myversion', 'individual_r_content_myversion');";
                                         $excerpt = recipe_utils::get_excerpt($node_version->body, NUMBER_WORDS_DESCRIPTION, $read_more_url);
                                         if ($excerpt == '') {
                                             $excerpt = "&nbsp;";
                                         }

                                    $width = recipe_utils::getImageWidthValue($node_version->field_recipe_image[0]['filepath'], RECIPE_IMAGE_WIDTH);
                                    $style = "float: left;" ;
                                    $bg_width = 530 - ($width + 5);
                                    if ($width){
                                        $style = "float: left;" . " width:" . $bg_width . "px;" ;
                                    ?>
                                    <div id="individual_image_myversion" class="individual_r_image">
                                        <img alt="<?php print $node_version->title?>" src="<?php print C_BASE_PATH . $node_version->field_recipe_image[0]['filepath']?>"  width="<?php print $width?>"/>
                                    </div>
                                    <?php }
                                    else{
                                        if($description != ""){
                                            print '	<div id="individual_image_myversion" class="individual_r_image" style="width:313px;">
                                                    <span>
                                                             <div id="individual_r_excerpt_desc_myversion" style="padding-top:0px;'.$style.'">
                                                            '.nl2br($excerpt).'
                                                        </div>
                                                        <div id="individual_r_content_myversion" style="padding-top:0px;display:none;'.$style.'">
                                                                 '.$description.'
                                                             </div>
                                                         </span>
                                                    </div>';
                                        }
                                    } ?>
                                       <span>
                                        <div id="individual_r_voted" style="padding:5px 0px 8px 0px;<?php print $style;?>">
                                            <div id="myversion_tips_bg01">
                                                <div id="myversion_tips_bg02">Difficulty: <?=$strDifficult;?></div>
                                                <div id="myversion_tips_bg03">
                                                    <div id="myversion_tips_bg03_l">
                                                        <div>Yield: <?php print $yieldValue . " " . $yieldUnit?></div>
                                                        <div>Prep Time: <?php print $preValue . " " . $preUnit?></div>
                                                    </div>
                                                    <div id="myversion_tips_bg03_r">
                                                        <div>Cook Time: <?php print $cookValue . " " . $cookUnit?></div>
                                                        <div>Ready In: <?php print $totalValue . " " . $totalUnit?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                    <?php if ($width){?>
                                         <span>
                                             <div id="individual_r_excerpt_desc_myversion" style="<?php print $style;?>">
                                            <?php print nl2br($excerpt)?>
                                        </div>
                                        <div id="individual_r_content_myversion" style="display:none;<?php print $style;?>">
                                                 <?php print $description; ?>
                                             </div>
                                         </span>
                                         <?php }?>
                                         </div>
                                </div>
                                 <?php }?>

                          <div id="news_p_menu_tip" style="<?php print $display_recipe;?>">
                            <div style="float: left; position: relative; left: 0; z-index: 11">
                                  <img alt="Recipes Tab" src="<?php print C_IMAGE_PATH ?>border/Recipes_tab01.png" width="622" height="51"/>
                            </div>
                            <div style="float: left; position: absolute; left: 148px; top: 0px; z-index: 10">
                                  <div class="reviews_counter" style="left: 83px; top: 6px;"><?php print $comment_count; ?></div>
                                  <span onclick="change_tab(2);"><img alt="Reviews version Tab" src="<? print C_IMAGE_PATH ?>border/Reviews_version_tab02.png" width="161" height="44" style="cursor:pointer;"/></span>
                            </div>
                            <div style="float: left; position: absolute; left: 148px; top: 0px; z-index: 12">
                                <span onclick="change_tab(2);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="161" height="44" style="cursor:pointer;"/></span>
                            </div>
                            <?php if($flag_customize){?>
                            <div style="float: left; position: absolute; left: 302px; top: 0px; width: 156px; height: 50px; z-index: 9">
                                <span onclick="change_tab(3);"><img alt="My version Tab" src="<? print C_IMAGE_PATH ?>border/myversion_tabs002.png" width="164" height="44" style="cursor:pointer;"/></span>
                            </div>
                            <div style="float: left; position: absolute; left: 302px; top: 0px; width: 156px; height: 50px; z-index: 13">
                                <span onclick="change_tab(3);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="164" height="44" style="cursor:pointer;"/></span>
                            </div>
                            <?php }?>
                        </div>
                        <div id="news_p_menu_comment" style="position: relative !important;<?php print $display_review;?>">
                                 <div>
                                  <span onclick="change_tab(1);"><img alt="Recipes Tab" src="<? print C_IMAGE_PATH ?>border/Recipes_tab02.png" width="153" height="44" /></span>
                                <div style="float: left; position: absolute; left: 0px; top: 0px; width: 153px; height: 51px; z-index: 111">
                                    <span onclick="change_tab(1);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="153" height="35" style="cursor:pointer;"/></span>
                                </div>
                                <?php if($flag_customize){?>
                                   <div style="float: left; position: absolute; left: 302px; top: 0px; width: 156px; height: 51px;">
                                          <img alt="My version Tab" src="<? print C_IMAGE_PATH ?>border/myversion_tabs002.png" width="164" height="44" />
                                </div>
                                <div style="float: left; position: absolute; left: 302px; top: 0px; width: 156px; height: 51px; z-index: 112">
                                    <span onclick="change_tab(3);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="164" height="44" style="cursor:pointer;"/></span>
                                </div>
                                <?php }?>
                                <div style="float: left; position: absolute; left: 0px; top: 0px;">
                                    <div style="float:left; position: absolute; height: 30px; left: 231px; top: 6px;" id="reviews_counter_contain"><?php print $comment_count; ?></div>
                                    <img alt="Reviews version Tab" src="<? print C_IMAGE_PATH ?>border/Reviews_version_tab01.png" width="622" height="51" />
                                   </div>
                            </div>
                         </div>
                         <?php if($flag_customize){?>
                         <div id="news_p_menu_myversion" style="position: relative !important;<?php print $display_myversion?>">
                                 <div>
                                  <span onclick="change_tab(1);"><img alt="Recipes Tab" src="<? print C_IMAGE_PATH ?>border/Recipes_tab02.png" width="153" height="44"/></span>
                                <div style="float: left; position: absolute; left: 148px; top: 0px;">
                                    <span onclick="change_tab(2);"><div style="float:left; position: absolute; height: 30px; left: 83px; top: 6px;cursor:pointer;" class="reviews_counter"><?php print $comment_count; ?></div></span>
                                         <span onclick="change_tab(2);"><img alt="Reviews version Tab" src="<? print C_IMAGE_PATH ?>border/Reviews_version_tab02.png" width="161" height="44" style="cursor:pointer;"/></span>
                                   </div>
                                  <div style="float: left; position: absolute; left: 0px; top: 0px; width: 156px; height: 51px;">
                                      <img alt="My version Tab" src="<? print C_IMAGE_PATH ?>border/myversion_tabs001.png" width="622" height="51" />
                                  </div>
                                <div style="float: left; position: absolute; left: 0px; top: 0px;">
                                    <span onclick="change_tab(1);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="153" height="35" style="cursor:pointer;"/></span>
                                </div>
                                <div style="float: left; position: absolute; left: 148px; top: 0px;">
                                     <span onclick="change_tab(2);"><img src="<? print C_IMAGE_PATH ?>space.gif" width="161" height="44"  style="cursor:pointer;"/></span>
                                   </div>
                             </div>
                        </div>
                        <?php }
                        if (($node->field_recipe_type[0]['value'] == 1) || ($node->field_recipe_type[0]['value'] == 0 && $hasPer)){?>
                           <div id="individual_p_b" style="<?php print $display_recipe;?>">
                               <div id="individual_p_content">
                                   <div id="div_recipes_note_contain">
                                      <div id="recipes_note01_contain">
                                           <div id="recipes_note01_t">
                                            <div id="recipes_note_icon01">
                                                <?php if ($user->uid){?>
                                                    <a href="javascript:addFaviroteNode_new('<?php print C_BASE_PATH. "favorite_nodes/checked/node/". $node->nid ?>', '<?php print C_BASE_PATH. "favorite_nodes/add/". $node->nid ?>', '<?php print ADD_RECIPE_INTO_RECIPE_BOX_COMPLETE_INFO;?>', '<?php print ADD_RECIPE_INTO_RECIPE_BOX_ALREADY_COMPLETE_INFO;?>', '<?php print ERR_MSG_ADD_FAVORITE_NODE;?>');">
                                                        <img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" />
                                                    </a>
                                                <?php }else{?>
                                                      <a href="<?php print C_BASE_PATH . $login_url?>">
                                                          <img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" />
                                                      </a>
                                                  <?php }?>
                                            </div>
                                            <div id="recipes_note_icon02">
                                                <?php if ($user->uid){?>
                                                    <a href="<?php print $review_link;?>"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
                                                  <?php }else{
                                                      //print '<div id="icon_contain"><img src="' . C_IMAGE_PATH .'icon/03.gif" width="12" height="12"/></div>Write Review';
                                                      print '<a href="' . C_BASE_PATH . $login_url . '"><img src="' . C_IMAGE_PATH . 'space.gif" width="50" height="55" /></a>';
                                                  }
                                                  ?>
                                            </div>
                                                 <div id="recipes_note_icon03">
                                                     <a href="javascript:mailRecipe('<?php print $node->nid?>','<?php print str_replace("'","\'",$node->title)?>');"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55"/></a>
                                                 </div>
                                        </div>
                                        <div id="recipes_note01_b">
                                            <div id="recipes_note_icon01">
                                                <a href="<?php print C_BASE_PATH . "recipe_print/". $node->nid . "/" . $urlTitle?>" target="_blank"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
                                            </div>
                                              <div id="recipes_note_icon02">
                                                  <?php if ($user->uid){?>
                                                    <a href="javascript:addFaviroteNode_new('<?php print C_BASE_PATH. "favorite_nodes/checked/grocery/". $node->nid ?>', '<?php print C_BASE_PATH. "favorite_nodes/add_my_grocery/all/". $node->nid ?>', '<?php print ADD_ALL_GROCERY_COMPLETE_INFO;?>', '<?php print ADD_ALL_GROCERY_ALREADY_COMPLETE_INFO;?>', '<?php print ERR_MSG_ADD_FAVORITE_NODE;?>')"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
                                                  <?php }else{
                                                      print '<a href="' . C_BASE_PATH . $login_url . '"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55" /></a>';
                                                  }
                                                  ?>
                                              </div>
                                              <div id="recipes_note_icon03">
                                                  <?php
                                                    if ((recipe_utils::getUserRole() == C_ADMIN_USER) || (recipe_utils::getUserRole() == C_PREMIUM_USER)){
                                                        if (recipe_utils::getUserRole() == C_ADMIN_USER || ($node->uid == $user->uid)){
                                                            print '<a href="' . C_BASE_PATH. "recipes/" .$node->nid . "/edit/" . $urlTitle . "?destination=recipe/" . $node->nid .'"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                        }else{
                                                               print '<a href="javascript:checkCustomizeRecipe(\'' . C_BASE_PATH. "favorite_nodes/checked/node/". $node->nid . '\');"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                           }
                                                      }elseif($user->uid){
                                                          print '<a href="javascript:showConfirmBecomePrimium();"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                      }else{
                                                          print '<a href="' . C_BASE_PATH. $login_url. '"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                      }
                                                      ?>
                                              </div>
                                        </div>
                                        <?php
                                               $cur_url = urlencode(C_SITE_URL.$_SERVER["REQUEST_URI"]);
                                               $title = urlencode("Check out The Recipe Diva's recipe for ".$node->title);
                                           ?>
                                        <div id="recipes_note01_s">
                                            <div id="recipes_note_icon04">
                                                <a href="http://twitter.com/home?status=<?php print $title?>%20-%20<?php print $cur_url?>" target="_blank"><img src="<?php print C_IMAGE_PATH?>space.gif" width="80" height="35" /></a>
                                            </div>
                                              <div id="recipes_note_icon05">
                                                  <a href="http://www.facebook.com/share.php?u=<?php print $cur_url?>&t=<?php print $title?>" target="_blank"><img src="<?php print C_IMAGE_PATH?>space.gif" width="80" height="35" /></a>
                                              </div>
                                        </div>
                                    </div>
                                    <div id="recipes_converttool_contain">
                                           <a href="javascript:show_convert_tool()"><img alt="Convert" src="<?php print C_IMAGE_PATH?>icon/ic_convert.jpg"/></a>
                                       </div>
                               </div>

                                <div id="individual_l_col">
                                    <div id="individual_p_title_contain">
                                        <!--<div id="individual_p_title" class="individual_title"><img src="<? print C_IMAGE_PATH ?>label/recipes_ingredient_lbl.gif"/></div>-->
                                        <span id="individual_p_title" class="individual_title"><h2 style="font-size: 24px; font-weight: normal;">Ingredients</h2></span>
                                          <span id="add_all_ingredients">
                                              <?php
                                                      if ($user->uid){
                                                          print '<a href="javascript:addFaviroteNode_new(\'' . C_BASE_PATH . 'favorite_nodes/checked/grocery/'. $node->nid . '\', \'' . C_BASE_PATH . 'favorite_nodes/add_my_grocery/all/'. $node->nid . '\', \''. ADD_ALL_GROCERY_COMPLETE_INFO . '\', \'' . ADD_ALL_GROCERY_ALREADY_COMPLETE_INFO .'\', \'' . ERR_MSG_ADD_FAVORITE_NODE . '\');">' .
                                                                  '<span id="add_all_ingredients_icon">[+] Add all ingredients</span></a>';
                                                      }else{
                                                          print '<a href="' . C_BASE_PATH . $login_url . '">' . '<span id="add_all_ingredients_icon">[+] Add all ingredients</span></a>';
                                                      }

                                              ?>
                                          </span>
                                    </div>
                                    <div class="ingredients_content">
                                        <?php print $strIngredients?>
                                    </div>
                                  </div>
                            </div>
                             <div id="divatiptab_content" class="bg-none">
                                <!--<div id="recipe_method_title"><img src="<? print C_IMAGE_PATH ?>label/recipes_method_lbl.gif"/></div>-->
                                <div id="recipe_method_title"><h2 style="font-size: 24px; font-weight: normal;">Method</h2></div>
                                <div id="divatiptab_content_inner" class="instructions">
                                  <?php print $node->field_preparation[0]['value']?>
                                </div>
                            </div>
                        </div>
                        <?php }else{?>
                        <div id="individual_p_b" style="padding:8px 0px 0px 0px;<?php print $display_recipe;?>">
                            <div id="shop_recipe_premium">
                                  <div class="left_col">
                                       <a href="<?php print C_BASE_PATH . "shoprecipes"?>">
                                        <img alt="Shop recipe premium" src="<? print C_IMAGE_PATH ?>bg/shop_recipe_premium.gif" width="366" height="191" />
                                    </a>
                              </div>
                                <div class="right_col">
                                         <div  class="price_panel">
                                            <div class="price_text">
                                                <?php
                                                      print "$".$node->field_recipe_price[0]['value'];
                                              ?>
                                        </div>
                                        <div class="click_to_continue">
                                               <a href="<?php print C_BASE_PATH . "shoprecipe/order/". $node->nid . "/" . $urlTitle?>">
                                                <img alt="Click here to continue" src="<? print C_IMAGE_PATH ?>/label/click_here_to_continue_lbl.gif" width="118" height="18" />
                                            </a>
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <?php }?>

                        <?php if($flag_customize){

                            $urlTitle = recipe_utils::removeWhiteSpace($node_version->title);

                            $arrGroceries = load_grocery_by_recipe($node_version->nid);
                            $strIngredients = nl2br($node_version->field_ingredients[0][value]);
                            $strIngredients = $node_version->field_ingredients[0][value];
                            $strIngredientsNew = '';
                            $arrIngredients = explode(BREAK_LINE , $strIngredients);

                            if (is_array($arrIngredients))
                            {
                                $intCount = 1;
                                foreach($arrIngredients as $strIngItem)
                                {
                                    $arrGroceryItem = array();
                                    $strIngItemNew = strip_tags($strIngItem);
                                    $strReplace = $strIngItem;//recipe_utils::convertLinkToURL($strIngItem, $strIngItemNew);
                                    $is_ingredient = false;
                                    foreach ($arrGroceries as $objGrocery)
                                    {
                                        if (trim($objGrocery->title)==trim($strIngItemNew))
                                        {
                                            $is_ingredient = true;
                                            $strIngredientsNew .= $strReplace;
                                            $strIngredientsNew .= '&nbsp;<span class="individual_plus" id="individual_plus_' . $intCount . '">';
                                            if ($user->uid){
                                                $strIngredientsNew .= '<a href="javascript:addFaviroteNode_new(\'' . C_BASE_PATH . 'favorite_nodes/checked/grocery/'. $objGrocery->nid . '\', \'' . C_BASE_PATH . 'favorite_nodes/add_my_grocery/single/'. $objGrocery->nid . '\', \'' . ADD_GROCERY_COMPLETE_INFO . '\', \'' . ADD_GROCERY_ALREADY_COMPLETE_INFO . '\', \'' . ERR_MSG_ADD_FAVORITE_NODE . '\');"> [+]</a>';
                                                $intCount++;
                                            }else{
                                                $strIngredientsNew .= '<a href="' . C_BASE_PATH . $login_url . '">[+]</a>';
                                            }

                                            $strIngredientsNew .= '</span><br>';
                                            break;
                                        }
                                    }
                                    if(!$is_ingredient){
                                        $strIngredientsNew .= $strReplace . '<br>';
                                    }
                                }
                            }
                            $strIngredients = $strIngredientsNew;

                        ?>
                        <div id="recipe_myversion_p_b" style="<?php print $display_myversion;?>">
                               <div id="individual_p_content">
                                   <div id="div_recipes_note_contain">
                                      <div id="recipes_note02_contain">
                                           <div id="recipes_note01_t">
                                              <div id="recipes_note02_icon1">
                                                  <?php
                                                if ((recipe_utils::getUserRole() == C_ADMIN_USER) || (recipe_utils::getUserRole() == C_PREMIUM_USER)){
                                                    if (recipe_utils::getUserRole() == C_ADMIN_USER || ($node_version->uid == $user->uid)){
                                                        print '<a href="' . C_BASE_PATH. "recipes/" .$node_version->nid . "/edit/" . $urlTitle . "?destination=recipe/" . $node_version->nid .'"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                    }else{
                                                           print '<a href="javascript:checkCustomizeRecipe(\'' . C_BASE_PATH. "favorite_nodes/checked/node/". $node_version->nid . '\');"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                       }
                                                  }elseif($user->uid){
                                                      print '<a href="javascript:showConfirmBecomePrimium();"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                  }else{
                                                      print '<a href="' . C_BASE_PATH. $login_url. '"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55"/></a>';
                                                  }
                                                  ?>
                                              </div>
                                              <div id="recipes_note02_icon2">
                                                <a href="javascript:mailRecipe('<?php print $node_version->nid?>','<?php print str_replace("'","\'",$node_version->title)?>');"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
                                            </div>
                                           </div>

                                           <div id="recipes_note01_b">
                                               <div id="recipes_note02_icon1">
                                                   <?php if ($user->uid){?>
                                                    <a href="javascript:addFaviroteNode_new('<?php print C_BASE_PATH. "favorite_nodes/checked/grocery/". $node_version->nid ?>', '<?php print C_BASE_PATH. "favorite_nodes/add_my_grocery/all/". $node_version->nid ?>', '<?php print ADD_ALL_GROCERY_COMPLETE_INFO;?>', '<?php print ADD_ALL_GROCERY_ALREADY_COMPLETE_INFO;?>', '<?php print ERR_MSG_ADD_FAVORITE_NODE;?>')"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
                                                  <?php }else{
                                                      print '<a href="' . C_BASE_PATH . $login_url . '"><img src="' . C_IMAGE_PATH .'space.gif" width="50" height="55" /></a>';
                                                  }
                                                  ?>
                                               </div>
                                            <div id="recipes_note02_icon2">
                                                <a href="<?php print C_BASE_PATH . "recipe_print/". $node_version->nid . "/" . $urlTitle?>" target="_blank"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
                                            </div>
                                           </div>
                                       </div>
                                      <div id="recipes_converttool_contain">
                                           <a href="javascript:show_convert_tool()"><img alt="Convert" src="<?php print C_IMAGE_PATH?>icon/ic_convert.jpg"/></a>
                                       </div>
                               </div>
                                <div id="individual_l_col">
                                    <div id="individual_p_title_contain">
                                        <!--<div id="individual_p_title" class="individual_title"><img src="<? print C_IMAGE_PATH ?>label/recipes_ingredient_lbl.gif"/></div>-->
                                        <span id="individual_p_title" class="individual_title">Ingredients</span>
                                        <span id="add_all_ingredients">
                                              <?php
                                              if ($user->uid){
                                                  print '<a href="javascript:addFaviroteNode_new(\'' . C_BASE_PATH . 'favorite_nodes/checked/grocery/'. $node_version->nid . '\', \'' . C_BASE_PATH . 'favorite_nodes/add_my_grocery/all/'. $node_version->nid . '\', \''. ADD_ALL_GROCERY_COMPLETE_INFO . '\', \'' . ADD_ALL_GROCERY_ALREADY_COMPLETE_INFO .'\', \'' . ERR_MSG_ADD_FAVORITE_NODE . '\');">' .
                                                          '<span id="add_all_ingredients_icon">[+] Add all ingredients</span></a>';
                                              }else{
                                                  print '<a href="' . C_BASE_PATH . $login_url . '">' . '<span id="add_all_ingredients_icon">[+] Add all ingredients</span></a>';
                                              }
                                              ?>
                                          </span>
                                      </div>
                                    <div class="ingredients_content">
                                        <?php print $strIngredients?>
                                    </div>
                                  </div>
                             </div>
                             <div id="divatiptab_content" class="bg-none">
                                <!--<div id="recipe_method_title"><img src="<? print C_IMAGE_PATH ?>label/recipes_method_lbl.gif"/></div>-->
                                <div id="recipe_method_title">Method</div>
                                <div id="divatiptab_content_inner">
                                  <?php print $node_version->field_preparation[0]['value']?>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                        <div id="div_comment" style="margin-top:10px;<?php print $display_review; ?>;" class="content_tip">
                            <div id="comment_list" style="<?php print $display_comment_lst; ?>">
                            <?php print $strComment;?>
                            </div>
                            <div id="comment_form" style="<?php print $display_form; ?>">
                                <?php print $err_mess; ?>
                                <?php print $preview_form ?>
                                <div id="divatips_comment_content_contain">
                                    <span class="recipes_upload_lbl"><img alt="Write preview" src="<? print C_IMAGE_PATH ?>label/writereview_lbl.gif"/></span>
                                </div>
                                <div id="divatips_comment_content_contain">
                                    <div id="newtopic_title">Your name:</div>
                                    <div id="newtopic_name"><?php print recipe_utils::create_author_link($user->name, $user->name, 'newtopic_name');; ?></div>
                                </div>
                                <div id="divatips_comment_content_contain">
                                    <div id="newtopic_title">Rate this recipe:<span class="newtopic_title_red">*</span></div>
                                </div>
                                <?php print $comment_form; ?>
                            </div>
                        </div>
                    </div>
                 </div>
              </div>
        </div>
    <?php
    }else{
        drupal_set_title("Data not found");
    ?>
        <div id="news_p">
            <div id="news_p_b">
                <div id="news_border">
                    <div id="news_p_content" class="news_p_content_font" style="text-align:center;">
                        <?php print NOT_AUTHORIZED_TO_ACCESS_PAGE;?>
                    </div>
                 </div>
              </div>
        </div>
    <?php }
    include(".".C_BASE_PATH.'sites/all/themes/recipe_diva/recipe_mail.php');
    include('convert-tool.php');
    ?>

 <?php if ($node->field_recipe_type[0]['value'] == 0){?>

<SCRIPT LANGUAGE="JavaScript">
<!--
hp_ok=true;
function hp_d00(s)
{
  if(!hp_ok)return;
  document.write(s)
}
//-->
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function hp_ne()
{
  return true
}
onerror=hp_ne;
function hp_dn(a)
{
  return false
}
function hp_cm()
{
  alert("Copyright Alert!");
  return false
}
function hp_de(e)
{
  return(e.target.tagName!=null&&e.target.tagName.search('^(INPUT|TEXTAREA|BUTTON|SELECT)$')!=-1)
};
function hp_md(e)
{
  mac=navigator.userAgent.indexOf('Mac')!=-1;
  if(document.all)
  {
    if(event.button==2||(mac&&(event.ctrlKey||event.keyCode==91)))
    {
      alert("Copyright Alert!");
      return(false)
    }
  }
  else
  {
    if(e.which==3||(mac&&(e.modifiers==2||e.ctrlKey)))
    {
      alert("Copyright Alert!");
      return false
    }
    else if(e.which==1)
    {
      window.captureEvents(Event.MOUSEMOVE);
      window.onmousemove=hp_dn
    }
  }
}
function hp_mu(e)
{
  if(e.which==1)
  {
    window.releaseEvents(Event.MOUSEMOVE);
    window.onmousemove=null
  }
}
if(navigator.appName.indexOf('Internet Explorer')==-1||(navigator.userAgent.indexOf('MSIE')!=-1&&document.all.length!=0))
{
  if(document.all)
  {
    mac=navigator.userAgent.indexOf('Mac')!=-1;
    version=parseFloat('0'+navigator.userAgent.substr(navigator.userAgent.indexOf('MSIE')+5),10);
    if(!mac&&version>4)
    {
      document.oncontextmenu=hp_cm
    }
    else
    {
      document.onmousedown=hp_md;
      document.onkeydown=hp_md;
    }
    document.onselectstart=hp_dn
  }
  else if(document.layers)
  {
    window.captureEvents(Event.MOUSEDOWN|Event.modifiers|Event.KEYDOWN|Event.MOUSEUP);
    window.onmousedown=hp_md;window.onkeydown=hp_md;
    window.onmouseup=hp_mu
  }
  else if(document.getElementById&&!document.all)
  {
    document.oncontextmenu=hp_cm;document.onmousedown=hp_de
  }
}
function hp_dp1()
{
  for(i=0;i<document.all.length;i++)
  {
    if(document.all[i].style.visibility!="hidden")
    {
      document.all[i].style.visibility="hidden";document.all[i].id="hp_id"
    }
  }
};
function hp_dp2()
{
  for(i=0;i<document.all.length;i++)
  {
    if(document.all[i].id=="hp_id")document.all[i].style.visibility=""
  }
};
window.onbeforeprint=hp_dp1;
window.onafterprint=hp_dp2;
document.write('<style type="text/css" media="print"><!--body{display:none}--></style>');
function hp_dc()
{
  hp_ta.createTextRange().execCommand("Copy");
  setTimeout("hp_dc()",300)
}
if(navigator.appName.indexOf('Internet Explorer')==-1||(navigator.userAgent.indexOf('MSIE')!=-1&&document.all.length!=0))
{
  if(document.all&&navigator.userAgent.indexOf('Opera')==-1)
  {
    document.write('<div style="position:absolute;left:-1000px;top:-1000px"><input type="textarea" name="hp_ta" value=" " style="visibility:hidden"></div>');
    hp_dc()
  }
}
function hp_ndd()
{
  return false
}
document.ondragstart=hp_ndd;
//-->
</SCRIPT>

<?php }?>
