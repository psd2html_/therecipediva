<?php
// $Id: recipes-page.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-page.tpl.php
 * Theme implementation to display list of recipes.
 *
 * Available variables:
 * - recipes: The list of recipes
 *
 * @see theme_recipes_page()
 */

$title =  recipe_db::get_admin_enter_text(RECIPES_PAGE);
print (empty($title)? "": '<div class="registration_p_title">' . $title . '</div>');

$arrCat = null;
$intCount = 0;
foreach ($recipes as $objCat){
    if ($objCat['totalItem'] > 0){
        $arrCat[] = $objCat['tid'];
        $intCount ++;

?>
    <div id="Ingredients" style="margin-bottom:13px;">
     <!--<div id="recipes_title_contain">
        <div id="recipes_titles"><img height="21" width="94" src="<? print C_IMAGE_PATH ?>label/<?php print $objCat['name']?>_title_lbl.gif"></div>
     </div>-->
    <div style="float:left;position: relative; height: 44px;">
        <h1><img alt="<?php print $objCat['name']?>" src="<?php print C_BASE_PATH . $objCat['image_path_top']?>" /></h1>
    </div>
    <div id="Ingredients_b">
        <div id="Ingredients_content">
             <div id="Ingredients_content_inner">
                 <?php
                    $intCountSubCat = 0;
                    $arrSubcat = $objCat['items'];

                    foreach ($arrSubcat as $objSubCat){
                        if ($intCountSubCat == RECIPES_NUMBER_SUBCAT_PER_CATEGORY){
                            print '<span id="category_id_'. $objCat['tid'] .'" style="display:none;">';
                        }
                        $url_add = filter(1, encodeURIComponent($objSubCat['name']));
                ?>
                     <div id="img_container">
                            <div style="min-height: 75px;padding-top:6px;">
                            <?php
                            $width = recipe_utils::getImageWidthValue($objSubCat['image'],SUBCATE_IMAGE_WIDTH);
                            if ($width > 0){?>
                            <a href="<?php print $url_add ?>">
                                <img width="87" alt="<?php print $objSubCat['name']?>" src="<?php print C_BASE_PATH . $objSubCat['image']?>"/></a>
                            <?php }?>
                            </div>
                           <a href="<?php print $url_add ?>"><div id="img_title"><?php print $objSubCat['name'];?></div></a>
                    </div>
                <?php
                    $intCountSubCat++;
                }
                if ($intCountSubCat > RECIPES_NUMBER_SUBCAT_PER_CATEGORY){
                    print '</span>';
                }
                ?>

               </div>
               <div id="img_seeall_<?php print $objCat['tid'];?>" class="img_seeall">
                 <!--<div id="show_all_<?php print $objCat['tid'];?>" class="seeall_txt" onclick="show_all(<?php print $objCat['tid'];?>);">See all <?php print $objCat['plural_name']?></div>-->
                 <div id="show_all_<?php print $objCat['tid'];?>" class="seeall_txt" onclick="show_all(<?php print $objCat['tid'];?>);"><img alt="See all <?php print $objCat['name'];?>" src="<?php print C_BASE_PATH . $objCat['image_path_see']?>"/></div>
                 <div id="seeall_btn">
                     <input type="image" id="img_show_all_<?php print $objCat['tid'];?>" onclick="show_all(<?php print $objCat['tid'];?>);" src="<?php print C_IMAGE_PATH;?>button/seeall_btn.gif" width="19" height="19" />
                </div>
             </div>
             <div id="img_collapseall_<?php print $objCat['tid'];?>" class="img_collapseall"  style="display:none;">
                 <!--<div id="collapse_all_<?php print $objCat['tid'];?>" class="seeall_txt" onclick="collapse_all(<?php print $objCat['tid'];?>);">Collapse <?php print $objCat['plural_name']?> list</div>-->
                 <div id="collapse_all_<?php print $objCat['tid'];?>" class="seeall_txt" onclick="collapse_all(<?php print $objCat['tid'];?>);"><img src="<?php print C_BASE_PATH . $objCat['image_path_collapse']?>"/></div>
                <div id="collapseall_btn">
                     <input type="image" id="img_collapse_all_<?php print $objCat['tid'];?>" onclick="collapse_all(<?php print $objCat['tid'];?>);" src="<?php print C_IMAGE_PATH;?>button/seeall_btn.gif" width="19" height="19" />
                </div>
             </div>
        </div>
      </div>
  </div>
<?php
    }
}
if($intCount <= 0){?>

    <div id="recipes_title_contain">
        <div id="recipes_titles">&nbsp;</div>
     </div>
    <div id="Ingredients">
        <div id="Ingredients_content">
             <div id="Ingredients_content_inner">
                Data not found.
             </div>
        </div>
    </div>
<?php }?>
<form action="<?php print C_BASE_PATH?>recipes"  accept-charset="UTF-8" method="post" id="search-sort-form" name="search_sort_form">
    <input type="hidden" name="hdf_tid" id="edit-hdf-tid" value=""  />
    <input type="hidden" name="hdf_type" id="edit-hdf-type" value="1"  />
    <input type="hidden" name="page" id="edit-page" value=""  />
</form>
<script>
        var arrCatogery = new Array();
        <?php
            foreach ($arrCat as $key=>$value){
                print "arrCatogery[$key] = $value;";
                print "\n";
        }?>
        function show_all(category_id) {
            if (arrCatogery == null){
                return;
            }
            var length = arrCatogery.length;
            var showAll;
            var objShow;
            var objCollapse;

            for(var i=0; i<length;i++){

                objShow = document.getElementById("img_seeall_" + arrCatogery[i]);
                objCollapse = document.getElementById("img_collapseall_" + arrCatogery[i]);

                showAll = document.getElementById("category_id_" + arrCatogery[i]);

                if (arrCatogery[i] == category_id){
                    objShow.style.display = "none";
                    objCollapse.style.display = "";

                    if (showAll != null){
                        showAll.style.display = "";
                    }
                }else{
                    objShow.style.display = "";
                    objCollapse.style.display = "none";

                    if (showAll != null){
                        showAll.style.display = "none";
                    }
                }
            }
        }

        function collapse_all(category_id) {
            if (arrCatogery == null){
                return;
            }
            var length = arrCatogery.length;
            var showAll = null;
            var objShow;
            var objCollapse;

            for(var i=0; i<length;i++){
                objShow = document.getElementById("img_seeall_" + arrCatogery[i]);
                objCollapse = document.getElementById("img_collapseall_" + arrCatogery[i]);
                objBlank = document.getElementById("image_space_" + arrCatogery[i]);
                showAll = document.getElementById("category_id_" + arrCatogery[i]);

                if (arrCatogery[i] == category_id){
                    objShow.style.display = "";
                    objCollapse.style.display = "none";
                    if (showAll != null){
                        showAll.style.display = "none";
                    }
                    break;
                }
            }
        }

    </script>
