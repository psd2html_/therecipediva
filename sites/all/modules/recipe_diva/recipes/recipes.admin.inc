<?php
// $Id: recipes.admin.inc, 2010/06/09 01:31:26 drumm Exp $

/**
 * Get recipes list base on term
 * 
 * @return recipes list in HTML format
 */
	function recipes_list_admin_page(){
		if (sizeof($_POST)>0){
			recipes_admin_save_form();
		}
		$keyword_search = $_GET['txtSearch'];
		
		$header = array(array('data' => t('Title'), 'field' => 'n.title'),
						array('data' => t('Created'), 'field' => 'n.created'),
					    array('data' => t('Difficulty'), 'field' => 'difficulty'),
					    array('data' => t('Status'), 'field' => 'content_status'));
		$order = $_GET['order'];
		
		// Init query param
		recipe_utils::init_page_param();
		$strSql = " SELECT DISTINCT(n.nid)";               
		$strSql .= "  	, n.title";
		$strSql .= "  	, n.created as created";
		$strSql .= "  	, CASE WHEN c.field_difficulty_level_value = " . EASY_LEVEL_VALUE;
		$strSql .= "  					THEN '" . EASY_LEVEL_TEXT ."' ";
		$strSql .= "  	       WHEN c.field_difficulty_level_value = " . MODERATE_LEVEL_VALUE;
		$strSql .= "  					THEN '" . MODERATE_LEVEL_TEXT ."' ELSE '" . DIFFICULT_LEVEL_TEXT . "' END difficulty";
		//$strSql .= "  	, c.field_difficulty_level_value as difficulty";
		$strSql .= "  	, c.field_recipe_type_value as type_recipe";
		$strSql .= "  	, CASE WHEN c.field_content_status_value = " . SUBMITTED_STATUS;
		$strSql .= "  					THEN '" . SUBMITTED_STATUS_VALUE ."' ELSE '" . APPROVED_STATUS_VALUE . "' END content_status";
		//$strSql .= "  	, c.field_content_status_value as content_status";
		$strSql .= "  	, c.field_top_status_value as top_status";
		$strSql .= "  	, u.uid";
		$strSql .= "  	, u.name user_name";
		$strSql .= "  FROM node n";
		$strSql .= "  	INNER JOIN content_type_recipe c ON c.nid = n.nid";
		$strSql .= "  	LEFT JOIN users u ON u.uid = n.uid";	
		$strWhere = " WHERE n.type = 'recipe'";
		$strWhere .= " AND c.field_recipe_original_value = 0 ";
		if (recipe_utils::isNull($keyword_search) == false) {
			$arrField[] = "n.title";
			$strWhereKeyWord = recipe_utils::get_condition_search_by_keyword($keyword_search, $arrField, $args);
			$strWhere .= recipe_utils::isNull($strWhereKeyWord)? "": " AND (" . $strWhereKeyWord . ")";
		}
		$strSql .= $strWhere;
		
		if($order == ""){
			$strSql .= " ORDER BY created DESC";	
		}else{
			$strSql .= tablesort_sql($header);
		}
		
		$strSqlCount = " SELECT count(*) FROM (" . $strSql. ") AS search_tbl";
		$result = pager_query($strSql, ADMIN_RECIPE_PER_PAGE, 0, $strSqlCount, $args);
		
		while ($item = db_fetch_object($result)){
			$recipes[] = array('nid' => $item->nid
								, 'recipe_title' => $item->title
								, 'created' => $item->created
								, 'uid' => $item->uid
								, 'postuser' => $item->user_name
								, 'difficulty' => $item->difficulty
								, 'type_recipe' => $item->type_recipe
								, 'content_status' => $item->content_status
								, 'top_status' => $item->top_status
								);
		}
		
		$header = admin_recipes_create_header();	
		//set pager
		$strUrlGet = "page=%page%";
		$strUrlGet .= recipe_utils::get_url_adminrecipe();
		
		$url = C_BASE_PATH . "adminrecipe/recipes" . ($strUrlGet == ""? "": "?" . $strUrlGet);
		$paging = number_paging(ADMIN_RECIPE_PER_PAGE, 0, 1, 'frm_recipe_admin', $url);
		$action = "adminrecipe/recipes";
		$search_form = drupal_get_form('admin_search_form', $action);
		return theme('recipes_admin_list', $recipes, $paging, $search_form, $header);	
	}

	/**
	 * Execute recipe deletion
	 */
	function recipes_admin_save_form() {
		if ($_POST['op'] == 'Delete'){
			foreach($_POST as $key => $value){
				$pos = strpos($key, "recipe_del");
				if ($pos !== false){
					if (!empty($value)){
						node_delete($value);
						delete_grocery_by_recipe($value);
					}
				}
			}
		}elseif($_POST['op'] == 'DeleteItem'){
			$value = $_POST['delId'];
			if (!empty($value)){
				node_delete($value);
				delete_grocery_by_recipe($value);
			}
		}/*elseif($_POST['hdf_type'] == 'update'){
			foreach($_POST as $key => $value){
				$pos = strpos($key, "hdf_recipe_nid");
				
				if ($pos !== false){
					$nid = $value;
					$field_top_status_value = $_POST['chk_top_status_'. $nid] == 'on'? 1 : 0;
					$field_status_value = $_POST['cbb_status_'. $nid];
					
					$strSql = "  UPDATE content_type_recipe ";
					$strSql .= "  SET field_content_status_value = %d";
					$strSql .= "  , field_top_status_value = %d";
					$strSql .= "  WHERE nid = %d";
					$result = db_query($strSql, $field_status_value, $field_top_status_value, $nid);	
				}
			}
		}*/
	 }
	
	/**
	 * Edit the recipes
	 * 
	 * @param $node
	 * @return html 
	 */
	function recipe_edit_admin_page($node = null) {
		if (arg(0) == "adminrecipe" && arg(3)== "edit"){
			drupal_add_css(C_CSS_PATH.'jquery-ui.css');
	  		drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
	  		drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');	  		  		
		}
		drupal_add_js(C_SCRIPT_PATH.'jquery.form.js');
	  	drupal_set_title(check_plain($node->title));
	  	return theme('recipes_admin_edit', $node);	
	}
	
	/**
	 * Get Groceries
	 * 
	 * @param $node
	 * @return html 
	 */
	function recipe_grocery_admin_page($node){
		drupal_add_js(C_SCRIPT_PATH.'jquery.form.js');
		drupal_add_css(C_CSS_PATH.'jquery-ui.css');
	  	drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
	  	drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');
	  	
	  	//parse ingredients.
	  	//$ingredients = $node->field_ingredients[0]['value'];
	  	//$groceries = explode("\r" , $ingredients);
	  	
		global $user;		
		$unit = recipe_db::load_units_db('recipe_unit');
		$aisle = recipe_db::load_master_db('recipe_aisle','array', true);
		$groceries = load_grocery_by_recipe($node->nid);
		return theme('recipes_admin_grocery', $node, $groceries, $unit, $aisle);	
	}
	
	/**
	 * Delete Grocery
	 * @return: status
	 * */
	function admin_recipes_grocery_delete($nid) {
		$result = array();
		$result['status'] = "error";
		
		node_delete($nid);
		
		$result['status'] = "success";	
		echo drupal_json($result);
	    module_invoke_all('exit');
	    exit;
	}
	
	/**
	 * Update All Grocery
	 * @return: status
	 * */
	function admin_recipes_grocery_save() {
		$result = array();
		$result['status'] = "error";
		if (isset($_POST['hidListnid'])){
			global $user;
			$listNid = split(',',$_POST['hidListnid']);
			if (count($listNid) > 0){
				foreach ($listNid as $nid) { 
					$node = node_load($nid);
					$node->field_groceries_aisle[0]["value"] = $_POST['ddlAisle_'.$nid];
					node_save($node);
				}
			}
		}
		drupal_goto('adminrecipe/recipes');
	}
	
	/**
	 * Add Grocery
	 * @return: status
	 * */
	function admin_recipes_grocery_add($nid) {
		$result = array();
		$result['nid'] = 0;
		$date = date();
		global $user;
		module_load_include('inc', 'node', 'node.pages');
		$node = array('type' => CONTENT_TYPE_RECIPE_GROCERIES, 'name' => $user->name);
		$form_state = array();
		
		$form_state['values']['type'] = CONTENT_TYPE_RECIPE_GROCERIES;
		$form_state['values']['status'] = 1; 
		$form_state['values']['title'] = trim($_POST['txtTitle']);
		$form_state['values']['body'] = '';
	
		$form_state['values']['field_groceries_recipeid'][0]['value'] = $nid;
		$form_state['values']['field_groceries_quantity'][0]['value'] = trim($_POST['txtQuantity']);	
		$form_state['values']['field_groceries_aisle'][0]['value'] = $_POST['ddlAisle'];	
		$form_state['values']['field_groceries_unit'][0]['value'] = $_POST['ddlUnit'];
		$form_state['values']['op'] = t('Save');
		$errs  = drupal_execute(CONTENT_TYPE_RECIPE_GROCERIES.'_node_form', $form_state,(object)$node);
	    if (count($errs) > 0) {
	    	$result['status'] = "error";
	    }
	    else{
	    	$result['nid'] = $form_state['nid'];	
	    	$result['status'] = "success";
		}
			
	    echo drupal_json($result);
	    module_invoke_all('exit');
	    exit;
	}
	
	/**
	 * create header of recipe list
	 * @return: header
	 * */
	function admin_recipes_create_header(){
		$recipes_list_header = array(   NULL,
									    array('data' => t('Title'), 'field' => 'n.title'),
									    array('data' => t('Created'), 'field' => 'Created'),
									    array('data' => t('Difficulty'), 'field' => 'difficulty'),
									    array('data' => t('Status'), 'field' => 'content_status'),
									    array('data' => t('Aisle'), 'field' => ''),
									    array('data' => t('Operations'), 'field' => ''),
									  );
		$str_header = "";
		$header = array();
	  	// Create the tablesorting header.
	  	$ts = tablesort_init($recipes_list_header);
	  	$i = 0;
	  	
	  	$form_name = "frm_recipe_admin";
	  	$url =  C_BASE_PATH . "adminrecipe/recipes";
	  	$url .=  "?page=" . ($_GET['page']? $_GET['page']: 0);
//	  	$keyword_search =  rawurlencode($_GET['txtSearch']);
//	  	$url .= $_GET['txtSearch']? "&txtSearch=" . $keyword_search : "";
	  	
	  	foreach ($recipes_list_header as $cell) {
		    $cell = recipe_utils::create_cell_header($cell, $recipes_list_header, $ts, $form_name,$url);
	    	$header[$i] = $cell['data'];
	    	$i ++;
	  	}
	  	
	  	$str_header .= '<div id="admin_recipes_col_del" class="admin_title"><input type="checkbox" id="chk_all" name="chk_all" onclick="checkAll1(document.frm_recipe_admin, \'recipe_del\', document.frm_recipe_admin.chk_all);"/></div>';
	  	$str_header .= '<div id="admin_recipes_col_01" class="admin_title">'.$header[1] . '</div>';
	  	$str_header .= '<div id="admin_recipes_col02" class="admin_title">'.$header[2] . '</div>';
	  	$str_header .= '<div id="admin_recipes_col03" class="admin_title">'.$header[3] . '</div>';
	  	$str_header .= '<div id="admin_recipes_col05" class="admin_title">'.$header[4] . '</div>';
	  	$str_header .= '<!--<div id="admin_recipes_col06" class="admin_title">'.$header[5] . '</div>-->';
	  	$str_header .= '<div id="admin_recipes_col07" class="admin_title">'.$header[6] . '</div>';
	  	
	  	return $str_header;
  	}
?>