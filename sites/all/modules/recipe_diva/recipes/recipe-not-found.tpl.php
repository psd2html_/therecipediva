<div style="padding: 20px 0 0 0;">
    Please try again using the suggestions below, or begin a new search.
</div>
<div>
    <img src="<? print C_IMAGE_PATH ?>dot_black.gif"/>Check your spelling
</div>
<div>
    <img src="<? print C_IMAGE_PATH ?>dot_black.gif"/>Try more general words
</div>
<div>
    <img src="<? print C_IMAGE_PATH ?>dot_black.gif"/>Use less keywords
</div>
<div style="padding: 20px 0 0 0;">
    Other suggestions:
</div>
<div>
    <img src="<? print C_IMAGE_PATH ?>dot_black.gif"/>Browse <a href="<?echo C_BASE_PATH."recipes"?>">all recipes</a>
</div>
<div>
    <img src="<? print C_IMAGE_PATH ?>dot_black.gif"/>For a more specific search, use the <a href="<?echo C_BASE_PATH."advanced"?>">advanced Search</a>
</div>
<div>
    <img src="<? print C_IMAGE_PATH ?>dot_black.gif"/>Search by <a href="<?echo C_BASE_PATH."ingredients"?>">ingredient</a>
</div>
