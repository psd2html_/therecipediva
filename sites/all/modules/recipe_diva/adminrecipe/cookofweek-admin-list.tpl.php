<?php
// $Id: recipes-admin-list.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-admin-list.tpl.php
 * Theme implementation to display list of recipes.
 *
 * Available variables:
 * - recipes: The list of recipes
 * - paging
 * - cat: Catergory
 *
 * @see theme_recipes_list()
 */
     if (arg(1) == "units"){
         $title = "Units";
     }else{
         $title = "Aisles";
     }
?>
<div id="admin_border_content">
    <form name="frm_cookofweek_admin" id="frm_cookofweek_admin" accept-charset="UTF-8" method="post" onSubmit="searchData('<?php print C_BASE_PATH;?>adminrecipe/cookofweek', 'txtSearch');return false;">
        <div id="admin_recipe_content">
            <div class="admin_p_title">Cook of Week Management</div>
            <div id="admin_divatips_search_form">
                <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
                <div id="searchfor_input">
                    <input type="text" name="txtSearch" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
                 </div>
                <div class="archive_search_btn">
                    <input type="button" name="btnSearch" value="" class="bt_search" onClick="searchData('<?php print C_BASE_PATH;?>adminrecipe/cookofweek', 'txtSearch');"/>
                </div>
            </div>
            <div id="admin_recipes_content_title">
                <?php print $header;?>
                </div>
        <?php
              if (sizeof($cookofweeks)>0){
              $icount = 0;
              $query_back =  "?page=" . ($_GET['page']? $_GET['page']: 0);
              $query_back .= ($_GET['txtSearch']? "&txtSearch=" .  $_GET['txtSearch']: "");
            $query_back .= recipe_utils::get_url_adminrecipe();
              foreach ($cookofweeks as $objCOW){
                  if ($icount%2 == 0){
                      $classrow = "admin_recipes_content_inner01";
                  }else{
                      $classrow = "admin_recipes_content_inner";
                  }
                  $icount++;
                  ?>

              <div id="<?php print $classrow;?>">
                  <div id="admin_recipes_col_del"><input id="cookofweek_del_<?php print $objCOW->uid?>" name="cookofweek_del_<?php print $objCOW->uid?>" type="checkbox" value="<?php print $objCOW->uid?>" /></div>
                  <div id="admin_recipes_col01"><a href="<?php print C_BASE_PATH . "adminrecipe/cookofweek/edit/" . $objCOW->uid . $query_back?>"><?php print $objCOW->name?></a></div>
                  <div id="admin_recipes_col03"><?php print $objCOW->cook_of_week;?></div>
                  <div id="admin_recipes_col04" style="width:250px;text-align:left;padding-left:12px;"><?php print $objCOW->content;?></div>
                  <div id="admin_recipes_col07"><a href="javascript:deleteAdminItem('frm_cookofweek_admin', '', '<?php print $objCOW->uid?>', '<?php print CONFIRM_MSG_DELETE_ITEM;?>');">Delete</a></div>
              </div>
          <?}
          }?>
          </div>
          <div id="admin_recipe_content">
            <img src="<? print C_IMAGE_PATH ?>space.gif" height="10" width="1" />
        </div>
        <div id="admin_recipe_content">
            <input type="hidden" name="op" value="">
              <input type="hidden" name="delId" value="">
              <input type="hidden" name="page" value="<?php print $_POST['page']; ?>">
            <input type="button" name="delete" id="delete" onclick="return deleteAdminList('frm_cookofweek_admin', 'cookofweek_del_', '', '<?php print CONFIRM_MSG_DELETE_ITEMS;?>');"  class="admin_bt_delete" />
            <input type="button" name="add" id="add" value="" class="admin_bt_add" onclick="submitForm('frm_cookofweek_admin', '<?php print C_BASE_PATH."adminrecipe/cookofweek/add?destination=adminrecipe/cookofweek/"; ?>');"/>
        </div>
        <?php print $paging;?>
    </form>
 </div>









