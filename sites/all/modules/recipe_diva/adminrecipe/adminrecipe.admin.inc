<?php
// $Id: recipes.admin.inc, 2010/06/09 01:31:26 drumm Exp $

/**
 * Get recipes list base on term
 *
 * @return recipes list in HTML format
 */
    function adminrecipe_import_page(){
        return drupal_get_form('adminrecipe_import_form');
    }

    function adminrecipe_import_newsletter(){
        return drupal_get_form('adminrecipe_import_newsletter_form');
    }

    /**
     * Implementation of hook_form()
     *
     *
     * @return Import Form
     **/
    function adminrecipe_import_form() {
        $title_page = "Importing Management";

        $form['#prefix'] = '<div id="admin_border_content"><div id="admin_border_content"><div class="registration_p_title">' . $title_page .'</div><div style="padding-left:10px;">';
        $form['#suffix'] = '</div></div></div>';
        $form['#attributes']['enctype'] = 'multipart/form-data';

        $prefix .= '<div id="field_header_image_upload"><div id="category_image_upload_input">';
        $suffix = '</div></div>';

        $form['import']['recipe_import_file'] = array(
                                '#type' => 'file',
                                '#title' => t('Import file'),
                                '#size' => 22,
                                '#weight' => 2,);
        $form['import']['recipe_import_file']['#prefix'] = $prefix;
        $form['import']['recipe_import_file']['#suffix'] = $suffix;

        $form['submit'] = array(
                    '#type' => 'submit',
                    '#prefix' => '<div id="recipes_upload_input01">
                                          <img src="' . C_IMAGE_PATH . 'space.gif" height="30" width="1" />',
                    '#attributes' => array('class'=> 'recipe_save'));

        return $form;
    }

    /**
     * Implementation of hook_form()
     *
     *
     * @return Import Form
     **/
    function adminrecipe_import_newsletter_form() {
        $title_page = "Send Newsletter";

        $form['#prefix'] = '<div id="admin_border_content"><div id="admin_border_content"><div class="registration_p_title">' . $title_page .'</div><div style="padding-left:10px;">';
        $form['#suffix'] = '</div></div></div>';
        $form['#attributes']['enctype'] = 'multipart/form-data';

        $prefix .= '<div id="field_header_image_upload"><div id="category_image_upload_input">';
        $suffix = '</div></div>';

        $form['import']['newsletter_subject'] = array(
                                '#type' => 'textfield',
                                '#title' => t('Mail subject'),
                                '#size' => 40,
                                '#weight' => 1,);
        $form['import']['newsletter_subject']['#prefix'] = $prefix;
        $form['import']['newsletter_subject']['#suffix'] = $suffix;

        $form['import']['newsletter_import_file'] = array(
                                '#type' => 'file',
                                '#title' => t('Mail template'),
                                '#size' => 22,
                                '#weight' => 2,);
        $form['import']['newsletter_import_file']['#prefix'] = $prefix;
        $form['import']['newsletter_import_file']['#suffix'] = $suffix;



        $form['submit'] = array(
                    '#type' => 'submit',
                    '#prefix' => '<div id="recipes_upload_input01">
                                          <img src="' . C_IMAGE_PATH . 'space.gif" height="30" width="1" />',
                    '#attributes' => array('class'=> 'recipe_save'));

        return $form;
    }

    /**
     * Validation handler for the term edit form.
     *
     * @see adminrecipe_import_form()
     */
    function adminrecipe_import_newsletter_form_validate($form, &$form_state) {

        if (empty($_POST['newsletter_subject'])){
            form_set_error('newsletter_subject', t(ERR_MSG_REQUIRED, array('@field_name' => 'Mail subject')));
        }

        if (empty($_FILES['files']["tmp_name"]["newsletter_import_file"])){
            form_set_error('newsletter_import_file', t(ERR_MSG_REQUIRED, array('@field_name' => 'Mail template')));
        }else{
            $allowed_extensions = array('html');
            $file_name = $_FILES['files']["name"]['newsletter_import_file'];
            $file_extension = end(explode(".", $file_name));

            if (!in_array($file_extension , $allowed_extensions)) {
                  form_set_error('newsletter_import_file', t(ERR_MSG_INVALID, array('@field_name' => 'Mail template')));
              }
        }


    }

    /**
     * Implementation of hook_form()
     *
     *
     * @return Import Form Submit
     **/
    function adminrecipe_import_newsletter_form_submit($form, &$form_state) {
        $directory = file_directory_path() .'/import/newsletter.html';
        $validators = array();
        $filepath = $_FILES['files']["tmp_name"]["newsletter_import_file"];
        if (!file_exists($filepath)){
            exit();
        }
        $file = file_get_contents($filepath);
        $fileimport = file_save_data($file, $directory, FILE_EXISTS_RENAME);
        $subject = trim($_POST['newsletter_subject']);
        $filename = $fileimport;
        db_query("INSERT INTO newsletter (subject,file,created) VALUES ('%s','%s',%d)", $subject, $filename, time());
        //db_query("UPDATE users SET send_flg = 0");
        db_query("UPDATE users SET send_flg = 0
                                WHERE uid IN (
                                    SELECT v.uid
                                    FROM profile_fields f, profile_values v
                                    WHERE f.fid = v.fid
                                    AND f.name = 'profile_newsletters'
                                    AND v.value = '1'
                                )");


        //drupal_set_message("Importing newsletter is completed.");
    }

    /**
     * Validation handler for the term edit form.
     *
     * @see adminrecipe_import_form()
     */
    function adminrecipe_import_form_validate($form, &$form_state) {
        if (empty($_FILES['files']["tmp_name"]["recipe_import_file"])){
            form_set_error('recipe_import_file', t(ERR_MSG_REQUIRED, array('@field_name' => 'Import file')));
        }else{
            $allowed_extensions = array('csv');
            $file_name = $_FILES['files']["name"]['recipe_import_file'];
            $file_extension = end(explode(".", $file_name));

            if (!in_array($file_extension , $allowed_extensions)) {
                  form_set_error('recipe_import_file', t(ERR_MSG_INVALID, array('@field_name' => 'Import file')));
              }
        }
    }
    /**
     * Implementation of hook_form()
     *
     *
     * @return Import Form Submit
     **/
    function adminrecipe_import_form_submit($form, &$form_state) {
        //print_r($_FILES);
        $directory = file_directory_path() .'/import/' . $_FILES['files']["name"]['recipe_import_file'];
        $validators = array();
        $filepath = $_FILES['files']["tmp_name"]["recipe_import_file"];
        if (!file_exists($filepath)){
            exit();
        }
        $file = file_get_contents($filepath);
        $fileimport = file_save_data($file, $directory, FILE_EXISTS_RENAME);

        //read data from csv
        $arr_validate = array();
        if ($fileimport){
            $arr_col_name = array("Email"
                                    , "Title"
                                    , "Desciption"
                                    , "Ingredient"
                                    , "Directions"
                                    , "Difficulty Level"
                                    , "Recipe Type"
                                    , "Content Status"
                                    , "Location"
                                    , "Yield Quantity"
                                    , "Yield Unit"
                                    , "Preparation Time"
                                    , "Preparation Time Unit"
                                    , "Cooking Time"
                                    , "Cooking Time Unit"
                                    , "Recipe Image"
                                    , "Categorize recipe"
                                    );

            $arr_validate["Email"] = array("required" => true);
            $arr_validate["Title"] = array("required" => true);
            $arr_validate["Ingredient"] = array("required" => true);
            $arr_validate["Directions"] = array("required" => true);
            $arr_validate["Preparation_Time"] = array("required" => true);
            $arr_validate["Cooking_Time"] = array("required" => true);

            $arr_validate["Difficulty_Level"] = array("code" => array(strtolower(EASY_LEVEL_TEXT) => EASY_LEVEL_VALUE
                                                                    , strtolower(MODERATE_LEVEL_TEXT) => MODERATE_LEVEL_VALUE
                                                                    , strtolower(DIFFICULT_LEVEL_TEXT) => DIFFICULT_LEVEL_VALUE
                                                                    , 'default_value' => EASY_LEVEL_VALUE));

            $arr_validate["Content_Status"] = array("code" => array(strtolower(SUBMITTED_STATUS_VALUE) => SUBMITTED_STATUS
                                                                    , strtolower(APPROVED_STATUS_VALUE) => APPROVED_STATUS
                                                                    , 'default_value' => SUBMITTED_STATUS));

            $arr_validate["Location"] = array("code" => array(strtolower(LOCATION_HOME_PAGE_VALUE) => LOCATION_HOME_PAGE
                                                                    , 'default_value' => 0));

            $arr_validate["Preparation_Time_Unit"] = array("code" => array(strtolower(DURATION_TIME_HOUR_TEXT) => DURATION_TIME_HOUR_VALUE
                                                                    , strtolower(DURATION_TIME_MINUTES_TEXT) => DURATION_TIME_MINUTES_VALUE
                                                                    , strtolower(DURATION_TIME_SECONDS_TEXT) => DURATION_TIME_SECONDS_VALUE
                                                                    , 'default_value' => DURATION_TIME_MINUTES_VALUE));

            $arr_validate["Cooking_Time_Unit"] = array("code" => array(strtolower(DURATION_TIME_HOUR_TEXT) => DURATION_TIME_HOUR_VALUE
                                                                    , strtolower(DURATION_TIME_MINUTES_TEXT) => DURATION_TIME_MINUTES_VALUE
                                                                    , strtolower(DURATION_TIME_SECONDS_TEXT) => DURATION_TIME_SECONDS_VALUE
                                                                    , 'default_value' => DURATION_TIME_MINUTES_VALUE));
            $objData = recipe_utils::get_import_data($fileimport, $arr_col_name, $arr_validate);

            $arrData = array();

            if($objData){
                $arrData = $objData->array_data;
            }
            //get category from database
            $strSql = " SELECT DISTINCT(td.tid), td.name
                    FROM  term_data td
                          INNER JOIN vocabulary_node_types vnt ON vnt.vid = td.vid
                          INNER JOIN term_hierarchy th ON th.tid = td.tid AND th.parent != 0
                    WHERE vnt.type = 'recipe'";
            $result = db_query($strSql);
            while ($cat = db_fetch_object($result)) {
                $arrCategory[$cat->name] = $cat->tid;
            }

            $bImport = false;
            $arrFilePath = array();

            // rename image file
            renameFileInDirectory(file_directory_path() . '/import/');

            foreach ($arrData as $objRecipe){
                //insert recipe into database
                adminrecipe_import_save_recipe($objRecipe, $arrCategory, $arrFilePath);
                $bImport = true;
            }

            //delete file import
            file_delete($fileimport);

            //delete image file
            foreach($arrFilePath as $strPath){
                if($strPath){
                    file_delete($strPath);
                }
            }
            if($bImport){
                // set message
                drupal_set_message("Importing data is completed.", "status");
            }
        }
    }

    /**
     * Insert recipe
     * Create groceries
     *
     * @param: $objRecipe, $fileUp
     **/
    function adminrecipe_import_save_recipe($objRecipe, $arrCategory, &$arrFilePath, $fileUp = null, $parent = 0){
        $email = recipe_utils::sql_escape_string($objRecipe->Email);
        $user->name = db_result(db_query_range(db_rewrite_sql("SELECT u.name FROM {users} u WHERE u.mail = '%s' "), $email, 0, 1));
          if(!empty($user->name)){
            $date = time();
            $result = array();
            $result['nid'] = 0;
            $arrGrocery = array();

            $ingredients = 	$objRecipe->Ingredient;
            parseIngredient($ingredients, $arrGrocery);

            module_load_include('inc', 'node', 'node.pages');
            $node = array('type' => CONTENT_TYPE_RECIPE, 'name' => $user->name);
            $node = (object)$node;

            $directions = adminrecipe_parse_string_textarea($objRecipe->Directions);
            $desciption = adminrecipe_parse_string_textarea($objRecipe->Desciption);

            $node_type_default = variable_get('node_options_'. CONTENT_TYPE_RECIPE, array('status', 'promote'));
            $node->comment = variable_get('comment_'. CONTENT_TYPE_RECIPE, 2);
            $node->status = 1;
            $node->revision = in_array('revision', $node_type_default);
            $node->promote = in_array('promote', $node_type_default);
            $node->type = CONTENT_TYPE_RECIPE;
            $node->uid = $objRecipe->uid;

            $node->title = $objRecipe->Title;
            $node->body = $desciption;
            $node->field_ingredients[0]['value'] = $ingredients;
            $node->field_preparation[0]['value'] = $directions;
            $node->field_difficulty_level[0]['value'] = $objRecipe->Difficulty_Level;

            $node->field_recipe_type[0]['value'] = 1;
            $node->field_recipe_yield_quantity[0]['value'] = $objRecipe->Yield_Quantity;
            $node->field_recipe_yield_unit[0]['value'] = $objRecipe->Yield_Unit;
            $node->field_cook_time[0]['value'] = $objRecipe->Cooking_Time;
            $node->field_cook_unit[0]['value'] = $objRecipe->Cooking_Time_Unit;
            $node->field_preparation_time[0]['value'] = $objRecipe->Preparation_Time;
            $node->field_preparation_unit[0]['value'] = $objRecipe->Preparation_Time_Unit;

            $node->field_top_status[0]['value'] = $objRecipe->Location;
            $node->field_content_status[0]['value'] = $objRecipe->Content_Status;
            $node->field_recipe_original[0]['value'] = $parent;

            // insert file
            if($objRecipe->Recipe_Image){
                $objRecipe->Recipe_Image = strtolower($objRecipe->Recipe_Image);
                $filepath = file_directory_path() . '/import/' . $objRecipe->Recipe_Image;
                if (file_exists($filepath)){
                    $fileUp = create_file_recipe_by_user($filepath, $objRecipe->Recipe_Image);
                    if($fileUp != null && $fileUp['filesize'] > 0){
                        drupal_write_record('files', $fileUp);
                        $node->field_recipe_image[0]['fid'] = $fileUp['fid'];

                        //file_delete($filepath);
                        $arrFilePath[] = $filepath;
                    }
                }
            }
            if($objRecipe->Categorize_recipe){
                $arrCatIDs = adminrecipe_get_categories($objRecipe->Categorize_recipe, $arrCategory);
                if(sizeof($arrCatIDs) > 0){
                    $node->taxonomy = array();
                    $node->taxonomy[] = $arrCatIDs;
                    $node->changed = $date;
                }
            }
            //save recipe on database
            node_save($node);

            if ($node->nid) {
                //insert groceries
                update_grocery_by_recipe($arrGrocery, $node->nid);
            }else{
                log_message(C_LOG_INFO, CONTENT_TYPE_RECIPE ." - " . $node->title . " hasn't been created.");
            }
        }

    }

    /**
     * get list of ID category from categories string
     *
     * @param: category
     **/
    function adminrecipe_get_categories($categories, $arrCategory){
        $arrReturn = array();
        $arrCat = explode("," , $categories);
        foreach ($arrCat as $key => $value){
            $value = trim($value);
            if (!recipe_utils::isNull($value)){
                if (isset($arrCategory[$value])) {
                    $arrReturn[] = $arrCategory[$value];
                }
            }
        }
        return $arrReturn;
    }

    /**
     * get string from excel
     *
     * @param: category
     **/
    function adminrecipe_parse_string_textarea($strRichText){
        $strReturn = "";
        $arrDirections = explode(BREAK_LINE , $strRichText);
        foreach($arrDirections as $strValue){
            $strReturn .= "<p>" . $strValue . "</p>";
        }
        return $strReturn;
    }

    /**
     * rename image file
     *
     * @param: category
     **/
    function renameFileInDirectory( $path = '.', $level = 0 ){

        $ignore = array( 'cgi-bin', '.', '..' );

        $dh = @opendir( $path );

        while( false !== ( $file = readdir( $dh ) ) )
        {
            if( !is_dir( "$path/$file" ) )
            {
                $allowed_extensions = array('jpg', 'jpeg', 'gif', 'png', 'pjpeg');
                $file_extension = strtolower(end(explode(".", $file)));
                if (in_array($file_extension , $allowed_extensions)) {
                    rename($path."/".$file, $path."/".strtolower($file));
                  }
            }
        }
        closedir( $dh );
    }

?>
