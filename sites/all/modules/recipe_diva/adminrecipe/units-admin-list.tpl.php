<?php
// $Id: recipes-admin-list.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-admin-list.tpl.php
 * Theme implementation to display list of recipes.
 *
 * Available variables:
 * - recipes: The list of recipes
 * - paging
 * - cat: Catergory
 *
 * @see theme_recipes_list()
 */
	 if (arg(1) == "units"){
	 	$title = "Units";
	 }else{
	 	$title = "Aisles";
	 }
	 $query_back =  "?page=" . ($_GET['page']? $_GET['page']: 0);
	 $query_back .= ($_GET['txtSearch']? "&txtSearch=" .  $_GET['txtSearch']: "");
	 $query_back .= recipe_utils::get_url_adminrecipe();
?>
<div id="admin_border_content">
	<form name="frm_units_admin" id="frm_units_admin" accept-charset="UTF-8" method="post" onSubmit="searchData('<?php print C_BASE_PATH . 'adminrecipe/' . arg(1);?>', 'txtSearch');return false;">
		<div id="admin_recipe_content">
			<div class="admin_p_title"><?php echo $title;?> Management</div>
			<div id="admin_divatips_search_form">
				<div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
				<div id="searchfor_input">
	            	<input type="text" name="txtSearch" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
	 			</div>
	            <div class="archive_search_btn">
	            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="searchData('<?php print C_BASE_PATH . 'adminrecipe/' . arg(1);?>', 'txtSearch');"/>
	            </div>
			</div>
			<div id="admin_recipes_content_title">
            	<?php print $header;?>
  	      	</div>
		<?php
      		if (sizeof($units)>0){
      		$icount = 0;
      		foreach ($units as $objUnit){
      			if ($icount%2 == 0){
      				$classrow = "admin_recipes_content_inner01";
      			}else{
      				$classrow = "admin_recipes_content_inner";
      			}
      			$icount++;
      			?>
      		
          	<div id="<?php print $classrow;?>">
          		<div id="admin_recipes_col_del"><input id="unit_del_<?php print $objUnit->id?>" name="unit_del_<?php print $objUnit->id?>" type="checkbox" value="<?php print $objUnit->id?>" /></div>
          		<div id="admin_category_col01" style="font-weight:normal;"><a href="<?php print C_BASE_PATH . "adminrecipe/" . arg(1)  . "/edit/" . $objUnit->id . $query_back?>"><?php print $objUnit->name?></a></div>
          		<div id="admin_recipes_col07"><a href="javascript:deleteAdminItem('frm_units_admin', '', '<?php print $objUnit->id?>', '<?php print CONFIRM_MSG_DELETE_ITEM;?>');">Delete</a></div>
          	</div>
      	<?}          	
      	}?>	
      	</div>
      	<div id="admin_recipe_content">
        	<img src="<? print C_IMAGE_PATH ?>space.gif" height="10" width="1" />
        </div>
        <div id="admin_recipe_content">
        	<input type="hidden" name="op" value="">
  			<input type="hidden" name="delId" value="">
  			<input type="hidden" name="page" value="<?php print $_POST['page']; ?>">
        	<input type="button" name="delete" id="delete" onclick="return deleteAdminList('frm_units_admin', 'unit_del_', '', '<?php print CONFIRM_MSG_DELETE_ITEMS;?>');"  class="admin_bt_delete" />
        	<input type="button" name="add" id="add" value="" class="admin_bt_add" onclick="addNewItemAmin(document.frm_units_admin, '<?echo C_BASE_PATH?>adminrecipe/<?php print arg(1);?>/add');"/>
        </div>
        <?php print $paging;?>
    </form>
 </div>


          	
 



          	

