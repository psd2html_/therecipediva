<?php
// $Id: recipes-admin-list.tpl.php,v 1.0 2010/06/01 10:49:00 dries Exp $

/**
 * @file recipes-admin-list.tpl.php
 * Theme implementation to display list of recipes.
 *
 * Available variables:
 * - recipes: The list of recipes
 * - paging
 * - cat: Catergory
 *
 * @see theme_recipes_list()
 */
 	$parentOption = '<select name="%parentid%" id="%parentid%">';
	if (sizeof($category)>0){
		foreach($category as $row) {
			$parentOption.='<option value="'.$row->id.'">'.$row->name.'</option>';
		}	
	}		   
	$parentOption .= '</select>';	
	$vocabulary = recipe_db::get_vid_by_node_type('recipe');
?>
<div id="admin_border_content">
	<form name="frm_category_admin" id="frm_category_admin" accept-charset="UTF-8" method="post" onSubmit="searchData('<?php print C_BASE_PATH;?>adminrecipe/category', 'txtSearch');return false;">
		<div id="admin_recipe_content">
				<div class="admin_p_title">Categories Management</div>
				<div id="admin_divatips_search_form">
					<div id="searchfor_lbl">Search for:</div>
					<div id="searchfor_input">
		            	<input type="text" name="txtSearch" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
		 			</div>
		            <div class="archive_search_btn">
		            	<input type="button" name="btnSearch" value="" class="bt_search" onClick="searchData('<?php print C_BASE_PATH;?>adminrecipe/category', 'txtSearch');"/>
		            </div>
				</div>
				<div id="admin_recipes_content_title">
                	<?php print $header;?>
               </div>
		<?php
      	    if (sizeof($terms)>0){
      		$icount = 0;
      		$query_back =  "?page=" . ($_GET['page']? $_GET['page']: 0);
      		$query_back .= ($_GET['txtSearch']? "&txtSearch=" .  $_GET['txtSearch']: "");
			$query_back .= recipe_utils::get_url_adminrecipe();
      		foreach ($terms as $objCat){
      			if ($icount%2 == 0){
      				$classrow = "admin_recipes_content_inner01";
      				
      			}else{
      				$classrow = "admin_recipes_content_inner";
      			}
      			
      			$icount++;
      			$image_path = "";
      			if($objCat->tid == $objCat->parent){
      				$class = "admin_category_col01";
      				$type = 0;
      			}else{
      				$class = "admin_category_col01_1";
      				$type = 1;
      				$image_path= ($objCat->image_path == ""? "nophoto.jpg" : $objCat->image_path) ;
					
					$image_path =  C_BASE_PATH . file_directory_path()."/category_pictures/" . $image_path;
				}
			?>
      		<div id="<?php print $classrow;?>">
          		<input type="hidden" id="hdf_category_tid_<?php print $objCat->tid?>" name="hdf_category_tid_<?php print $objCat->tid?>" value="<?php print $objCat->tid?>">
          		<input type="hidden" id="hdf_category_parent_tid_<?php print $objCat->tid?>" name="hdf_category_parent_tid_<?php print $objCat->tid?>" value="<?php print $objCat->parent?>">
          		<div id="admin_recipes_col_del"><input id="category_del_<?php print $objCat->tid?>" name="category_del_<?php print $objCat->tid?>" type="checkbox" value="<?php print $objCat->tid?>" /></div>
          		<div id="<?php print $class;?>"><a href="<?php print C_BASE_PATH;?>adminrecipe/category/edit/<?php print $objCat->tid . "/" . $type . $query_back?>"><?php print $objCat->name?></a></div>
          		<!--<div id="admin_category_col02"><img src="<?php print $image_path ?>" height="40" width="40"/></div>-->
                <div id="admin_category_col03"><?php print $objCat->parent_name;?></div>
                <div id="admin_recipes_col04" class="admin_title">&nbsp;</div>
                <div id="admin_recipes_col07"><a href="javascript:deleteAdminItem('frm_category_admin', '', '<?php print $objCat->tid?>', '<?php print CONFIRM_MSG_DELETE_ITEM;?>');">Delete</a></div>
            </div>
      	<?}
      	
      	}?>	
      	</div>
      	<div id="admin_recipe_content">
        	<img src="<? print C_IMAGE_PATH ?>space.gif" height="10" width="1" />
        </div>
        <div id="admin_recipe_content">
        	<input type="hidden" name="op" value="">
  			<input type="hidden" name="delId" value="">
  			<input type="hidden" name="page" value="<?php print $_POST['page']; ?>">
        	<input type="button" name="delete" id="delete" onclick="return deleteAdminList('frm_category_admin', 'category_del_', '', '<?php print CONFIRM_MSG_DELETE_ITEMS;?>');"  class="admin_bt_delete" />
        	<input type="button" name="add_cat" id="add_cat" value="" class="admin_bt_add_cat" onclick="addNewItemAmin(document.frm_category_admin, '<?echo C_BASE_PATH?>adminrecipe/category/<?php echo $vocabulary?>/add/0');"/>
        	<input type="button" name="add_subcate" id="add_subcate" value="" class="admin_bt_add_subcat" onclick="addNewItemAmin(document.frm_category_admin, '<?echo C_BASE_PATH?>adminrecipe/category/<?php echo $vocabulary?>/add/1');"/>
        	
        </div>
        <?php print $paging;?>
    </form>
</div>




          	
 



          	

