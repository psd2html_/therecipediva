<?php
// $Id: recipe.tpl.php,v 1.0 2010/05/18 10:49:00 dries Exp $

/**
 * @file recipe.tpl.php
 * Theme implementation to display information of the recipe.
 *
 * Available variables:
 * - $recipe: recipe information
 *
 * @see theme_recipe_page()
 */
 	global $user;
 	$author_info = recipe_utils::create_author_info($user->uid, $user->name, time());
 	$arrIngredients = explode(BREAK_LINE , $_POST['ingredients']);
 	
 	if (is_array($arrIngredients))
		{
			$strIngredients = "";
			foreach($arrIngredients as $strIngItem)
			{
				$strIngredients .= $strIngItem;
				$strIngredients .= "<br>";
			}
		}
	
	
	//time 	
	$arrTimeUnit = recipe_db::get_list_duration_time();
	
	//category
	$terms = recipe_db::load_master_category();
	$arrSubCat = explode(",", $_POST['hdf_terms']);
	if (sizeof($terms) != 0){
		$arrSubcat = array();
		//get category
		foreach ($terms as $objTerm){
			if(in_array($objTerm->tid, $arrSubCat)){
				$arrSubcat[] = $objTerm;
			}
		}
	}
	
	//image
	$fileUp = null;
	$filepath = $_POST['hdf_filepath_image']; 
	//$filepath = file_get_contents($filepath);
	//echo $file . $filepath;
	if($filepath){
    	$width = recipe_utils::getImageWidthValue($filepath, RECIPE_IMAGE_WIDTH);
    }
    
   // yield
	$yieldValue = $_POST['txtYieldQuantity']; 
	$yieldUnit = $_POST['txtYieldUnit'];
	
	// preparation time
	$preValue = $_POST['txtPreTime'];
	// cooking time
	$cookValue = $_POST['txtCookingTime'];
	
	$arrTimeUnit = recipe_db::get_view_list_duration_time();
	$preUnitValue = $_POST['cbbPreTimeUnit'];
	$cookUnitValue = $_POST['cbbCookTimeUnit'];
	foreach($arrTimeUnit as $key => $value){
		if ($preUnitValue == $key){
			$preUnit = $value;
		}
		if ($cookUnitValue == $key){
			$cookUnit = $value;
		}
	}
	
	$totalValue = 0;
	$totalUnit = $preUnit;
	if ($preUnit == $cookUnit){
		$totalValue = $preValue + $cookValue;
		$totalUnit = $preUnit;
	}elseif ($preUnitValue == DURATION_TIME_HOUR_VALUE){
		if($cookUnitValue == DURATION_TIME_MINUTES_VALUE){
			$totalValue = $preValue * 60 + $cookValue;
		}else{
			$totalValue = $preValue * 60 * 60 + $cookValue;
		}
		$totalUnit = $cookUnit;
	}elseif($preUnitValue == DURATION_TIME_MINUTES_VALUE){
		if($cookUnitValue == DURATION_TIME_HOUR_VALUE){
			$totalValue = $preValue + $cookValue * 60;
			$totalUnit = $preUnit;
		}else{
			$totalValue = $preValue * 60 + $cookValue;
			$totalUnit = $cookUnit;
		}                         					
	}else{
		if($cookUnitValue == DURATION_TIME_HOUR_VALUE){
			$totalValue = $preValue + $cookValue * 60 * 60;
		}else{
  			$totalValue = $preValue + $cookValue * 60;
  		}
  		$totalUnit = $preUnit;
	}
	  		//difficult
	$arrDifficult = recipe_db::get_list_recipe_difficult_level();
	$strDifficult = $arrDifficult[$_POST['difficulty_level']];
?>    
	<div id="news_p">
			<div id="news_p_b">
		    	<div id="news_border">
		        	<div id="news_p_content" class="news_p_content_font">
		        		<div id="recipes_img_contain">
		                	<div id="recipe_right_content" style="width: 535px;">
		                		<span>
	                                <div id="individual_r_title">
	                                   	<?php print $_POST['txtTitle']?>
	                                </div>
	                                <div id="individual_r_posted" style="padding-bottom:5px;">
                                    	<div><?php print $author_info?>
                                        </div>
                               		</div>
                               	</span>
		                		<?php 
		                			$description = $_POST['description'];
                            		$read_more_url = "javascript:expand_description('individual_r_excerpt_desc', 'individual_r_content');";
					   		  		$excerpt = recipe_utils::get_excerpt($description, NUMBER_WORDS_DESCRIPTION, $read_more_url);
					   		  		if ($excerpt == '') {
					   		  			$excerpt = "&nbsp;";
					   		  		}
					   		  		if ($description == '') {
					   		  			$description = "&nbsp;";
					   		  		}
					   		  		
		                			$style = "float: left;" ;
	                            	$bg_width = 530 - ($width + 5);
	                            	if ($width){
	                            		$style = "float: left;" . " width:" . $bg_width . "px;" ;
	                            ?>
	                            <div id="individual_image" class="individual_r_image">  		
					            	<img src="<?php print C_BASE_PATH . $filepath?>"  width="<?php print $width?>"/>
					            </div>		
						        <?php } 
						        	else{
					                	if(trim($description) != ""){
						                	print '	<div id="individual_image" class="individual_r_image" style="width:313px;">  		
						            				<span>
						   		  					    <div id="individual_r_excerpt_desc" style="padding-top:0px;'.$style.'">
					                                    	'.nl2br($excerpt).'
					                                    </div>
					                                	<div id="individual_r_content" style="padding-top:0px;display:none;'.$style.'" itemprop="summary"> 
									   		  				'.$description.'
									   		  			</div>
									   		  		</span>	
						            				</div>';
					                	}
					                }
						        ?> 
	                            <span>
                                    <div id="individual_r_voted" style="<?php print $style;?>padding-bottom:0px;">
                                        <div id="myversion_tips_bg01">
                                        	<div id="myversion_tips_bg02">Difficulty: <?=$strDifficult;?></div>
	                                        <div id="myversion_tips_bg03">
                                            	<div id="myversion_tips_bg03_l">
                                                	<div>Yield: <?php print $yieldValue . " " . $yieldUnit?></div>
                                                    <div>Prep Time: <?php print $preValue . " " . $preUnit?></div>
                                                </div>
                                                <div id="myversion_tips_bg03_r">
                                                	<div>Cook TIme: <?php print $cookValue . " " . $cookUnit?></div>
                                                    <div>Ready In: <?php print $totalValue . " " . $totalUnit?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </span>
                            	<?php if ($width){?>
	   		  						<div id="individual_r_excerpt_desc" style="display:none;<?php print $style;?>">
                                    	<?php print nl2br($excerpt)?>
                                    </div>
                                	<div id="individual_r_content" style="<?php print $style;?>"> 
				   		  				<?php print $description; ?>
				   		  			</div>
				   		  		<?php }?>
				   		  	</div>
                            <div id="news_p_menu_tip" style="<?php print $display_recipe;?>">
		                		<div style="float: left; position: relative; left: 0; z-index: 11">
	                              	<img src="<?php print C_IMAGE_PATH ?>border/Recipes_tab01.png" width="541" height="50"/>
	                            </div>
	                        </div>
	                       	<!--- comment is active -->
						</div>
		               	<div id="individual_p_b" style="<?php print $display_recipe;?>">
		               		<div id="individual_p_content">
			               		<div id="div_recipes_note_contain">
		                        	<div id="recipes_note01_contain">
		                        		<div id="recipes_note01_t">
	                                    	<div id="recipes_note_icon01">
                                    			<a href="#">
                                    				<img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" />
                                    			</a>
	                                    	</div>
	                                        <div id="recipes_note_icon02">
	                                        	<a href="#"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
	                          				</div>
	                                   	  	<div id="recipes_note_icon03">
	                                   	  		<a href="#"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55"/></a>
	                                   	  	</div>
	                                    </div>
	                                    <div id="recipes_note01_b">
	                                    	<div id="recipes_note_icon01">
	                                    		<a href="#" target="_blank"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
	                                    	</div>
	                                      	<div id="recipes_note_icon02">
	                                      		<a href="#"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55" /></a>
	                          				</div>
	                                      	<div id="recipes_note_icon03">
	                                      		<a href="#"><img src="<?php print C_IMAGE_PATH?>space.gif" width="50" height="55"/></a>
		                                    </div>
	                                    </div>    
		                           	</div>
		                           	<div id="recipes_converttool_contain">
			                           		<a href="javascript:show_convert_tool()"><img src="<?php print C_IMAGE_PATH?>icon/ic_convert.jpg"/></a>
		                           	</div>  
			                 	</div>
		                		<div id="individual_l_col">
		                        	<div id="individual_p_title_contain">
		                            	<!--<div id="individual_p_title" class="individual_title"><img src="<? print C_IMAGE_PATH ?>label/recipes_ingredient_lbl.gif"/></div>-->
		                            	<span id="individual_p_title" class="individual_title">Ingredients</span>
		                          		<span id="add_all_ingredients">
		                          			<?php 
	                      						print '<a href="#">' .
	                      								'<span id="add_all_ingredients_icon">[+] Add all ingredients</span></a>';
		                          					
		                          			?>
		                              	</span>
		                            </div>
		                            <div class="ingredients_content">
		                            	<?php print $strIngredients?>
	                                </div>    
		                      	</div>
	                      	</div>		                      	
		                 	<div id="divatiptab_content" class="bg-none">
		                    	<!--<div id="recipe_method_title"><img src="<? print C_IMAGE_PATH ?>label/recipes_method_lbl.gif"/></div>-->
		                    	<div id="recipe_method_title">Method</div>
		                        <div id="divatiptab_content_inner">
		                          <?php print $_POST['preparation']?>
		                        </div>
		                    </div>
		                </div>
		            </div>
		     	</div>
		  	</div>    	
		</div>    
<script>
$(document).ready(function() {
	var divImage = document.getElementById("individual_image");
	var divTitle = document.getElementById("individual_r_title");
	var divDate = document.getElementById("individual_r_posted");
	var divDesc = document.getElementById("individual_r_content");
	var divInfo = document.getElementById("myversion_tips_bg01");
	var divExcDesc = document.getElementById("individual_r_excerpt_desc");
	var contentHeight = divDesc.offsetHeight + divInfo.offsetHeight;
		
	var imageHeight = 0;
	if (divImage != undefined) {
		imageHeight = divImage.offsetHeight
	}
	contentHeight = contentHeight - 10;
	
	if (contentHeight > imageHeight) {
		divDesc.style.display = 'none';
		divExcDesc.style.display = '';
	}
		
	$('#news_p_menu_tip').pngFix();	
	ResizeImage('individual_r_content', 300);
	ResizeImage('divatiptab_content_inner', 520);
});
</script>
					

