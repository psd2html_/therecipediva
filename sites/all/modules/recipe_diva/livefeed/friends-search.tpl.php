<?php
//if(arg(1) == 'search'){
//    $site_url = C_SITE_URL.C_BASE_PATH;
//    drupal_add_link(array("rel"=>"canonical", "href"=>$site_url."friendsearch"));
//}
?>
<div id="body_right_of_left_col">
    <div id="border_type002">
        <div id="border_type002_top">
            <div id="border_type002_titles">
                <img alt="Search result" src="<?php print $_GET['re']? C_IMAGE_PATH."label/lbl_search_result.gif" : C_IMAGE_PATH."label/lbl_friend_search_title.gif"?>" />
            </div>
        </div>
        <div id="border_type002_b">
                 <div id="border_type002_content">
                <?php if($_GET['re'] && sizeof($userlist)>0){?>
                    <div id="search_result_contain">
                <?php foreach ($userlist as $objUser){?>
                    <div id="search_result_contain_inner">
                        <div class="request_friends_img">
                            <?php $file_path = $objUser->picture;
                            if(file_exists($file_path)){ ?>
                                <a href="<?php print C_BASE_PATH."user/".$objUser->name?>"><img alt="<?php print $objUser->name?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,60) ?> /></a>
                            <?php }else{?>
                                <a href="<?php print C_BASE_PATH."user/".$objUser->name?>"><img alt="<?echo $objUser->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?echo C_IMAGE_PATH?>photo/<?echo $objUser->profile_gender=="0"? "male":"female"?>.gif"></a>
                            <?php }?>
                        </div>
                        <div class="request_friends_center_content1">
                            <div id="outer">
                              <div id="middle">
                                <div id="inner">
                                    <a href="<?php print C_BASE_PATH."user/".$objUser->name?>"><?php print $objUser->name?></a><br>
                                    <?php print $objUser->city?>
                                    <?php print $objUser->city?", ".$objUser->state:$objUser->state?>
                                    <?php print $objUser->city||$objUser->state?", ".$objUser->country:$objUser->country?>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    </div>
                    <div id="img_seeall">
                    <?php print $paging;?>
                     </div>
                <?php }else{?>
                <div id="border_type002_contain">
                    <img src="<? print C_IMAGE_PATH ?>space.gif" height="12" width="1" />
                </div>
                <form id="frm_friend_search_form" name="frm_friend_search_form" method="GET" onSubmit="javascript:return searchFriends('frm_friend_search_form', '<?echo C_BASE_PATH?>recipes');" accept-charset="UTF-8" action="<? print C_BASE_PATH ?>recipes">
                    <div id="border_type002_searchfor_contain">
                        <div id="border_type_search_friend">

                            <?php if($_GET['re'] && sizeof($userlist)==0){?>
                            <div id="border_type002_search_friend">
                                We're sorry, we could not find any foodies.
                            </div>
                            <?php }?>

                            <div id="border_type002_search_friend">
                                  <div id="friend_search_for_lbl"><img alt="Search For" src="<?echo C_IMAGE_PATH?>label/lbl_search_for_friends.gif"></div>
                                  <div id="searchfor_input">
                                    <input type="text" name="txtKeyword" id="txtKeyword" value="<?php print $_GET['hkw']?urldecode($_GET['hkw']):urldecode($_GET['kw']);?>" class="Archive_Search_inp" />
                                  </div>
                                <div class="archive_search_btn">
                                    <input type="button" name="op" id="submit" onClick="searchFriends(1, '<?echo C_BASE_PATH?>friendsearch');" value="" class="advanced_search" />
                                </div>
                            </div>
                            <img alt="Search Friend Description" src="<? print C_IMAGE_PATH ?>label/lbl_search_friend_description.gif"/>
                            <img src="<? print C_IMAGE_PATH ?>space.gif" height="15" width="200" />
                            <div id="searchfriend_input_right" style="border-top:1px solid #4F2924">
                                <div><img alt="Search Friend Location" src="<? print C_IMAGE_PATH ?>label/lbl_search_friend_location.gif"/></div>
                                <div><img src="<? print C_IMAGE_PATH ?>space.gif" height="6" width="200" /></div>
                                <div><img alt="Search friend description" src="<? print C_IMAGE_PATH ?>label/lbl_search_friend_description_1.gif"/></div>
                            </div>
                            <div id="searchfriend_input_right">
                                <img alt="Country" src="<?echo C_IMAGE_PATH?>label/country_lbl.gif"><br>
                                <!--<div class="admin_require_field"><span title="This field is required." class="form-required">&nbsp;*</span></div>-->
                                <select class="myprofile_select" name="ddlCountry" id="ddlCountry" onchange="loadState()">
                                    <option value=""></option>
                                    <?php 	$countries = profile_location_countries();
                                            $cur_country = $_GET['co']?$_GET['co']:"";
                                            foreach ($countries as $key => $value) {
                                                if($key == $cur_country){
                                                    print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                                }
                                                else{
                                                    print '<option value="'.$key.'">'.$value.'</option>';
                                                }
                                            }
                                    ?>
                                </select>
                                <br>
                                <div id="div_country_error" class="spanError"></div>
                            </div>
                            <div id="searchfriend_input_right">
                                <img alt="City" src="<?echo C_IMAGE_PATH?>label/city_lbl.gif"><br>
                                <input type="text" class="myprofile_input"  maxlength="40" name="txtCity" id="txtCity" value="<?php print htmlspecialchars(urldecode($_GET['ci']))?>">
                            </div>
                            <div id="searchfriend_input_right">
                                <div id="myprofile_state">
                                    <img alt="State Province" src="<?echo C_IMAGE_PATH?>label/state_province_lbl.gif"><br>
                                    <select class="myprofile_select01"  name="ddlState" id="ddlState">
                                        <option value=""></option>
                                        <?php $states = profile_location_states($cur_country);
                                              $cur_state = $_GET['sa'];//?$_GET['sa']:"TX";
                                                foreach ($states as $key => $value) {
                                                    if($key == $cur_state){
                                                        print '<option selected="true" value="'.$key.'">'.$value.'</option>';
                                                    }
                                                    else{
                                                        print '<option value="'.$key.'">'.$value.'</option>';
                                                    }
                                                }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="right" style="padding-bottom:12px;">
                                <img alt="Search Friend" class="img_click" src="<?echo C_IMAGE_PATH?>button/submit_btn.gif" onClick="searchFriends(5, '<?echo C_BASE_PATH?>friendsearch');">
                            </div>
                            <div id="searchfriend_input_right" style="border-top:1px solid #4F2924;padding-bottom:20px;">
                                <img alt="See top reviewers" class="img_click" src="<? print C_IMAGE_PATH ?>label/lbl_see_top_reviewers.gif" onClick="searchFriends(3, '<?echo C_BASE_PATH?>friendsearch');"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img alt="See top submitters" class="img_click" src="<? print C_IMAGE_PATH ?>label/lbl_see_top_submitters.gif" onClick="searchFriends(4, '<?echo C_BASE_PATH?>friendsearch');"/>
                            </div>
                        </div>
                     </div>
                </form>

                <?php }?>
            </div>
        </div>
    </div>
</div>
<script>
    function searchFriends(type, url){
        url = url + "?type=4&re=1";
        if(type == 1){
            url = url + "&kw=" + encodeURIComponent($.trim($("#txtKeyword").val()));
        }
        else if(type == 2){
            url = url + "&co=" + $("#ddlCountry").val() + "&ci=" + encodeURIComponent($.trim($("#txtCity").val()))
                         + "&sa=" + $("#ddlState").val();
        }
        else if(type == 3){
            url = url + "&tr=1";
        }
        else if(type == 4){
            url = url + "&tr=2";
        }
        else if(type == 5){
            url = url + "&kw=" + encodeURIComponent($.trim($("#txtKeyword").val())) + "&co=" + $("#ddlCountry").val() + "&ci=" + encodeURIComponent($.trim($("#txtCity").val()))
                         + "&sa=" + $("#ddlState").val();
        }
        window.location = url;
        return false;
    }
    function loadState(){
        $.get("<?php print C_BASE_PATH."mydiva/profile/load_state/"?>" + $("#ddlCountry").val(),
               function(data){
                    $("#ddlState").html("<option value=''></option>"+data);
               });
    }
</script>
