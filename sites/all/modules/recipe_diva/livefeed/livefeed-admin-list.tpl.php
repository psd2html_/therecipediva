<div id="admin_border_content">
    <form name="frmlivefeedList" method="post" id="livefeed-admin-form" onSubmit="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/livefeed', 'txtSearch');return false;">
    <div id="admin_recipe_content">
        <div class="admin_p_title">Live feed Management</div>
        <div id="admin_divatips_search_form">
            <div id="searchfor_lbl"><img src="<?echo C_IMAGE_PATH?>label/search_for_lbl.gif"></div>
            <div id="searchfor_input">
                <input type="text" id="txtSearch" class="Archive_Search_inp" value="<?php print htmlspecialchars($_GET['txtSearch']); ?>"/>
             </div>
            <div class="archive_search_btn">
                <input type="button" name="btnSearch" value="" class="bt_search" onClick="submit_search_form('<?php print C_BASE_PATH;?>adminrecipe/livefeed', 'txtSearch');return false;"/>
            </div>
        </div>
        <?php
            $delete_q = drupal_query_string_encode($_GET, array('q'));
            $title_q = drupal_query_string_encode($_GET, array('q','sort','order', 'txtSearch'));
            $title_q = $title_q != "" ? $title_q.'&':"";

        ?>
        <div id="admin_recipes_content_title">
            <div id="admin_recipes_col_del" class="admin_title">
            <input type="checkbox" name="chkAll" onclick="checkAll1(document.frmlivefeedList, 'chk_', this)" />
            </div>

              <div id="admin_home_title" style="width: 480px;">
                  <a href="javascript:changePaging('livefeed-admin-form', '<?echo C_BASE_PATH?>adminrecipe/livefeed?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="name")?"desc":"asc"?>&order=name', '0');">
                  <span class="admin_title">Content</span></a>
            </div>
              <div id="admin_home_created">
                  <a href="javascript:changePaging('livefeed-admin-form', '<?echo C_BASE_PATH?>adminrecipe/livefeed?<?php print $title_q?>sort=<?php print ($_GET['sort']=="asc" && $_GET['order']=="created")?"desc":"asc"?>&order=created', '0');">
                  <span class="admin_title">Created</span></a>
            </div>

            <div id="admin_home_op_link" class="admin_title">Operations</div>
          </div>
          <?php
        $id = 0;
        $return_data = '';
        foreach ($livefeedlist as $row) {

            $return_data = '
                    <div style="padding-bottom:4px;">
                        <a rel="nofollow" class="by_author" href="'.C_BASE_PATH."user/".$row->name.'">'.$row->name.'</a> ';

            if($row->feed_type == LIVEFEEDTYPE_UPDATE_PROFILE){
                $gender = $row->profile_gender=="0"? "his":"her";
                $return_data .= str_replace("gender",$gender,$row->comment);
                $return_data .= " <br>";
            }else{
                $return_data .= $row->comment;
                if($row->feed_type == LIVEFEEDTYPE_CHANGE_STATUS){
                    $return_data .= " <br>";
                }else if($row->feed_type == LIVEFEEDTYPE_NEW_MEMBER){
                    $return_data .= " <br>";
                }
            }

            if($row->feed_type == LIVEFEEDTYPE_BECOME_FRIEND){
                $return_data .= ' <a rel="nofollow" class="by_author" href="'.C_BASE_PATH.'user/'.$row->friend_name.'">'.$row->friend_name.'</a><br>';
            }else if($row->feed_type == LIVEFEEDTYPE_UPLOAD_RECIPE || $row->feed_type == LIVEFEEDTYPE_UPLOAD_TIP){
                $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row->title)."-".$row->nid;
                $return_data .= ' <span class="by_author"><a href="'.$nodeLink.'">'.recipe_utils::get_excerpt_limit($row->title,10,false,50).'</a></span><br>';
            }else if($row->feed_type == LIVEFEEDTYPE_REPLY_TOPIC || $row->feed_type == LIVEFEEDTYPE_POST_TOPIC){
                $nodeLink = C_BASE_PATH."divagossip/topic/".$row->title.'-'.$row->nid;
                $return_data .= ' <span class="by_author"><a href="'.$nodeLink.'">'.recipe_utils::get_excerpt_limit($row->title,10,false,50).'</a></span><br>';
            }else if($row->feed_type == LIVEFEEDTYPE_REVIEW_RECIPE || $row->feed_type == LIVEFEEDTYPE_REVIEW_TIP){
                $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row->review_title)."-".$row->review_nid."?cid=".$row->cid;
                $return_data .= ' <span class="by_author"><a href="'.$nodeLink.'">'.recipe_utils::get_excerpt_limit($row->review_title,10,false,50).'</a></span>';
            }else if($row->feed_type == LIVEFEEDTYPE_SAVE_RECIPE || $row->feed_type == LIVEFEEDTYPE_SAVE_TIP){
                $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row->title)."-".$row->nid;
                $return_data .= ' <span class="by_author"><a href="'.$nodeLink.'">'.recipe_utils::get_excerpt_limit($row->title,10,false,50).'</a></span><br>';
            }
            $return_data .= '</div>';

        ?>
        <div id="admin_recipes_content_inner<?php print $id % 2 ? "":"01"?>">
            <div class="admin_title" id="admin_recipes_col_del">
                <input type="checkbox" onclick="checkAllChange(document.frmlivefeedList, 'chk_', this, document.frmlivefeedList.chkAll)" value="<?php print $row->id?>" name="chk_<?php print $id?>">
            </div>
            <div id="admin_home_title" style="width: 480px;">
                <?php print $return_data?>
            </div>
            <div id="admin_home_created">
                <span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$row->created)?></span>
            </div>

            <div class="admin_title" id="admin_home_op_link">
                <a onclick="return deleteAdminItem('frmlivefeedList', '<?php print C_BASE_PATH."adminrecipe/livefeed?".$delete_q?>', '<?php print $row->id?>')" style="cursor: pointer;"><span class="admin_delete_title">Delete</span>
                </a>
            </div>
          </div>
        <?php $id++;} ?>
          <input type="hidden" name="hidPageBack" value="<?php print C_BASE_PATH."adminrecipe/livefeed"?>">
          <input type="hidden" name="page" value="<?php print $_GET['page']; ?>">
          <input type="hidden" name="op" value="">
          <input type="hidden" name="delId" value="">
    </div>

    <div id="admin_recipe_content">
        <div id="admin_divatips_delete">
              <input type="button" name="btnDelete" value=""  class="admin_bt_delete" onClick="return deleteAdminList('frmlivefeedList', 'chk_', '<?php print C_BASE_PATH."adminrecipe/livefeed?".$delete_q; ?>')" />
        </div>
        <?php print $pager ?>
    </div>
    </form>
</div>
