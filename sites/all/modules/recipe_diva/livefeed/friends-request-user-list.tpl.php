<div id="mydiva_profiles_content_inner">
	<div id="upload_recipe">
		<div class="request_friends_header">
			<div style="float: left; position: relative;">
				<img alt="Friend Requests" src="<?echo C_IMAGE_PATH?>label/lbl_icon_friend_requests.gif">
			</div>
			<div class="num_request_friends">			
				<?php print "(". sizeof($userlist). "):"?>	
			</div>
		</div>
		<?php	global $user; 
		foreach ($userlist as $objUser){?>
		<div id="div_user_<?php print $objUser->fid?>" class="request_friends_center_container">
			<div class="request_friends_img">
	            <?php $file_path = $objUser->picture;
				if(file_exists($file_path)){ ?>
					<a href="<?php print C_BASE_PATH."user/".$objUser->name?>"><img alt="<?php print $objUser->name?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,60) ?> /></a>
				<?php }else{?>
					<a href="<?php print C_BASE_PATH."user/".$objUser->name?>"><img alt="<?echo $objUser->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?echo C_IMAGE_PATH?>photo/<?echo $objUser->profile_gender=="0"? "male":"female"?>.gif"></a>
				<?php }?>
	        </div>
	        <div class="request_friends_center_content">
	            <div id="outer">
				  <div id="middle">
				    <div id="inner">
	                	<a href="<?php print C_BASE_PATH."user/".$objUser->name?>"><?php print $objUser->name?></a><br>	            	 
	            		<?php print $objUser->num_mutual?> Mutual Foodie<?php print $objUser->num_mutual>1?"s":""?>
	            	</div>
	              </div>
				</div>
	        </div>
	        <div class="request_friends_center_content1">            	
            	<div id="outer">
				  <div id="middle">
				    <div id="inner">
				      	<?php print nl2br($objUser->personal_message)?>&nbsp;
				    </div>
				  </div>
				</div>
	        </div>	        
	        <div class="request_friends_btn">
				<div id="outer">
				  <div id="middle">
				    <div id="inner">
			        	<img alt="Accept request" src="<?echo C_IMAGE_PATH?>button/btn_confirm.gif" onclick="acceptrequest(<?php print $user->uid?>,<?php print $objUser->fid?>)" style="cursor: pointer;">
						<img alt="Remove request" src="<?echo C_IMAGE_PATH?>button/btn_ignore.gif" onclick="removerequest(<?php print $user->uid?>,<?php print $objUser->fid?>)" style="cursor: pointer;">
				    </div>
				  </div>
				</div>				
	        </div>
	    </div>
    	<?php }?>
    	<div class="request_friends_center_container">
    	</div>
		<div id="div_space">
        	<img width="1" height="100" src="<?echo C_IMAGE_PATH?>space.gif">
		</div>      		
	</div>
</div>
 
<script>
	var num_request = <?php print sizeof($userlist)?>;
	function acceptrequest(uid,fid) {
		var url = "<?php print C_BASE_PATH."acceptfriendrequest/"?>" + uid + "/" + fid ;  
		$.post(url, function(data){
				var json = eval("(" + data + ")");
		        if(json['status'] == "success"){
		        	$("#div_user_"+fid).remove();
		        	changenumrequest();
		        }								        
		   }, "text");
								
	}
	function changenumrequest(){
		num_request--;
		$("div[class=num_request_friends]").html("("+num_request+")");
	}
	function removerequest(uid,fid) {
		var message = "<?print CONFIRM_MSG_IGNORE_FRIEND?>";
		
		$("#dialogConfirm").attr("title", message);
		$("#ui-dialog-title-dialogConfirm").text(message);	
		$("#dialogConfirm").dialog(
			{ modal: true },
			{ resizable: false },
			{ minHeight: 0 },
			{ buttons:
				{ 						
					"Yes": function() {
								var url = "<?php print C_BASE_PATH."removefriendrequest/"?>" + uid + "/" + fid;
								$.get(url, function(data){
										var json = eval("(" + data + ")");
								        if(json['status'] == "success"){
								        	$("#div_user_"+fid).remove();
								        	changenumrequest();
								        }
								   },"text");
								$(this).dialog("close");								   
							},
					"No": function() {																		
								$(this).dialog("close");						
							}														
				}
			}
		);
	}
</script>