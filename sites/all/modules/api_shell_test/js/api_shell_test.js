// $Id$
// cobbled together by Blake Senftner www.BlakeSenftner.com
// use this example to learn, improve it and share...
//
// this is mostly the same logic as documented at http://drupal.org/node/783460,
// the section "Writing a simple JavaScript client". I've added some
// logic for resetting the UI because when operations using POST or PUT fail,
// the UI is left in a bad state. Also, because the Retreive operation was
// only being called upon a successful POST or PUT, there's a routine one
// can call that tests Retreive directly.
//
// Currently this code's testing UI must be executed by using FireBug's js console
// to launch the UI directly. It must be launched from your site's home page, unless
// you use an absolute URL for your api end point. (explained below)
//
// Usage: From your site's home page, in the FireBug js console, enter this:
//      gApiShell.note.test()
// This will put the testing UI in the upper right of the browser. After a moment, you
// should see two 'notes' appear beneath the 'Reset UI' button. This is the result of
// an initial call to gApiShell.note.index(), which executes a GET on your api end point.
// If you do not see two notes appear, something is wrong with your setup.
//
// Granted the initial index of the end point worked:
// 1) Entering anything into the Subject or Body fields and clicking Save will
//    test the POST operation. If the POST is successful, the success js callback
//    is called and the Retreive (GET) operation is triggered. Note that if the
//    POST operation fails, the Reset button on the Test UI is needed to
//    get the Test UI back in a usable state.
//
// !!! Note that the 'new note' that appears in the Test UI will not have the contents
//     you gave it before clicking 'save'; the reason being that this testing framework
//     does not actually save the 'notes' to the database.
//     Api_shell.module only demos the API setup logic, and api_shell_test.module only
//     demonstrates how to call the API setup in api_shell.module. 
//
// 2) To directly test the Retreive (GET) operation, enter this in the Firebug js console:
//      gApiShell.note.testRetreive()
//    this should work, and you'll see a new note appear in the Test UI, as well
//    as messages in both the js console and Drupal server side showing the
//    Retreive success callback being called. 
// 3) Clicking Targeted Action will fail; FireBug js messages appear but the
//    server side prints never occur, signaling they've not been reached.
//    (I'm still working on this part of the example framdework.)
// 4) Clicking a note's 'Delete' link will work - both js console and server side
//    messages appear.
// 5) Clicking a note's 'Edit' link activates the 'Cancel' button, and when clicking
//    the 'Save' button it tests a PUT operation, which should work.
// 6) Enter this in the Firebug js console to test the Index operation receiving a parameter:
//      gApiShell.note.testIndexParam()
//    The Index Services callback (server side) gets called, but the parameter is not present.
//    I'm also still working on that part...
//
// Additional debugging hint:
// inside the Services file services/servers/rest_server/includes/RESTServer.inc you will find
// a routine called getControllerArguments(). That routine is where the data you send from here
// and traveling through different channels (POST,path,params) is consolidated into the single
// argument array that your api callback in PHP is called. So, if you have step debugging you
// can break on that routine and examine your data entering the drupal environment. If you don't
// have step debugging of Drupal, you can place print-to-file logic like is used in api_shell.module.
//
// This can also be useful:
// http://drupalcode.org/viewvc/drupal/contributions/modules/services/tests/functional/ServicesResourceNodeTests.test?revision=1.1.2.3&view=markup


// the preferred Drupal DOM wrapper
Drupal.behaviors.initApiShellTester = function() {
  
  // "use strict"; /*global gApiShell:true */
  
  // create a global object that will hold our api hooks,
  // global so developers can examine in Firebug while developing.
  gApiShell = {};
  
  // under 'note' will go all our note api resource logic:
  gApiShell.note = {
      // this path is local, meaning the invoked path during runtime will be relative to
      // the current page. For this reason, this testing framework only functions correctly
      // when tested from the site's homepage.
      apiPath: 'api_shell/dev/note'
      
      // (to make your apiPath non-relative to the current page, use a complete URL, as in
      // it includes http://www.yoursite.com/[apiPath] )
  };

  gApiShell.dbgFlag = true; // set this to false if you don't want debug message output
  
  // a simple function that all debugging messages travel thru, 
  // change this if you want to direct them somewhere other than the js console:
  gApiShell.dbgmsg = function(msg) {
    if (gApiShell.dbgFlag) {
      console.log(msg);
    }
  };
  
  // ----------------------------------------------------------
  // the REST functions for the 'note' resource.
  // ----------------------------------------------------------

  // Note how the passed in 'data' object is further packaged inside another object before 
  // being passed into JSON.stringify(). This is the key difference from the drupal.org
  // documentation at http://drupal.org/node/783460, the section "Writing a simple JavaScript client"
  gApiShell.note.create = function (data, callback) {
    gApiShell.dbgmsg('inside note.create, about to POST to ' + this.apiPath);
    $.ajax({ type:        "POST",
             url:         this.apiPath,
             data:        JSON.stringify({data: data}),
             dataType:    'json',
             contentType: 'application/json',
             success:     callback
          });
  };
  
  gApiShell.note.retreive = function (id, callback) {
    gApiShell.dbgmsg('inside note.retreive, about to execute GET on url: ' + this.apiPath + '/' + id);
    $.ajax({ type: "GET",
             url: this.apiPath + '/' + id,
             dataType: 'json',
             success: callback
          });
  };

  // Note how the passed in 'data' object is further packaged inside another object before 
  // being passed into JSON.stringify(). This is the key difference from the drupal.org
  // documentation at http://drupal.org/node/783460, the section "Writing a simple JavaScript client"
  gApiShell.note.update = function (data, callback) {
    gApiShell.dbgmsg('inside note.update, about to execute PUT on url: ' + this.apiPath + '/' + data.id);
    $.ajax({ type:        "PUT",
             url:         this.apiPath + '/' + data.id,
             data:        JSON.stringify({data: data}),
             dataType:    'json',
             contentType: 'application/json',
             success:     callback
          });
  };

  gApiShell.note.del = function (id, callback) {
    gApiShell.dbgmsg('inside note.del, about to execute DELETE to ' + this.apiPath + '/' + id);
    $.ajax({ type:     "DELETE",
             url:      this.apiPath + '/' + id,
             dataType: 'json',
             success:  callback
          });
  };

  gApiShell.note.index = function (callback) {
    gApiShell.dbgmsg('inside note.index, about to execute $.getJSON on ' + this.apiPath );
    $.getJSON(this.apiPath, callback);
  };

  // fails! (no idea if this is how one would call a Targeted Action)
  gApiShell.note.deactivate = function (id, callback) {
    gApiShell.dbgmsg('inside note.deactivate, about to POST to ' + this.apiPath + '/' + id + '/deactivate');
    $.ajax({ type:        "POST",
             url:         this.apiPath + '/' + id + '/deactivate',
             data:        JSON.stringify(id),
             dataType:    'json',
             contentType: 'application/json',
             success:     callback
          });
  };

  // ----------------------------------------------------------
  // REST functions for the note resource are completed
  // ----------------------------------------------------------
 
  // testing routines
  
  // directly test retreive:
  gApiShell.note.testRetreive = function() {
    var data = {};
    data.id = '14';
    gApiShell.note.retreive(data.id, function (res) {
        gApiShell.dbgmsg('inside note testRetreive callback!');
        gApiShell.note_append(res,true);
      });
  };
  
  // test the 'note deactivate' targeted action
  // fails!
  gApiShell.note.testDeactivate = function() {
    gApiShell.note.deactivate('13', function (res) {
        gApiShell.dbgmsg('inside note testDeactivate callback!');
      });
  };
  
  // test sending a parameter to the index operation
  // fails! (it reaches the Service calback, but the parameter is not there!)
  gApiShell.note.testIndexParam = function() {
    gApiShell.dbgmsg('inside note.testIndexParam, about to execute $.getJSON on ' + this.apiPath );
    $.getJSON(this.apiPath, { id: '14' }, function (res) {
        gApiShell.dbgmsg('inside note.testIndexParam callback!');
      });
  };
  
  // this is the initialization routine for this testing framework.
  // call this first from the Firebug javascript console.
  gApiShell.note.test = function () {
    var subject, text, list, note, note_saved, note_append;

    $('<div id="notetaking"><form>' +
        '<div class="subject-wrapper"><label for="note-subject">Subject</label></div>' +
        '<div class="note-wrapper"></div>' + 
        '<input class="cancel" type="submit" value="Cancel" />' +
        '<input class="save" type="submit" value="Save" />' +
        '<input class="action" type="submit" value="Targeted Action" />' +
        '<input class="reset" type="submit" value="Reset UI" />' +
      '</form></div>').appendTo('body');
    
    subject = $('<input class="subject" type="text" />').appendTo('#notetaking .subject-wrapper');
    text = $('<textarea class="note" cols="20" rows="5" />').appendTo('#notetaking .note-wrapper');
    list = $('<ul></ul>').appendTo('#notetaking');

    // Stop the form from submitting
    $('#notetaking form').submit(function () {
      return false;
    });

    // hide the cancel button and then attach a click callback
    // (the cancel button only appears when editing a note)
    $('#notetaking input.cancel').hide().click(function () {
      note = null;
      $(this).hide();
      $(subject).val('');
      $(text).val('');
    });
    
    // attach a click callback to the targeted action button:
    $('#notetaking input.action').click(function () {
      gApiShell.note.testDeactivate();
    });

    // attach a click callback to the Reset UI button:
    $('#notetaking input.reset').click(function () {
      note = null;
      $('#notetaking input.save').show();
      $('#notetaking input.cancel').hide();
      $(subject).val('');
      $(text).val('');
    });

    // attach a click callback to the save button
    $('#notetaking input.save').click(function () {
      var data;
      
      gApiShell.dbgmsg('inside save callback!');
      
      $(this).hide(); // hide ourselves and the cancel button
      $('#notetaking input.cancel').hide();

      data = { // populate the data object with information
        subject: $(subject).val(),
        note: $(text).val()
      };

      // if the user was editing an existing note, note will be non-NULL,
      // if note is NULL then the user is creating a new note:
      if (note) {
        data.id = note.id;
        note = null;
        gApiShell.dbgmsg('calling note update!');
        gApiShell.note.update(data, note_saved);
      }
      else {
        gApiShell.dbgmsg('calling note create!');
        gApiShell.note.create(data, note_saved);
      }
      note = null;
      gApiShell.dbgmsg('leaving save click callback');
    });

    // this callback is executed upon successful note update/create
    note_saved = function (res) {
      gApiShell.dbgmsg('inside note_saved!');
      
      note = null;
      $(subject).val('');
      $(text).val('');
      $('#notetaking input.save').show();

      gApiShell.dbgmsg('calling note retreive!');
      gApiShell.note.retreive(res.id, function (res) {
        gApiShell.dbgmsg('inside note retreive callback!');
        note_append(res);
      });
    };

    // this callback is executed upon successful note retrieve and index:
    note_append = function (noteData, bottom) {
      gApiShell.dbgmsg('inside note_append with nodeData.id of ' + noteData.id);
      var noteNode;

      $('#note-' + noteData.id).remove();
      
      noteNode = $('<li class="note">' +
        '<strong class="subject"></strong> ' +
        '<span class="text"></span>' +
        '<ul>' +
          '<li><a class="delete">Delete</a></li>' +
          '<li><a class="edit">Edit</a></li>' +
        '</ul>' +
      '</li>');
      noteNode.attr('id', 'note-' + noteData.id);

      if (bottom) {
        noteNode.appendTo(list);
      }
      else {
        noteNode.prependTo(list);
      }

      $('.subject', noteNode).text(noteData.subject);
      $('.text', noteNode).text(noteData.note);
      $('.delete', noteNode).click(function () {
        gApiShell.note.del(noteData.id, function () {
          $(noteNode).remove();
        });
      });
      $('.edit', noteNode).click(function () {
        note = noteData;
        subject.val(noteData.subject);
        text.val(noteData.note);
        $('#notetaking input.cancel').show();
      });
    };
    // make available to the testRetreive() function:
    gApiShell.note_append = note_append;

    // calling the note index function, passing in a handling callback:
    gApiShell.note.index(function (res) {
      var i, length;
      gApiShell.dbgmsg('inside note index success callback!');
      gApiShell.dbgmsg('res len is ' + res.length);
      for (i = 0, length = res.length; i < length; i++) {
        var data = res[i];
        gApiShell.dbgmsg('calling note_append with id ' + data.id);
        note_append(res[i], true);
      }
    });
  };
  
};