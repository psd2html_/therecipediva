<?php
$width = "";
$height = "";
$slot = "";
$divID = "";
$divID1 = "";
switch ($block->content->position){
    case 0:
        $width = "321";
        $height = "76";
        $slot = "7510730039";
        $divID = "banner1";
        $divID1 = "banner_ads";
        break;
    case 1:
        $width = "220";
        $height = "317";
        $slot = "6365282506";
        $divID = "ads_left_col";
        $divID1 = "ads_left_col_content";
        break;
    case 2:
        $width = "413";
        $height = "164";
        $slot = "1119878223";
        $divID = "ads_content_contain";
        $divID1 = "ads_content";
        break;
    case 3:
        $width = "340";
        $height = "463";
        $slot = "1119878223";
        $divID = "ads_content_contain_right";
        $divID1 = "ads_content_right";
        break;
    case 4:
        $width = "222";
        $height = "700";
        $slot = "1119878223";
        $divID = "ingredients_left_ads";
        $divID1 = "ingredients_left_ads_content";
        break;
    case 5:
        $width = "638";
        $height = "150";
        $slot = "1119878223";
        $divID = "ads_content_contain_bot";
        $divID1 = "ads_content_bot";
        break;
}

$httpLink = "http://";
$url_ad = $block->content->field_advertise_link_value;
if (strpos($url_ad, $httpLink) !== 0){
    $url_ad = $httpLink . $url_ad;
}
?>

<div id="<?php print $divID?>">
    <div id="<?php print $divID1?>">
        <?php if(file_exists($block->content->filepath)){
            $file_path = recipe_utils::get_resize_image_path($block->content->filepath, $width);
            print t('<a href="'.$url_ad.'" target="_blank"><img src="'.C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path, $width, $height).' border="0"></a>');
        }?>
    </div>
</div>
