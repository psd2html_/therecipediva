// show hide player
function MM_findObj(n, d) { //v4.01
  var p,i,x;
  if(!d)
  d=document;
  if((p=n.indexOf("?"))>0&&parent.frames.length)
    {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
    }
  if(!(x=d[n])&&d.all) x=d.all[n];
  for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  if ((obj=MM_findObj(args[i]))!=null) {
    v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function previewNews(base_url){
    if($.trim($("#edit-field-news-image-0-upload").val()) != ""){
        var options = {
                        success:       function(responseText, statusText, xhr, $form)  {
                                            if(responseText != "error"){
                                                var pathfile = "";
                                                if(responseText != ""){
                                                    pathfile = responseText;
                                                    $("#hdf_filepath_image").val(pathfile);
                                                    var myform = document.forms['node-form'];
                                                    var faction = myform.action;
                                                    var windowname = "preview";
                                                    myform.action = base_url + 'news_preview';
                                                    window.open('', windowname, 'height=1200,width=1200,scrollbars=yes');
                                                    myform.target=windowname;
                                                    myform.submit();
                                                    myform.action = faction;
                                                }
                                            }
                                        } ,
                        url:          base_url + 'news_check_image_upload'
                        };
        $('#node-form').ajaxSubmit(options);
    }else{
        var myform = document.forms['node-form'];
        var windowname = "preview";
        var faction = myform.action;
        myform.action = base_url + 'news_preview';
        window.open('', windowname, 'height=1200,width=1200,scrollbars=yes');
        myform.target=windowname;
        myform.submit();
        myform.action = faction;
    }
    return false;
}

function submit_search_form(url,kwid) {
    url_encoded = url;
    if($.trim($("#"+kwid).val()) != ""){
        url_encoded += "?"+kwid+"=" + encodeURIComponent($.trim($("#"+kwid).val()));
    }
    window.location = url_encoded;
    return true;
}
function removeImage(name){
    var objDeleteImage= document.getElementById("edit-hdf-delete-image-" + name);
    var objHdfImage = document.getElementById("edit-hdf-filepath-image-" + name);
    objHdfImage.value = "";

    objDeleteImage.value = "1";
    $("#div_image_" + name).hide();
    $("#img_" + name).hide();
    $("#btn_remove_image_" + name).hide();
    $("#div_upload_image_" + name).show();

}
