function addFaviroteNode(url, id, id_display, message, sel_index){
    $.post(url, function(data){
        if(data != "false"){
            addFavorite(id, id_display, sel_index);
            nid_copy = data;
            flagAddFavorite = true;
            showInfoMessage(message);
        }else{
            showInfoMessage("Adding into favorite is fail.");
        }

    });

}

function addFaviroteNode_new(url_check, url, message, message_complete, message_error){
    $.post(url_check, function(bExisted){
        //Data do not exist in user's favorite
        if(bExisted == "false"){
            $.post(url, function(data){
                if(data != "false"){
                    showInfoMessage(message);
                }else{
                    showInfoMessage(message_error);
                }

            });
        }else{
            showInfoMessage(message_complete);
        }
    });
}

function addFavorite(id, id_display, sel_index){
    var objAddfavorite= document.getElementById(id);
    var objNoneAdd= document.getElementById(id_display);
    if (id == "add_all_grocery" || id == "add_all_ingredients"){
        document.getElementById("add_all_grocery").style.display = "none";
        document.getElementById("add_all_ingredients").style.display = "none";
        document.getElementById("add_all_ingredients_none").style.display = "";
        document.getElementById("add_all_grocery_none").style.display = "";
        if (sel_index>0){
            for (var idx=1; idx <= sel_index; idx++){
                var idIngredient = "individual_plus_" + idx;
                document.getElementById(idIngredient).innerHTML = '<a href="javascript:showInfoMessage(\'The ingredient have already been added to your Grocery List.\');">[+]</a>';
            }
        }
    }else{
        if (id.indexOf("individual_plus")>=0 && sel_index>0){
            objAddfavorite.innerHTML = '<a href="javascript:showInfoMessage(\'The ingredient have already been added to your Grocery List.\');">[+]</a>';
        }else{
            if (objAddfavorite != null) {
                objAddfavorite.style.display = "none";
                if (sel_index != undefined && nodeids != undefined) {
                    nodeids[sel_index] = '';
                }
            }
            if (objNoneAdd != null) {
                objNoneAdd.style.display = "";
            }
        }
    }
}
//show message
function showInfoMessage(message){
    //alert(message);
    $("#dialogInfo").attr("title", message);
    $("#ui-dialog-title-dialogInfo").text(message);
    $("#ui-dialog-title-dialogInfo").css("text-align", "center");
    $("#dialogInfo").css("text-align", "center");
    $("#dialogInfo").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "OK": function() {
                        $(this).dialog("close");

                        if(message == msg_recipe || message == msg_tip){
                            /*$("#div_live_feed").html('');
                            num_page = -1;
                            showMoreFeed();*/
                            var nid = $("#nid_saved").html();
                            showRecipeTipSaved(nid);
                        }
                    }
                }
            },
            { open: function(event, ui) {
                        $("#dialogInfo").parents("div[class^=ui-dialog]").css("width", "auto");
                    }
            }
        );
}

//show message and close window
function showInfoMessage_1(message){
    //alert(message);
    $("#dialogInfo").attr("title", message);
    $("#ui-dialog-title-dialogInfo").text(message);
    $("#ui-dialog-title-dialogInfo").css("text-align", "center");
    $("#dialogInfo").css("text-align", "center");
    $("#dialogInfo").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "OK": function() {
                        $(this).dialog("close");
                        window.close();
                    }
                }
            }
        );
}

function addFolder(url, id, id_display, message,id_dialog, sel_index){
    addFaviroteNode(url + "/" + 0, id, id_display, message, sel_index);
    /*$(id_dialog).dialog({ closeText: 'hide' },
                    { modal: true },
                    { resizable: false },
                    { buttons:
                        {  "Cancel": function() { $(this).dialog("close"); }
                            ,"OK": function() {
                                var folder = 0;
                                var objSelect= document.getElementById("cbb_folder_recipe");

                                if (objSelect != null) {
                                    folder = objSelect.value;
                                }
                                addFaviroteNode(url + "/" + folder, id, id_display, message, sel_index);
                                $(this).dialog("close");
                            }
                        }
                    }
                );*/
}

function confirmDeleteComment(url, cid, message){
    $("#dialogConfirm").attr("title", message);
    $("#ui-dialog-title-dialogConfirm").text(message);
    $("#dialogConfirm").dialog(
        { modal: true },
        { resizable: false },
        { minHeight: 0 },
        { buttons:
            {
                "Yes": function() {
                            document.getElementById("cid").value = cid;
                            document.forms['commentconfirmdelete'].action = url;
                            document.forms['commentconfirmdelete'].submit();
                            $(this).dialog("close");
                            return true;
                        },
                "No": function() {
                            $(this).dialog("close");
                            return false;
                        }
            }
        }
    );
}

function addNewItemAmin(frm, url){
    frm.action = url;
    frm.submit();
}
function changePaging(frm_name, url, index) {
    var frm = document.getElementById(frm_name);
    frm.page.value = index;
    if(url != "" && (url.indexOf("adminrecipe/category") >= 0
        || url.indexOf("adminrecipe/recipes") >= 0
        || url.indexOf("adminrecipe/units") >= 0
        || url.indexOf("adminrecipe/aisles") >= 0
        || url.indexOf("adminrecipe/cookofweek") >= 0
        || url.indexOf("adminrecipe/news") >= 0
        || url.indexOf("adminrecipe/advertise") >= 0
        || url.indexOf("adminrecipe/entertext") >= 0
        || url.indexOf("adminrecipe/maincontent") >= 0
        || url.indexOf("adminrecipe/staticpage") >= 0
        || url.indexOf("adminrecipe/user") >= 0))
    {
        url_encoded = url;
        if($.trim($("#txtSearch").val()) != ""){
            url_encoded += "&txtSearch=" + encodeURIComponent($.trim($("#txtSearch").val()));
        }
        window.location = url_encoded;
    }else if(url != "" && (url.indexOf("search_result") >= 0)){
        url_encoded = url;
        if($.trim($("#txtKeyword").val()) != ""){
            url_encoded += "&txtKeyword=" + encodeURIComponent($.trim($("#txtKeyword").val()));
        }
        window.location = url_encoded;
    }else if(url != "" && (url.indexOf("mydiva/") >= 0)){
        window.location = url;
    }
    else{
        if (url != '') {
            frm.action = url;
        }
        frm.submit();
    }

}
function goPaging(frm_name, url, page_index) {
    url = url + '\/' + document.getElementById('edit-hdf-tid').value;
    url = url + "?page=" + page_index;
    url = url + "&type=" + document.getElementById('edit-hdf-type').value;
    url = url + "&sort=" + document.getElementById('edit-cbbSort').value;
    url = url + "&kw=" +  encodeURIComponent(document.getElementById('edit-txtKeyword').value);
    url = url + "&terms=" + encodeURIComponent(document.getElementById('edit-hdf-terms').value);
    for(var idx=1; idx <6; idx++){
        var strKey = encodeURIComponent(document.getElementById("edit-txtSearchIn-" + idx).value);
        if(trim(strKey) != ""){
            url = url + "&In_" + idx + "=" + strKey;
        }
        var strNotKey = encodeURIComponent(document.getElementById("edit-txtSearchEx-" + idx).value);
        if(trim(strNotKey) != ""){
            url = url + "&Ex_" + idx + "=" + strNotKey;
        }
    }
    window.location = url.replace(/\\/g,'');

}
function expandedCategory(idExpanded, idCollapsed){
    var objExpanded= document.getElementById(idExpanded);
    var objCollapsed= document.getElementById(idCollapsed);
    if (objExpanded != null) {
        objExpanded.style.display = "";
    }
    if (objCollapsed != null) {
        objCollapsed.style.display = "none";
    }
}

function deletePageItem(frmName, url, delId, message) {
    $("#dialogConfirm").attr("title", message);
    $("#ui-dialog-title-dialogConfirm").text(message);
    $("#dialogConfirm").dialog(
        { modal: true },
        { resizable: false },
        { minHeight: 0 },
        { buttons:
            {
                "OK": function() {
                    deleteItem(frmName, url, delId);
                    $(this).dialog("close");
                    return true;
                },
                "Cancel": function() {
                    $(this).dialog("close");
                    return false;
                }
            }
        }
    );
}

function deleteItem(frmName, url, delId) {
    var frm = document.forms[frmName];
    if (url != '') {
        frm.action = url;
    }
    frm.op.value = 'DeleteItem';
    frm.delId.value = delId;
    frm.submit();
}
function deleteAdminList(frmName, fieldNameCheck, url, message) {
    var frm = document.forms[frmName];
    var isCheck = isChecked(frm, fieldNameCheck);
    if (isCheck == false) {
        showInfoMessage('Please select item to delete.');
        return false;
    }
    if(message == undefined || message == ''){
        message = "Are uoy sure you want to delete these items?";
    }
    $("#dialogConfirm").attr("title", message);
    $("#ui-dialog-title-dialogConfirm").text(message);
    $("#dialogConfirm").dialog(
        { modal: true },
        { resizable: false },
        { minHeight: 0 },
        { buttons:
            {
                "Yes": function() {
                            if (url != '') {
                                frm.action = url;
                            }
                            frm.op.value = 'Delete';
                            frm.submit();
                            $(this).dialog("close");
                            return true;
                        },
                "No": function() {
                            $(this).dialog("close");
                            return false;
                        }
            }
        }
    );
}

function deleteAdminItem(frmName, url, delId, message) {
    if(message == undefined || message == ""){
        message = "Are you sure you want to delete this item?";
    }
    $("#dialogConfirm").attr("title", message);
    $("#ui-dialog-title-dialogConfirm").text(message);
    $("#dialogConfirm").dialog(
        { modal: true },
        { resizable: false },
        { minHeight: 0 },
        { buttons:
            {
                "Yes": function() {
                            deleteItem(frmName, url, delId);
                            $(this).dialog("close");
                            return true;
                        },
                "No": function() {
                            $(this).dialog("close");
                            return false;
                        }
            }
        }
    );
}

function isChecked(frm, fieldNameCheck) {
    var isCheck = false;
    for (var i=0; i < frm.elements.length; i++)
    {
        var e = frm.elements[i];
        if (e.type=='checkbox' && e.name.indexOf(fieldNameCheck)>=0 && !e.disabled && e.checked)
        {
            isCheck = true;
        }
    }
    return isCheck;
}

function saveNode(opVal, url) {
    var op = document.getElementById('hidOp');
    if(op != null){
         op.value = opVal;
    }
    var option  = document.getElementById('op');
    if(option != null){
         option.value = opVal;
    }
    var frm = document.getElementById('node-form');
    if (frm != undefined) {
        frm.target = '_self';
        if (url != undefined && url != '') {
            frm.action = url;
        }
    }
    return true;
}

function previewNode(opVal, url) {
    var op = document.getElementById('hidOp');
    if(op != null){
         op.value = opVal;
    }
    var frm = document.getElementById('node-form');
    if (frm != undefined) {
        if (url != '') {
            frm.action = url;
        }
        frm.target = '_blank';
        /**var windowname = "preview";
        window.open('', windowname, 'height=1200,width=1200,scrollbars=yes');
        frm.target=windowname;**/
    }
    return true;
}

function deleteTermConfirm() {
    if (confirm('Do you want to delete this item?')){
        var op = document.getElementById('edit-delete');
        if(op != null){
             op.value = "Delete";
        }
        alert(op.value);
        return true;
    }
    return false;
}
// Removes leading whitespaces
function LTrim( value ) {

    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");

}

// Removes ending whitespaces
function RTrim( value ) {

    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");

}

// Removes leading and ending whitespaces
function trim( value ) {

    return LTrim(RTrim(value));

}

/**
 * START-------------------- DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dteNow = new Date();
var dtCh= "/";
var minYear=1900;
var maxYear=dteNow.getFullYear();

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
        if (i==2) {this[i] = 29}
   }
   return this
}

function isDate(dtStr){
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strMonth=dtStr.substring(0,pos1)
    var strDay=dtStr.substring(pos1+1,pos2)
    var strYear=dtStr.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
        //alert("The date format should be : mm/dd/yyyy")
        return false
    }
    if (strMonth.length<1 || month<1 || month>12){
        //alert("Please enter a valid month")
        return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        //alert("Please enter a valid day")
        return false
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        //alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
        return false
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        //alert("Please enter a valid date")
        return false
    }
    return true
}
/**
 * END-------------------- DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */

/**
  * selected all check box with another field
  * @param: form, field
  * @return none
  * **/
function checkAll(frm, FieldAll)
{
    var str = '';
    for (var i=0;i<frm.elements.length;i++)
    {
        var e=frm.elements[i];
        str = FieldAll + '--';
        if (e.type=='checkbox' && e.name!=FieldAll.name && !e.disabled)
        {
            e.checked=FieldAll.checked;
        }
    }
}

/**
  * selected all check box with another field
  * @param: form, fieldcheck, field name
  * @return none
  * **/
function checkAll1(frm, FieldNameCheck, FieldAll)
{
    for (var i=0;i<frm.elements.length;i++)
    {
        var e=frm.elements[i];
        if (e.type=='checkbox' && e.name.indexOf(FieldNameCheck)>=0 && !e.disabled)
        {
            e.checked=FieldAll.checked;
        }
    }
}

/**
  * selected all check box with another field
  * @param: form, fieldcheck, field name
  * @return none
  * **/
function checkAllChange(frm, fieldNameCheck, inputfield, fieldAll)
{
    var isChecked = true;
    for (var i=0;i<frm.elements.length;i++)
    {
        var e = frm.elements[i];
        if (e.type=='checkbox' && e.name.indexOf(fieldNameCheck)>=0 && !e.disabled)
        {
            if (e.checked == false) {
                isChecked = false;
                break;
            }
        }
    }
    fieldAll.checked = isChecked;

}

/**
  * selected all check box with another field
  * @param: form, fieldcheck, field name
  * @return none
  * **/
function checkAll2(frm, FieldAll, FieldEnd)
{
    var iStart;
    var iEnd;
    for (var i=0;i<frm.elements.length;i++)
    {
        var e=frm.elements[i];
        if (e.type=='checkbox' && e.name==FieldAll.name)
        {
            iStart=i;
        }
        if (e.name==FieldEnd.name)
        {
            iEnd=i;
        }
    }
    for (var i=0;i<frm.elements.length;i++)
    {
        var e=frm.elements[i];
        if (e.type=='checkbox' && e.name!=FieldAll.name && (i>iStart && i<iEnd) && !e.disabled)
        {
            e.checked=FieldAll.checked;
        }
    }
}
function validatePassword(value) {
    var error = "";
    var illegalChars = /[\s]/;

    if (value.length > 0 && value.length < 6) {
        //error = "The password length at least 6 characters";
        return false;
    }

    if (illegalChars.test(value)) {
        //error = "The password contains illegal characters";
        return false;
    }
       return true;
}
function validateEmail(value) {
    var error="";
    var emailFilter = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ ;

    if (!emailFilter.test(value)) {
        return false;
    }

    return true;
}
function validatePostcode(value) {
    var error="";
    var pcFilter = /^[\s0-9a-zA-Z-]+$/;

    if ($.trim(value)!="" && !pcFilter.test(value)) {
        return false;
    }

    return true;
}


function changeAdminStatus(frmName, fieldNameCheck, url) {
    var frm = document.forms[frmName];
    var isCheck = isChecked(frm, fieldNameCheck);
    if (isCheck == false) {
        alert('Please select item to change status.');
        return false;
    }
    if (url != undefined && url != '') {
        frm.action = url;
    }

    var selVal = frm.cbbStatus.options[frm.cbbStatus.selectedIndex].text;

    if ( selVal == 'Delete') {
        return deleteAdminList(frmName, fieldNameCheck, url);
    }

    frm.op.value = 'ChangeStatus';
    frm.submit();
    return true;
}

function submitForm(frmName, url, operation) {
    var frm = document.forms[frmName];
    if (url != undefined && url != '') {
        frm.action = url;
    }
    if (operation != undefined) {
        var op = frm.op;
        if (op != undefined) {
            op.value = operation;
        }
    }
    frm.submit();
    return true;
}

/**
 * Check numeric
 **/
function IsNumeric(sText, ValidChars)
{
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++)
      {
      Char = sText.charAt(i);
      if (ValidChars.indexOf(Char) == -1)
         {
         IsNumber = false;
         }
      }
   return IsNumber;

}
/**
 * Resize images
 **/
function ResizeImage(contain_id, max_width)
{
   var listImage = $("#"+contain_id).find("img");
   for (i = 0; i < listImage.length; i++) {
           if(listImage[i].width > max_width){
               listImage[i].width = max_width;
               listImage[i].removeAttribute('height');
           }
   }
}



/**
  * selected all check box with another field
  * @param: form, fieldcheck, field name
  * @return none
  * **/
function checkAllImage(frm, FieldNameCheck, value)
{
    for (var i=0;i<frm.elements.length;i++)
    {
        var e=frm.elements[i];
        var imageCheck = "img_check_" + FieldNameCheck;
        var imageUnCheck = "img_uncheck_" + FieldNameCheck;

        if (e.type=='hidden' && e.name.indexOf(FieldNameCheck)>=0 && e.name.indexOf("hdf_")<0 && !e.disabled)
        {
            var name = "hdf_" + e.name;
            var objItem= document.getElementById(name);

            if (value == 0){
                e.value = "";
            }else if(objItem != null){
                e.value = objItem.value;
            }

        }
        if (e.type=='button' && e.name.indexOf(imageCheck)>=0 && !e.disabled)
        {
            if (value == 0){
                e.style.display = "none";
            }else{
                e.style.display = "";
            }
        }
        if (e.type=='button' && e.name.indexOf(imageUnCheck)>=0 && !e.disabled)
        {
            if (value == 0){
                e.style.display = "";
            }else{
                e.style.display = "none";
            }
        }
    }
}
/**
  * set subcat had checked
  * @param: form, fieldcheck
  * @return none
  * **/
function getTerm(frm, fieldNameCheck){
    var objTerms= document.getElementById("hdf_terms");
    var arrTerms = new Array();
    var idx = 0;

    for (var i=0;i<frm.elements.length;i++)
    {
        var e=frm.elements[i];

        if (e.type =='hidden' && e.name.indexOf(fieldNameCheck)>=0 && !e.disabled)
        {
            var hdf_name = "hdf_" + e.name;
            if ((e.name.indexOf("hdf_")< 0) && e.value != ""){
                arrTerms[idx] = e.value;
                idx++;
            }
        }
    }
    objTerms.value = arrTerms.join(",");
}
/**
  * check on image check button
  * @param: form, fieldcheck,
  * @return none
  * **/
function checkItemImage(frm, fieldNameCheck, fieldNameCheck1, value) {
    var objTermValue= document.getElementById("hdf_" + fieldNameCheck);
    var objItem= document.getElementById(fieldNameCheck);
    var objImgItemCheck = document.getElementById("img_check_" + fieldNameCheck);
    var objImgItemUnCheck = document.getElementById("img_uncheck_" + fieldNameCheck);

    if (value == 0){
        objImgItemCheck.style.display = "none";
        objImgItemUnCheck.style.display = "";
        objItem.value = "";
    }else{
        objImgItemCheck.style.display = "";
        objImgItemUnCheck.style.display = "none";
        objItem.value = objTermValue.value;
    }
    getTerm(frm, fieldNameCheck1);
}

function showConfirmBecomePrimium(){
    $("#dialogConfirmBecomePrimium").dialog(
        { modal: true },
        { resizable: false },
        { width: 310 },
        { minHeight: 0 }
    );
}

function closeDialogConfirmBecomePrimium(){
    $("#dialogConfirmBecomePrimium").hide();
    $("#dialogConfirmBecomePrimium").dialog("close");
}

function onload_radio_checkbox(divID) {
    $('body').addClass('has-js');
    var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
    var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
    if(divID == undefined){
        $('.label_check, .label_radio').click(function(){
            if (jQuery.browser.msie && (ie55 || ie6)) {
                if($(this).find('input').is(":checked")){
                    if($(this).find('input').attr("type")=="checkbox"){
                        $(this).find('input').attr("checked", false);
                    }
                }else{
                    $(this).find('input').attr("checked", true);
                }
            }
            setupLabel(divID);
        });
    }
    else{
        $('#'+divID).find('.label_check, .label_radio').click(function(){
            if (jQuery.browser.msie && (ie55 || ie6)) {
                if($(this).find('input').is(":checked")){
                    if($(this).find('input').attr("type")=="checkbox"){
                        $(this).find('input').attr("checked", false);
                    }
                }else{
                    $(this).find('input').attr("checked", true);
                }
            }
             setupLabel(divID);
        });
    }
    setupLabel(divID);
}
function setupLabel(divID) {
    if(divID == undefined){
        if ($('.label_check input').length) {
            $('.label_check').each(function(){
                $(this).removeClass('c_on');
            });
            $('.label_check input:checked').each(function(){
                $(this).parent('label').addClass('c_on');
            });
        };
        if ($('.label_radio input').length) {
            $('.label_radio').each(function(){
                $(this).removeClass('r_on');
            });
            $('.label_radio input:checked').each(function(){
                $(this).parent('label').addClass('r_on');
            });
        };
    }
    else{
        if ($('#'+divID).find('.label_check input').length) {
            $('#'+divID).find('.label_check').each(function(){
                $(this).removeClass('c_on');
            });
            $('#'+divID).find('.label_check input:checked').each(function(){
                $(this).parent('label').addClass('c_on');
            });
        };
        if ($('#'+divID).find('.label_radio input').length) {
            $('#'+divID).find('.label_radio').each(function(){
                $(this).removeClass('r_on');
            });
            $('#'+divID).find('.label_radio input:checked').each(function(){
                $(this).parent('label').addClass('r_on');
            });
        };
    }
};

function onload_feedback_radio_checkbox(divID) {
    $('body').addClass('has-js');
    if(divID == undefined){
        $('.label_feedback_radio').click(function(){
            setupFeedbackLabel(divID);
        });
    }
    else{
        $('#'+divID).find('.label_feedback_radio').click(function(){
            setupFeedbackLabel(divID);
        });
    }
    setupFeedbackLabel(divID);
}
function setupFeedbackLabel(divID) {
    if(divID == undefined){
        if ($('.label_feedback_radio input').length) {
            $('.label_feedback_radio').each(function(){
                $(this).removeClass('r_on');
            });
            $('.label_feedback_radio input:checked').each(function(){
                $(this).parent('label').addClass('r_on');
            });
        };
    }
    else{
        if ($('#'+divID).find('.label_feedback_radio input').length) {
            $('#'+divID).find('.label_feedback_radio').each(function(){
                $(this).removeClass('r_on');
            });
            $('#'+divID).find('.label_feedback_radio input:checked').each(function(){
                $(this).parent('label').addClass('r_on');
            });
        };
    }
};

function change_image_elements(url, fid) {
    var delFid = document.getElementById("hidDeleteFid");
    var divInputImage = document.getElementById("image_input");
    var divRemoveBt = document.getElementById("image_remove");
    var divPreviewImage = document.getElementById("image_preview");
    if (divInputImage != undefined && divInputImage != null) {
        divInputImage.style.display = '';
    }
    if (divRemoveBt != undefined && divRemoveBt != null) {
        divRemoveBt.style.display = 'none';
    }
    if (divPreviewImage != undefined && divPreviewImage != null) {
        divPreviewImage.style.display = 'none';
    }
    if (delFid != undefined && delFid != null) {
        delFid.value = fid;
    }
    $.post(url, function(data){
    });
    return false;
}
function onload_image_upload() {
    var divInputImage = document.getElementById("image_input");
    if (divInputImage != undefined && divInputImage != null) {
        divInputImage.style.display = 'none';
    }
}
function removeImageCategory(id_undisplay, id_display, id_delete){
        var objUpload= document.getElementById(id_display);
        var objDelete= document.getElementById(id_delete);

        objDelete.value = "1";

        $("#" + id_undisplay).hide();
        objUpload.style.display = "";
    }
function expand_description(id1, id2){
    var objHide= document.getElementById(id1);
    var objShow= document.getElementById(id2);

    objHide.style.display = "none";
    objShow.style.display = "";
}

/*open popup termofuse, privacy*/
function termofuse(url_base){
    window.open(url_base, null, 'width=610,height=500,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no,resizable=yes');
}

//admin search
function searchData(url, id){
    url = url + "?" + id + "=" + encodeURIComponent(document.getElementById(id).value);
    window.location = url;
}
