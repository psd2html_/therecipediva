// Gallery_upload.php
function	openGallery(){
        $("#btn_gallery_upload").unbind();
        $("#btn_gallery_upload").click(function(){sendGallery();});
        $("#div_gallery").dialog(
            {	modal:	true	},
            {	resizable:	false	},
            {	height:	530},
            {	width:	480},
            {	draggable:	false},
            {	buttons:
                {
                    "Close":	function()	{
                                $(this).dialog("close");
                            }
                }
            },
            {	open:	function(event,	ui)	{
                        $("div[class^=ui-dialog-titlebar]").hide();
                        $("div[class^=ui-dialog-buttonpane]").hide();
                        $("#div_gallery").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                    }
            },
            {	beforeclose:	function(event,	ui)	{
                        $("#uploadFileGallery").val("");
                        $("#gallery_upload_error").html("");
                        $("#div_error_gallery_upload").hide();
                        $("#div_gallery").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                        $("div[class^=ui-dialog-titlebar]").show();
                        $("div[class^=ui-dialog-buttonpane]").show();
                    }
            }
        );
    }



    function	sendGallery(){

        //Check	Validate
        $("#gallery_upload_error").html("");
        $("#div_error_gallery_upload").hide();

        if($.trim($("#uploadFileGallery").val())	!=	""){

            document.getElementById("frmUpload").submit();

            timeInterval=setInterval("iframeUpload()",	500);

            $("#error").html("");
        }
        else
        {
            $("#gallery_upload_error").html("Please	choose	image	to	upload!");
            $("#div_error_gallery_upload").show();
        }
    }

    function	iframeUpload()
    {
        var	myIFrame	=	document.getElementById("iframe");
        var	content	=	trim(myIFrame.contentWindow.document.body.innerHTML);

        if(content	&&	content!=0)
        {
            clearInterval(timeInterval);

            if(content.match("sites/default/files")==null)
            {
                $("#gallery_upload_error").html(content);
                $("#div_error_gallery_upload").show();
            }
            else
            {
                myIFrame.contentWindow.document.body.innerHTML	=	"";
                $("#gallery_upload_error").html("Upload	success!");
                $("#div_error_gallery_upload").show();
                document.getElementById("frmUpload").reset();
            }

        }
    }


// cook_of_week.php


function openCOW(){
    $("#btn_send_message_cow").unbind();
    $("#btn_send_message_cow").click(function(){sendCOW();});
    $("#div_cookofweek").dialog(
        { modal: true },
        { resizable: false },
        { height: 530},
        { width: 480},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_cookofweek").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#txtSendMessageCow").val($.trim($("#txtSendMessageCow").val()));
                    $("#send_message_error_cow").html("");
                    $("#div_error_send_message_cow").hide();
                    $("#div_cookofweek").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
    $("#txtSubject").focus();
}

// diva_network_add_friend.php
function openRequestFriend(uid, fid, username){
    $("span[name=spn_username]").html(username);
    $("#img_request_user").attr("src",$("#img_user").attr("src"));
    $("#btn_request_friend").unbind();
    $("#btn_request_friend").click(function(){sendRequestFriend(uid, fid);});
    $("#div_request_friend").dialog(
        { modal: true },
        { resizable: false },
        { height: 530},
        { width: 480},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_request_friend").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#txtRequestMessage").val("");
                    $("span[name=spn_username]").html("");
                    $("#send_request_friend_error").html("");
                    $("#div_error_send_request_friend").hide();
                    $("#div_request_friend").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
    $("#txtRequestMessage").focus();
}

//diva_network_message.php
function openMessage(uid){
    $("#btn_send_message").unbind();
    $("#btn_send_message").click(function(){sendMessage(uid);});
    $("#div_message").dialog(
        { modal: true },
        { resizable: false },
        { height: 535},
        { width: 480},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_message").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#txtSendMessage").val("");
                    $("#txtSendSubject").val("");
                    $("#send_message_error").html("");
                    $("#div_error_send_message").hide();
                    $("#div_message").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
    $("#txtSubject").focus();
}


function mceToggle(id, linkid) {
    element = document.getElementById(id);
    link = document.getElementById(linkid);
    img_assist = document.getElementById('img_assist-link-'+ id);

    if (tinyMCE.getEditorId(element.id) == null) {
      tinyMCE.addMCEControl(element, element.id);
      element.togg = 'on';
      link.innerHTML = 'disable rich-text';
      link.href = "javascript:mceToggle('" +id+ "', '" +linkid+ "');";
      if (img_assist)
        img_assist.innerHTML = '';
      link.blur();
    }
    else {
      tinyMCE.removeMCEControl(tinyMCE.getEditorId(element.id));
      element.togg = 'off';
      link.innerHTML = 'enable rich-text';
      link.href = "javascript:mceToggle('" +id+ "', '" +linkid+ "');";
      if (img_assist)
        img_assist.innerHTML = img_assist_default_link;
      link.blur();
    }
  }



function showSecurityCode(){
        //$("#dialogSecurityCode").css("background","#fef3dd");
        $("#dialogSecurityCode").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "Close": function() {
                                $(this).dialog("close");
                            }
                }
            }
        );
    }

    function showAddressUSCA(){
        if($("#div_address_usca").css("display") == "inline"){
            $("#div_address_usca").hide();
            $("#txtCity").val("");
            $("#txtPostalCode").val("");
            $("#ddlState").val("");
            $("#txtAptSuite").val("");
            $("#txtStreetAddress").val("");
            $("#div_address_notusca").show();
        }else{
            $("#div_address_usca").show();
            $("#txtStreetAddressArea").val("");
            $("#div_address_notusca").hide();
        }
    }


    function openTerm(){
    $("#div_term").dialog(
        { modal: true },
        { resizable: false },
        { height: 600},
        { width: 480},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_term").parents("div[class^=ui-dialog]").removeClass("ui-widget-content");
                    $("#div_term_content").scrollTop(0);
                }
        },
        { beforeclose: function(event, ui) {
                    $("#div_term").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
}
function openPrivacy(){
    $("#div_privacy").dialog(
        { modal: true },
        { resizable: false },
        { height: 600},
        { width: 480},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_privacy").parents("div[class^=ui-dialog]").removeClass("ui-widget-content");
                    $("#div_privacy_content").scrollTop(0);
                }
        },
        { beforeclose: function(event, ui) {
                    $("#div_privacy").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
}


