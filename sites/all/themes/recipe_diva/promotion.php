<?php
global $user;
$current_user = user_load($user->uid);
profile_load_profile($current_user);
?>
<script>
function openAuthRequest(){
    $("#div_auth").dialog(
        { modal: true },
        { resizable: false },
        { height: 530},
        { width: 833},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_auth").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#div_auth").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
}
function openDonateRequest(){
    $("#div_donate").dialog(
        { modal: true },
        { resizable: false },
        { height: 530},
        { width: 833},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_donate").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#div_donate").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
}
function openPromotionRegistry(){
    $("#btn_send_info").unbind();
    $("#btn_send_info").click(function(){sendRegisterRequest();});
    $("#div_promotion").dialog(
        { modal: true },
        { resizable: false },
        { height: 730},
        { width: 833},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_promotion").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#txtAge").val($.trim($("#txtAge").val()));
                    $("#selIncome").val($("#selIncome").val());
                    $("#selEdu").val($("#selEdu").val());
                    $("#rdoOwnHome").val($("#rdoOwnHome").val());

                    $("#message_error").html("");
                    $("#div_error_message").hide();
                    $("#div_promotion").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
    $("#txtAge").focus();
}

function sendRegisterRequest(){
    var hasError = false;
    var errorMessage = "";
    var x=0;

    if ($.trim($("#txtAge").val()) == ""){
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Age'));?>" + "</li>";
        if(hasError == false){
            $("#txtAge").focus();
        }
        hasError = true;
    }

    if ($("#selIncome").val() == '') {
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Income'));?>" + "</li>";
        if(hasError == false){
            $("#selIncome").focus();
        }
        hasError = true;
    }

    if ($("#selEdu").val() == '') {
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Education'));?>" + "</li>";
        if(hasError == false){
            $("#selEdu").focus();
        }
        hasError = true;
    }

    if ($("#rdoOwnHome").val() == '') {
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Do you own a home'));?>" + "</li>";
        if(hasError == false){
            $("#rdoOwnHome").focus();
        }
        hasError = true;
    }

    var objError = document.getElementById("div_error_message");
    if(hasError){
        $("#message_error").html(errorMessage);
        objError.style.display = "";
        return false;
    }else{
        objError.style.display = "none";
    }
    $("#btn_send_info").unbind();
    $.post("<?php print C_BASE_PATH."popup/promotion"?>",
                   { txtAge:$("#txtAge").val(), selIncome:$("#selIncome").val(), selEdu:$("#selEdu").val(), rdoOwnHome:$("#rdoOwnHome").val(), pid:$("#pid").val() },
                   function(data){
                        var json = eval("(" + data + ")");
                        $('#div_promotion').dialog('close');
                        if (json['status'] == "success") {
                            if (json['register'] == "unregistered") {
                                showInfoMessage("<?php print PROMOTION_REQUEST_SEND_SUCCESS ?>");
                            } else {
                                showInfoMessage("<?php print PROMOTION_REQUEST_ALREADY_COMPLETE_INFO ?>");
                            }
                        }
                        else{

                        }
                   },"text");
}
</script>

<div id="div_auth" style="display:none;overflow:hidden;">
    <div id="promotion_contain" class="promotion_contain">
        <div class="top">
             <div><img width="833" src="<?php print empty($popupBgPath) ? C_IMAGE_PATH."border/promotion_popup_t_bg11.png" : C_BASE_PATH.$popupBgPath?>"></div>
        </div>

        <div class="content">
            <div class="content_inner">
                <div>
                    <div class="title">
                        You must be logged in to register for this promotion
                    </div>
                    <!--<div class="title">
                         For every new user registered during the month of May, The Recipe Diva will donate $1 to Jamie Oliver's Food Revolution!
                    </div>-->

                    <img src="<?echo C_IMAGE_PATH?>space.gif" height="15" width="1" />
                    <?php
                    $login_url = "user/register?destination=index%3Fopenpromotion=1";
                    ?>
                    <div class="inner">
                        Please <a onclick="$('#div_auth').dialog('close');" href="<?php print C_BASE_PATH. $login_url?>">click here</a> to register as a Recipe Diva member. Once registered you will be able to enter to win Diva prizes and become a participating member of the site!
                    </div>
                    <!--<div class="inner">
                        Please <a onclick="$('#div_auth').dialog('close');" href="<?php print C_BASE_PATH. $login_url?>">click here</a> to register. Once registered, $1 will be donated in your name to Jamie Oliver's Food Revolution.<br/><br/>
                        If you are already a registered member and would like to show your support for Jamie Oliver's Food Revolution campaign,
                        please  <a onclick="$('#div_auth').dialog('close');" href="http://www.jamieoliver.com/us/foundation/jamies-food-revolution/sign-petition" target="_blank">click here</a>
                        to sign the petition!
                    </div>-->

                </div>
              <div class="btn_contain">
                <div class="dot_bg">
                    &nbsp;
                </div>
                <div class="register_btn_contain">
                    <a class="close_btn" onclick="$('#div_auth').dialog('close');" href="#"></a>
                </div>
            </div>
          </div>

        </div>

        <div class="bot">
            <div></div>
        </div>
    </div>
</div>

<div id="div_donate" style="display:none;overflow:hidden;">
    <div id="promotion_contain" class="promotion_contain">
        <div class="top">
             <div></div>
        </div>

        <div class="content">
            <div class="content_inner">
                <div>
                    <!--<div class="title">
                        You must be logged in to register for this promotion
                    </div>-->
                    <div class="title">
                        For every new user registered during the month of May, The Recipe Diva will donate $1 to Jamie Oliver's Food Revolution!
                    </div>
                    <img src="<?echo C_IMAGE_PATH?>space.gif" height="15" width="1" />
                    <div class="inner">
                        You are already a registered member.  If you would like to show your support for Jamie Oliver's Food Revolution campaign,
                        please <a onclick="$('#div_donate').dialog('close');" href="http://www.jamieoliver.com/us/foundation/jamies-food-revolution/sign-petition" target="_blank">click here</a>
                        to sign the petition!
                    </div>
                </div>
                <div class="btn_contain">
                    <div class="dot_bg">
                        &nbsp;
                    </div>
                    <div class="register_btn_contain">
                        <a class="close_btn" onclick="$('#div_donate').dialog('close');" href="#"></a>
                    </div>
                </div>
          </div>
        </div>

        <div class="bot">
            <div></div>
        </div>
    </div>
</div>

<div id="div_promotion" style="display:none;overflow:hidden;">
    <div id="promotion_contain" class="promotion_contain">
        <div class="promotion_button">
              <!--<a onclick="$('#div_promotion').dialog('close');" class="promotion_close_btn"></a>-->
        </div>
        <div class="top">
             <div><img width="833" src="<?php print empty($popupBgPath) ? C_IMAGE_PATH."border/promotion_popup_t_bg11.png" : C_BASE_PATH.$popupBgPath?>"></div>
        </div>
        <div class="content">
            <div class="content_inner">
                <div>
                    <div class="title">
                        <?php print $description?>
                    </div>
                    <div id="div_space"><img src="<?echo C_IMAGE_PATH?>space.gif" height="10" width="1" /></div>
                    <div class="inner">
                        <?php
                        $login_url = "logout?destination=user/login%3Fdestination=index%3Fopenpromotion=1";
                        ?>
                        You are logged in as <?php print $user->name?>, to login as a different user please <a onclick="$('#div_promotion').dialog('close');" href="<?php print C_BASE_PATH. $login_url?>">click here</a>. We will need to capture some additional information from you to register. This information will be used for our
    database only and not distributed.
                  </div>
                  <div id="div_space"><img src="<?echo C_IMAGE_PATH?>space.gif" height="10" width="1" /></div>
                    <div id="div_error_message" class="message error" style="display:none;padding-bottom:20px;">
                        <ul style="margin-bottom:0px;margin-top:15px;"><span id="message_error"></span></ul>
                    </div>
                      <div class="form_contain">
                        <div class="form_content">
                            <div class="col1">
                                     <img src="<?echo C_IMAGE_PATH?>label/promotion_age_lbl.gif" width="28" height="15" />
                                <img src="<?echo C_IMAGE_PATH?>/space.gif" height="1" width="150" />
                                <input id="txtAge" name="txtAge" type="text" class="myprofile_input" value="<?php print $current_user->profile_age ?>" />
                            </div>
                            <div class="col2">
                                <img src="<?echo C_IMAGE_PATH?>label/promotion_income_lbl.gif" width="48" height="15" />
                                <img src="<?echo C_IMAGE_PATH?>space.gif" height="1" width="100" />
                                <select id="selIncome" name="selIncome" class="myprofile_select">
                                    <option <?php print $current_user->profile_income==""? 'selected="true"' : '' ?> value="">&nbsp;</option>
                                    <option <?php print $current_user->profile_income=="0"? 'selected="true"' : '' ?> value="0">Less than 30,000</option>
                                    <option <?php print $current_user->profile_income=="1"? 'selected="true"' : '' ?> value="1">30,000-40,000</option>
                                    <option <?php print $current_user->profile_income=="2"? 'selected="true"' : '' ?> value="2">40,000-50,000</option>
                                    <option <?php print $current_user->profile_income=="3"? 'selected="true"' : '' ?> value="3">50,000-60,000</option>
                                    <option <?php print $current_user->profile_income=="4"? 'selected="true"' : '' ?> value="4">60,000-70,000</option>
                                    <option <?php print $current_user->profile_income=="5"? 'selected="true"' : '' ?> value="5">70,000-80,000</option>
                                    <option <?php print $current_user->profile_income=="6"? 'selected="true"' : '' ?> value="6">80,000-90,000</option>
                                    <option <?php print $current_user->profile_income=="7"? 'selected="true"' : '' ?> value="7">Greater than 100,000</option>
                                </select>
                            </div>
                        </div>
                        <div class="form_content">
                            <div class="col1">
                                     <img src="<?echo C_IMAGE_PATH?>label/promotion_education_lbl.gif" width="68" height="15" />
                                <img src="<?echo C_IMAGE_PATH?>space.gif" height="1" width="100" />
                                <select id="selEdu" name="selEdu" class="myprofile_select">
                                    <option <?php print $current_user->profile_education==""? 'selected="true"' : '' ?> value="">&nbsp;</option>
                                    <option <?php print $current_user->profile_education=="0"? 'selected="true"' : '' ?> value="0">Less than High School</option>
                                    <option <?php print $current_user->profile_education=="1"? 'selected="true"' : '' ?> value="1">High School</option>
                                    <option <?php print $current_user->profile_education=="2"? 'selected="true"' : '' ?> value="2">Some College</option>
                                    <option <?php print $current_user->profile_education=="3"? 'selected="true"' : '' ?> value="3">College</option>
                                    <option <?php print $current_user->profile_education=="4"? 'selected="true"' : '' ?> value="4">Graduate</option>
                                    <option <?php print $current_user->profile_education=="5"? 'selected="true"' : '' ?> value="5">Post Graduate</option>
                                </select>
                          </div>
                            <div id="profile_own_home_radio" class="col2"><img src="<?echo C_IMAGE_PATH?>label/promotion_own_home_lbl.gif" width="135" height="15" /><img src="<?echo C_IMAGE_PATH?>space.gif" height="1" width="100" />
                                <span class="yes">
                                    Yes
                                    <label class="label_radio"><input id="rdoOwnHome" name="rdoOwnHome" type="radio" <?php print $current_user->profile_own_home=="1"? 'checked="checked"' : '' ?> value="1" /></label>
                                </span>
                                <span class="no">
                                    No
                                    <label class="label_radio"><input id="rdoOwnHome" name="rdoOwnHome" type="radio" <?php print $current_user->profile_own_home=="0"? 'checked="checked"' : '' ?> value="0" /></label>
                                </span>
                            </div>
                        </div>
                  </div>
                  <div class="inner_smalltext">
                        <?php if($prize <> '' && $prize <> null) {?>
                        This promotion entitles the user to win <?php print $prize?>.
                        <?php }?>
                        Please note that this offer is only available online at <a href="http://www.therecipediva.com">www.therecipediva.com</a>.
                        The offer cannot be used in conjunction with any other offer, discount or promotion.
                        You must be 21 years of age or over to participate in this promotion.
                        Underage entries will be void.
                        None of the employees, or family members of employees, of therecipediva.com and its affiliate companies may take part in this promotion.
                        Promotions are available only once to any person (unless indicated otherwise in these Promotion Terms).
                        The Recipe Diva website may determine at its sole discretion whether it believes different entries or accounts are connected to the same person, using whatever methods it deems appropriate.
                        Where multiple entries/ accounts have been used, The Recipe Diva website reserves the right to cancel the entries and withhold any promotional benefits.
                        By choosing submit, you agree to be bound by these Promotion Terms.
                        The Recipe Diva website's main <a href="<?php print C_BASE_PATH?>termofuse">Terms and Conditions</a> also apply to you.
                        In the event of any discrepancy or conflict between the <a href="<?php print C_BASE_PATH?>termofuse">Terms and Conditions</a> and these Promotion Terms, the Promotion Terms will prevail.

                    </div>
                    <div class="btn_contain">
                        <div class="dot_bg_shorter">
                            &nbsp;
                        </div>
                        <div class="register_btn_contain">
                            <a id="btn_send_info" class="register_btn" href="#"></a>
                        </div>
                        <div class="register_btn_contain" style="padding-left:5px;">
                            <a class="cancel_btn" onclick="$('#div_promotion').dialog('close');" href="#"></a>
                        </div>
                    </div>
                </div>
          </div>
        </div>

        <div class="bot">
            <div></div>
        </div>
    </div>
</div>
<script>
    onload_radio_checkbox('profile_own_home_radio');
</script>
