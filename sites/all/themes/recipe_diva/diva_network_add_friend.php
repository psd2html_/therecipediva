<script>
function sendRequestFriend(uid, f_uid){
    var objMessage = document.getElementById("txtRequestMessage");
    var hasError = false;
    var errorMessage = "";
    var x=0;

    if ($.trim($("#txtRequestMessage").val()).length > 500) {
        errorMessage += "<li>" + "<?php print t(ERR_MSG_INVALID, array('@field_name' => 'Personal Message'));?>" + "</li>";
        objMessage.focus();
        hasError = true;
    }

    var objError = document.getElementById("div_error_send_request_friend");
    if(hasError){
        $("#send_request_friend_error").html(errorMessage);
        objError.style.display = "";
        return false;
    }else{
        objError.style.display = "none";
    }

    $.post("<?php print C_BASE_PATH."sendfriendrequest/"?>" + f_uid,
                   {txtRequestMessage:$("#txtRequestMessage").val()},
                   function(data){
                        var json = eval("(" + data + ")");
                        $('#div_request_friend').dialog('close');
                        if (json['status'] == "success") {
                            $("#img_req_friend").attr("src","<?echo C_IMAGE_PATH?>button/btn_remove_pending.gif");
                            $("#img_req_friend").removeAttr('onclick');
                            $("#img_req_friend").unbind("click");
                            $("#img_req_friend").click(function(){openRemoveFriend(uid,f_uid,0);});
                            showInfoMessage("<?php print SEND_REQUEST_SUCCESS ?>");
                        }
                        else{
                            showInfoMessage("<?php print SEND_REQUEST_FAIL ?>");
                        }
                   },"text");
}
</script>

<div id="div_request_friend" style="display:none;overflow:hidden;cursor:move">
    <div id="email_recipe_contain">
        <div class="email_recipe_top">
            <div>&nbsp;</div>
        </div>
        <div class="email_recipe_m">
            <div class="request_friend_content">
                <div id="email_popup">
                    <div style="float:left;padding-right:3px"><img width="62" id="img_request_user" src="" /></div>
                    <div class="add_friend_request_title">
                    <div id="outer1">
                          <div id="middle">
                            <div id="inner">
                                Follow <span name="spn_username"></span>?</div>
                            </div>
                        </div>
                    </div>
                    <div id="dot_bg1">
                        <img width="1" height="14" src="<?php print C_IMAGE_PATH;?>space.gif">
                    </div>
                </div>
                <div id="popup_left_col">
                      <div id="mail_content">
                          <div id="mail_content_col" style="font-weight:normal;">
                            <div id="div_error_send_request_friend" class="message error" style="display:none;">
                                <ul style="margin-bottom:0px;"><span id="send_request_friend_error"></span></ul>
                            </div>
                        </div>
                      </div>
                      <div id="mail_titles">
                        <div id="mail_title_col"><span name="spn_username"></span>&nbsp;will have to confirm that you can follow <?echo $view_user->profile_gender=="0"? "him":"her"?>.</div>
                      </div>
                      <div id="mail_titles">
                        <div id="mail_title_col">Would you like to add a personal message?</div>
                        <div id="mail_content_col"><textarea style="height:200px;overflow:auto;" type="text" id="txtRequestMessage" name="txtRequestMessage"  maxlength="500" value=""></textarea></div>
                      </div>
                      <div style="text-align:left;">
                          <div id="mail_title_col" style="padding-top:5px;">&nbsp;</div>
                          <input type="button" class="btn_request_friend" id="btn_request_friend"/>
                        <input type="button" class="btn_cancel_mail" onclick="javascript:$('#div_request_friend').dialog('close')"/>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
