<!-- START LAST TIP -->
<!--<div id="DivaTip">
    <a href="<?php print C_BASE_PATH."divatips"?>" class="img"></a>
</div>-->
<!-- END LAST TIP -->
<!-- START NEWEST RECIPES -->
<div id="TopRecipes">
    <div id="NewestRecipes_b">
        <div id="TopRecipes_content">
            <div id="div_newest_recipes">
            <?php 	if($block->content != " "){
                    $i = 0;
                    foreach ($block->content as $row) {
                        if($i==NUMBERS_NEW_RECIPE_LIST){
                             break;
                         }
                         $i ++;
                $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row["title"])."-".$row["nid"];
                ?>
                <div id="TopRecipes_content_inner">

                    <div id="img_contain">
                        <?php
                        $file_path = recipe_utils::get_thumbs_image_path($row["filepath"]);
                        if(file_exists($file_path)){ ?>
                            <img alt="<?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,61) ?> />
                        <?php }else{?>
                            <img alt="Default Recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                        <?php }?>
                    </div>

                    <div id="divatiptop_right">
                        <div id="divatoptip_line1">
                            <div class="divatiptop_left divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?></a></div>
                            <div class="divatiptop_left divatop_title">
                            <?php if($row["uid"]!=0){?>
                            <span class="by">by</span> <a rel="nofollow" class="by_author" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><?php print $row["name"]?></a>
                            <?php }else{
                                print '<span class="by">by '.C_UNKNOWN_USER.'</span>';
                            }?>
                            </div>
                        </div>
                        <!--
                        <div id="divatoptip_line2">
                            <div class="divatiptop_left"><?php print theme('fivestar_static', $row["vote_average_value"], 5, 'vote');?></div>
                        </div>
                        -->
                    </div>
                </div>
            <?php }
            }?>
        </div>
        <div id="div_show_more_newest_recipes" class="user_content_bot">
                <a href="javascript:showMoreNewestRecipes()" rel="nofollow">&lt;&lt;show more&gt;&gt;</a>
        </div>
        </div>
    </div>
</div>
<!-- END NEWEST RECIPES -->
