<div id="div_space"><img src="<?php print C_IMAGE_PATH?>space.gif" width="1" height="4" /></div>
<div id="mydiva_search_result">
	<div id="mydiva_search_result_title">
    	<div id="mydiva_search_result_title_content">
        	<img alt="Filter" src="<?php print C_IMAGE_PATH?>label/filter_title_lbl.gif"/>
        </div>
    </div>
    <div id="mydiva_search_result_b">
    	<div id="mydiva_search_result_content">
        	<div id="mydiva_filter_content_contain">       
        <?php 
          	$idCategory = 0;
			$intCountSubCat = 0;
			$arr = explode("," , $_GET['term_id']);
			
			foreach ($block->content as $term) {
              if ($idCategory == 0 || $idCategory != $term['parent_id']){
              		if ($intCountSubCat > RECIPES_RESULT_PER_CATEGORY){
              			print '</div>';
              		}
		    		if ($idCategory != 0)
		    			print '</div>';
		    			
		    		print '<div id="filter_content"><span class="filter_title">' . $term['parent_name'] .':</span>' ;
		    		$idCategory = $term['parent_id'];
		    		$intCountSubCat = 0; 
		    	}
		    	if ($intCountSubCat == RECIPES_RESULT_PER_CATEGORY){
		    		print '<div class="filter_showmore" id="read_more_'. $idCategory .'" onclick="show(' . $idCategory . ')">[+] Show More</div>';
		    		print '<div id="category_'. $idCategory .'" style="display:none;cursor:pointer;">';
		    	}
		    	$bFlag = false;
		    	foreach ($arr as $key =>$value){
					if (is_numeric($value) === TRUE && $term['tid'] == $value){
						$bFlag = true;
						break;
					}
				}
		    	
		    	if($bFlag){
					print '<div id="search_result_content_subcat">
								<div style="float:left;height:18px;">
									<img alt="Bullet" src="'.C_IMAGE_PATH.'icon/search_results_dot_brown.gif"> <a class="focus_filter" href="javascript:submit_filter('.$term['tid'].',2)">' . $term['subcatName'] . '</a> (' . $term['total_subcat'] . ')' .
								'</div>
								<div style="float:left;padding-left:5px;padding-top:2px;">
                					<img alt="Delete groceries" style="cursor:pointer;" src="'.C_IMAGE_PATH.'button/groceries_del_btn.gif" onclick="submit_filter('.$term['tid'].',0);">
            					</div>
            					<div style="clear:both;height:0px;"></div>
            				</div>';
		    	}
		    	else{
		    		print '<div id="search_result_content_subcat"><img alt="Bullet" src="'.C_IMAGE_PATH.'icon/search_results_dot_brown.gif"> <a href="javascript:submit_filter('.$term['tid'].',1)">' . $term['subcatName'] . '</a> (' . $term['total_subcat'] . ')</div>' ;
		    	}
		    	
		    	$intCountSubCat++;
		    }
		    if ($intCountSubCat > RECIPES_RESULT_PER_CATEGORY){
      			print '</div>';
      		}
		    if ($idCategory != 0){
		    	print "</div>";
		    }
			?>
          </div>
      </div>
	</div>
</div>
<script>
	function show(category_id) {
		$("#read_more_" + category_id).hide();
		$("#category_" + category_id).show();
	}
	function getParameterByName(name)
	{
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( window.location.href );
	  if( results == null )
	    return "";
	  else
	    return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	$(document).ready(function(){	  
		$("#mydiva_filter_content_contain").find("a[class=focus_filter]").each(function(){ 
	    	carID = $(this).parents("div[id^=category_]").attr("id");
    		if(carID != null && $("#"+carID).is(":hidden")){
				carID = carID.replace("category_","");
				show(carID);
			}
			$(this).parents("div[id=search_result_content_subcat],div[id^=category_]").siblings("div").hide();
	    });	   
	    $("#edit-term-id").val(getParameterByName('term_id')); 
	});
</script>
