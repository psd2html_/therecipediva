<?php
	global $user;
	$fmail = $user->mail;	
	$result = 0;
?>
	<script>
	var fromEmail = "<?print $fmail?>";
	function mailRecipe(nid,title){
		$("#txtSubject").val("The Recipe Diva: " + title);
		$("#txtEmailFrom").val(fromEmail);		
		$("#btn_send_mail").unbind();
		$("#btn_send_mail").click(function(){sendRecipe(nid);});
    	$("#div_email").dialog(
			{ modal: true },
			{ resizable: false },
			{ height: 435},
			{ width: 480},
			{ draggable: false},
			{ buttons:
				{ 	
					"Close": function() {																		
								$(this).dialog("close");						
							}														
				}
			},
			{ open: function(event, ui) { 
						$("div[class^=ui-dialog-titlebar]").hide();
						$("div[class^=ui-dialog-buttonpane]").hide();
						$("#div_email").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
					}
			},			
			{ beforeclose: function(event, ui) {						
						$("#txtEmailFrom").val("");
						$("#txtEmailTo").val("");
						$("#message_recipe_error").html("");
						$("#div_error_message").hide();
						$("#div_email").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
						$("div[class^=ui-dialog-titlebar]").show();
						$("div[class^=ui-dialog-buttonpane]").show();											
					}
			}			
		);
		$("#txtEmailTo").focus();
    }
    
	function sendRecipe(nid){
		var objTo = document.getElementById("txtEmailTo");
    	var objFrom = document.getElementById("txtEmailFrom");
    	var hasError = false;
    	var errorMessage = "";
    	var x=0;
    	
    	if (trim(objTo.value) == ""){
			errorMessage = "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'To'));?>" + "</li>";  
			objTo.focus();
			hasError = true;
		}else{
			arrEmail = objTo.value.split(",");	
			for (x in arrEmail){
				var strEmail = trim(arrEmail[x]);
				if(trim(strEmail) == ""){
					errorMessage = "<li>" + "<?php print t(ERR_MSG_INVALID, array('@field_name' => 'To'));?>" + "</li>";  
					objTo.focus();
					hasError = true;
					break;
				}
				if (!validateEmail(strEmail)) {
					errorMessage = "<li>" + "<?php print t(ERR_MSG_INVALID, array('@field_name' => 'To'));?>" + "</li>";  
					objTo.focus();
					hasError = true;
					break;
				}
			}
		}
		if (trim(objFrom.value) == ""){
			errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'From'));?>" + "</li>";
			if(hasError == false){
				objFrom.focus();
			}  
			hasError = true;
		}else{
			if (!validateEmail(objFrom.value)) {
				errorMessage += "<li>" + "<?php print t(ERR_MSG_INVALID, array('@field_name' => 'From'));?>" + "</li>";  
				objFrom.focus();
				hasError = true;
			}
		}
		var objError = document.getElementById("div_error_message");
		if(hasError){
			$("#message_recipe_error").html(errorMessage);
			objError.style.display = "";
			return false;
		}else{
			objError.style.display = "none";
		}
		
		$.post("<?php print C_BASE_PATH."recipe_mail/"?>"+nid, 
					   { txtEmailTo: $("#txtEmailTo").val(), txtEmailFrom:$("#txtEmailFrom").val(), txtSubject:$("#txtSubject").val() },
					   function(data){
							var json = eval("(" + data + ")");												        
					        var nid = json['nid'];
					        $('#div_email').dialog('close');
					        if (json['status'] == "1") {
					        	showInfoMessage("<?php print RECIPE_EMAIL_SEND_FAIL ?>");					        	
				        	}
				        	else if (json['status'] == "2") {
				        		showInfoMessage("<?php print RECIPE_EMAIL_SEND_SUCCESS ?>");				        		
				        	}
				        	else if (json['status'] == "3") {
				        		showInfoMessage("<?php print GROCERY_EMAIL_SEND_SUCCESS ?>");				        		
				        	}
					   },"text");
			
	}
	</script>
	
<div id="div_email" style="display:none;overflow:hidden;cursor:move">	
	<div id="email_recipe_contain">
		<div class="email_recipe_top">
	    	<div>&nbsp;</div>
	    </div>	    
	    <div class="email_recipe_m">
	    	<div class="email_recipe_content">
				<div id="email_popup">
					<div class="pre_membership_p_title_1">Email <?php print (arg(0)=="mydiva" && arg(1)=="grocerylist")? "Grocery List":"Recipe"?></div>
				    <div id="dot_bg1">
			        	<img width="1" height="14" src="<?php print C_IMAGE_PATH;?>space.gif">
			        </div>
			    </div>
			    
					<div id="popup_left_col">  
						  <div id="mail_content">
						  	<div id="mail_content_col" style="font-weight:normal;">
								<div id="div_error_message" class="message error" style="display:none;">
						            <ul style="margin-bottom:0px;"><span id="message_recipe_error"></span></ul>
						        </div>
						    </div>
					      </div>
						  <div id="mail_titles">
						    <div id="mail_title_col">To:<span id="require">*</span></div>
						    <div id="mail_content_col"><input type="text" name="txtEmailTo" id="txtEmailTo" maxlength="255" /></div>
						  </div>
						  <div id="mail_comment_col">
						  	<div id="mail_content_col">(use commas to separate emails)</div>
						  </div>
						  <div id="mail_titles">
						    <div id="mail_title_col">From:<span id="require">*</span></div> 
						    <div id="mail_content_col"><input type="text" id="txtEmailFrom" name="txtEmailFrom"  maxlength="50" value=""></div>
						  </div>
						  <div id="mail_comment_col">
						    <div id="mail_content_col">(enter your email)</div>
						  </div>
						  <div id="mail_titles">
						    <div id="mail_title_col">Subject:</div> 
						    <div id="mail_content_col"><input type="text" id="txtSubject" name="txtSubject" value="" maxlength="100"></div>
						  </div>

						  <div style="text-align:left;">
						  	<div id="mail_title_col" style="padding-top:5px;">&nbsp;</div>
						  	<input type="button" class="btn_send_mail" id="btn_send_mail"/>
						    <input type="button" class="btn_cancel_mail" onclick="javascript:$('#div_email').dialog('close')"/>
						  </div>
					</div>
				
			</div>
	    </div>
	</div>				
</div>