<div id="DivaNetwork">
    <div id="DivaNetwork_b">
            <?php
            global $user;
            if (!$user->uid) {?>
            <div id="DivaNetwork_content">
                <div class="diva_network_top"><img alt="Connect with cooks" src="<?php print C_IMAGE_PATH?>label/lbl_connect_with_cooks.gif" /></div>
                <div class="diva_network_inner">
                    <div class="diva_network_left"><img alt="New message" src="<?php print C_IMAGE_PATH?>icon/ico_new_message.gif" /></div>
                    <div class="diva_network_right"><img alt="Send and Receive message" src="<?php print C_IMAGE_PATH?>label/lbl_send_receive_messages.gif" /></div>
                </div>
                <div class="diva_network_inner">
                    <div class="diva_network_left1"><img alt="Become cook" src="<?php print C_IMAGE_PATH?>icon/ico_red_hat.gif" /></div>
                    <div class="diva_network_right"><img alt="Create your profile" src="<?php print C_IMAGE_PATH?>label/lbl_create_your_profile.gif" /></div>
                </div>
                <div class="diva_network_inner">
                    <div class="diva_network_left1"><img alt="Request friend" src="<?php print C_IMAGE_PATH?>icon/ico_request_friend.gif" /></div>
                    <div class="diva_network_right"><img alt="Keep up with friends" src="<?php print C_IMAGE_PATH?>label/lbl_keep_up_with_friends.gif" /></div>
                </div>
                <div class="diva_network_inner1">
                    <a rel="nofollow" href="<?php print C_BASE_PATH?>user/register">
                        <img alt="Get started today" src="<?php print C_IMAGE_PATH?>label/lbl_get_started_today.gif" />
                    </a>
                </div>
            </div>

            <?php }//else{?>
            <div id="DivaNetwork_content_1">
                <?php if ($user->uid) {?>
                <div class="livefeed_content_top">
                    <div style="padding-left:5px;">
                        <a rel="nofollow" href="<?php print C_BASE_PATH?>mydiva/friends"><img alt="Go to my network" src="<?php print C_IMAGE_PATH?>label/lbl_go_to_my_network.gif" /></a>
                    </div>
                    <div class="livefeed_content_status">
                    <div style="float:left;">
                    <input type="text" maxlength="256" class="livefeed_input" value="" name="txtYourMind" id="txtYourMind">
                    </div>
                    <div style="float:left;padding:2px 2px 0 7px;">
                        <a rel="nofollow" href="javascript:submitStatus()"><img alt="Submit your mind" src="<?php print C_IMAGE_PATH?>button/submit_btn.gif"/></a>
                    </div>
                    </div>
                </div>
                <!--<div class="livefeed_content">
                    <div class="livefeed_content_fill">
                        <input type="text" maxlength="256" class="livefeed_input" value="" name="txtYourMind" id="txtYourMind">
                        <div style="float:right;padding:2px 2px 0 0;">
                            <a rel="nofollow" href="javascript:submitStatus()"><img alt="Submit your mind" src="<?php print C_IMAGE_PATH?>button/btn_submit_1.gif"/></a>
                        </div>
                    </div>
                </div>-->
                <?php }?>
                <div id="div_live_feed">
                <?php if($block->content != " "){
                    $i=0;
                foreach ($block->content as $row) {
                    if($i == 0){
                        echo '<span id="maxIdLiveFeed" style="display:none;">' . $row["id"] . '</span>';
                    }else if($i==NUMBERS_FEED_LIST){
                        break;
                    }
                    $i++;
                ?>
                    <div class="livefeed_content">
                        <div class="livefeed_content_fill">

                                <div class="livefeed_img">
                                    <?php if(file_exists($row["picture"])){ ?>
                                        <img alt="<?php print $row["name"]?>" src="<?php print C_BASE_PATH.$row["picture"]?>"  />
                                    <?php }else{ ?>
                                        <img alt="<?echo $block->content->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" src="<?php echo C_IMAGE_PATH?>photo/<?php echo $row["profile_gender"]=="0"? "male":"female"?>45.gif">
                                    <?php }?>
                                </div>

                            <div style="width:253px; padding-bottom:4px; float: right;">
                                <a rel="nofollow" class="by_author" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><?php print $row["name"]?></a>
                                <?php if($row["feed_type"] == LIVEFEEDTYPE_UPDATE_PROFILE){
                                    $gender = $row["profile_gender"]=="0"? "his":"her";
                                    print str_replace("gender",$gender,$row["comment"]);
                                    print "<br>";
                                }else{
                                    print $row["comment"];
                                    if($row["feed_type"] == LIVEFEEDTYPE_CHANGE_STATUS){
                                        print " <br>";
                                    }else if($row["feed_type"] == LIVEFEEDTYPE_NEW_MEMBER){
                                        print " <br>";
                                    }
                                }?>

                                <?php if($row["feed_type"] == LIVEFEEDTYPE_BECOME_FRIEND){?>
                                    <a rel="nofollow" class="by_author" href="<?php print C_BASE_PATH."user/".$row["friend_name"]?>"><?php print $row["friend_name"]?></a>
                                    <br>
                                <?php }else if($row["feed_type"] == LIVEFEEDTYPE_UPLOAD_RECIPE || $row["feed_type"] == LIVEFEEDTYPE_UPLOAD_TIP){
                                    $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row["title"])."-".$row["nid"];?>
                                    <span class="divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?></a></span>
                                    <br>
                                <?php }else if($row["feed_type"] == LIVEFEEDTYPE_REPLY_TOPIC || $row["feed_type"] == LIVEFEEDTYPE_POST_TOPIC){
                                    $nodeLink = C_BASE_PATH."divagossip/topic/".recipe_utils::removeWhiteSpace($row["title"])."-".$row["nid"]?>
                                    <span class="divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?></a></span>
                                    <br>
                                <?php }else if($row["feed_type"] == LIVEFEEDTYPE_REVIEW_RECIPE || $row["feed_type"] == LIVEFEEDTYPE_REVIEW_TIP){
                                    $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row["review_title"])."-".$row["review_nid"]."?cid=".$row["cid"];?>
                                    <span class="divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["review_title"],10,false,50)?></a></span>
                                    <?php print theme('fivestar_static', $row["vote_average_value"], 5, 'vote');?>
                                <?php }else if($row["feed_type"] == LIVEFEEDTYPE_SAVE_RECIPE || $row["feed_type"] == LIVEFEEDTYPE_SAVE_TIP){
                                    $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row["title"])."-".$row["nid"];?>
                                    <span class="divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?></a></span>
                                    <br>
                                <?php }?>

                                <span class="feed_date"><?php print recipe_utils::convert_to_monthday($row["created"])?></span>
                            </div>
                        </div>
                    </div>
                    <div class="livefeed_content"><img width="1" height="3" src="<?php print C_IMAGE_PATH?>space.gif"></div>

                <?php }
                }?>
                </div>
                <?php if(sizeof($block->content) > NUMBERS_FEED_LIST){?>
                <div class="livefeed_content_top" id="div_show_more">
                    <a rel="nofollow" href="javascript:showMoreFeed()">&lt;&lt;show more&gt;&gt;</a>
                </div>
                <?php }?>
            </div>
            <?php //}?>
    </div>
</div>
