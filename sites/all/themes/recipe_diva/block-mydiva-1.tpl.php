<?php
    if (arg(1) == "recipebox"){
        $linkFolder = C_BASE_PATH."mydiva/recipebox/";
        $contName = "Recipes";
        $folderName = "Cookbook";
        $cssClass = "myrecipebox_content_right";
    }else{
        $linkFolder = C_BASE_PATH."mydiva/tip/";
        $contName = "Tips";
        $folderName = "Folder";
        $cssClass = "mydiva_content_right";
    }
    $user_role = recipe_utils::getUserRole();
    $hasPer =  $user_role == C_ADMIN_USER || $user_role == C_PREMIUM_USER ? true : false || $user_role == C_REGISTER_USER ? true : false;
?>

<div id="dialog" style="display:none" title="Add to <?php print $folderName;?>">
    <div style="padding: .4em .4em;">
        <?php print $folderName;?> Name:
        <select id="ddlFolderDropdown" name="ddlFolderDropdown" class="folder_dropdown">
            <option value="0">My Saved <?php print $contName;?></option>
            <?php foreach ($block->content as $row) { ?>
                <option value="<?php print $row["fid"]?>"><?php print $row["fname"]?></option>
            <?php }?>
        </select>
    </div>
</div>

<div id="dialogConfirm" style="display:none" title="Are you sure you want to delete?">
</div>

<div id="mydiva_container">
    <div id="mydiva_container_pad">
    <?php if (arg(1) == "recipebox"){?>
        <a onclick="submit_folder('mycookbook','0','0')" href="#"><img alt="My cook book" width="84" height="17" src="<?php print C_IMAGE_PATH?>label/mycookbook_lbl.gif"></a></div>
    <?php }else{ ?>
        <a onclick="submit_folder('mycookbook','0','0')" href="#"><img alt="My saved tips" width="84" height="16" src="<?php print C_IMAGE_PATH?>label/my_savetips_lbl.gif"></a></div>
    <?php }?>
    <div id="mydiva_content_contain">
        <div id="<?php print $cssClass . (($_GET['folder_name'] == "mysave")? "_selected":"")?>">
            <a onclick="submit_folder('mysave','0','0')" href="#">My Saved <?php print $contName;?></a></div>
        <div id="mydiva_content_left">
        </div>
    </div>
    <div id="mydiva_content_contain">
        <div id="<?php print $cssClass . (($_GET['folder_name'] == "myupload")? "_selected":"")?>">
            <a onclick="submit_folder('myupload','1','0')" href="#">My Uploaded <?php print $contName;?></a></div>
        <div id="mydiva_content_left">
        </div>
    </div>
    <!--
    <div id="mydiva_content_contain">
        <div id="<?php print $cssClass . (($_GET['folder_name'] == "mypurchase")? "_selected":"")?>">
            <a onclick="submit_folder('mypurchase','2','0')" href="#">My Purchased <?php print $contName;?></a></div>
        <div id="mydiva_content_left">
        </div>
    </div>
    -->
    <?php if($hasPer && arg(1) == "recipebox"){?>
    <!--<div id="mydiva_content_contain">
        <div id="mydiva_content_right<?php print ($_GET['folder_name'] == "myversion")? "_selected":""?>">
            <a onclick="submit_folder('myversion','0','0')" href="#">My Version</a></div>
        <div id="mydiva_content_left">
        </div>
    </div> -->
    <?php }?>

    <div id="div_folder_content">
    <?php foreach ($block->content as $row) { ?>
    <div id="filter_content_<?php print $row["fid"]?>">
        <div id="mydiva_content_contain">
            <div id="<?php print $cssClass . (($_GET['folder_id'] == $row["fid"])? "_selected":"")?>">
                <a name='foldername_<?php print $row["fid"]?>' onclick="<?php print "submit_folder('myfolder',".$row["fid"].",0)" ?>" href="#"><?php print $row["fname"]?></a>
            </div>
            <div id="mydiva_content_left">
                <img alt="Delete folder" style="cursor:pointer;" width="18" height="18" src="<?php print C_IMAGE_PATH?>icon/del_icon.gif" onclick="deleteFolder('<?php print $row["fid"]?>')">
            </div>
        </div>
    </div>
    <?php }?>
    </div>
    <div id="mydiva_content_contain">
        <div id="divFolderName" style="display:none">
            <div id="spanError" style="display: none;">&nbsp;
            </div>
            New <?php print $folderName;?> Name: <input id='txtFolderName' class="mydiva_input" type='text' value='' maxlength="20">
            <br>
            <div id="myrecipes_btn">
                <img alt="Create folder" style="cursor:pointer;" onclick="createFolder()" src="<?php print C_IMAGE_PATH?>button/save_btn_1.gif">
                <img alt="Cancel folder creation" style="cursor:pointer;" onclick="showFolderName(false)" src="<?php print C_IMAGE_PATH?>button/cancel_btn_2.gif">
            </div>
        </div>
    </div>

    <div id="mydiva_content_contain">
        <?php $destination = drupal_get_destination(); ?>
        <?php if (arg(1) == "recipebox"){?>
        <div id="myrecipes_btn">
            <img alt="Create new cook book" style="cursor:pointer;" onclick="showFolderName(true)" src="<?php print C_IMAGE_PATH?>button/create_a_new_cook_book_btn.gif">
            &nbsp;
            <img alt="Share recipe" style="cursor:pointer;" onclick="window.location = '<?php print url("recipes/add", array('query' => $destination)); ?>';" src="<?php print C_IMAGE_PATH?>button/share_recipe_lbl.gif"></div>
        <?php }else{?>
        <div id="myrecipes_btn">
            <img alt="Create new folder" style="cursor:pointer;" onclick="showFolderName(true)" src="<?php print C_IMAGE_PATH?>button/create_newfolder_btn.gif">
            &nbsp;
            <img alt="Share tips" style="cursor:pointer;" onclick="window.location = '<?php print url("divatips/add", array('query' => $destination)); ?>';" src="<?php print C_IMAGE_PATH?>button/share_tips_lbl.gif"></div>
        <?php }?>
    </div>
</div>
<script>
    function showFolderName(hasShow) {
        if (hasShow) {
            $("#divFolderName").fadeIn("slow");
        }
        else {
            $("#spanError").html("");
            $("#divFolderName").fadeOut("fast");
        }
    }
    function createFolder() {
        var folderName = $("#txtFolderName").val();

        if (jQuery.trim(folderName) == ""){
                $("#spanError").html("+ <?php print t(ERR_MSG_REQUIRED, array('@field_name' => $folderName. " Name"))?>");
                $("#spanError").show();
                return;
            }

            var folderArray = $("a[name^=foldername_]");
            for (i = 0; i < folderArray.length; i++) {
                if(document.all){
                    if ($.trim(folderArray[i].innerText).toUpperCase() == $.trim(folderName).toUpperCase()) {
                        $("#spanError").html("+ <?php print t(ERR_MSG_EXIST, array('@field_name' => $folderName. " Name"));?>");
                        $("#spanError").show();
                        return;
                    }
                }
                else{
                    if ($.trim(folderArray[i].text).toUpperCase() == $.trim(folderName).toUpperCase()) {
                        $("#spanError").html("+ <?php print t(ERR_MSG_EXIST, array('@field_name' => $folderName. " Name"));?>");
                        $("#spanError").show();
                        return;
                    }
                }
            }


        var fid = 0;
        $.post("<?php print $linkFolder."create_folder"?>", { txtFolderName: folderName},
           function(data){
                var json = eval("(" + data + ")");
                fid = json['fid'];
                if (fid != 0) {
                    var divfolder = "<div id=\"filter_content_"+fid+"\"><div id=\"mydiva_content_contain\"><div id=\"<?print $cssClass?>\">";
                        divfolder += "<a name=\"foldername_"+fid+"\" onclick=\"submit_folder('myfolder'," + fid + ",0)\" href=\"#\" class=\"filter_title\">" + $("#txtFolderName").val() + "</a>";
                        divfolder += "</div><div id=\"mydiva_content_left\">";
                        divfolder += "<img alt=\"Delete folder\" style=\"cursor:pointer;\"  src=\"<?php print C_IMAGE_PATH?>icon/del_icon.gif\" onclick=\"deleteFolder('"+fid+"')\"></div>";
                        divfolder += "</div></div>";

                    $("#div_folder_content").append(divfolder);
                    $("#ddlFolderDropdown").append("<option value=\""+fid+"\">"+$("#txtFolderName").val()+"</option>");

                    $("#spanError").text("");
                    $("#txtFolderName").val("");
                    $("#divFolderName").fadeOut("fast");
                }
                else{
                    $("#spanError").text("Error Create");
                }
           },"text");
    }
    function deleteFolder(fid) {
        var message = "Are you sure you want to delete '" + $("a[name=foldername_" + fid + "]").text() + "'?"
        $("#dialogConfirm").attr("title", message);
        $("#ui-dialog-title-dialogConfirm").text(message);
        $("#dialogConfirm").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "Yes": function() {
                                var url = "<?php print $linkFolder."delete_folder/"?>" + fid;
                                $.get(url, function(data){
                                        var json = eval("(" + data + ")");
                                        if(json['status'] == "success"){
                                            if($("#edit-folder-id").val() == fid){
                                                submit_folder('mycookbook','0','0');
                                            }
                                            else{
                                                $("#filter_content_" + fid).remove();
                                                $("#ddlFolderDropdown").find("option[value*='"+fid+"']").remove();
                                            }

                                        }
                                        else{
                                            // Error alert(json['status']);
                                        }
                                   },"text");
                                $(this).dialog("close");
                            },
                    "No": function() {
                                $(this).dialog("close");
                            }
                }
            }
        );
    }
    function submit_folder(fname,fid,tid){
        $("#edit-folder-name").val(fname);
        $("#edit-folder-id").val(fid);
        $("#edit-term-id").val("");
        $("#edit-txtKeyword").val("");
        $("#edit-cbbSort").val(0);
        $("#edit-page").val(0);
        submit_filter(tid,0);
    }
    function submit_filter(tid,clearfilter){
        var termIds = $("#edit-term-id").val();
        //Clear All Filter
        if(clearfilter == 0 && tid == 0){
            termIds = "";
        }
        //Clear One Filter
        else if (clearfilter == 0 && tid != 0){
            var arr = termIds.split(",");
            termIds = "";
            for(x in arr){
                if (tid != arr[x]){
                    termIds += "," + arr[x];
                }
            }

            // Remove first ","
            if(termIds != ""){
                termIds = termIds.substring(1);
            }
        }
        //Add One Filter
        else if (clearfilter == 1 && tid != 0){
            if(termIds == ""){
                termIds = tid;
            }else{
                termIds += "," + tid;
            }
        }

        $("#edit-term-id").val(termIds);
        $("#edit-submit").click();
    }

</script>
