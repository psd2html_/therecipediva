<script>
function sendCOW(){
    var objMessage = document.getElementById("txtSendMessageCow");
    var hasError = false;
    var errorMessage = "";
    var x=0;

    if ($.trim(objMessage.value) == ""){
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Why should you be Cook of the Week'));?>" + "</li>";
        if(hasError == false){
            objMessage.focus();
        }
        hasError = true;
    }else{
        if ($.trim($("#txtSendMessageCow").val()).length > 1500) {
            errorMessage += "<li>" + "<?php print COW_MAXLENGTH_ERROR?>" + "</li>";
            objMessage.focus();
            hasError = true;
        }
    }
    var objError = document.getElementById("div_error_send_message_cow");
    if(hasError){
        $("#send_message_error_cow").html(errorMessage);
        objError.style.display = "";
        return false;
    }else{
        objError.style.display = "none";
    }
    $("#btn_send_message_cow").unbind();
    $.post("<?php print C_BASE_PATH."popup/cow"?>",
                   { txtSendMessage:$("#txtSendMessageCow").val()},
                   function(data){
                        var json = eval("(" + data + ")");
                        $('#div_cookofweek').dialog('close');
                        if (json['status'] == "success") {
                            showInfoMessage("<?php print COOKOFWEEK_SEND_SUCCESS ?>");
                        }
                        else{
                        }
                   },"text");
}
</script>

<div id="div_cookofweek" style="display:none;overflow:hidden;cursor:move">
    <div id="email_recipe_contain">
        <div class="email_recipe_top">
            <div>&nbsp;</div>
        </div>
        <div class="email_recipe_m">
            <div class="email_recipe_content">
                <div id="email_popup">
                    <div class="pre_membership_p_title_1">Want to be Cook of the Week?</div>
                    <div id="dot_bg1">
                        <img width="1" height="14" src="<?php print C_IMAGE_PATH;?>space.gif">
                    </div>
                </div>
                <div id="popup_left_col">
                      <div id="mail_content">
                          <div id="mail_content_col" style="font-weight:normal;">
                            <div id="div_error_send_message_cow" class="message error" style="display:none;">
                                <ul style="margin-bottom:0px;margin-top:15px;"><span id="send_message_error_cow"></span></ul>
                            </div>
                        </div>
                      </div>
                      <div id="cow_titles">
                            Each week The Recipe Diva will choose a cook from our community to be featured as the Cook-of-the-Week.  If you believe that you have put extra work in the kitchen this week, have had any great successes, learned something new, or you just deserve it, please let us know.
                      </div>
                      <div id="mail_titles">
                        <div id="mail_title_col">Why should you be Cook of the Week?<span id="require">*</span></div>
                        <div id="mail_content_col">
                        <textarea style="height:150px;overflow:auto;" type="text" id="txtSendMessageCow" name="txtSendMessageCow"  maxlength="500" value=""><?php print cook_of_week_page();?></textarea></div>
                      </div>
                      <div style="text-align:left;">
                          <div id="mail_title_col" style="padding-top:5px;">&nbsp;</div>
                          <input type="button" class="cookofweek_submit" id="btn_send_message_cow"/>
                        <input type="button" class="btn_cancel_mail" onclick="javascript:$('#div_cookofweek').dialog('close')"/>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
