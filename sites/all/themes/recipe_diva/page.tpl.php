<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8,IE=7" />
    <?php
        if (isset($_REQUEST['page']) || (arg(0) == "recipe" && arg(1) == "index")) {
               print '<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">';
        }
        $canonical_url = 'http://www.therecipediva.com' . strtolower(mysql_real_escape_string($_SERVER["REQUEST_URI"]));
        print '<link rel="canonical" href="' . $canonical_url . '">';
    ?>
    <?php print $head ?>
    <?php print $styles; ?>
    <?php print $scripts ?>
    <?php if (drupal_is_front_page()){
            global $user;
            if($_GET["opencow"] == 1 && !$user->uid){
                header( 'Location: user/login?destination=index%3Fopencow=1' ) ;
            }
            if($_GET["openpromotion"] == 1 && !$user->uid){
                header( 'Location: user/login?destination=index%3Fopenpromotion=1' ) ;
            }
            if($_GET["openglr"] == 1 && !$user->uid){
                header( 'Location: user/login?destination=index%3Fopenglr=1' ) ;
            }
            print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'simplegallery.js"></script>';

            print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'common.js"></script>';
            //print '<link type="text/css" rel="stylesheet" media="all" href="'.C_BASE_PATH.C_CSS_PATH.'jquery-ui.css" />';

            //print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.min.js"></script>';
            //print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery-ui.min.js"></script>';
    }?>
    <?php print '<link type="text/css" rel="stylesheet" media="all" href="'.C_BASE_PATH.C_CSS_PATH.'jquery-ui.css" />';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.min.js"></script>';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery-ui.min.js"></script>';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.filestyle.js"></script>';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.form.js"></script>';?>

    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.ad-gallery.js"></script>';?>
    <?php print '<link type="text/css" rel="stylesheet" media="all" href="'.C_BASE_PATH.C_CSS_PATH.'jquery.ad-gallery.css" />';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.pngFix.pack.js"></script>';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'externalize.js"></script>';?>

    <?php
    global $user;
    if($user->uid){
        print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.watermark.min.js"></script>';
    }?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php print GOOGLE_ANALYTICS_ID?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
    <div id="main">
        <!-- START HEADER -->
        <div id="header">
            <div id="logo">
            <?php $site_url = C_SITE_URL.C_BASE_PATH;?>
            <a class="logo" href="<?php print $site_url?>" title="Quick, Easy, Healthy Recipes - Recipe Tips and Reviews - Free at TheRecipeDiva.com"></a>
            </div>
            <!-- START BANNER HEADER -->
            <div id="banner_top">
            <?php print $header_left ?>&nbsp;
            </div>
            <!-- END BANNER HEADER -->
            <div id="login_form">
                <?php print $header_right ?>
            </div>

            <div id="menu">
                <ul id="main-menu">
                <?php print build_menu_bar(); ?>
                </ul>
            </div>

        </div>
        <!-- END HEADER -->

        <!-- START BODY -->
        <div id="body">
          <!-- START LEFT COL -->
             <div id="left_col">
                 <?php
                     if (arg(0) == "mydiva" && $user->uid){?>
                     <div id="mydiva">
                         <?php print build_mydiva_menu_bar(); ?>
                    <div id="mydiva_b">
                        <div id="mydiva_content">
                            <?php print build_network_menu_bar(); ?>
                            <?php if (arg(1) == "recipebox" || arg(1) == "tip"){?>
                                <div id="mydiva_content_inner">
                                    <div id="mydiva_l_col">
                                        <?php print $content_bot?>
                                    </div>
                                    <div id="mydiva_r_col">
                                        <?php print $content?>
                                    </div>
                                </div>
                            <?php }else{ print $content;}?>
                        </div>
                    </div>
                </div>
            <?php }elseif(arg(0) == "aboutus" || arg(0) == "contactus" || arg(0) == "privacy" || arg(0) == "advertising" || arg(0) == "termofuse" ){?>
                <div id="news_p">
                    <div id="news_p_b">
                             <div id="news_border">
                                 <div class="news_p_content_font" id="news_p_content">
                                     <?php print $content?>
                                 </div>
                        </div>
                    </div>
                </div>
            <?php }elseif(arg(0) == "become_premium"){?>
                    <div id="news_p">
                        <div id="news_p_t_contain">
                            <div></div>
                        </div>
                        <div id="news_p_b">
                               <div id="news_border">
                                   <?php print $content?>
                                </div>
                        </div>
                      </div>
            <?php }else{
                if(arg(0) == "user" && (arg(1) =="password" || arg(1) =="login")){
                    if ($show_messages && $messages){print $messages;}
                }

                if(((arg(0) == "recipe" || arg(0) == "divagossip" || arg(0) == "divatips") && (arg(1) != '' && arg(1) != 'search')) || (arg(0) == "recipes" && (arg(1) != "" && arg(1) != "add" && arg(2) != "edit"))) {
                    print $breadcrumb;
                }

                if((arg(0) == "recipes" && (arg(1) == "" || arg(1) == "add" || arg(2) == "edit") && empty($_GET["type"])) || (arg(0) != "recipes" && arg(0) != "search_result" && arg(0) != "ingredients" && arg(0) != "advanced" && arg(0) != "friendsearch")){
                    print $content;
                    print $content_bot;
                }
             }?>

             <?php if(arg(0) != "recipes" && arg(0) != "search_result" && arg(0) != "ingredients" && arg(0) != "advanced" && arg(0) != "friendsearch"){?>
                 <div id="body_left_of_left_col">
                    <?php print $content_bot_left?>
                </div>
                <div id="body_right_of_left_col">
                    <?php print $content_bot_right?>
                </div>
             <?php }else{?>
                 <div id="body_left_of_left_col" style="padding-top:0px;">
                     <?php print $content_bot?>
                    <?php print $content_bot_left?>
                </div>
                 <?php
                $flag = 0;
                if(arg(0) != "recipes"){
                    $flag = 1;
                }else if(((arg(1) != "" && arg(1) != "add" && arg(2) != "edit")) || isset($_GET["type"])){
                    $flag = 1;
                }
                if($flag == 1){?>
                <div id="body_right_of_left_col">
                    <?php print $content?>
                    <?php print $content_bot_right?>
                </div>
                <?php }?>
             <?php }?>

             <!-- START BOTTOM RECIPES ADS -->
                <?php print $content_ads_bot?>
             <!-- END BOTTOM RECIPES ADS -->
          </div>
            <!-- END LEFT COL -->

            <!-- START RIGHT COL -->
            <div id="right_col">
                <!-- START SEARCH FORM -->

                    <?php
                            if(isset($_GET["type"])){
                                $hdf_type = $_GET["type"];
                                if($hdf_type == RECIPES_SEARCH_RECIPES){
                                    $searchAction = "recipes";
                                }
                                elseif($hdf_type == RECIPES_SEARCH_INGREDIENTS){
                                    $searchAction = "ingredients";
                                }
                                elseif($hdf_type == RECIPES_SEARCH_ADVANCED){
                                    $searchAction = "advanced";
                                }
                                elseif($hdf_type == RECIPES_SEARCH_FRIEND){
                                    $searchAction = "friendsearch";
                                }
                            }else{
                                $hdf_type = RECIPES_SEARCH_RECIPES;
                                $searchAction = "recipes";
                            }
                    ?>
                    <form id="home-search-form" method="post" accept-charset="UTF-8" onsubmit="submitSearchForm('home-search-form','<?echo C_BASE_PATH.$searchAction?>','<?php print $hdf_type;?>');return false;" action="<?echo C_BASE_PATH.$searchAction?>">
                        <div id="Search_Form">
                            <div id="Search_top_contain">
                                <div></div>
                            </div>
                            <div id="Search_b">
                                <div id="search_content">
                                       <div id="search_content_contain">
                                        <div id="search_input_div">
                                          <input type="text" name="txtHomeKeyWord" style="height:16px;margin-top:2px;" id="txtHomeKeyWord" class="search_input_homepage" value="" />
                                          <input type="hidden" name="hdf_type" id="hdf_type" value="<?php print $hdf_type;?>"/>
                                        </div>
                                        <div id="search_btn">
                                            <a href="#">
                                            <input type="image" src="<?echo C_IMAGE_PATH?>search_btn.png" onclick="submitSearchForm('home-search-form','<?echo C_BASE_PATH.$searchAction?>','<?php print $hdf_type;?>');return false;" />
                                            </a>
                                        </div>
                                        <div id="search_form_text_contain" style="height: 14px !important;">
                                            <div <?php echo ($hdf_type == RECIPES_SEARCH_RECIPES)? 'id="recipe_lbl_f"':'id="recipe_lbl"'; ?>>
                                                <a href="javascript:submitSearchForm('home-search-form','<?echo C_BASE_PATH."recipes"?>','<?php print RECIPES_SEARCH_RECIPES;?>')"><img width="36" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></a>
                                               </div>
                                            <div id="search_dot">
                                                <img src="<?echo C_IMAGE_PATH?>index_dot.gif" height="14" >
                                            </div>
                                            <div <?php echo ($hdf_type == RECIPES_SEARCH_INGREDIENTS)? 'id="ingredients_lbl_f"':'id="ingredients_lbl"'; ?>>
                                                <a href="javascript:submitSearchForm('home-search-form','<?echo C_BASE_PATH."ingredients"?>','<?php print RECIPES_SEARCH_INGREDIENTS;?>')"><img width="52" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></a>
                                            </div>
                                            <div id="search_dot">
                                                  <img src="<?echo C_IMAGE_PATH?>index_dot.gif" height="14" >
                                            </div>
                                            <div <?php echo ($hdf_type == RECIPES_SEARCH_ADVANCED)? 'id="advanced_lbl_f"':'id="advanced_lbl"'; ?>>
                                                <a href="javascript:submitSearchForm('home-search-form','<?echo C_BASE_PATH."advanced"?>','<?php print RECIPES_SEARCH_ADVANCED;?>')"><img width="49" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></a>
                                            </div>
                                            <div id="search_dot">
                                                  <img src="<?echo C_IMAGE_PATH?>index_dot.gif" height="14" >
                                            </div>
                                            <div <?php echo ($hdf_type == RECIPES_SEARCH_FRIEND)? 'id="friend_lbl_f"':'id="friend_lbl"'; ?>>
                                                <a href="javascript:submitSearchForm('home-search-form','<?echo C_BASE_PATH."friendsearch"?>','<?php print RECIPES_SEARCH_FRIEND;?>')"><img width="35" height="14" src="<?echo C_IMAGE_PATH?>space.gif"></a>
                                            </div>
                                         </div>
                                      </div>
                                  </div>
                              </div>
                        </div>
                    </form>
                <!-- END SEARCH FORM -->

                <!-- START FOLLOW SOCIAL NETWORK FORM -->
                <div id="DivaSocialNetwork">
                    <img height="32" width="103" src="<?echo C_IMAGE_PATH?>label/join_the_diva_conversation_lbl.gif">
                    <a rel="nofollow" href="http://twitter.com/recipediva" target="_blank"><img alt="Follow Twitter" src="<?echo C_IMAGE_PATH?>icon/icon_twitter.gif" style="padding-bottom: 2px;" /></a>
                    <a rel="nofollow" href="http://www.facebook.com/therecipediva" target="_blank"><img alt="Follow Facebook" src="<?echo C_IMAGE_PATH?>icon/icon_facebook.gif" style="padding-bottom: 2px;" /></a>
                    <a rel="nofollow" href="<?echo C_BASE_PATH."recipes/add?destination=mydiva/recipebox"?>"><img alt="Share Recipe Today" src="<?echo C_IMAGE_PATH?>button/share_today_lbl.gif" /></a>
                </div>
                <div id="facebook_like">
                    <iframe src="http://www.facebook.com/plugins/likebox.php?href=<?php print urlencode("http://www.facebook.com/therecipediva");?>&amp;width=340&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=false&amp;header=false&amp;height=62" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:340px; height:62px;" allowTransparency="true"></iframe>
                    <!--<iframe src="http://www.facebook.com/plugins/like.php?href=<?php print urlencode("http://www.facebook.com/therecipediva");?>&amp;layout=standard&amp;show_faces=false&amp;width=340&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:340px; height:30px;" allowTransparency="true"></iframe>-->
                </div>
                <!-- START LOGO LINK -->
                <div id="tip">
                    <div class="content">
                        <a href="<?php print C_BASE_PATH."divatips"?>" class="lastest_diva_tip"></a><a href="<?php print C_BASE_PATH."divatips?type=1"?>" class="weekly_health_tip"></a>
                    </div>
                </div>
                <!-- END LOGO LINK  -->
                <!-- END FOLLOW SOCIAL NETWORK FORM -->

                <!-- START HEATHY TIP
                <div id="DivaHeathyTip">
                    <a href="#" class="img"></a>
                </div>
                END HEATHY TIP -->
                <!-- START TOP RECIPES -->
                <!-- START TOP RECIPES ADS -->
                <?php print $sidebar_right ?>
        </div>
            <!-- END RIGHT COL -->
        </div>
        <!-- END BODY -->

        <!-- START FOOTER -->
        <div id="footer">
            <div id="footer_content">
                <a href="<?echo C_BASE_PATH."recipes"?>">Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Healthy')?>">Healthy Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/chicken"?>">Chicken Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Dessert')?>">Dessert Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Potatoes')?>">Potato Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Pasta')?>">Pasta Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Soup')?>">Soup Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Main Dish')?>">Dinner Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Indian ')?>">Indian Recipes</a> <span style="color:#9A7971;">|</span><br>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Appetizer')?>">Appetizer Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Breakfast/ Brunch')?>">Breakfast Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Mexican')?>">Mexican Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Quick/ Easy')?>">Easy Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Vegetables')?>">Vegetable Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."divatips"?>">Recipe Tips</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes?type=1&sort=2"?>">Recipe Reviews</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipes/".encodeURIComponent('Quick/ Easy')?>">Quick Recipes</a> <span style="color:#9A7971;">|</span>
                <a href="<?echo C_BASE_PATH."recipe/index"?>">Recipe Index</a><br><br>

                <a href="<?echo C_BASE_PATH."aboutus"?>"><img alt="About us" src="<?echo C_IMAGE_PATH?>footer/footer_aboutus.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a rel="nofollow" href="<?echo C_BASE_PATH."contactus"?>"><img alt="Contact us" src="<?echo C_IMAGE_PATH?>footer/footer_contactus.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a rel="nofollow" href="<?echo C_BASE_PATH."advertising"?>"><img alt="Advertising" src="<?echo C_IMAGE_PATH?>footer/footer_advertising.gif"  /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a rel="nofollow" href="<?echo C_BASE_PATH."privacy"?>"><img alt="Privacy" src="<?echo C_IMAGE_PATH?>footer/footer_privacy.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a rel="nofollow" href="<?echo C_BASE_PATH."termofuse"?>"><img alt="Terms of us" src="<?echo C_IMAGE_PATH?>footer/footer_termsofuse.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_slash.gif" />
                <img alt="Copyright" src="<?echo C_IMAGE_PATH?>footer/footer_copyright.gif" />


            </div>
        </div>
        <!-- END FOOTER -->
    </div>
    <div id="dialogInfo" style="display:none" title="">
    </div>
    <div id="dialogConfirm" style="display:none" title="Are you sure you want to delete?">
    </div>

<script>
<?php if(arg(0) == "news"){?>
    $(document).ready(function(){
            ResizeImage('span_body_news', 530);
    });
<?php }?>
<?php if(arg(0) == "aboutus" || arg(0) == "contactus"
        || arg(0) == "advertising" || arg(0) == "privacy" || arg(0) == "termofuse" ){?>
        $(document).ready(function(){
            ResizeImage('left_col', 618);
        });
<?php }
      if($_GET["opencow"] == 1 && drupal_is_front_page()){?>
        $(document).ready(function(){
            openCOW();
        });
<?php }
      if($_GET["openpromotion"] == 1 && drupal_is_front_page()){?>
        $(document).ready(function(){
            openPromotionRegistry();
        });
<?php }
    if($_GET["openglr"] == 1 && drupal_is_front_page()){?>
        $(document).ready(function(){
            openGallery();
        });
<?php }
    if($_GET["openunsubscribe"] == 1 && drupal_is_front_page()){?>
        $(document).ready(function(){
            showInfoMessage("<?php print MSG_UNSUBSCRIBE ?>");
        });
<?php }
    if($_GET["opensubscribenewsletter"] == 1 && drupal_is_front_page()){?>
        $(document).ready(function(){
            showInfoMessage("<?php print MSG_SUBSCRIBE_NEWSLETTER ?>");
        });
<?php }
    if($_GET["openunsubscribenewsletter"] == 1 && drupal_is_front_page()){?>
        $(document).ready(function(){
            showInfoMessage("<?php print MSG_UNSUBSCRIBE_NEWSLETTER ?>");
        });
<?php }?>

     //  START Son Nguyen 12/07/2010:Login Form Block Post to Login Page
    if ($("#user-login-form").length > 0 ){
        $("#edit-user-login-block").val("user_login");
        $("#edit-user-login-block").attr("id","edit-user-login");
        $("#user-login-form").attr("action","<?php print C_BASE_PATH."user/login?destination=".$_GET['q']?>");
        $("#user-login-form").attr("id","user-login");
    }
     //  END Son Nguyen 12/07/2010:Login Form Block Post to Login Page

    function submitSearchForm(frmName, url, hidType) {
        $("#search_input_div > #hdf_type").val(hidType);
        var url_encoded = url + "?type=" + hidType;
        if($.trim($("#txtHomeKeyWord").val()) != ""){
            url_encoded += "&hkw=" + encodeURIComponent($.trim($("#txtHomeKeyWord").val()));
        }
        window.location = url_encoded;
        return true;
    }

    var num_page = 0;
    var num_page_user = 0;
    var num_page_newest_recipes = 0;
    var action = '';
    function showMoreFeed(){
        num_page++;
        $.get("<?php print C_BASE_PATH."showmorefeed/"?>" + num_page,
               function(data){
                    var json = eval("(" + data + ")");
                    if (json['status'] == "success") {
                        $("#div_live_feed").append(json['data']);
                        if(json['size'] <= <?php print NUMBERS_FEED_LIST?>){
                            $("#div_show_more").hide();
                        }
                    }
                    else{
                        //$(this).hide();
                    }
               },"text");
    }

    function showMoreUser(){
        num_page_user++;
        $.get("<?php print C_BASE_PATH."showmoreuser/"?>" + num_page_user,
               function(data){
                    var json = eval("(" + data + ")");
                    if (json['status'] == "success") {
                        $("#div_newest_users").append(json['data']);
                        if(json['size'] <= <?php print NUMBERS_NEW_MEMBER_LIST?>){
                            $("#div_show_more_user").hide();
                        }
                    }
                    else{
                        //$(this).hide();
                    }
               },"text");
    }

    function showMoreNewestRecipes(){
        num_page_newest_recipes++;
        $.get("<?php print C_BASE_PATH."showmorenewestrecipes/"?>" + num_page_newest_recipes,
               function(data){
                    var json = eval("(" + data + ")");
                    if (json['status'] == "success") {
                        $("#div_newest_recipes").append(json['data']);
                        if(json['size'] <= <?php print NUMBERS_NEW_RECIPE_LIST?>){
                            $("#div_show_more_newest_recipes").hide();
                        }
                    }
                    else{
                        //$(this).hide();
                    }
               },"text");
    }

    // check refeshLiveFeed is doing....
    var flag_refresh_live_feed = 1;

    function refeshLiveFeed(){
        if(flag_refresh_live_feed == 1) {

            // refeshLiveFeed is doing
            flag_refresh_live_feed = 0;

            if(action == 'submitstatus' || action == 'saverecipetip'){
                setTimeout("refeshLiveFeed()",3000);
            }else{
                var id = $("#maxIdLiveFeed").html();
                $.post("<?php print C_BASE_PATH."shownewestfeed/"?>" + id,
                   function(data){

                        // refeshLiveFeed is finish
                        flag_refresh_live_feed = 1;

                        var json = eval("(" + data + ")");
                        if (json['status'] == "success") {
                            if(json['maxId'] < id){
                                $("#div_live_feed").html('');
                                $("#div_live_feed").prepend('<span id="maxIdLiveFeed" style="display:none;"></span>');
                            }
                            $("#div_live_feed").prepend(json['data']);
                            $("#maxIdLiveFeed").html(json['maxId']);
                        }
                        else{
                            //
                        }
                        setTimeout("refeshLiveFeed()",30000);
                   },"text");
            }
        }
    }

    <?php if($user->uid){?>
    var msg_recipe = '<?php print ADD_RECIPE_INTO_RECIPE_BOX_COMPLETE_INFO;?>';
    var msg_tip = '<?php print ADD_MY_SAVED_TIP_MSG;?>';

    function submitStatus(){
        if($.trim($("#txtYourMind").val()) == "" || $("#txtYourMind").val() == "<?php print INFO_MSG_WHAT_YOUR_MIND?> "){
            return;
        }else{
            action = 'submitstatus';
            $.post("<?php print C_BASE_PATH."submitstatus"?>",
                   {txtYourMind:$("#txtYourMind").val()},
                   function(data){
                        var json = eval("(" + data + ")");
                        if (json['status'] == "success") {
                            $("#div_live_feed").prepend(json['data']);
                            $("#maxIdLiveFeed").html(json['maxId']);
                            action = '';
                        }
                        else{
                            action = '';
                        }
                   },"text");

            $("#txtYourMind").val("");
            $.watermark.show('#txtYourMind');
        }
    }
    function showRecipeTipSaved(nid){
        action = 'saverecipetip';
        $.post("<?php print C_BASE_PATH."showrecipetipsaved/"?>" + nid,
           function(data){
                var json = eval("(" + data + ")");
                if (json['status'] == "success") {
                    $("#div_live_feed").prepend(json['data']);
                    $("#maxIdLiveFeed").html(json['maxId']);
                    action = '';
                }
                else{

                    action = '';
                }
           },"text");

    }
    <?php }?>
    $(document).ready(function(){
        <?php if($user->uid){?>
        $('#txtYourMind').watermark('<?php print INFO_MSG_WHAT_YOUR_MIND?> ');
        <?php }?>
        //refeshLiveFeed();
    });

</script>
</body>
</html>
