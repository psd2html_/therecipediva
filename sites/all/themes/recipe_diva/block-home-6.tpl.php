<!-- START NEW FOODIES -->
<div id="newfoodies">
	<div id="newfoodies_b">
    	<div id="newfoodies_content">
    	  	<div class="newfoodies_content_inner">
              <div>
                <ul id="div_newest_users">
                    <?php 
             		$i = 0;
             		foreach ($block->content as $row){			             			
             			if($i==NUMBERS_NEW_MEMBER_LIST){
             				break;
             			}
             			$i ++;
             		?>
                    <li>
                        <div class="img_contain">
                            <?php $file_path = $row["picture"];
	    					if(file_exists($file_path)){ ?>
	    						<a rel="nofollow" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><img alt="<?php print $row["name"]?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,67) ?> /></a>
	    					<?php }else{?>
	    						<a rel="nofollow" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><img alt="<?echo $row["profile_gender"]=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?echo C_IMAGE_PATH?>photo/<?echo $row["profile_gender"]=="0"? "male":"female"?>.gif"></a>
	    					<?php }?>
                        </div>
                        <div class="img_label">
                            <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><?php print $row['name']?></a>
                        </div>
                    </li>
                    <?php }?>
                </ul>
              </div>
              <div id="div_show_more_user" class="user_content_bot">
				<a href="javascript:showMoreUser()" rel="nofollow">&lt;&lt;show more&gt;&gt;</a>					
			</div>
            </div>
            
   	  </div>
    </div>
</div>
<!-- END NEW FOODIES -->