<!-- START COOK OF WEEK -->
<div id="cookofweek">
        <div id="cookofweek_b">
            <div id="cow_content">
            <?php if($block->content != " "){?>
                    <div id="img_contain">
                    <?php if(file_exists($block->content->picture)){ ?>
                        <img alt="<?php print $block->content->name?>" src="<?php print C_BASE_PATH.$block->content->picture.'" '.recipe_utils::getImageWidthHeight($block->content->picture,60,60) ?>  />
                    <?php }else{ ?>
                        <img alt="<?echo $block->content->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?echo C_IMAGE_PATH?>photo/<?echo $block->content->profile_gender=="0"? "male":"female"?>.gif">
                    <?php }?>
                    </div>
                <div id="cook_contain">
                    <div style="padding-bottom:4px;padding-left:70px;"><a rel="nofollow" href="<?php print C_BASE_PATH."user/".$block->content->name?>" class="title"><?php print $block->content->name?></a></div>
                    <?php
                        $location = profile_location_get_country($block->content->profile_country);
                        if ($block->content->profile_state)
                        {
                            $location .= " &gt; " . profile_location_get_state($block->content->profile_country,$block->content->profile_state);
                        }
                        print $location;?>
                    <br />

                    <img src="<?echo C_IMAGE_PATH?>space.gif" width="1" height="6" /><br />
                    <?php print nl2br(recipe_db::get_cow_admin_comment($block->content->uid))?></div>

              <?php }?>
              <div id="tobeCookOfWeek">
                  <?php global $user;
                        $user_role = recipe_utils::getUserRole();
                      $hasPer =  $user_role == C_ADMIN_USER || $user_role == C_PREMIUM_USER ? true : false || $user_role == C_REGISTER_USER ? true : false;
                if($hasPer){?>
                    <img alt="Cook Of Week" src="<?echo C_IMAGE_PATH?>cookofweek.png" style="border: 0px;cursor:pointer;"
                        onclick="openCOW()" />
                <?php }
                elseif ($user->uid){
                    print '<img alt="Cook Of Week" onclick="showConfirmBecomePrimium();return false;" src="' . C_IMAGE_PATH . 'cookofweek.png" style="border: 0px;cursor:pointer;" >';
                }
                else{
                    $login_url = "user/login?destination=index%3Fopencow=1";
                    print '<a rel="nofollow" href="' . C_BASE_PATH. $login_url. '"><img alt="Cook Of Week" src="' . C_IMAGE_PATH . 'cookofweek.png" style="border: 0px;cursor:pointer;" ></a>';
                }?>
              </div>
          </div>
        </div>
</div>
<!-- END COOK OF WEEK -->
<div id="dialogConfirmBecomePrimium" style="display:none;text-align:center;" title="<?php print INFO_MSG_PREMIUM_FUNCTION?>">
    <div style="margin:7px 0px 0px 5px;text-align:center;">
        <input type="button" value="Yes, sign me up!" style="width:110px;" onclick="document.location.href='<?php print C_BASE_PATH?>become_premium'">
        <input type="button" value="No thanks" style="width:110px;"  onclick="closeDialogConfirmBecomePrimium();">
    </div>
</div>
<?php include('cook_of_week.php');?>

<?php //include('gallery_upload.php');?>
