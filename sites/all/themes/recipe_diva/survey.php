<?php
global $user;
$current_user = user_load($user->uid);
profile_load_profile($current_user);
?>
<script>
function openAuthRequest(){
    $("#div_auth").dialog(
        { modal: true },
        { resizable: false },
        { height: 530},
        { width: 833},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_auth").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#div_auth").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
}
function openDonateRequest(){
    $("#div_donate").dialog(
        { modal: true },
        { resizable: false },
        { height: 530},
        { width: 833},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_donate").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#div_donate").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
}
function openPromotionRegistry(){
    $("#btn_send_info").unbind();
    $("#div_promotion").dialog(
        { modal: true },
        { resizable: false },
        { height: 730},
        { width: 833},
        { draggable: false},
        { buttons:
            {
                "Close": function() {
                            $(this).dialog("close");
                        }
            }
        },
        { open: function(event, ui) {
                    $("div[class^=ui-dialog-titlebar]").hide();
                    $("div[class^=ui-dialog-buttonpane]").hide();
                    $("#div_promotion").parents("div[class^=ui-dialog]").draggable().removeClass("ui-widget-content");
                }
        },
        { beforeclose: function(event, ui) {
                    $("#txtAge").val($.trim($("#txtAge").val()));
                    $("#selIncome").val($("#selIncome").val());
                    $("#selEdu").val($("#selEdu").val());
                    $("#rdoOwnHome").val($("#rdoOwnHome").val());

                    $("#message_error").html("");
                    $("#div_error_message").hide();
                    $("#div_promotion").parents("div[class^=ui-dialog]").addClass("ui-widget-content");
                    $("div[class^=ui-dialog-titlebar]").show();
                    $("div[class^=ui-dialog-buttonpane]").show();
                }
        }
    );
    $("#txtAge").focus();
}


</script>

<div id="div_auth" style="display:none;overflow:hidden;">
    <div id="promotion_contain" class="promotion_contain">
        <div class="top">
             <div><img width="833" src="<?php print empty($popupBgPath) ? C_IMAGE_PATH."border/promotion_popup_t_bg11.png" : C_BASE_PATH.$popupBgPath?>"></div>
        </div>

        <div class="content">
            <div class="content_inner">
                <div>
                    <div class="title">
                        You must be logged in to register for this promotion
                    </div>


                    <img src="<?echo C_IMAGE_PATH?>space.gif" height="15" width="1" />
                    <?php
                    $login_url = "user/register?destination=index%3Fopenpromotion=1";
                    ?>
                    <div class="inner">
                        Please <a onclick="$('#div_auth').dialog('close');" href="<?php print C_BASE_PATH. $login_url?>">click here</a> to register. Once registered, you will be able to enter to win Diva prizes and become a participating member of the site!
                    </div>


                </div>
              <div class="btn_contain">
                <div class="dot_bg">
                    &nbsp;
                </div>
                <div class="register_btn_contain">
                    <a class="close_btn" onclick="$('#div_auth').dialog('close');" href="#"></a>
                </div>
            </div>
          </div>

        </div>

        <div class="bot">
            <div></div>
        </div>
    </div>
</div>



<div id="div_promotion" style="display:none;overflow:hidden;">
    <div id="promotion_contain" class="promotion_contain">
        <div class="promotion_button">
              <!--<a onclick="$('#div_promotion').dialog('close');" class="promotion_close_btn"></a>-->
        </div>
        <div class="top">
             <div><img width="833" src="<?php print empty($popupBgPath) ? C_IMAGE_PATH."border/promotion_popup_t_bg11.png" : C_BASE_PATH.$popupBgPath?>"></div>
        </div>
        <div class="content">
            <div class="content_inner">
                <div>
                    <div class="title">
                        Welcome back <?php print $user->name?>
                    </div>
                    <div id="div_space"><img src="<?echo C_IMAGE_PATH?>space.gif" height="10" width="1" /></div>
                    <div class="inner">
                        <?php
                        $login_url = "logout?destination=user/login%3Fdestination=index%3Fopenpromotion=1";
                        ?>
                        You are logged in as <?php print $user->name?>, to login as a different user please <a onclick="$('#div_promotion').dialog('close');" href="<?php print C_BASE_PATH. $login_url?>">click here</a>.
                  </div>
                  <div id="div_space"><img src="<?echo C_IMAGE_PATH?>space.gif" height="10" width="1" /></div>
                    <div id="div_error_message" class="message error" style="display:none;padding-bottom:20px;">
                        <ul style="margin-bottom:0px;margin-top:15px;"><span id="message_error"></span></ul>
                    </div>


                    <div class="btn_contain">
                        <div class="dot_bg_shorter">
                            &nbsp;
                        </div>
                        <div class="register_btn_contain">
                            <a id="btn_send_info" class="survey_btn" href="<?php print C_BASE_PATH?>userfeedback"></a>
                        </div>
                        <div class="register_btn_contain" style="padding-left:5px;">
                            <a class="cancel_btn" onclick="$('#div_promotion').dialog('close');" href="#"></a>
                        </div>
                    </div>

                    <div class="inner_smalltext">
                        This promotion entitles the user to win an OXO box grater.
                        Please note that this offer is only available online at <a href="http://www.therecipediva.com">www.therecipediva.com</a>.
                        You must be 18 years of age or over to participate in this promotion.
                        Promotions are available only once to any person.
                        By entering, you accept and agree to be bound by these Promotion Terms and The Recipe Diva <a href="<?php print C_BASE_PATH?>termofuse">Terms and Conditions</a>.

                    </div>

                </div>
          </div>
        </div>

        <div class="bot">
            <div></div>
        </div>
    </div>
</div>
