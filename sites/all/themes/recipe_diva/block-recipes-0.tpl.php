<?php
if ($block->content != " "){?>
<div id="search_result">
    <div id="search_result_title">
        <div id="search_result_title_content">
            <img alt="Filter" src="<?php print C_IMAGE_PATH?>label/filter_title_lbl.gif"/>
        </div>
    </div>
    <div id="search_result_b">
        <div id="search_result_content">
            <div id="filter_content_contain">
                  <!--<div>
                    <img src="<?php print C_IMAGE_PATH?>label/refineby_lbl.gif"/>
                </div>-->
              <?php


                $idCategory = 0;
                $intCountSubCat = 0;
                $strFocus = "";

                $arr = explode("/" , $_POST['hdf_tid']);

                foreach ($block->content as $term) {
                    $bFlag = false;
                    $class = "";
                    foreach ($arr as $key =>$value){
                        $value = urldecode(str_replace("--","/",$value));
                        if ($value != '' && strtolower($term['subcatName']) == strtolower($value)){
                            $strFocus = "term_" . $term['tid'];
                            $class = ' class="focus_filter"';
                            $bFlag = true;
                            break;
                        }
                    }

                  if ($idCategory == 0 || $idCategory != $term['parent_id']){
                          if ($intCountSubCat > RECIPES_RESULT_PER_CATEGORY){
                              print '</div>';
                          }
                        if ($idCategory != 0)
                            print '</div>';

                        print '<div id="filter_content"><span class="filter_title">' . $term['parent_name'] .':</span>' ;
                        $idCategory = $term['parent_id'];
                        $intCountSubCat = 0;
                    }
                    if ($intCountSubCat == RECIPES_RESULT_PER_CATEGORY){
                        print '<div class="filter_showmore" id="read_more_'. $idCategory .'" onclick="show(' . $idCategory . ');">[+] Show More</div>';
                        print '<div id="category_'. $idCategory .'" style="display:none;">';
                    }

                    $url_add = filter(1, encodeURIComponent($term['subcatName']));
                    $url_del = filter(0, encodeURIComponent($term['subcatName']));

                    //print '<div><img src="' . C_IMAGE_PATH . 'icon/search_results_dot.gif">&nbsp;<a id="term_' . $term['tid'] .'" href="' . C_BASE_PATH. "search_result/" . $term['tid'] . '">' . $term['subcatName'] . '</a> (' . $term['total_subcat'] . ')</div>' ;
                    if ($bFlag){

                        print '<div id="search_result_content_subcat">
                                <div style="float:left;height:18px;">
                                    <img alt="Bullet" src="'.C_IMAGE_PATH.'icon/search_results_dot_brown.gif">&nbsp;<a class="focus_filter" href="' . $url_add . '">' . $term['subcatName'] . '</a> (' . $term['total_subcat'] . ')' .
                                '</div>
                                <div style="float:left;padding-left:5px;padding-top:2px;">
                                    <a href="' . $url_del . '"' . $class .'><img alt="Delete groceries" style="cursor:pointer;" src="'.C_IMAGE_PATH.'button/groceries_del_btn.gif"></a>
                                </div>
                                <div style="display: block;clear:both;"></div>
                            </div>';
                    }else{
                        print '<div id="search_result_content_subcat">' .
                                '<img alt="Bullte" src="' . C_IMAGE_PATH . 'icon/search_results_dot_brown.gif">&nbsp;' .
                                '<a id="term_' . $term['tid'] .'" href="' . $url_add . '"' . $class .'>' . $term['subcatName'] . '</a> (' . $term['total_subcat'] . ')
                               </div>' ;
                    }
                    $intCountSubCat++;
                }
                if ($intCountSubCat > RECIPES_RESULT_PER_CATEGORY){
                      print '</div>';
                  }
                if ($idCategory != 0){
                    print "</div>";
                }
                ?>
          </div>
      </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#ingredients_left_ads").hide();

        $("#search_result_b").find("a[class^=focus_filter]").each(function(){
            carID = $(this).parents("div[id^=category_]").attr("id");
            if(carID != null && $("#"+carID).is(":hidden")){
                carID = carID.replace("category_","");
                show(carID);
            }
            $(this).parents("div[id=search_result_content_subcat],div[id^=category_]").siblings("div").hide();
        });
    });

    function show(category_id) {
        var showMore = document.getElementById("read_more_" + category_id);
        var category= document.getElementById("category_" + category_id);

        showMore.style.display = "none";
        category.style.display = "";
    }

</script>
<? }?>


