<?php
$postuser = user_load(array('uid' => $node->uid));
$url_news = trim($node->field_news_url[0]['value']);
if (empty($url_news) == false){
	$httpLink = "http://";
	if (strpos($url_news, $httpLink) !== 0){
			$url_news = 'Check it out at <a href="'. $httpLink . $url_news.'" target="_blank">' . $httpLink . $url_news . '</a>';
	}else{
		$url_news = 'Check it out at <a  href="'.$url_news.'" target="_blank">' . $url_news . '</a>';
	}
}

?>
<div id="news_p">
    <div id="news_p_b">
   	  	<div id="news_border">
	   	  	<div class="news_p_content_font" id="news_p_content">
			    <div class="new_p_title" style="padding-left:5px;">
			    	<?php print $node->title?>
			    </div>
			    <img width="530" height="5" src="<?echo C_IMAGE_PATH?>space.gif">
			    <div class="new_p_posted" style="display:none">
			    by <a class="by_author" href="<?php print C_BASE_PATH."user/".$postuser->name?>"><?php print $postuser->name?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="feed_date"><?php print date(STANDARD_DATE_FORMAT,$node->created);?></span>
				</div>
		        <div id="img_contain" style="padding-left:5px;">
			       	<?php if(file_exists($node->field_news_image[0]['filepath'])){
			       		$file_path = $node->field_news_image[0]['filepath'];			       		
			       	?>
				    	<img alt="<?php print $node->title?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,530,309) ?>  />
			    	<?php }else{?>		
			       		<img width="530" height="48" src="<?echo C_IMAGE_PATH?>space.gif"> 			       		
			       	<?php }?>
		        	<div id="news_p_menu_1"><img alt="News" width="541" height="51" src="<?echo C_IMAGE_PATH?>news_bar.gif"></div>
		        </div>
		        <div id="content_b">
		        	<span id="span_body_news">
						<?php print $node->body?>
					</span>
		        </div>
				<div id="content_b">
		         	<?php print $url_news?>
		        </div>
	        </div>
    	</div>
    </div>
</div>