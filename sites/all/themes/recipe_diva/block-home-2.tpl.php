<!-- START TOP RECIPES -->
<div id="most_recipe">
    <div id="most_recipe_b">
        <div id="most_recipe_content">
            <?php 	if($block->content != " "){
                    foreach ($block->content as $row) {
                $nodeLink = C_BASE_PATH."recipe/".recipe_utils::removeWhiteSpace($row["title"])."-".$row["nid"];
                ?>
                <div class="most_recipe_content_inner">
                    <div class="image_contain">
                        <?php
                        $file_path = recipe_utils::get_thumbs_image_path($row["filepath"]);
                        if(file_exists($file_path)){ ?>
                            <img alt="<?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,61) ?> />
                        <?php }else{?>
                            <img alt="Default Recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                        <?php }?>
                    </div>
                    <div class="content">
                        <div class="name" style="font-weight: bold !important;">
                            <a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?></a>
                        </div>
                        <div class="upload_by">
                            <?php if($row["uid"]!=0){?>
                            <span class="by">by</span> <a rel="nofollow" class="by_author" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><?php print $row["name"]?></a>
                            <?php }else{
                                print '<span class="by">by '.C_UNKNOWN_USER.'</span>';
                            }?>
                        </div>
                        <div class="rate">
                            <?php print theme('fivestar_static', $row["vote_average_value"], 5, 'vote');?>
                        </div>
                    </div>
                    <?php
                    //comments of the recipe
                    $result = recipe_db::get_node_comment($row["nid"]);
                    $strComment = "";
                    //Get newest review
                    while ($comment = db_fetch_object($result)){
                        $strComment = $comment->comment;
                        $cid = $comment->cid;
                        break;
                    }
                    ?>
                    <div class="review">
                        <div class="review_content">
                            <div class="line1">
                            <?php
                            $strComment1 = recipe_utils::get_excerpt_limit($strComment,10,false,40);
                            $strEtc = "";
                            if(strlen($strComment) > 60){
                                $strEtc = ETC_STRING;
                            }
                            print $strComment1.$strEtc;
                            ?>
                            </div>
                            <div class="line2" style="font-weight: bold !important;">
                                <a class="line2" href="<?php print C_BASE_PATH?>recipe/<?php print recipe_utils::removeWhiteSpace($row["title"])?>-<?php print $row["nid"]?>&cid=<?php print $cid?>">Read all (<?php print $row["total_review"]?>) Reviews</a>
                                <!--<a class="line2" href="<?php print C_BASE_PATH?>recipe/<?php print recipe_utils::removeWhiteSpace($row["title"])?>-<?php print $row["nid"]?>"><?php print $row["like_count"]?> people like this</a>-->
                            </div>
                            <!--
                            <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
                            <fb:like href="<?php print C_SITE_URL.$nodeLink?>" send="false" width="166" show_faces="false" font=""></fb:like>
                            -->
                        </div>
                    </div>
                </div>

                <!--<div id="TopRecipes_content_inner">

                    <div id="img_contain">
                        <?php
                        $file_path = recipe_utils::get_thumbs_image_path($row["filepath"]);
                        if(file_exists($file_path)){ ?>
                            <img alt="<?php print $row["title"]?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,60,61) ?> />
                        <?php }else{?>
                            <img alt="Default Recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                        <?php }?>
                    </div>

                    <div id="divatiptop_right">
                        <div id="divatoptip_line1">
                            <div class="divatiptop_left divatop_title"><a href="<?php print $nodeLink?>"><?php print recipe_utils::get_excerpt_limit($row["title"],10,false,50)?></a></div>
                            <div class="divatiptop_left divatop_title">
                            <?php if($row["uid"]!=0){?>
                            <span class="by">by</span> <a rel="nofollow" class="by_author" href="<?php print C_BASE_PATH."user/".$row["name"]?>"><?php print $row["name"]?></a>
                            <?php }else{
                                print '<span class="by">by '.C_UNKNOWN_USER.'</span>';
                            }?>
                            </div>
                        </div>
                        <div id="divatoptip_line2">
                            <div class="divatiptop_left"><?php print theme('fivestar_static', $row["vote_average_value"], 5, 'vote');?></div>
                        </div>
                    </div>
                </div>-->
            <?php }
            }?>
        </div>
    </div>
</div>
<!-- END TOP RECIPES -->
