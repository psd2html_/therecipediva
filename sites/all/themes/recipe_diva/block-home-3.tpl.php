<?php foreach ($block->content as $row) {
    $url_mc = trim($row['url']);
    $target = '';
    if (empty($url_mc) == false){
        $httpLink = "http://";
        if (strpos($url_mc, $httpLink) !== 0){
                $url_mc = $httpLink . $url_mc;
        }

        $url_domain = recipe_utils::getDomainName($url_mc);
        $main_domain = recipe_utils::getDomainName(C_SITE_URL.C_BASE_PATH);
        if($url_domain != "www.therecipediva.com" && $url_domain != "therecipediva.com") {
            $target = '_blank';
        }
    }
    //$tilte = trim(str_replace('"','\"',recipe_utils::get_excerpt_limit($row["title"], 10, false, 52)));

    if(file_exists($row["filepath"])){
        $file_path = $row["filepath"];
        $recipe_img .= '["'.C_BASE_PATH.$file_path.'", "'.$url_mc.'", "'.$target.'", ""],';
    }
    else{
        $recipe_img .= '["", "'.$nodeLink.'", "", ""],';
    }

}
if ($recipe_img)
{
    $recipe_img = substr($recipe_img,0,strlen($recipe_img)-1);
}

$admin_upload_image = recipe_db::get_admin_upload_image_url(HOME_PAGE);
?>
<!-- START FEATURE RECIPES -->
<div id="FeaturedRecipe">
    <div id="homepage_title_contain"
        style="background: url(<?php print file_exists($admin_upload_image) ? C_BASE_PATH.$admin_upload_image : C_IMAGE_PATH.'border/homepage_top.png'?>) top left no-repeat;">
    </div>
    <div id="FeatureRecipe_b">
        <div id="FeaturedRecipe_content">
             <div id="simplegallery1"></div>
      </div>
    </div>
</div>

<script type="text/javascript">
    var recipe_des = [<?php print $recipe_img?>]

    var simpleGallery_navpanel={
        panel: {height:'45px', opacity:0, paddingTop:'0px', fontStyle:'bold 11px Verdana'}, //customize nav panel container
        images: ['<?echo C_IMAGE_PATH?>previous.png', '<?echo C_IMAGE_PATH?>next.png', '<?echo C_IMAGE_PATH?>circle_r.png', '<?echo C_IMAGE_PATH?>circle.png'], //nav panel images (in that order)
        imageSpacing: {offsetTop:[2, 2, 2 ,2, 2, 2, 2], spacing:5}, //top offset of left, play, and right images, PLUS spacing between the 3 images
        slideduration: 50 //duration of slide up animation to reveal panel
    }
    var mygallery = new simpleGallery({
        wrapperid: "simplegallery1", //ID of main gallery container,
        dimensions: [627, 238], //width/height of gallery in pixels. Should reflect dimensions of the images exactly
        imagearray: [<?php print $recipe_img?>],
        autoplay: [false, 5000, 5], //[auto_play_boolean, delay_btw_slide_millisec, cycles_before_stopping_int]
        persist: false, //remember last viewed slide and recall within same session?
        fadeduration: 500, //transition duration (milliseconds)
        navpanelheight:32,
        oninit: function() { //event that fires when gallery has initialized/ ready to run

            //Keyword "this": references current gallery instance (ie: try this.navigate("play/pause"))
            $("img[id^=navImage]").css("filter", "");
            var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
            var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
            if (jQuery.browser.msie && (ie55 || ie6)) {
                $("img[id^=navImage]").attr("style","");
                $("div.navpanellayer").pngFix();
                $("span[id^=navImage]").css("cursor","pointer");
                $("span[id^=navImage]").css("top","6px");
                $("span[id^=navImage]").css("margin-right","5px");

                $("span[id^=navImage]").click(function(){
                    $("img[id="+this.id+"]").click();
                });
            }

            mygallery.navigate('playpause');
        },
        onslide: function(curslide, i) { //event that fires after each slide is shown
            //Keyword "this": references current gallery instance
            //curslide: returns DOM reference to current slide's DIV (ie: try alert(curslide.innerHTML)
            //i: integer reflecting current image within collection being shown (0=1st image, 1=2nd etc)
            var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
            var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
            if (jQuery.browser.msie && (ie55 || ie6)) {
                var style = "";
                for (var j = 0; j < recipe_des.length; j++) {
                    style = "cursor:pointer; top:6px; margin-right:5px; DISPLAY: inline-block; BACKGROUND: none transparent scroll repeat 0% 0%;";
                    if (j == i) {
                        $("img[id=navImage"+(j + 1)+"]").attr("src", simpleGallery_navpanel.images[2]);
                        style += " FILTER: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+simpleGallery_navpanel.images[2]+"', sizingMethod='scale');WIDTH: 11px; POSITION: relative; HEIGHT: 19px;";
                    }
                    else {
                        $("img[id=navImage"+(j + 1)+"]").attr("src", simpleGallery_navpanel.images[2]);
                        style += " FILTER: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+simpleGallery_navpanel.images[3]+"', sizingMethod='scale');WIDTH: 13px; POSITION: relative; HEIGHT: 19px;";
                    }
                    $("span[id=navImage"+(j + 1)+"]").attr("style",style);
                }
            }
            else{
                for (var j = 0; j < recipe_des.length; j++) {
                    if (j == i) {
                        $("#navImage" + (j + 1)).attr("src", simpleGallery_navpanel.images[2]);
                    }
                    else {
                        $("#navImage" + (j + 1)).attr("src", simpleGallery_navpanel.images[3]);
                    }
                }
            }
        }
    })
</script>

<!-- END FEATURE RECIPES -->
