<script>
function sendMessage(receive_uid){
    var objSubject = document.getElementById("txtSendSubject");
    var objMessage = document.getElementById("txtSendMessage");
    var hasError = false;
    var errorMessage = "";
    var x=0;

    if (trim(objSubject.value) == ""){
        errorMessage = "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Subject'));?>" + "</li>";
        objSubject.focus();
        hasError = true;
    }

    if (trim(objMessage.value) == ""){
        errorMessage += "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Message'));?>" + "</li>";
        if(hasError == false){
            objMessage.focus();
        }
        hasError = true;
    }else{
        if ($.trim($("#txtSendMessage").val()).length > 1500) {
            errorMessage += "<li>" + "<?php print MESSAGE_MAXLENGTH_ERROR?>" + "</li>";
            objMessage.focus();
            hasError = true;
        }
    }
    var objError = document.getElementById("div_error_send_message");
    if(hasError){
        $("#send_message_error").html(errorMessage);
        objError.style.display = "";
        return false;
    }else{
        objError.style.display = "none";
    }
    $("#btn_send_message").unbind();
    $.post("<?php print C_BASE_PATH."send_message/"?>"+receive_uid,
                   { txtSendSubject: $("#txtSendSubject").val(), txtSendMessage:$("#txtSendMessage").val()},
                   function(data){
                        var json = eval("(" + data + ")");
                        $('#div_message').dialog('close');
                        if (json['status'] == "success") {
                            showInfoMessage("<?php print SEND_MESSAGE_SUCCESS ?>");
                        }
                        else{
                            showInfoMessage("<?php print SEND_MESSAGE_FAIL ?>");
                            $("#btn_send_message").click(function(){sendMessage(receive_uid);});
                        }
                   },"text");
}
</script>

<div id="div_message" style="display:none;overflow:hidden;cursor:move">
    <div id="email_recipe_contain">
        <div class="email_recipe_top">
            <div>&nbsp;</div>
        </div>
        <div class="email_recipe_m">
            <div class="email_recipe_content">
                <div id="email_popup">
                    <div class="pre_membership_p_title_1">Send Message</div>
                    <div id="dot_bg1">
                        <img width="1" height="14" src="<?php print C_IMAGE_PATH;?>space.gif">
                    </div>
                </div>
                <div id="popup_left_col">
                      <div id="mail_content">
                          <div id="mail_content_col" style="font-weight:normal;">
                            <div id="div_error_send_message" class="message error" style="display:none;">
                                <ul style="margin-bottom:0px;"><span id="send_message_error"></span></ul>
                            </div>
                        </div>
                      </div>
                      <div id="mail_titles">
                        <div id="mail_title_col">Subject:<span id="require">*</span></div>
                        <div id="mail_content_col"><input type="text" id="txtSendSubject" name="txtSendSubject" value="" maxlength="100"></div>
                      </div>
                      <div id="mail_titles">
                        <div id="mail_title_col">Message:<span id="require">*</span></div>
                        <div id="mail_content_col"><textarea style="height:200px;overflow:auto;" type="text" id="txtSendMessage" name="txtSendMessage"  maxlength="500" value=""></textarea></div>
                      </div>
                      <div style="text-align:left;">
                          <div id="mail_title_col" style="padding-top:5px;">&nbsp;</div>
                          <input type="button" class="btn_send_mail" id="btn_send_message"/>
                        <input type="button" class="btn_cancel_mail" onclick="javascript:$('#div_message').dialog('close')"/>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
