<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Print My Groceries</title>
	<?php print $scripts ?>
	<?php print $styles; ?>
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?echo C_CSS_PATH?>ie.css" media="screen"/><![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php print GOOGLE_ANALYTICS_ID?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>	
</head>

<body id="printable_no_bg">
	<div id="main_printable">
	    <!-- START HEADER -->
    	<div style="height: 170px;" id="header_printable">
        	<a class="logo_small">
        		<img alt="Logo" src="<?echo C_IMAGE_PATH?>logo_small.gif">
        	</a>
      </div>
        <!-- END HEADER -->
        
        <!-- START BODY -->
      <div id="body_printable">
        	<!-- START LEFT COL -->
                <!-- START FEATURE RECIPES -->
                <div id="printable_groceries_container">
                	<div>                   	
                    <div id="printable_groceries_content">
                    	<div id="printable_groceries_title">
                        	My Groceries
                        </div>
                    </div>
                    <!--
                    <div id="printable_groceries_content">
                    	<div id="printable_groceries_top">
                    	<img src="<?echo C_IMAGE_PATH?>border/printable_groceryList_top.gif">
                    	</div>
                  	</div>
                  	-->
                  	<?php
		    			$groceries = load_procery_list();
		    			$aisle_id = -1;
		    			foreach ($groceries as $row) { 
		    				if($aisle_id != $row->aisle_id){	    					
		    					if($aisle_id != -1){
		    						print '</div></div>';
		    					}
		    					$aisle_id = $row->aisle_id;
		    					print '<div id="printable_groceries_content"><div id="printable_groceries_content_title">'.$row->aisle_name.'</div><div id="printable_groceries_content_inner">';
		    				}?>			
				                <div id="printable_groceries_l_col">
				                	<?php print $row->title?>				                    
				                </div>
				                <!--
				                <div id="printable_groceries_r_col">
				                    <?php print $row->quantity?> <?php print $row->unit_name?>
			                	</div>		
			                	<div id="div_space">
			                    	<img width="1" height="0" src="<?echo C_IMAGE_PATH?>space.gif">
			                    </div>
			                    -->		        
			        <?php }
		    	    	if($aisle_id != -1){
							print '</div></div>';
						}
			        ?>
			        <?php global $user;
			        if($user->profile_groceries_note){?>
                    <div id="printable_groceries_content">                    	
                        <div id="printable_groceries_content_title">
                        	Notes
                        </div>
                        
                        <div id="printable_groceries_content_inner">
                            <div id="printable_groceries_m_col">
                            	<?php print nl2br($user->profile_groceries_note);?>
                            </div>
                        </div>
                    </div>
					<?php }?>
					<div id="printable_groceries_content">
                    	<div id="printable_groceries_rintlist">
                   	    	<a onclick="window.print();" href="#"><img alt="Print" src="<?echo C_IMAGE_PATH?>button/print_list_btn.gif"></a>                   	    	
                        </div>
                    </div>      
                    </div>              
					<div id="printable_groceries_content">
	                    <div class="printable_copyright" id="printable_copyright">
	                        <img alt="Copyright" src="<?echo C_IMAGE_PATH?>footer/footer_copyright.gif"/>
	                    </div>
	                </div>
	                
                    <div id="div_space">
                    	<img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>   
                                     
                </div>                
	       	</div>
                <!-- END FEATURE RECIPES -->
     </div>
        <!-- END BODY -->

    </div>
</body>
</html>
