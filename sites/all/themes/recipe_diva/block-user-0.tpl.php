<?php 
global $user;
if (!$user->uid) {
	print $block->content;
}	
else{
	profile_load_profile($user);
?>
<div id="logged_left_col">
	<div id="welcomeback_lbl"><img alt="Welcome back" src="<?echo C_IMAGE_PATH?>label/welcomeback_lbl.jpg"></div>
	<div id="username_lbl"><?php print $user->name?>!</div>
	<div style="width:160px;float:left">
		<?php if (recipe_utils::getUserRole() == C_PREMIUM_USER){?>
		<img alt="Premium User" src="<?echo C_IMAGE_PATH?>icon/reg_comform_pre_icon.gif">
		<?php }?>
		<a href="<?php echo C_BASE_PATH."mydiva/profile"?>">
			<img alt="Your account" src="<?echo C_IMAGE_PATH?>label/your_account_lbl.jpg"></a>
		<img  alt="" src="<?echo C_IMAGE_PATH?>label/line_lbl.gif">
		<a href="<?php echo C_BASE_PATH."logout"?>"><img alt="Logout" src="<?echo C_IMAGE_PATH?>label/logout_lbl.jpg"></a>		
	</div>
</div>
<div id="logged_right_col">
	<a href="<?php echo C_BASE_PATH.'user/'.$user->name?>">
		<?php if(file_exists($user->picture)){?>
			<img alt="<?php print $user->name?>" src="<?php print C_BASE_PATH.$user->picture.'" '.recipe_utils::getImageWidthHeight($user->picture,60,60) ?> />
		<?php }else{?>	
			<img alt="<?echo $user->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" width="60px" src="<?echo C_IMAGE_PATH?>photo/<?echo $user->profile_gender=="0"? "male":"female"?>.gif"></a>
		<?php }?>
	</a>
</div>	

<?php if (recipe_utils::getUserRole() == C_ADMIN_USER){?>
	<?php if (arg(0) != "adminrecipe"){?>
<div id="joinnow">
	<div id="siteadmin_lbl">
		<a href="<?php echo C_BASE_PATH."adminrecipe/news"?>">Site Admin</a>
	</div>
</div>	
	<?php }else{?>
	<div id="joinnow1">&nbsp;</div>
	<?php }?>		
<?php }else{?>
<div id="joinnow1">&nbsp;</div>
<?php }
$num_msg = recipe_db::count_user_message($user->uid,1);
$num_req = recipe_db::count_user_friend($user->uid,0);
$href_msg = C_BASE_PATH.'mydiva/' .($num_msg == 0?"":"new").'messages';
$href_fr = C_BASE_PATH.'mydiva/friends' .($num_req == 0?"":"request");
?>
<div style="width:160px;float:left">
	<div class="div_network_ico_m"><a href="<?echo $href_msg?>"><img alt="New message" src="<?echo C_IMAGE_PATH?>icon/ico_<?echo $num_msg == 0?"no_":""?>new_message.gif"></a></div>	
	<div class="div_network_ico_f"><a href="<?echo $href_fr?>"><img alt="Request friend" src="<?echo C_IMAGE_PATH?>icon/ico_<?echo $num_req == 0?"no_":""?>request_friend.gif"></a></div>	
	<br>
	
	<div class="div_network_lbl"><a href="<?echo $href_msg?>"><img alt="Messages" src="<?echo C_IMAGE_PATH?>label/lbl_message.png"></a></div>
	<div class="div_network_num_m">
	<?php if($num_msg > 0){?>	
	(<span class="div_network_num_lbl"><?php echo $num_msg?></span>)	
	<?php }else{print "&nbsp;";}?>		
	</div>	
	
	<div class="div_network_lbl"><a href="<?echo $href_fr?>"><img alt="Friends" src="<?echo C_IMAGE_PATH?>label/lbl_friends.png"></a></div>
	<div class="div_network_num">
	<?php if($num_req > 0){?>
	(<span class="div_network_num_lbl"><?php echo $num_req?></span>)
	<?php }else{print "&nbsp;";}?>		
	</div>
</div>          
<?php }?>

<?php //if(recipe_utils::getUserRole() == C_REGISTER_USER){?>
<!--<div style="width:160px;float:left">
	<a href="<?php echo C_BASE_PATH."become_premium"?>">
		<img alt="Become premium user" src="<?echo C_IMAGE_PATH?>label/become_premium_lbl.jpg">
	</a>
</div>-->
<?php //}?>

