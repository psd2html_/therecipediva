<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php print $head_title ?></title>
    <?php print $styles; ?>
    <?php print $scripts ?>
    <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>admin.js"></script>
    <?php if (!in_array("add", arg()) && !in_array("edit", arg()) && !in_array("import", arg())) {?>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print C_BASE_PATH.C_CSS_PATH ?>jquery-ui.css" />
    <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>jquery.min.js"></script>
    <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>jquery-ui.min.js"></script>
    <?php }else{?>
    <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>jquery.filestyle.js"></script>
        <?if(arg(1)=="news"){?>
            <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>jquery.form.js"></script>
        <?php }?>
    <?php }?>

    <?if(arg(1)=="promotion"){?>
        <link type="text/css" rel="stylesheet" media="all" href="<?php print C_BASE_PATH.C_CSS_PATH ?>jquery-ui.css" />
        <link type="text/css" rel="stylesheet" media="all" href="<?php print C_BASE_PATH.C_CSS_PATH ?>jquery.ui.theme.css" />
        <link type="text/css" rel="stylesheet" media="all" href="<?php print C_BASE_PATH.C_CSS_PATH ?>jquery.ui.datepicker.css" />
        <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>jquery.ui.core.js"></script>
        <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>jquery.ui.datepicker.js"></script>

    <?php }?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php print GOOGLE_ANALYTICS_ID?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
    <div id="main">
        <!-- START HEADER -->
        <div id="header">
            <div id="logo"><a class="logo" href="<?php print C_BASE_PATH?>"></a></div>

            <!-- START BANNER HEADER -->
            <div id="banner_top">
            <?php print $header_left ?>&nbsp;
            </div>
            <!-- END BANNER HEADER -->

            <div id="login_form">
                <?php print $header_right ?>
            </div>

            <div id="admin_menu">
                <div id="admin_menu_contain">
                        <ul id="admin-menu">
                            <?php print build_admin_menu_bar()?>
                        </ul>
                    </div>
                    <div id="admin_sub_menu_contain">
                    <div id="admin_sub_menu">
                        <div id="home_menu">
                               <span id="menu_span"><a id="news" href="<?php print C_BASE_PATH."adminrecipe/news"?>">News</a> | <a id="advertise" href="<?php print  C_BASE_PATH."adminrecipe/advertise"?>">Advertising</a>
                                | <a id="entertext" href="<?php print C_BASE_PATH."adminrecipe/entertext"?>">Admin enter text</a> | <a id="cook_of_week" href="<?php print C_BASE_PATH."adminrecipe/cookofweek"?>">Cook of week</a>
                                 | <a id="maincontent" href="<?php print C_BASE_PATH."adminrecipe/maincontent"?>">Main content</a> | <a id="promotion" href="<?php print C_BASE_PATH."adminrecipe/promotion"?>">Promotion</a>
                                 | <a id="livefeed" href="<?php print C_BASE_PATH."adminrecipe/livefeed"?>">Live feed</a></span>
                        </div>
                        <div id="recipe_menu">
                               <span id="menu_span"><a id="recipes" href="<?php print C_BASE_PATH."adminrecipe/recipes"?>">Recipes</a> | <a id="category" href="<?php print C_BASE_PATH."adminrecipe/category"?>">Categories</a> | <a id="units" href="<?php print C_BASE_PATH."adminrecipe/units"?>">Units</a> | <a href="<?php print C_BASE_PATH."adminrecipe/aisles"?>">Aisles</a></span>
                        </div>
                        <div id="divagossip_menu">
                               <span id="menu_span"><a id="divagossip" href="<?php print C_BASE_PATH."adminrecipe/divagossip"?>">Forums</a> | <a id="topics" href="<?php print C_BASE_PATH."adminrecipe/divagossip/topics"?>">Topics</a></span>
                          </div>
                        <div id="divatips_menu">
                               <span id="menu_span"></span>
                        </div>
                        <div id="mydiva_menu">
                               <span id="menu_span"><a id="user" href="<?php print C_BASE_PATH."adminrecipe/user"?>">User</a> | <a id="registrypromotion" href="<?php print  C_BASE_PATH."adminrecipe/registrypromotion"?>">Promotion Registration</a></span>
                        </div>
                        <div id="others_menu">
                               <span id="menu_span"><a id="staticpage" href="<?php print C_BASE_PATH."adminrecipe/staticpage"?>">Static page</a> | <a id="import" href="<?php print C_BASE_PATH."adminrecipe/import/recipes"?>">Import recipes</a>
                               | <a id="newsletter" href="<?php print C_BASE_PATH."adminrecipe/import/newsletter"?>">Send Newsletter</a>
                                | <a id="sections" href="<?php print C_BASE_PATH."adminrecipe/feedback/sections"?>">Feedback sections</a> | <a id="questions" href="<?php print C_BASE_PATH."adminrecipe/feedback/questions"?>">Feedback questions</a> | <a id="userrate" href="<?php print C_BASE_PATH."adminrecipe/feedback/user/rate"?>"> Feedback rate by user</a></span>
                        </div>
                      </div>
               </div>
            </div>

        </div>
        <!-- END HEADER -->

        <!-- START BODY -->
        <div id="body">
            <div id="admin_container">
                <div id="admin_border">
                    <div id="admin_border_t">
                        <div id="admin_border_contain">
                            <div id="admin_border_content">

                                <?php
                                if (in_array("add", arg()) || in_array("edit", arg()) || in_array("import", arg())) {
                                    if ($show_messages && $messages){print $messages;}
                                ?>
                                <?php
                                }
                                print $content;
                                ?>
                              </div>
                          </div>
                      </div>
                </div>
            </div>
          </div>
        <!-- END BODY -->

        <!-- START FOOTER -->
        <div id="footer">
            <div id="footer_content">
                <a href="<?echo C_BASE_PATH."aboutus"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_aboutus.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."contactus"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_contactus.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."advertising"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_advertising.gif"  /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."privacy"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_privacy.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."termofuse"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_termsofuse.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_slash.gif" />
                <img src="<?echo C_IMAGE_PATH?>footer/footer_copyright.gif" />

            </div>
        </div>
        <!-- END FOOTER -->
    </div>
    <blockquote>&nbsp;</blockquote>
    <div id="dialogInfo" style="display:none" title="">
    </div>
    <div id="dialogConfirm" style="display:none" title="Are you sure you want to delete?">
    </div>
<?php
    $sub_menu = arg(1);
    if(arg(2) == "topics"){
        $sub_menu = arg(2);
    }

    if(arg(1) == "feedback"){
        $sub_menu = arg(2);
        if(arg(2) == "user"){
            $sub_menu = "userrate";
        }
    }

    if(arg(1) == "import"){
        $sub_menu = arg(1);
        if(arg(2) == "newsletter"){
            $sub_menu = "newsletter";
        }
    }

    if(arg(1) == "news" || arg(1) == "entertext" || arg(1) == "advertise" || arg(1) == "cookofweek" || arg(1) ==  "maincontent" || arg(1) ==  "promotion" || arg(1) ==  "promotion" || arg(1) ==  "livefeed"){
        $menu="home_menu";
        if(arg(1) == "cookofweek"){
            $sub_menu = "cook_of_week";
        }
    }
    elseif(arg(1) == "recipes" || arg(1) == "category" || arg(1) == "units" || arg(1) == "aisles"){
        $menu="recipe_menu";
    }
    elseif(arg(1) == "shoprecipes"){

    }
    elseif(arg(1) == "divagossip"){
        $menu="divagossip_menu";
    }
    elseif(arg(1) == "divatips"){
        $menu="divatips_menu";
    }
    elseif(arg(1) == "user" || arg(1) ==  "registrypromotion"){
        $menu="mydiva_menu";
    }
    else{
        $menu="others_menu";
    }
?>
<script>
    $("div[class^='messages error']").insertAfter($("div[class=registration_p_title]"));
    $(document).ready(function(){
        $("#<?php print $menu?>").css("visibility","visible");
        $("#<?php print $sub_menu?>").css("color","#FC0");
        if($("div[class^='messages error']").length > 0){
            if($("div[class^='messages error'] > ul").length == 0){
                $("div[class^='messages error']").html("<ul><li>"+ $("div[class^='messages error']").html() + "</li></ul>");
            }
        }
        <?php if (in_array("add", arg()) || in_array("edit", arg()) || in_array("import", arg())) {?>
        $("input[type=file]").filestyle({
             image: "<?php print C_IMAGE_PATH."button/browse_btn.gif"?>",
             imageheight : 25,
             imagewidth : 75,
             width : 170
        });

        <?if(arg(1) == "advertise" || arg(1) == "news" || arg(1) == "entertext" || arg(1) == "maincontent" || arg(1) == "staticpage" ){
            print '$(":text").addClass("adminrecipe_input");
                   $("select").addClass("adminrecipe_select");
                   $("input[class^=file]").addClass("adminrecipe_file");';
        }?>

        <?php }?>

        <?php if (in_array("edit", arg())) {?>
        // hidden input image field
        <?php if(arg(1) == "advertise" && arg(2)){
                    $node = node_load(arg(2));
                    if(isset($node->field_advertise_image[0][fid]) && $node->field_advertise_image[0][fid] > 0){?>
                        var divImagePreview = document.getElementById("imagefield-preview");
                        if (divImagePreview != undefined) {
                            onload_image_upload();
                        }
        <?php		} }?>

        <?php if(arg(1) == "news" && arg(2)){
                    $node = node_load(arg(2));
                    if(isset($node->field_news_image[0][fid]) && $node->field_news_image[0][fid] > 0){?>
                        var divImagePreview = document.getElementById("imagefield-preview");
                        if (divImagePreview != undefined) {
                            onload_image_upload();
                        }
        <?php		} }?>

        <?php if(arg(1) == "entertext" && arg(2)){
                    $node = node_load(arg(2));
                    if(isset($node->field_upload_image[0][fid]) && $node->field_upload_image[0][fid] > 0){?>
                        var divImagePreview = document.getElementById("imagefield-preview");
                        if (divImagePreview != undefined) {
                            onload_image_upload();
                        }
        <?php		} }?>

        <?php if(arg(1) == "maincontent" && arg(2)){
                    $node = node_load(arg(2));
                    if(isset($node->field_maincontent_image[0][fid]) && $node->field_maincontent_image[0][fid] > 0){?>
                        var divImagePreview = document.getElementById("imagefield-preview");
                        if (divImagePreview != undefined) {
                            onload_image_upload();
                        }
        <?php		} }?>

        <?php }?>
    });

</script>
</body>
</html>
