<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php print $head_title ?></title>
	<?php print $scripts ?>
	<?php print $styles; ?>
	<?php if(arg(1)=="cow"){?>		
		<?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.min.js"></script>';?>
		<?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery-ui.min.js"></script>';?>
		<link type="text/css" rel="stylesheet" media="all" href="<?php print C_BASE_PATH.C_CSS_PATH .'jquery-ui.css'?>"/>
	<?php }?>
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?echo C_CSS_PATH?>ie.css" media="screen"/><![endif]-->
	
</head>
<body id="body_popup">
	<div id="email_popup">
		<div id="body_header" style="height: 160px;">
			<a class="logo_small" href="<?php print C_BASE_PATH?>"></a>
	    </div>
	    <div>
	    	<div id="popup_left_col">
	    		<?php if(arg(1)=="cow"){?>
	    		 <div>
				  	<div id="mail_titles">
				  		<div id="mail_title_col" style="font-size:30px;">Want to be Cook of the Week?</div>
				  		<div id="mail_content_col" style="font-size:11.5px;">
 							Each week The Recipe Diva will choose a cook from our community to be featured as the Cook-of-the-Week.  If you believe that you have put extra work in the kitchen this week, have had any great successes, learned something new, or you just deserve it, please let us know.
 						</div>
 					</div>
 				 </div>
				 <div>
			  		<div id="div_error_message" class="message error" style="display:none;">
			            <ul><span id="message_recipe_error"></span></ul>
			        </div>
				  </div>
			     <form accept-charset="UTF-8" method="post" id="cooking_of_week_form" name="cooking_of_week_form">
				 <div>
				  	<div id="mail_titles">
				  		<div id="mail_title_col">Why should you be Cook of the Week?<span id="require">*</span></div>
				  		<div id="mail_content_col">
 							<textarea cols="60" rows="5" name="txtContent" id="txtContent"><?php print $content;?></textarea>
 						</div>
 					</div>
 				</div>
 				<div>
 					<div id="mail_titles">
 						<div id="mail_title_col">&nbsp;</div>
 						<input type="button" class="cookofweek_submit" onclick="cookofweek();"/>
		            </div>
				</div>
				</form>
				<script>
					<?php if(sizeof($_POST)>0){?>
						$(document).ready(function(){
							showInfoMessage_1("<?php print COOKOFWEEK_SEND_SUCCESS ?>");
							
						});
					<?php }?>
					
					function cookofweek(){
						var objContent = document.getElementById("txtContent");
				    	
				    	if (trim(objContent.value) == ""){
							errorMessage = "<li>" + "<?php print t(ERR_MSG_REQUIRED, array('@field_name' => 'Want to be Cook of the Week'));?>" + "</li>";
							$("#message_recipe_error").html(errorMessage);  
							objContent.focus();
							var objError = document.getElementById("div_error_message");
							objError.style.display = "";
						}else{
							document.forms["cooking_of_week_form"].submit();	
						}
					}
				</script>
				<?php }
				else{
					print $content;	
				}?>
			</div>    
	    </div>    
	</div>
	<div id="dialogInfo" title="">
	</div>
	
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php print GOOGLE_ANALYTICS_ID?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
