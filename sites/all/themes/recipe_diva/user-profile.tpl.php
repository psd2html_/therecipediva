<?php
    global $user;

    //$uid = arg(1);
    $arg_url = explode("/", mysql_real_escape_string($_SERVER["REQUEST_URI"]));
    $uid = str_replace('%20',' ',$arg_url[2]);

    $exits_user = user_load(array('name' => $uid));
    if (isset($exits_user->uid)) {
        $uid = $exits_user->uid;
    }

    $view_user = user_load($uid);

    profile_load_profile($view_user);
    $hasPer = recipe_utils::getUserRole() == C_ADMIN_USER ? true : $user->uid == $view_user->uid? true: false;
    $is_friend = recipe_db::check_user_friend($view_user->uid,$user->uid);
    $profileSetting = false;
    if($view_user->profile_profile_settings == "0" || $is_friend == "1"){
        $profileSetting = true;
    }

    $arr_level = array(-1 => "Not rated",0 => "Beginner",1 => "Intermediate",2 => "Expert",3 => "Professional");


    function openlink($link="") {
        $link = trim($link);

        if(substr($link, 0, 4) != "http") {
            $link = "http://" . $link;
        }

        return $link;
    }
 ?>
<div id="news_p">
    <div id="news_p_t_contain">
        <div>
        </div>
    </div>
    <div id="news_p_b">
        <div id="news_border">
            <div id="pre_membership_content">
                <div class="pre_membership_p_title_1">
                    <?php print $view_user->name?></div>
                <div id="div_space">
                    <img width="1" height="20" src="<?echo C_IMAGE_PATH?>space.gif">
                </div>

                <div id="premium_membership_content_contain">
                      <div id="public_profile_l_col">
                        <div id="public_avatar_contain">
                            <?php if(file_exists($view_user->picture) && ($hasPer || $profileSetting)){
                                $file_path = $view_user->picture;//recipe_utils::get_resize_image_path($view_user->picture,189);
                            ?>
                                <img alt="<?php print $view_user->name?>" id="img_user" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageWidthHeight($file_path,189,230) ?> />
                            <?php }else{?>
                                <img alt="<?echo $view_user->profile_gender=="0"? "Male Foodie":"Female Foodie"?>" id="img_user" width="138" src="<?echo C_IMAGE_PATH?>photo/<?echo $view_user->profile_gender=="0"? "male":"female"?>138.gif">
                            <?php }?>
                        </div>
                    </div>

                    <div id="public_profile_r_col">
                        <div id="public_profile_r_content">
                            <?php if(!$profileSetting && !$hasPer){?>
                                <div id="public_profile_r_content_line">
                                    <div class="divatop_title">This user's profile is not public.  In order to view all of <?echo $view_user->profile_gender=="0"? "his":"her"?> details, you must request to follow <?echo $view_user->profile_gender=="0"? "him":"her"?></div>
                                </div>
                                <div id="public_profile_r_content_line01">
                                &nbsp;
                                </div>
                            <?php }else{?>
                            <div id="public_profile_r_content_line">
                                <div class="admin_require_field01"><img alt="Username" src="<?echo C_IMAGE_PATH?>label/username_lbl.gif"></div>
                                <div class="admin_require_field02"><?php print $view_user->name?></div>
                            </div>
                            <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php if($hasPer){?>
                            <div id="public_profile_r_content_line">
                               <div class="admin_require_field01"><img alt="Email Address" src="<?echo C_IMAGE_PATH?>label/email_address_lbl.gif"></div>
                               <div class="admin_require_field02"><?php print $view_user->mail?></div>
                             </div>
                             <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>
                            <?php if($view_user->profile_full_name){?>
                           <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Full Name" src="<?echo C_IMAGE_PATH?>label/full_name_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print $view_user->profile_full_name?></div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>
                            <?php if($hasPer){?>
                            <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Gender" src="<?echo C_IMAGE_PATH?>label/gender_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print $view_user->profile_gender == "0"?"Male":"Female"?></div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php if($view_user->profile_birth_date){?>
                           <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Birthdate" src="<?echo C_IMAGE_PATH?>label/birtdate_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print $view_user->profile_birth_date?></div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>
                            <?php }?>

                            <?php if($view_user->profile_cooking_level != -1){?>
                            <div id="public_profile_r_content_line">
                                <div class="admin_require_field01"><img alt="Cooking level" src="<?echo C_IMAGE_PATH?>label/cooking_level01_lbl.gif"></div>
                                <div class="admin_require_field02"><?php print $arr_level[$view_user->profile_cooking_level]?></div>
                            </div>
                            <div id="public_profile_r_content_line01">&nbsp;
                            </div>
                            <?php }?>

                            <?php if($view_user->profile_profession){?>
                            <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Profession" src="<?echo C_IMAGE_PATH?>label/profession_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print $view_user->profile_profession?></div>
                               </div>
                            <div id="public_profile_r_content_line01">&nbsp;
                            </div>
                            <?php }?>
                            <?php if($hasPer){?>
                               <?php if($view_user->profile_postal_code){?>
                           <div id="public_profile_r_content_line" style="display:none">
                                   <div class="admin_require_field01"><img alt="Postal code" src="<?echo C_IMAGE_PATH?>label/postal_code_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print $view_user->profile_postal_code?></div>
                                 </div>
                              <div id="public_profile_r_content_line01" style="display:none">
                            &nbsp;
                            </div>
                            <?php }?>
                            <?php }?>

                             <?php if($view_user->profile_twitter_link){?>
                           <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Twitter Link" style="padding-top:2px;" src="<?echo C_IMAGE_PATH?>label/twitter_link_lbl.gif"></div>
                                   <div class="admin_require_field02"><a rel="nofollow" href="<?php print openlink($view_user->profile_twitter_link)?>" target="_blank"><?php print $view_user->profile_twitter_link?></a></div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>

                             <?php if($view_user->profile_fb_link){?>
                           <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Facebook Link" style="padding-top:3px;" src="<?echo C_IMAGE_PATH?>label/fb_link_lbl.gif"></div>
                                   <div class="admin_require_field02"><a rel="nofollow" href="<?php print openlink($view_user->profile_fb_link)?>" target="_blank"><?php print $view_user->profile_fb_link?></a></div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>

                            <?php }?>

                            <?php if($view_user->profile_blog_link){?>
                           <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="Blog Link" style="padding-top:3px;" src="<?echo C_IMAGE_PATH?>label/blog_link_lbl.gif"></div>
                                   <div class="admin_require_field02"><a rel="nofollow" href="<?php print openlink($view_user->profile_blog_link)?>" target="_blank"><?php print $view_user->profile_blog_link?></a></div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>

                            <?php
                                $geography = "";
                                $country = profile_location_get_country($view_user->profile_country);
                                if($hasPer){
                                    //$geography .= $view_user->profile_address != "" ? ", ".$view_user->profile_address:"";
                                }else{
                                    $geography = "";
                                }
                                $geography .= $view_user->profile_city != "" ? ", ".$view_user->profile_city:"";
                                $state = "";
                                if ($view_user->profile_state)
                                {
                                    $state = profile_location_get_state($view_user->profile_country,$view_user->profile_state);
                                    $geography .= $state != "" ? ", ".$state:"";
                                }

                                $geography .= $country != "" ? ", ".$country:"";

                            if($geography != ""){
                                $geography = substr($geography,2);

                            ?>


                            <div id="public_profile_r_content_line" style="clear: both;">
                                   <div class="admin_require_field01"><img alt="Geography" src="<?echo C_IMAGE_PATH?>label/geography_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print $geography?></a></div>
                            </div>
                            <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>
                            <?php if($view_user->profile_about_me){?>
                            <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01"><img alt="About me" src="<?echo C_IMAGE_PATH?>label/about_me_lbl.gif"></div>
                                   <div class="admin_require_field02"><?php print nl2br($view_user->profile_about_me)?></a></div>
                                 </div>
                            <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>
                            <?php }?>

                            <?php }?>


                            <?php if($user->uid != $view_user->uid){
                                /*drupal_add_css(C_CSS_PATH.'jquery-ui.css');
                                  drupal_add_js(C_SCRIPT_PATH.'jquery.min.js');
                                  drupal_add_js(C_SCRIPT_PATH.'jquery-ui.min.js');*/
                            ?>

                            <div id="public_profile_r_content_line">
                                   <div class="admin_require_field01">&nbsp;</div>
                                   <div class="admin_require_field02" style="margin-top: 10px;">

                            <div class="admin_require_field">
                                <img alt="Send message" onclick="openMessage(<?php print $view_user->uid?>)" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/btn_send_message.gif">
                            </div>
                            <div class="admin_require_field">
                            <?php
                            //$is_friend = recipe_db::check_user_friend($view_user->uid, $user->uid);
                            if($is_friend == ""){?>
                                   <img alt="Request as friend" id="img_req_friend" onclick="openRequestFriend(<?php print $user->uid?>,<?php print $view_user->uid?>,'<?php print $view_user->name?>')" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/btn_request_as_friend.gif">
                               <?php }
                               else if($is_friend == "0"){?>
                                   <img alt="Remove pending" id="img_req_friend" onclick="openRemoveFriend(<?php print $user->uid?>,<?php print $view_user->uid?>,0)" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/btn_remove_pending.gif">
                               <?php }
                               else if($is_friend == "1"){?>
                                   <img alt="Remove friend" id="img_req_friend" onclick="openRemoveFriend(<?php print $user->uid?>,<?php print $view_user->uid?>,1)" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/btn_remove_friend.gif">
                               <?php }
                               else if($is_friend == "2"){?>
                                   <img alt="Confirm Foodie request" id="img_req_friend" onclick="acceptrequest(<?php print $user->uid?>,<?php print $view_user->uid?>)" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/btn_confirm_foodie_request.gif">
                               <?php }?>

                               </div>

                               </div>
                                 </div>
                              <div id="public_profile_r_content_line01">
                            &nbsp;
                            </div>

                               <?php	include(".".C_BASE_PATH.'sites/all/themes/recipe_diva/diva_network_message.php');
                                    include(".".C_BASE_PATH.'sites/all/themes/recipe_diva/diva_network_add_friend.php');
                               }
                               ?>
                      </div>
                    </div>
                </div>
                <?php
                    if($view_user->profile_privacy_settings == "0" || $is_friend == "1" || $hasPer){
                    $user_friend = load_user_friend_list($view_user->uid);
                    ?>
                    <div id="upload_recipe">
                        <div style="float: left; position: relative;">
                            <img alt="Friends" src="<?echo C_IMAGE_PATH?>label/lbl_icon_friends.gif">
                        </div>
                        <div id="upload_recipe_b">
                            <div id="upload_recipe_content">
                                 <div id="upload_recipe_content_inner" style="padding-bottom:20px;">
                                         <?php
                                         $i = 0;
                                         foreach ($user_friend as $objUser){
                                             if($i==NUMBERS_UPLOADED_RECIPE){
                                                 break;
                                             }
                                             $i ++;
                                         ?>
                                         <div id="img_container_upload_recipe">
                                            <div style="min-height: 80px; padding-top: 6px;">
                                                <?php $file_path = $objUser["picture"];
                                                if(file_exists($file_path)){ ?>
                                                    <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser["user_name"]?>"><img alt="<?php print $objUser["user_name"]?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,67) ?> /></a>
                                                <?php }else{?>
                                                    <a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser["user_name"]?>"><img alt="<?echo $objUser["profile_gender"]=="0"? "Male Foodie":"Female Foodie"?>" width="60" src="<?echo C_IMAGE_PATH?>photo/<?echo $objUser["profile_gender"]=="0"? "male":"female"?>.gif"></a>
                                                <?php }?>
                                             </div>
                                               <span class="divatop_title"><a rel="nofollow" href="<?php print C_BASE_PATH."user/".$objUser["user_name"]?>"><?php print $objUser['user_name']?></a></span>
                                        </div>
                                        <?php }?>
                                        <span id="span_use_friend">
                                        </span>
                                   </div>
                                   <div class="img_seeall" <?php print sizeof($user_friend)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                                     <div onclick="expandDiv('img_friend','span_use_friend','<?echo C_IMAGE_PATH?>label/see_all_friends.gif','<?echo C_IMAGE_PATH?>label/hide_all_friends.gif')" class="see_all_txt"><img alt="See all friends" id="img_friend" src="<?echo C_IMAGE_PATH?>label/see_all_friends.gif"></div>
                                     <div id="see_all_btn">
                                         <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_friend','span_user_friend','<?echo C_IMAGE_PATH?>label/see_all_friends.gif','<?echo C_IMAGE_PATH?>label/hide_all_friends.gif')">
                                    </div>
                                 </div>
                            </div>
                          </div>

                    <div id="dot_bg2">
                        <img width="1" height="14" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                </div>
                <?php }?>

                <?php
                if($view_user->profile_recipe_settings == "0" || $is_friend == "1" || $hasPer){
                $uploaded_recipes = load_user_recipes_list($view_user->uid);
                ?>
                <div id="upload_recipe">
                    <div style="float: left; position: relative;">
                        <img alt="Upload recipe" src="<?echo C_IMAGE_PATH?>label/lbl_upload_recipe.gif">
                    </div>
                    <div id="upload_recipe_b">
                        <div id="upload_recipe_content">
                             <div id="upload_recipe_content_inner">
                                     <?php
                                     $i = 0;
                                     foreach ($uploaded_recipes as $objRecipe){
                                         if($i==NUMBERS_UPLOADED_RECIPE){
                                             break;
                                         }
                                         $i ++;
                                     ?>
                                     <div id="img_container_upload_recipe">
                                        <div style="min-height: 55px; padding-top: 6px;">
                                            <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                                            if(file_exists($file_path)){ ?>
                                                <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                                            <?php }else{?>
                                                <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                                            <?php }?>
                                         </div>
                                           <span class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></span>
                                    </div>
                                    <?php }?>
                                    <span id="span_uploaded_recipe">
                                    </span>
                               </div>
                               <div class="img_seeall" <?php print sizeof($uploaded_recipes)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                                 <div onclick="expandDiv('img_uploaded','span_uploaded_recipe','<?echo C_IMAGE_PATH?>label/see_all_uploaded.gif','<?echo C_IMAGE_PATH?>label/hide_all_uploaded.gif')" class="see_all_txt"><img alt="See all uploaded recipes" id="img_uploaded" src="<?echo C_IMAGE_PATH?>label/see_all_uploaded.gif"></div>
                                 <div id="see_all_btn">
                                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_uploaded','span_uploaded_recipe','<?echo C_IMAGE_PATH?>label/see_all_uploaded.gif','<?echo C_IMAGE_PATH?>label/hide_all_uploaded.gif')">
                                </div>
                             </div>
                        </div>
                      </div>

                    <div id="dot_bg2">
                        <img width="1" height="14" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                  </div>
                <?php $reviewed_recipes = load_user_recipes_list($view_user->uid,0,0);
                ?>
                <div id="upload_recipe">
                    <div style="float: left; position: relative;">
                        <img alt="Review recipe" src="<?echo C_IMAGE_PATH?>label/lbl_review_recipe.gif">
                    </div>
                    <div id="upload_recipe_b">
                        <div id="upload_recipe_content">
                             <div id="upload_recipe_content_inner">
                                     <?php
                                     $i = 0;
                                     foreach ($reviewed_recipes as $objRecipe){
                                         if($i==NUMBERS_UPLOADED_RECIPE){
                                             break;
                                         }
                                         $i ++;
                                     ?>
                                     <div id="img_container_upload_recipe">
                                        <div style="min-height: 55px; padding-top: 6px;">
                                            <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                                            if(file_exists($file_path)){ ?>
                                                <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                                            <?php }else{?>
                                                <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                                            <?php }?>
                                         </div>
                                           <span class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></span>
                                    </div>
                                    <?php }?>
                                    <span id="span_reviewed_recipe">
                                    </span>
                               </div>
                               <div class="img_seeall" <?php print sizeof($reviewed_recipes)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                                 <div onclick="expandDiv('img_reviewed','span_reviewed_recipe','<?echo C_IMAGE_PATH?>label/see_all_reviewed.gif','<?echo C_IMAGE_PATH?>label/hide_all_reviewed.gif')" class="see_all_txt"><img alt="See all reviewed recipes" id="img_reviewed" src="<?echo C_IMAGE_PATH?>label/see_all_reviewed.gif"></div>
                                 <div id="see_all_btn">
                                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_reviewed','span_reviewed_recipe','<?echo C_IMAGE_PATH?>label/see_all_reviewed.gif','<?echo C_IMAGE_PATH?>label/hide_all_reviewed.gif')">
                                </div>
                             </div>
                        </div>
                      </div>

                    <div id="dot_bg2">
                        <img width="1" height="14" src="<?echo C_IMAGE_PATH?>space.gif">
                    </div>
                </div>
                <?php $saved_recipes = load_user_recipes_list($view_user->uid,0,2);
                ?>
                <div id="upload_recipe">
                    <div style="float: left; position: relative;">
                        <img alt="Saved recipes" src="<?echo C_IMAGE_PATH?>label/lbl_saved_recipe.gif">
                    </div>
                    <div id="upload_recipe_b">
                        <div id="upload_recipe_content">
                             <div id="upload_recipe_content_inner">
                                     <?php
                                     $i = 0;
                                     foreach ($saved_recipes as $objRecipe){
                                         if($i==NUMBERS_UPLOADED_RECIPE){
                                             break;
                                         }
                                         $i ++;
                                     ?>
                                     <div id="img_container_upload_recipe">
                                        <div style="min-height: 55px; padding-top: 6px;">
                                            <?php $file_path = recipe_utils::get_thumbs_image_path($objRecipe['image']);
                                            if(file_exists($file_path)){ ?>
                                                <img alt="<?php print $objRecipe['recipe_title']?>" src="<?php print C_BASE_PATH.$file_path.'" '.recipe_utils::getImageHeightWidth($file_path,60,40) ?> />
                                            <?php }else{?>
                                                <img alt="Default recipe" src="<?php print C_IMAGE_PATH?>recipe_default_image.gif" />
                                            <?php }?>
                                         </div>
                                           <span class="divatop_title"><a href="<?php print $objRecipe['recipe_url']?>"><?php print $objRecipe['recipe_title']?></a></span>
                                    </div>
                                    <?php }?>
                                    <span id="span_saved_recipe">
                                    </span>
                               </div>
                               <div class="img_seeall" <?php print sizeof($saved_recipes)>NUMBERS_UPLOADED_RECIPE?"":"style='display:none'" ?>>
                                 <div onclick="expandDiv('img_saved','span_saved_recipe','<?echo C_IMAGE_PATH?>label/see_all_saved.gif','<?echo C_IMAGE_PATH?>label/hide_all_saved.gif')" class="see_all_txt"><img alt="See all saved recipes" id="img_saved" src="<?echo C_IMAGE_PATH?>label/see_all_saved.gif"></div>
                                 <div id="see_all_btn">
                                     <img alt="See all" style="cursor:pointer" src="<?echo C_IMAGE_PATH?>button/seeall_btn.gif" onclick="expandDiv('img_saved','span_saved_recipe','<?echo C_IMAGE_PATH?>label/see_all_saved.gif','<?echo C_IMAGE_PATH?>label/hide_all_saved.gif')">
                                </div>
                             </div>
                        </div>
                      </div>
                </div>
                <?php }?>

            </div>
        </div>
    </div>
</div>
<script>
    function expandDiv(img_id, div_id, img_path_add, img_path_subtract){
        if($.trim($("#"+div_id).html()) == ""){
            if(div_id == "span_use_friend"){
                var url = "<?php print C_BASE_PATH."getallfriend/".$view_user->uid."/1/"?>";
            }else{
                var url = "<?php print C_BASE_PATH."getallrecipeuploaded/".$view_user->uid."/1/"?>";
                if(img_id == "img_uploaded" ){
                    url += "1";
                }else if(img_id == "img_reviewed" ){
                    url += "0";
                }else{
                    url += "2";
                }
            }

            $.get(url, function(data){
                        $("#"+div_id).html(data);
                   },"text");
        }

        if($("#"+img_id).attr("src") == img_path_add){
            $("#"+img_id).attr("src",img_path_subtract);
            $("#"+div_id).show();
        }
        else{
            $("#"+img_id).attr("src",img_path_add);
            $("#"+div_id).hide();
        }

    }
    function openRemoveFriend(uid,fid,rev_type){
        var username = $("div[class=pre_membership_p_title_1]").html();
        var message = "<?print CONFIRM_MSG_REMOVE_FRIEND?>";
        if(rev_type == 0){
            var message = "<?print CONFIRM_MSG_REMOVE_PENDING?>";
        }

        $("#dialogConfirm").attr("title", message);
        $("#ui-dialog-title-dialogConfirm").text(message);
        $("#dialogConfirm").dialog(
            { modal: true },
            { resizable: false },
            { minHeight: 0 },
            { buttons:
                {
                    "Yes": function() {
                                var url = "<?php print C_BASE_PATH."removefriendrequest/"?>" + uid + "/" + fid;
                                $.get(url, function(data){
                                        var json = eval("(" + data + ")");
                                        if(json['status'] == "success"){
                                            $("#img_req_friend").attr("src","<?echo C_IMAGE_PATH?>button/btn_request_as_friend.gif");
                                            $("#img_req_friend").removeAttr('onclick');
                                            $("#img_req_friend").unbind("click");
                                            $("#img_req_friend").click(function(){openRequestFriend(uid,fid,username);});
                                        }
                                        else{

                                        }
                                   },"text");
                                $(this).dialog("close");
                            },
                    "No": function() {
                                $(this).dialog("close");
                            }
                }
            }
        );
    }
    function acceptrequest(uid,fid) {
        var url = "<?php print C_BASE_PATH."acceptfriendrequest/"?>" + uid + "/" + fid ;
        $.post(url, function(data){
                var json = eval("(" + data + ")");
                if(json['status'] == "success"){
                    $("#img_req_friend").attr("src","<?echo C_IMAGE_PATH?>button/btn_remove_friend.gif");
                    $("#img_req_friend").removeAttr('onclick');
                    $("#img_req_friend").unbind("click");
                    $("#img_req_friend").click(function(){openRemoveFriend(uid,fid,1);});
                }
           }, "text");

    }
</script>
