<!-- saved from url=(0022)http://internet.e-mail -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8,IE=7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php print $head ?>
    <?php print $styles; ?>
    <?php print $scripts ?>
</head>

<body>



<table align="center" width="720" height="142" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <!-- MENU START -->
              <map name="channels" id="channelMap">
                    <area shape="rect" alt="Home" target="_top" coords="30,5,190,130" href="http://therecipediva.com" id="dbArea" />
                    <area shape="rect" target="_top" coords="589,0,705,30" href="http://www.therecipediva.com/newsletter/august2011" id="dbArea" />
                    <area shape="rect" alt="Home" target="_top" coords="210,100,265,130" href="http://www.therecipediva.com/" id="dbArea" />
                    <area shape="rect" alt="Recipes" target="_top" coords="276,100,339,130" href="http://www.therecipediva.com/recipes" id="mzArea" />
                    <area shape="rect" alt="Diva Gossip" target="_top" coords="356,100,442,130" href="http://www.therecipediva.com/divagossip" id="piArea" />
                    <area shape="rect" alt="Diva Tips" target="_top" coords="459,100,527,130" href="http://www.therecipediva.com/divatips" id="asArea" />
                    <area shape="rect" alt="My Diva" target="_top" coords="542,100,607,130" href="http://www.therecipediva.com/mydiva/recipebox" id="euArea" />
                    <area shape="rect" alt="facebook" target="_top" coords="611,100,637,130" href="http://www.facebook.com/therecipediva" id="asArea" />
                    <area shape="rect" alt="Twitter" target="_top" coords="638,100,662,130" href="http://twitter.com/recipediva" id="euArea" />
              </map>

                <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/header.gif" border="0" usemap="#channels" id="menu_newsletter">
            <br style="clear:both;" />

                <!-- MENU END -->
            </td>
        </tr>
        <tr>
        <td width="15"></td>

        <td>
            <table width="690" cellpadding="0" cellspacing="0" style="background-color:#FFF">
            <tr>
              <td valign="top" width="464">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td><a href="https://www.facebook.com/GenerationRescue.OfficialPage?sk=app_201143516562748
"><img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/recipe_contest_03.png" border="0" /></a></td>
                        </tr>
                </table>

                <table width="454" cellpadding="0" cellspacing="0" style="margin-left:20px;">
<tr>
                            <td height="46" colspan="2">
                                <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/border/seasonal_menu.gif" border="0" />
                            </td>
                  </tr>
                        <tr style="background: url(http://therecipediva.com/sites/all/themes/recipe_diva/images/border/diva_bg.gif) top left repeat-y;">
                            <td>
                                <table style="margin-left:10px; margin-right:10px;">
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="208" class="TopRecipes_content_inner">
                                                <tr>
                                                    <td valign="top" width="60">
                                                        <img width="60" src="http://www.therecipediva.com/sites/default/files/imagefield_thumbs/bean%20salad_1.jpg">
                                                    </td>
                                                    <td valign="top" style="padding-left:5px;">
                                                        <a style="color: #DD0021;text-decoration:none;font-size: 11.5px;font-weight:bold;" href="http://www.therecipediva.com/recipe/Easy-Bean-Salad-24976">Easy Bean Salad</a> <br />
                                                        <span style="color: #552419;font-weight: normal; font-size:11.5px;">by</span> <a style="color: #DD0021;text-decoration:none; font-size:11.5px; font-weight:normal;" href="http://www.therecipediva.com/user/The%20Recipe%20Diva">The Recipe Diva</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="10">&nbsp;</td>
                                        <td valign="top" rowspan="2">
                                          <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/simple_salads_01.gif">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="208" class="TopRecipes_content_inner">
                                                <tr>
                                                    <td valign="top" width="60">
                                                        <img width="60" src="http://www.therecipediva.com/sites/default/files/imagefield_thumbs/salad.jpg">
                                                    </td>
                                                    <td valign="top" style="padding-left:5px;">
                                                        <a style="color: #DD0021;text-decoration:none;font-size: 11.5px;font-weight:bold;" href="http://www.therecipediva.com/recipe/Cucumber-Onion-and-Tomato-Salad-9416">Cucumber, Onion, and Tomato Salad</a> <br />
                                                        <span style="color: #552419;font-weight: normal; font-size:11.5px;">by</span> <a style="color: #DD0021;text-decoration:none; font-size:11.5px; font-weight:normal;" href="http://www.therecipediva.com/user/Jacqui">Jacqui</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="10">&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="208" class="TopRecipes_content_inner">
                                                <tr>
                                                    <td valign="top" width="60">
                                                        <img width="60" src="http://www.therecipediva.com/sites/default/files/imagefield_thumbs/zuccsalad.jpg">
                                                    </td>
                                                    <td valign="top" style="padding-left:5px;">
                                                        <a style="color: #DD0021;text-decoration:none;font-size: 11.5px;font-weight:bold;" href="http://www.therecipediva.com/recipe/Zucchini-Carpaccio-Salad-7897">Zucchini Carpaccio Salad</a> <br />
                                                        <span style="color: #552419;font-weight: normal; font-size:11.5px;">by</span> <a style="color: #DD0021;text-decoration:none; font-size:11.5px; font-weight:normal;" href="http://www.therecipediva.com/user/The%20Recipe%20Diva">The Recipe Diva</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="10">&nbsp;</td>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="208" class="TopRecipes_content_inner">
                                                <tr>
                                                    <td valign="top" width="60">
                                                        <img width="60" src="http://www.therecipediva.com/sites/default/files/imagefield_thumbs/tunapotatosalad_0.jpg">
                                                    </td>
                                                    <td valign="top" style="padding-left:5px;">
                                                        <a style="color: #DD0021;text-decoration:none;font-size: 11.5px;font-weight:bold;" href="http://www.therecipediva.com/recipe/Tuna-Parsley-and-Potato-Salad-6602">Tuna, Parsley, and Potato Salad</a> <br />
                                                        <span style="color: #552419;font-weight: normal; font-size:11.5px;">by</span> <a style="color: #DD0021;text-decoration:none; font-size:11.5px; font-weight:normal;" href="http://www.therecipediva.com/user/The%20Recipe%20Diva">The Recipe Diva</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="208" class="TopRecipes_content_inner">
                                                <tr>
                                                    <td valign="top" width="60">
                                                        <img width="60" src="http://www.therecipediva.com/sites/default/files/imagefield_thumbs/strawberrysalad.jpg">
                                                    </td>
                                                    <td valign="top" style="padding-left:5px;">
                                                        <a style="color: #DD0021;text-decoration:none;font-size: 11.5px;font-weight:bold;" href="http://www.therecipediva.com/recipe/Spinach-and-Strawberry-Salad-46629">Spinach and Strawberry Salad </a> <br />
                                                        <span style="color: #552419;font-weight: normal; font-size:11.5px;">by</span> <a style="color: #DD0021;text-decoration:none; font-size:11.5px; font-weight:normal;" href="http://www.therecipediva.com/user/ShannaSaul">ShannaSaul</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="10">&nbsp;</td>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" width="208" class="TopRecipes_content_inner">
                                                <tr>
                                                    <td valign="top" width="60">
                                                        <img width="60" src="http://www.therecipediva.com/sites/default/files/imagefield_thumbs/salad1.jpg">
                                                    </td>
                                                    <td valign="top" style="padding-left:5px;">
                                                        <a style="color: #DD0021;text-decoration:none;font-size: 11.5px;font-weight:bold;" href="http://www.therecipediva.com/recipe/Rocket-Peach-Pinenut-Salad-8087">Rocket, Peach, & Pinenut Salad</a> <br />
                                                        <span style="color: #552419;font-weight: normal; font-size:11.5px;">by</span> <a style="color: #DD0021;text-decoration:none; font-size:11.5px; font-weight:normal;" href="http://www.therecipediva.com/user/The%20Recipe%20Diva">The Recipe Diva</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/border/diva_bottom.gif" border="0" />
                            </td>
                        </tr>
                </table>

                     <table width="454" cellpadding="0" cellspacing="0" style="margin-top:10px; margin-left:10px;">
                        <tr>
                            <td colspan="2"><img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/diva_lime_light.png" border="0" usemap="#Map_Divalime" /></td>
                            <map name="Map_Divalime" id="Map_Divalime">
                              <area shape="rect" coords="16,40,460,181" href="http://www.jamieoliver.com/us/foundation/jamies-food-revolution/news-content/therecipediva-com-donates-546-to-the-foo" />
                            </map>
                        </tr>
                     </table>

                     <table width="454" cellpadding="0" cellspacing="0" style="background-color:#F1F2F5; margin:10px;">

                      <tr>
                            <td>
                                <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/diva_bites.png" border="0" usemap="#Map_Divabites" />
                                <map name="Map_Divabites" id="Map_Divabites">
                                  <area shape="rect" coords="20,18,171,50" href="https://www.facebook.com/therecipediva" />
                                </map>
                            </td>
                       </tr>
                     </table>

              </td>
              <td align="center" valign="top">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-bottom:10px;">
                                <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/notes_chicken.gif" border="0" />
                            </td>
                        </tr>
                    </table>

                <table width="200" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                              <img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/diva_sampling.png" border="0" usemap="#Map_DivaSampling" />
                            <map name="Map_DivaSampling" id="Map_DivaSampling">
                              <area shape="rect" coords="20,45,191,77" href=" http://twitter.com/#!/recipediva" />
                            </map>
                            </td>
                        </tr>

                </table>

                    <table width="200" cellpadding="0" cellspacing="0" style="background-color:#F5F5F5; margin-top:10px;">
                        <tr align="left">
                            <td><img src="http://therecipediva.com/sites/all/themes/recipe_diva/images/photo/diva_morsels.png" border="0" /></td>
                        </tr>
                    </table>
            </td>
              </tr>
          </table>
          </td>
            <td width="15">
            </td>
        </tr>

     </table>
</body>
</html>
