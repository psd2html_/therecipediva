<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php print $head_title ?></title>
    <?php print $styles; ?>
    <?php print $scripts ?>
    <script type="text/javascript" src="<?php print C_BASE_PATH.C_SCRIPT_PATH ?>admin.js"></script>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery.min.js"></script>';?>
    <?php print '<script type="text/javascript" src="'.C_BASE_PATH.C_SCRIPT_PATH.'jquery-ui.min.js"></script>';?>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print C_BASE_PATH.C_CSS_PATH .'jquery-ui.css'?>"/>

</head>
<body>
    <div id="main">
        <!-- START HEADER -->
        <div id="header">

            <div id="logo">
            <?php $site_url = C_SITE_URL.C_BASE_PATH;?>
            <a class="logo" href="<?php print $site_url?>" title="Quick, Easy, Healthy Recipes - Recipe Tips and Reviews - Free at TheRecipeDiva.com"></a>
            </div>
            <!-- START BANNER HEADER -->
            <div id="banner_top">
            <?php print $header_left ?>&nbsp;
            </div>
            <!-- END BANNER HEADER -->
            <div id="login_form">
                <?php print $header_right ?>
            </div>
            <!--
            <div id="admin_menu">
                <div id="admin_menu_contain">
                    <ul id="admin-menu">
                        <li class="admin-home-focus"><a href="javascript:change_tab('<?php print HOME_PAGE_FEEDBACK; ?>');">Home</a></li>
                        <li class="admin-recipes"><a href="javascript:change_tab('<?php print RECIPES_PAGE_FEEDBACK; ?>');">Recipes</a></li>
                        <li class="admin-divagossip"><a href="javascript:change_tab('<?php print DIVAGOSSIP_PAGE_FEEDBACK; ?>');">Diva Gossip</a></li>
                        <li class="admin-divatips"><a href="javascript:change_tab('<?php print DIVATIPS_PAGE_FEEDBACK; ?>');">Diva Tips</a></li>
                        <li class="admin-mydiva"><a href="javascript:change_tab('<?php print MYDIVA_MYRECIPEBOX_PAGE_FEEDBACK; ?>');">My Diva</a></li>
                        <li class="admin-others"><a href="javascript:change_tab('<?php print MYDIVA_RECIPES_PAGE_FEEDBACK; ?>');">Other</a></li>
                    </ul>
                </div>

                <div id="admin_sub_menu_contain">
                    <div id="admin_sub_menu">
                        <div id="mydiva_menu_mydiva">
                               <span id="menu_span">
                               <a id="recipe_box" href="javascript:change_tab('<?php print MYDIVA_MYRECIPEBOX_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_MYRECIPEBOX_PAGE_FEEDBACK; ?></a> |
                               <a id="my_profile" href="javascript:change_tab('<?php print MYDIVA_MYPROFILE_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_MYPROFILE_PAGE_FEEDBACK; ?></a> |
                               <a id="my_tips" href="javascript:change_tab('<?php print MYDIVA_MYTIPS_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_MYTIPS_PAGE_FEEDBACK; ?></a> |
                               <a id="my_groceries" href="javascript:change_tab('<?php print MYDIVA_GROCERIES_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_GROCERIES_PAGE_FEEDBACK; ?></a>
                               </span>
                        </div>
                        <div id="mydiva_menu_others">
                               <span id="menu_span">
                                   <a id="my_recipe" href="javascript:change_tab('<?php print MYDIVA_RECIPES_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_RECIPES_PAGE_FEEDBACK; ?></a> |
                               <a id="search" href="javascript:change_tab('<?php print MYDIVA_SEARCH_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_SEARCH_PAGE_FEEDBACK; ?></a> |
                               <a id="overall" href="javascript:change_tab('<?php print MYDIVA_OVERALLWEBSITE_PAGE_FEEDBACK; ?>');"><?php print MYDIVA_OVERALLWEBSITE_PAGE_FEEDBACK; ?></a>
                               </span>
                        </div>
                      </div>
                </div>
            </div>
            -->

         </div>
        <!-- END HEADER -->

        <!-- START BODY -->
        <div id="body">
              <div id="admin_container">
                <div id="admin_border">
                    <div id="admin_border_t">
                        <div id="admin_border_contain">
                            <div id="admin_border_content">
                            <?php print $content; ?>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        <!-- END BODY -->

        <!-- START FOOTER -->
       <div id="footer">
            <div id="footer_content">
                <a href="<?echo C_BASE_PATH."aboutus"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_aboutus.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."contactus"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_contactus.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."advertising"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_advertising.gif"  /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."privacy"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_privacy.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_dot.gif" />
                <a href="<?echo C_BASE_PATH."termofuse"?>"><img src="<?echo C_IMAGE_PATH?>footer/footer_termsofuse.gif" /></a>
                <img src="<?echo C_IMAGE_PATH?>footer/footer_slash.gif" />
                <img src="<?echo C_IMAGE_PATH?>footer/footer_copyright.gif" />

            </div>
        </div>
        <!-- END FOOTER -->
    </div>
    <div id="dialogInfo" title="">
    </div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php print GOOGLE_ANALYTICS_ID?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
