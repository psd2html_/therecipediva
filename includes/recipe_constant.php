<?php
/**
 * Recipe Constants
 * **/
 define('C_BASE_PATH', base_path());
 define('C_SCRIPT_PATH', 'sites/all/themes/recipe_diva/js/');
 define('C_CSS_PATH', 'sites/all/themes/recipe_diva/css/');
 define('C_ADMIN_CSS_PATH', 'sites/all/themes/recipe_diva/');
 define('C_IMAGE_PATH', C_BASE_PATH . 'sites/all/themes/recipe_diva/images/');
 define('C_HTML_PATH',  'sites/all/views/');
 define("C_IMAGE_LIST_PATH", C_SCRIPT_PATH."lists/image_list.js");

 define('C_ADMIN_USER', 'admin user');
 define('C_PREMIUM_USER', 'premium user');
 define('C_REGISTER_USER', 'register user');
 define('C_ADMIN_USER_NAME', 'The Recipe Diva');
 define('C_UNKNOWN_USER', 'Anonymous');
 define('C_SEPERATE_CHARACTER', '/');
 define("NONE_APPROVED_STATUS", 0);
 define("EASY_LEVEL_VALUE", 0);
 define("MODERATE_LEVEL_VALUE", 1);
 define("DIFFICULT_LEVEL_VALUE", 2);
 define("EASY_LEVEL_TEXT", "Easy");
 define("MODERATE_LEVEL_TEXT", "Moderate");
 define("DIFFICULT_LEVEL_TEXT", "Difficult");
 define("RECIPE_TYPE_PRIVATE_VALUE", 0);
 define("RECIPE_TYPE_PUBLIC_VALUE", 1);
 define("RECIPE_TYPE_PRIVATE_TEXT", "Private");
 define("RECIPE_TYPE_PUBLIC_TEXT", "Public");
 define("MIN_RECIPE_PRICE_VALUE", 0);
 define("MED_RECIPE_PRICE_VALUE", 1);
 define("MAX_RECIPE_PRICE_VALUE", 2);
 define("MIN_RECIPE_PRICE_TEXT", '0.99');
 define("MED_RECIPE_PRICE_TEXT", '1.49');
 define("MAX_RECIPE_PRICE_TEXT", '1.99');
 define("APPROVED_STATUS", 1);
 define("TOP_STATUS", 1);
 define("SUBMITTED_STATUS", 0);
 define("DELETE_STATUS", -1);
 define("NONE_APPROVED_STATUS_VALUE", "None approved");
 define("SUBMITTED_STATUS_VALUE", "Submitted");
 define("APPROVED_STATUS_VALUE", "Approved");
 define("TOP_STATUS_VALUE", "Top");
 define("DELETE_STATUS_VALUE", "Delete");
 define("DURATION_TIME_HOUR_TEXT", "Hours");
 define("DURATION_TIME_MINUTES_TEXT", "Minutes");
 define("DURATION_TIME_SECONDS_TEXT", "Seconds");
 define("DURATION_TIME_HOUR_ACRONYM", "Hour(s)");
 define("DURATION_TIME_MINUTES_ACRONYM", "Min(s)");
 define("DURATION_TIME_SECONDS_ACRONYM", "Sec(s)");
 define("DURATION_TIME_HOUR_VALUE", 0);
 define("DURATION_TIME_MINUTES_VALUE", 1);
 define("DURATION_TIME_SECONDS_VALUE", 2);
 define("NUMBER_WORDS_DESCRIPTION", 40);
 define("MAX_IMAGE_SIZE_UPLOAD", 6);

 define("BREAK_LINE", chr(10));
/**
 * Define error messages
 */
define("ERR_MSG_REQUIRED", "@field_name is required.");
define("ERR_MSG_ISNUMERIC", "@field_name only allows numeric values and may not contain any white spaces.");
define("ERR_MSG_ISALPHANUMERIC", "@field_name only allows alphanumeric values.");
define("ERR_MSG_DUPLICATE", "@field_name is duplicated.");
define("ERR_MSG_INVALID", "Please input a valid @field_name.");
define("ERR_MSG_PASSWORD", "The password must be at least 6 characters and may not contain any white spaces.");
define("ERR_MSG_PASSWORD_NOT_MATCH", "Passwords do not match.");
define("ERR_MSG_EXIST", "This @field_name already exists.");
define("ERR_MSG_INVALID_COLNAME", "The column name @field_name is invalid.");
define("ERR_MSG_FILE_NONE_EXIST", "File does not existed.");
define("ERR_MSG_IMPORT_REQUIRED", "Error at line @index: @field_name is required.");
define("ERR_MSG_EMAIL_INVALID", "Error at line @index: Email [@value] is invalid.");
define("ERR_MSG_NUMERIC_GREATER", "@field_name is greater than max value [@num].");
define("ERR_MSG_NUMERIC_LESS", "@field_name is less than min value [@num].");
define("ERR_MSG_IMG_EXT_INVALID", "The image is an invalid file type. Supported file types are png gif jpg jpeg.");
define("ERR_MSG_IMG_SIZE_INVALID", "The image exceeds the allowable size.");
define("ERR_MSG_AGREE_TERM_COND", "You must agree to the Terms and Conditions and Privacy Policy to become a registered user.");
define("ERR_MSG_DIVATIPS_SEARCH_NO_RESULT", "We're sorry, we could not find any tips containing \"@search_item\".");
/**
 * Define confirm messages
 */
define("CONFIRM_MSG_DELETE_ITEM", "Do you want to delete this item?");
define("CONFIRM_MSG_DELETE_ITEMS", "Do you want to delete those items?");

/**
 * Define constants for DIVA TIPS MENU
 */
define("DIVATIPS_DETAIL_IMG_WIDTH", 627);
define("LOCATION_HOME_PAGE", 1);
define("LOCATION_HOME_PAGE_VALUE", "Home");
define("ARCHIVED_STATUS", 0);
define("INUSE_STATUS", 1);
define("ARCHIVED_STATUS_VALUE", "Archived");
define("INUSE_STATUS_VALUE", "Inuse");
define("DIVA_TIP_TYPE", 0);
define("HEALTH_TIP_TYPE", 1);
define("ARCHIVED_TIP_PER_PAGE", 20);
define("DIVA_TIP_NUMBER_PAGER", 1);
define("INUSE_TIP_PER_PAGE", 5);
define("DIVATIPS_TEMPLATE_PATH", C_HTML_PATH."diva_tips.html");
define("ARCHIVED_TEMPLATE_PATH", C_HTML_PATH."diva_tips_archived.html");
define("ETC_STRING", "...");
define("INUSE_EXCERPT_MAX_LENGTH", 30);
define("ARCHIVED_EXCERPT_MAX_LENGTH", 30);
define("ADD_MY_SAVED_TIP_MSG", "This tip has been added to your Saved Tips.");
define("ADMIN_TIP_PER_PAGE", 40);
define("MSG_ADDED_TIP", "The tip has already been added to your Saved Tips.");
define("CONF_MSG_TIP_DEL", 'Are you sure you want to delete this tip?');
define("CONF_MSG_COMMENT_DEL", 'Are you sure you want to delete this comment?');
/**
 * Define constants for DIVA GOSSIP MENU
 */
define("TOPIC_COMMENT_PER_PAGE", 20);
define("TOPIC_THREAD_PER_PAGE", 20);
define("ADMIN_TOPIC_THREAD_PER_PAGE", 40);
define("MAX_ADMIN_CONTAINER_ORDER", 999);
define("MAX_ADMIN_FORUM_ORDER", 999);
define("MIN_ADMIN_CONTAINER_ORDER", 0);
define("MIN_ADMIN_FORUM_ORDER", 0);
define("MSG_CONTAINER_ORDER_DES", 'Containers are displayed in ascending order by container order (containers with equal orders are displayed alphabetically).');
define("MSG_FORUM_ORDER_DES", 'Forums are displayed in ascending order by forum order (forums with equal orders are displayed alphabetically).');
define("CONF_MSG_TOPIC_DEL", 'Are you sure you want to delete this topic?');
define("CONF_MSG_REPLY_DEL", 'Are you sure you want to delete this reply?');
define("NONE_SELECT_TEXT", "None selected");
define("FORUM_IMG_WIDTH", 70);
define("ERR_MSG_DIVAGOSSIP_SEARCH_NO_RESULT", "We're sorry, we could not find any topics containing \"@search_item\".");
/**
 * Define constants for DIVA FEEDBACK
 */
define("HOME_PAGE_FEEDBACK", 'The Recipe Diva');
define("RECIPES_PAGE_FEEDBACK", 'Recipes');
define("DIVATIPS_PAGE_FEEDBACK", 'Diva Tips');
define("HEALTHTIPS_PAGE_FEEDBACK", 'Health Tips');
define("DIVAGOSSIP_PAGE_FEEDBACK", 'Diva Gossip');
define("MYDIVA_PAGE_FEEDBACK", 'My Diva');
define("MYDIVA_MYRECIPEBOX_PAGE_FEEDBACK", 'My Recipe Box');
define("MYDIVA_MYPROFILE_PAGE_FEEDBACK", 'My Profile');
define("MYDIVA_MYTIPS_PAGE_FEEDBACK", 'My Tips');
define("MYDIVA_GROCERIES_PAGE_FEEDBACK", 'Groceries');
define("OTHERS_PAGE_FEEDBACK", 'Others');
define("MYDIVA_RECIPES_PAGE_FEEDBACK", 'Recipe Pages');
define("MYDIVA_SEARCH_PAGE_FEEDBACK", 'Searches');
define("MYDIVA_OVERALLWEBSITE_PAGE_FEEDBACK", 'Overall Website');

define("ADMIN_FEEDBACK_QUESTION_PER_PAGE", 40);
define("FEEDBACK_SAVE_SUCCESS", "Feedback has been saved.");

define("HOME_PAGE_FEEDBACK_VALUE", 1);
define("RECIPES_PAGE_FEEDBACK_VALUE", 2);
define("DIVATIPS_PAGE_FEEDBACK_VALUE", 3);
define("DIVAGOSSIP_PAGE_FEEDBACK_VALUE", 4);
define("MYDIVA_PAGE_FEEDBACK_VALUE", 5);
define("MYDIVA_MYRECIPEBOX_PAGE_FEEDBACK_VALUE", 6);
define("MYDIVA_MYPROFILE_PAGE_FEEDBACK_VALUE", 7);
define("MYDIVA_MYTIPS_PAGE_FEEDBACK_VALUE", 8);
define("MYDIVA_GROCERIES_PAGE_FEEDBACK_VALUE", 9);
define("MYDIVA_RECIPES_PAGE_FEEDBACK_VALUE", 10);
define("MYDIVA_SEARCH_PAGE_FEEDBACK_VALUE", 11);
define("MYDIVA_OVERALLWEBSITE_PAGE_FEEDBACK_VALUE", 12);
define("OTHERS_PAGE_FEEDBACK_VALUE", 13);


define("FEEDBACK_QUESTION_TYPE_NORMAL", 0);
define("FEEDBACK_QUESTION_TYPE_NORMAL_VALUE", "Normal");
define("FEEDBACK_QUESTION_TYPE_COMMENT", 1);
define("FEEDBACK_QUESTION_TYPE_COMMENT_VALUE", "Comment");
define("FEEDBACK_QUESTION_TYPE_MULTIPLE", 2);
define("FEEDBACK_QUESTION_TYPE_MULTIPLE_VALUE", "Multiple");

define("FEEDBACK_RATE_STRONGLY_AGREE", 1);
define("FEEDBACK_RATE_STRONGLY_AGREE_VALUE", "Strongly Agree");
define("FEEDBACK_RATE_AGREE", 2);
define("FEEDBACK_RATE_AGREE_VALUE", "Agree");
define("FEEDBACK_RATE_MOSTLY_AGREE", 3);
define("FEEDBACK_RATE_MOSTLY_AGREE_VALUE", "Mostly Agree");
define("FEEDBACK_RATE_DISAGREE", 4);
define("FEEDBACK_RATE_DISAGREE_VALUE", "Disagree");
define("FEEDBACK_RATE_STRONGLY_DISAGREE", 5);
define("FEEDBACK_RATE_STRONGLY_DISAGREE_VALUE", "Strongly Disagree");
define("FEEDBACK_SECTION_VID", 100);
define("MSG_FEEDBACK_SECTION_ORDER_DES", 'Sections are displayed in ascending order by section order (sections with equal orders are displayed alphabetically).');
define("MSG_FEEDBACK_QUESTION_ORDER_DES", 'Questions are displayed in ascending order by question order (questions with equal orders are displayed in created date).');
/**
 * Define constants for RECIPES MENU
 */
define("RECIPE_INDEX_PER_PAGE", 100);
define("ADMIN_RECIPE_PER_PAGE", 40);
define("ARCHIVED_RECIPE_PER_PAGE", 20);
define("RECIPES_RESULT_PER_CATEGORY", 3);
define("RECIPES_NUMBER_SUBCAT_PER_CATEGORY", 7);
define("MYDIVA_RESULT_PER_PAGE", 20);
define("RECIPES_SEARCH_RECIPES", 1);
define("RECIPES_SEARCH_INGREDIENTS", 2);
define("RECIPES_SEARCH_ADVANCED", 3);
define("RECIPES_SEARCH_FRIEND", 4);
define('RECIPES_TIME_UNIT', 'Hour');
define("RECIPE_DETAIL_IMAGE_WIDTH", 627);
define('RECIPE_IMAGE_WIDTH', 313);
define('SUBCATE_IMAGE_WIDTH', 75);
define("MSG_CATEGORY_ORDER_DES", 'Catygoies are displayed in ascending order by weight.');
define("INFO_MSG_AGREE_PRIMIUM_USERS", 'Yes, sign me up!');
define("INFO_MSG_REFUSE_PRIMIUM_USERS", 'No thanks');
/**
 * Define constants for MY DIVA MENU
 */
define("CONF_MSG_RECIPE_TIP_DEL", 'Are you sure you want to delete this @filename?');
define("CONF_MSG_GROCERIES_DEL", 'Are you sure you want to delete Grocery List?');
define("INFO_MSG_PREMIUM_FUNCTION", 'You must be a Premium Member for this function, would you like to upgrade to Premium Membership?');
define("ERR_MSG_ABOUTME_MAXLENGTH", "Maxlength of @field_name have to be @number characters.");
define("INFO_MSG_PASSWORD_SENT", 'A temporary password has been sent.');
define("INFO_MSG_GROCERIES_TITLE", 'Type here to add an item (e.g. 2 gallons of milk)');
define("INFO_MSG_GROCERIES_NOTES", 'Type any additional notes here');
define("USER_PROFILE_IMAGE_WIDTH", 189);
define("PROFILE_COW_FID", 1);
define("PROFILE_RECEIVE_MAIL_SETTINGS_FID", 28);
define("PROFILE_PRIVACY_SETTINGS_FID", 29);
define("PROFILE_ACTIVITY_SETTINGS_FID", 32);
define("PROFILE_ALL_ACTIVITY_SETTINGS_FID", 33);
define("NUMBERS_UPLOADED_RECIPE", 7);
define("NEWEST_PREMIUM_RECIPES", 1);
define("TOP_RATED", 2);
define("DIVA_RECOMMENDATIONS", 3);

/**
 * Define content type name
 */
define("CONTENT_TYPE_RECIPE", "recipe");
define("CONTENT_TYPE_RECIPE_TIPS", "recipe_tips");
define("CONTENT_TYPE_HEALTH_TIPS", "health_tips");
define("CONTENT_TYPE_RECIPE_GROCERIES", "recipe_groceries");
define("CONTENT_TYPE_GALLERY",	"gallery");
/**
 * Define Admin Enter Text page id
 */
define("HOME_PAGE", 0);
define("RECIPES_PAGE", 1);
define("RECIPE_UPLOAD_PAGE", 2);
define("INGREDIENTS_SEARCH_PAGE", 3);
define("ADVANCED_SEARCH_PAGE", 4);
define("DIVA_TIPS_PAGE", 5);
define("DIVA_TIPS_DETAIL_PAGE", 6);
define("DIVA_TIPS_ADD_PAGE", 7);
define("DIVA_TIPS_NEW_COMMENT_PAGE", 8);
define("DIVA_GOSSIP_PAGE", 9);
define("DIVA_GOSSIP_LIST_PAGE", 10);
define("DIVA_GOSSIP_TOPIC_PAGE", 11);
define("DIVA_GOSSIP_POST_REPLY_PAGE", 12);
define("DIVA_GOSSIP_NEW_TOPIC_PAGE", 13);
define("DIVA_GOSSIP_EDIT_TOPIC_PAGE", 14);
define("DIVA_TIPS_EDIT_PAGE", 15);
define("DIVA_TIPS_EDIT_COMMENT_PAGE", 16);
define("DIVA_GOSSIP_SEARCH_RESULT", 17);
define("DIVA_GOSSIP_LIST_PAGE_BG", 18);
define("DIVA_GOSSIP_TOPIC_PAGE_BG", 19);
define("DIVA_GOSSIP_POST_REPLY_PAGE_BG", 20);

/**
 * Define Static page id
 */
define("COOK_OF_THE_WEEK_PAGE", 0);
define("PREMIUM_MEMBERSHIP_DETAIL_PAGE", 1);
define("ABOUT_US_PAGE", 2);
define("CONTACT_US_PAGE", 3);
define("ADVERTISING_PAGE", 4);
define("PRIVACY_PAGE", 5);
define("TERM_OF_USE_PAGE", 6);

/**
 * Define Static page name
 */
define("COOK_OF_THE_WEEK_PAGE_NAME", "Cook of the week");
define("PREMIUM_MEMBERSHIP_DETAIL_PAGE_NAME", "Premium Membership Details");
define("ABOUT_US_PAGE_NAME", "About Us");
define("CONTACT_US_PAGE_NAME", "Contact Us");
define("ADVERTISING_PAGE_NAME", "Advertising");
define("PRIVACY_PAGE_NAME", "Privacy");
define("TERM_OF_USE_PAGE_NAME", "Term of Use");

/**
 * Define global variables for Recipe Site
 */
$node_type = "";
define("INGREDIENTS_SEARCH", "beef, shrimp, fish, chicken, vegetable");
define("INGREDIENTS_SEARCH_NOT", "stock, broth");
define("WORDS_GROCERY_NONE_DISPLAY", "of");
define("DEFAULT_HEALTHTIPS_AUTHOR", 'TX Shannon');


define("RECIPE_UPLOAD_INGREDIENTS_ERROR", "Ingredients is not valid.");
define("RECIPE_UPLOAD_TITLE_ERROR", "Title is required.");
define("RECIPE_UPLOAD_DESCRIPTION_MAXLENGTH_ERROR", "Maxlength of Description have to be 600 characters.");
define("RECIPE_UPLOAD_DIRECTIONS_ERROR", "Directions is required.");
define("RECIPE_UPLOAD_DIRECTIONS_MAXLENGTH_ERROR", "Maxlength of Directions have to be 5000 characters.");
define("RECIPE_UPLOAD_INGREDIENTS_REQUIRED_ERROR", "Ingredients is required.");
define("RECIPE_UPLOAD_INGREDIENTS_MAXLENGTH_ERROR", "Maxlength of Ingredients have to be 1800 characters.");
define("RECIPE_UPLOAD_CATEGORY_REQUIRED_ERROR", "Category is required.");
define("RECIPE_UPLOAD_PRETIME_ERROR", "Preparation Time is required.");
define("RECIPE_UPLOAD_COOKTIME_ERROR", "Cooking Time is required.");
define("UNIT_NAME_REQUIRED_ERROR", "Name is required.");
define("RECIPE_UPLOAD_PRETIME_INVALID_ERROR", "Preparation Time is not valid.");
define("RECIPE_UPLOAD_COOKTIME_INVALID_ERROR", "Cooking Time is not valid.");
define("RECIPE_UPLOAD_YIELD_INVALID_ERROR", "Yield is not valid.");
define("RECIPES_DELETE_INVALID_ERROR", "No item is chosen to delete.");
define("RECIPE_DELETE_CONFIRM", "Do you want to delete this recipe?");
define("RECIPES_DELETE_CONFIRM", "Do you want to delete chose recipes?");
define("CATEGORY_WEIGHT_INVALID", "Weight value must be numeric.");


define("ADD_ALL_GROCERY_COMPLETE_INFO", "All ingredients have been added to your Grocery List.");
define("ADD_ALL_GROCERY_ALREADY_COMPLETE_INFO", "All ingredients have already been added to your Grocery List.");
define("ADD_GROCERY_COMPLETE_INFO", "The ingredient have been added to your Grocery List.");
define("ADD_GROCERY_ALREADY_COMPLETE_INFO", "The ingredient have already been added to your Grocery List.");
define("ADD_RECIPE_INTO_RECIPE_BOX_COMPLETE_INFO", "This recipe has been added to your Recipe Box.");
define("ADD_RECIPE_INTO_RECIPE_BOX_ALREADY_COMPLETE_INFO", "This&nbsp;recipe&nbsp;has&nbsp;already&nbsp;been&nbsp;added&nbsp;to&nbsp;your&nbsp;Recipe&nbsp;Box.");
define("RECIPE_EMAIL_SEND_SUCCESS", "Recipe successfully sent!");
define("COOKOFWEEK_SEND_SUCCESS", "You have been submitted as Cook of the Week.");
define("GROCERY_EMAIL_SEND_SUCCESS", "Grocery List successfully sent!");
define("PROMOTION_REQUEST_SEND_SUCCESS", "Congratulations!  You have successfully registered!");
define("PROMOTION_REQUEST_ALREADY_COMPLETE_INFO", "Good luck!  You are already registered!");
define("RECIPE_EMAIL_SEND_FAIL", "Recipe mail hasn't sent. Please check your input.");
define("NOT_AUTHORIZED_TO_ACCESS_PAGE", "You are not authorized to access this page.");
define("ADD_RECIPE_FAVORITE_REQUEST_INFO", "This recipe should be added to your Recipe Box.");
define("ERR_MSG_ADD_FAVORITE_NODE", "Adding into favorite is fail.");

/**
 * Define Standardize all dates format
 */
define("STANDARD_DATE_FORMAT", "M d, Y");
define("MONTHDAY_DATE_FORMAT", "F d");
define("STANDARD_DATETIME_FORMAT", "M d, Y H:i");

/**
 * Define Paypal paramaters
 */
define("API_USERNAME", "truong_1277872940_biz_api1.quantic.com.vn");
define("API_PASSWORD", "1277872945");
define("API_SIGNATURE", "AFGhF6tpvB.TYMwl7g4e7mS818c9AVpA1OgOtGu494UfPa5mYTqLrwYp");
define("API_ENDPOINT", "https://api-3t.paypal.com/nvp");

/**
 * Define Base Url
 */
//define("C_SITE_URL", "http://".$_SERVER["HTTP_X_FORWARDED_HOST"]);
define("C_SITE_URL", "http://".$_SERVER["HTTP_HOST"]);

/**
 * Define Log parameters
 */
define("C_LOG_ERROR", "error");
define("C_LOG_DEBUG", "debug");
define("C_LOG_INFO", "info");
define("C_LOG_ALL", "all");

/**
 * Define Site Information
 */
define('C_SITE_NAME', 'The Recipe Diva');
define('C_SITE_EMAIL', 'noreply@therecipediva.com');

/**
 * Define Social Network
 */
define("SEND_MESSAGE_SUCCESS", "Message successfully sent!");
define("SEND_MESSAGE_FAIL", "Message hasn't sent. Please check your input.");
define("SEND_REQUEST_SUCCESS", "Request successfully sent!");
define("SEND_REQUEST_FAIL", "Request has already been sent.");
define("MSG_UNSUBSCRIBE", "You have unsubscribed email notifications");
define("MSG_SUBSCRIBE_NEWSLETTER", "You have subscribed newsletter");
define("MSG_UNSUBSCRIBE_NEWSLETTER", "You have unsubscribed newsletter");
define("CONFIRM_MSG_REMOVE_PENDING", "Are you sure you want to cancel foodie request?");
define("CONFIRM_MSG_REMOVE_FRIEND", "Are you sure you want to remove from foodies?");
define("CONFIRM_MSG_IGNORE_FRIEND", "Are you sure you want to ignore foodie request?");
define("CONFIRM_MSG_DELETE_THIS_MESSAGE", "Are you sure you want to delete this message?");
define("CONFIRM_MSG_DELETE_THESE_MESSAGES", "Are you sure you want to delete these messages?");
define("NUMBERS_FRIEND_LIST", 18);
define("NUMBERS_FINDFRIEND_LIST", 20);
define("NUMBERS_NEW_MEMBER_LIST", 8);
define("NUMBERS_MESSAGE_LIST", 20);
define("NUMBERS_FEED_LIST", 5);
define("NUMBERS_NEW_RECIPE_LIST", 5);
define("LIVEFEEDTYPE_CHANGE_STATUS", 0);
define("LIVEFEEDTYPE_UPDATE_PROFILE", 1);
define("LIVEFEEDTYPE_UPLOAD_RECIPE", 2);
define("LIVEFEEDTYPE_UPLOAD_TIP", 3);
define("LIVEFEEDTYPE_REVIEW_RECIPE", 4);
define("LIVEFEEDTYPE_REVIEW_TIP", 5);
define("LIVEFEEDTYPE_BECOME_FRIEND", 6);
define("LIVEFEEDTYPE_REPLY_TOPIC", 7);
define("LIVEFEEDTYPE_POST_TOPIC", 8);
define("LIVEFEEDTYPE_NEW_MEMBER", 9);
define("LIVEFEEDTYPE_SAVE_RECIPE", 10);
define("LIVEFEEDTYPE_SAVE_TIP", 11);
define("INFO_MSG_WHAT_YOUR_MIND", "What\'s on your mind...");
define("MESSAGETYPE_NEW_MESSAGE", 0);
define("MESSAGETYPE_RECIPE_APPROVED", 1);
define("MESSAGETYPE_POST_THREAD", 2);
define("MESSAGETYPE_RECIPE_REVIEWED", 3);
define("MESSAGETYPE_ACCEPT_REQUEST", 4);
define("MESSAGETYPE_FRIEND_REQUEST", 5);
define("MESSAGETYPE_FRIEND_ADD_RECIPE", 6);
define("MESSAGETYPE_FRIEND_REVIEW_RECIPE", 7);
define("MESSAGETYPE_FRIEND_ADD_TIP", 8);
define("MESSAGETYPE_FRIEND_REVIEW_TIP", 9);
define("MESSAGETYPE_FRIEND_POST_THREAD", 10);
define("MESSAGETYPE_FRIEND_REPLY_THREAD", 11);
define("MESSAGETYPE_TIP_ADDED", 12);
define("MESSAGETYPE_TIP_REVIEWED", 13);
define("MESSAGETYPE_POST_TOPIC", 14);
define("GOOGLE_ANALYTICS_ID", "UA-19864548-1");
define("MESSAGE_MAXLENGTH_ERROR", "Maxlength of messages have to be 1500 characters.");
define("COW_MAXLENGTH_ERROR", "Maxlength of content have to be 1500 characters.");
/**
*	Define	constants	for	GALLERY
*/
define("GALLERY_IMAGE_WIDTH", 330);
define("GALLERY_THUMB_IMAGE_WIDTH", 64);

/**
*	Define	constants	for	PROMOTION
*/
define("PROMOTION_IMAGE_WIDTH", 628);
define("PROMOTION_THUMB_IMAGE_WIDTH", 64);
?>
