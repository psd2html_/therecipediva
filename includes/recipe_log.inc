<?php

/**
* Error Logging Interface
*
* We use this as a simple mechanism to access the logging
* class and send messages to be logged.
*
* @access	public
* @return	void
*/
/*
function log_message($level = 'error', $message)
{
	static $LOG;
	$LOG = &new CI_Log;
	$LOG->write_log($level, $message);
}
*/

/**
 * Logging Class
 *
 */
class CI_Log {

	var $log_path;
	var $_threshold	= 1;
	var $_date_fmt	= 'Y-m-d H:i:s';
	var $_enabled	= TRUE;
	var $_levels	= array('ERROR' => '1', 'DEBUG' => '2',  'INFO' => '3', 'ALL' => '4');

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	string	the log file path 
	 */
	function CI_Log()
	{		
		$this->log_path = getcwd()."/log/";
	}

	// --------------------------------------------------------------------

	/**
	 * Write Log File
	 *
	 * Generally this function will be called using the global log_message() function
	 *
	 * @access	public
	 * @param	string	the error level
	 * @param	string	the error message
	 * @param	bool	whether the error is a native PHP error
	 * @return	bool
	 */
	function write_log($level = 'error', $msg)
	{		
		if ($this->_enabled === FALSE)
		{			
			return FALSE;
		}				

		$filepath = $this->log_path.'log-'.date('Y-m').".txt";
		//$filepath = $this->log_path."log.txt";
		$message  = '';		
		if ( ! file_exists($filepath))
		{
			if(!file_exists(getcwd()."/log")){
				mkdir(getcwd()."/log");	
			}			
			$fh = fopen($filepath, "w");
			if($fh==false)
			die("unable to create file");			
			fclose($fh);
		}		
		if ( ! $fp = @fopen($filepath, "a"))
		{
			return FALSE;
		}
		$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.date($this->_date_fmt). ' --> '.$msg."\n";
		flock($fp, LOCK_EX);
		fwrite($fp, $message);
		flock($fp, LOCK_UN);
		fclose($fp);

		@chmod($filepath, 0666);
		return TRUE;
	}

}
