<?php
/*
 * Created on 04-May-2010
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 class recipe_db{

     /**
      * Load data master from DB
      * */
     function get_master_db($strType){
         $arrOption = null;
        $strSQL = "SELECT n.nid, n.title";
        $strSQL .= " FROM node n ";
        $strSQL .= " WHERE n.type = '%s'";
        $strSQL .= " ORDER BY n.nid ";
        $data = db_query($strSQL, $strType);

        while ($record = db_fetch_object($data)) {
                $arrOption[$record->nid] = $record->title;
        }
        return $arrOption;
    }

    /**
      * Load data master from DB
      * */
     function load_master_db($tableName, $return_tyle = 'object', $order = false){
         $arrOption = null;
         if($order == false){
             $strSQL = "SELECT id, name FROM ".$tableName. " ORDER BY name" ;
         }else{
             $strSQL = "(SELECT id, name, 1 as number FROM ".$tableName. " WHERE id != 24 ORDER BY name)" ;
             $strSQL .= " UNION (SELECT id, name, 2 as number FROM ".$tableName. " WHERE id = 24) ORDER BY number, name" ;
         }

        $data = db_query($strSQL);
        if($return_tyle == 'object'){
            while ($record = db_fetch_object($data)) {
                    $arrOption[$record->id] = $record->name;
            }
        }
        else{
            while ($record = db_fetch_object($data)) {
                    $arrOption[] = $record;
            }
        }

        return $arrOption;
    }


    /**
      * Load data master from category
      * */
     function load_master_category(){
        $strSql = " SELECT DISTINCT(td.tid), th.name parent_name, th.parent parent_id, th.weight parent_weight, td.name subcatName, td.weight";
        $strSql .= " FROM term_data td";
        $strSql .= " INNER JOIN vocabulary_node_types vnt ON td.vid = vnt.vid  ";
        $strSql .= " INNER JOIN (SELECT DISTINCT(th.tid), td.name, th.parent, td.weight";
        $strSql .= "        		FROM term_hierarchy th";
        $strSql .= "             		INNER JOIN term_data td ON td.tid = th.parent) th";
        $strSql .= " 			ON td.tid = th.tid";
        $strSql .= " WHERE vnt.type = 'recipe'";
        $strSql .= " ORDER BY parent_weight ASC, parent_id ASC, subcatName ASC";
        $result = db_query($strSql);
        $terms = array();
        if(sizeof($result)>0){
            while($term = db_fetch_object($result)){
                $terms[] = $term;
            }
        }
        return $terms;
     }

     /**
      * Load data master from category for recipe upload/approve
      * */
     function load_master_category_recipe_upload(){
        $strSql = " SELECT DISTINCT(td.tid), th.name parent_name, th.parent parent_id, th.weight parent_weight, td.name subcatName, td.weight";
        $strSql .= " FROM term_data td";
        $strSql .= " INNER JOIN vocabulary_node_types vnt ON td.vid = vnt.vid  ";
        $strSql .= " INNER JOIN (SELECT DISTINCT(th.tid), td.name, th.parent, td.weight";
        $strSql .= "        		FROM term_hierarchy th";
        $strSql .= "             		INNER JOIN term_data td ON td.tid = th.parent) th";
        $strSql .= " 			ON td.tid = th.tid";
        $strSql .= " WHERE vnt.type = 'recipe'";
        $strSql .= " ORDER BY parent_name='Course' DESC " .
                ",parent_name='Convenience' DESC " .
                ",parent_name='Cuisine' DESC " .
                ",parent_name='Ingredient' DESC " .
                ",parent_name='Cooking Method' DESC " .
                ",parent_name='Occasions' DESC " .
                ",parent_name='Dietary Restriction' DESC" .
                ",parent_weight ASC, parent_id ASC, subcatName ASC";
        $result = db_query($strSql);
        $terms = array();
        if(sizeof($result)>0){
            while($term = db_fetch_object($result)){
                $terms[] = $term;
            }
        }
        return $terms;
     }

    /**
      * Load data master from recipe_units
      * */
     function load_units_display_name(){
         $arrOption = null;
         $strSQL = "SELECT id, display_name FROM recipe_unit ORDER BY display_name" ;

         $data = db_query($strSQL);

        while ($record = db_fetch_object($data)) {
                $arrOption[] = $record;
        }

        return $arrOption;
    }

    /**
      * Load data master from recipe_units
      * */
     function load_units_db($tableName){
         $arrOption = null;
         $strSQL = "SELECT id, name, plural_name, replaced_name FROM " . $tableName. " ORDER BY name" ;

         $data = db_query($strSQL);

        while ($record = db_fetch_object($data)) {
                $arrOption[] = $record;
        }

        return $arrOption;
    }

    /**
     * Get comment list of specified node
     *
     * @param $nid node id
     *
     * @return comment list of specified node
     */
    function get_node_comment($nid) {
        $current_rating = votingapi_select_results(array('content_id' => $nid, 'function' => 'average'));

        $sql = recipe_db::create_comment_list_sql("DESC");
        $args[] = $nid;
        //$strSqlCount = " SELECT count(*) FROM (" . $strSql. ") AS search_tbl";
        $result = db_query($sql, $args);
        return $result;
    }

    /**
     * Create sql select stament to get the comment list of specified node
     *
     * @return sql select stament string
     */
    function create_comment_list_sql($orderby="DESC") {
        $sql = 'SELECT c.cid as cid, c.pid, c.nid, c.subject, c.comment, c.format, c.timestamp
                        , c.name, c.mail, c.homepage, u.uid, u.name AS registered_name, u.signature
                        , u.signature_format, u.picture, u.data, c.thread, c.status, fc.value
                FROM comments c
                INNER JOIN users u ON c.uid = u.uid
                LEFT JOIN fivestar_comment fc ON fc.cid = c.cid
                WHERE c.nid = %d
                ORDER BY c.timestamp '.$orderby;
        return $sql;
    }

    /**
     * Create sql select stament to get the comment list of specified node
     *
     * @return sql select stament string
     */
    function get_average_vote_value($nid) {
        $sql = 'SELECT avg(fc.value) value
                FROM comments c
                    LEFT JOIN fivestar_comment fc ON fc.cid = c.cid
                WHERE c.nid = %d';
        $args[] = $nid;
        $result = db_fetch_object(db_query($sql, $args));
        return $result->value;
    }

    /**
     * Create sql select stament to get the comment list of specified node
     *
     * @return sql select stament string
     */
    function get_count_vote_value($nid) {
        $sql = 'SELECT COUNT(value) AS value
                FROM comments c
                    LEFT JOIN fivestar_comment fc ON fc.cid = c.cid
                WHERE c.nid = %d';
        $args[] = $nid;
        $result = db_fetch_object(db_query($sql, $args));
        return $result->value;
    }

    /**
     * Get node type
     *
     * @param $nid node id
     *
     * @return node type
     */
    function get_node_type($nid) {
        $node_type = db_result(db_query('SELECT type FROM {node} WHERE nid = %d', $nid));
        return $node_type;
    }

    /**
     * Get node type from specified vocabulary id
     *
     * @param $vid vocabulary id
     *
     * @return node type
     */
    function get_type_from_vid($vid) {
        $node_type = db_result(db_query('SELECT type FROM {vocabulary_node_types} WHERE vid = %d', $vid));
        return $node_type;
    }

    /**
     * Get vocabulary id from specified node type
     *
     * @param node type
     *
     * @return $vid vocabulary id
     */
    function get_vid_by_node_type($type) {
        $vid = db_result(db_query("SELECT vid FROM {vocabulary_node_types} WHERE type = '%s'", $type));
        return $vid;
    }

    /**
     * Get image upload url from specified page id
     *
     * @param $page page id
     *
     * @return image url
     */
    function get_admin_upload_image_url($page, $position = 1)
    {
        $strSQL = "SELECT
                      f.filepath
                    FROM node n
                    INNER JOIN content_type_recipe_upload_image a ON a.nid = n.nid
                    INNER JOIN files f ON f.fid = a.field_upload_image_fid
                    WHERE n.type = 'recipe_upload_image' AND a.field_display_position_value = %d
                    AND a.field_display_page_value = %d ORDER BY n.changed DESC
                    LIMIT 1";
        $image_path = db_result(db_query($strSQL, $position, $page));
        return $image_path;
    }

    /**
     * Get Admin enter text from specified page id
     *
     * @param $page page id
     *
     * @return Admin enter text
     */
    function get_admin_enter_text($page, $position = 1)
    {
        $strSQL = "SELECT
                      n.title
                    FROM node n
                    INNER JOIN content_type_recipe_upload_image a ON a.nid = n.nid
                    WHERE n.type = 'recipe_upload_image' AND a.field_display_position_value = %d
                    AND a.field_display_page_value = %d ORDER BY n.changed DESC
                    LIMIT 1";
        $enter_text = db_result(db_query($strSQL, $position, $page));
        return $enter_text;
    }

    /**
     * Get Admin Email
     * @return Admin Email
     */
    function get_admin_email()
    {
        $strSQL = "SELECT u.mail FROM users u
                    INNER JOIN users_roles ur ON u.uid = ur.uid
                    INNER JOIN role r ON r.rid = ur.rid
                    WHERE r.name = '%s' AND u.name LIKE '%%%s%%' ORDER BY u.created DESC
                    LIMIT 1";
        return db_result(db_query($strSQL, C_ADMIN_USER, C_ADMIN_USER_NAME));
    }

    /**
     * Get Admin id
     * @return Admin id
     */
    function get_admin_id()
    {
        $strSQL = "SELECT u.uid FROM users u
                    INNER JOIN users_roles ur ON u.uid = ur.uid
                    INNER JOIN role r ON r.rid = ur.rid
                    WHERE r.name = '%s' AND u.name LIKE '%%%s%%' ORDER BY u.created DESC
                    LIMIT 1";
        return db_result(db_query($strSQL, C_ADMIN_USER, C_ADMIN_USER_NAME));
    }

    /**
     * Get Max reply id
     * @return reply id
     */
    function get_max_reply_id()
    {
        $strSQL = "SELECT reply_id FROM recipe_message ORDER BY reply_id DESC LIMIT 1";
        $result =db_result(db_query($strSQL));
        if($result){
            $result++;
            return $result;
        }
        return 1;
    }

    /**
     * Get Static page conent from specified page id
     *
     * @param $page page id
     *
     * @return Static page conent
     */
    function get_static_page_content($page)
    {
        $strSQL = "SELECT
                      nr.body
                    FROM node n
                    INNER JOIN content_type_recipe_static_page c ON c.nid = n.nid
                    LEFT JOIN node_revisions nr ON nr.nid = n.nid
                    WHERE n.type = 'recipe_static_page' AND c.field_static_page_position_value = %d ORDER BY n.changed DESC
                    LIMIT 1";
        $static_text = db_result(db_query($strSQL, $page));
        return $static_text;
    }

    /**
     * Get list of level of recipe
     *
     * @return array
     */
    function get_list_recipe_difficult_level() {
        $arr = array(EASY_LEVEL_VALUE => EASY_LEVEL_TEXT,
                            MODERATE_LEVEL_VALUE => MODERATE_LEVEL_TEXT,
                            DIFFICULT_LEVEL_VALUE => DIFFICULT_LEVEL_TEXT);
        return $arr;
    }

    /**
     * Get list of type of recipe
     *
     * @return array
     */
    function get_list_recipe_type() {
        $arr = array(RECIPE_TYPE_PRIVATE_VALUE => RECIPE_TYPE_PRIVATE_TEXT,
                            RECIPE_TYPE_PUBLIC_VALUE => RECIPE_TYPE_PUBLIC_TEXT);
        return $arr;
    }

    /**
     * Get list of price of recipe
     *
     * @return array
     */
    function get_list_recipe_price() {
        $arr = array(MIN_RECIPE_PRICE_VALUE => MIN_RECIPE_PRICE_TEXT,
                            MED_RECIPE_PRICE_VALUE => MED_RECIPE_PRICE_TEXT,
                            MAX_RECIPE_PRICE_VALUE => MAX_RECIPE_PRICE_TEXT);
        return $arr;
    }

    /**
     * Get list of type of recipe
     *
     * @return array
     */
    function get_list_recipe_content_status() {
        $arr = array(SUBMITTED_STATUS => SUBMITTED_STATUS_VALUE
                    , APPROVED_STATUS => APPROVED_STATUS_VALUE);
        return $arr;
    }

    /**
     * Get duration time
     *
     * @return array
     */
    function get_list_duration_time() {
        $arr = array(DURATION_TIME_HOUR_VALUE => DURATION_TIME_HOUR_TEXT
                    , DURATION_TIME_MINUTES_VALUE => DURATION_TIME_MINUTES_TEXT
                    , DURATION_TIME_SECONDS_VALUE => DURATION_TIME_SECONDS_TEXT);
        return $arr;
    }

    /**
     * Get duration time
     *
     * @return array
     */
    function get_view_list_duration_time() {
        $arr = array(DURATION_TIME_HOUR_VALUE => DURATION_TIME_HOUR_ACRONYM
                    , DURATION_TIME_MINUTES_VALUE => DURATION_TIME_MINUTES_ACRONYM
                    , DURATION_TIME_SECONDS_VALUE => DURATION_TIME_SECONDS_ACRONYM);
        return $arr;
    }

    /**
     * Check if a user already has a node in their favorite list.
     */
    function favorite_grocery_nodes_check($nid) {
      global $user;
      $sql = "SELECT COUNT(*) FROM {recipe_mygroceries} WHERE uid = %d AND nid = %d";
      return db_result(db_query($sql, $user->uid, $nid));
    }

    /**
     * Get user id from user email
     *
     * @param $email user email
     * @return user id
     */
    function get_uid_by_email($email) {
        $email = recipe_utils::sql_escape_string($email);
          return db_result(db_query_range(db_rewrite_sql("SELECT u.uid FROM {users} u WHERE u.mail = '%s' "), $email, 0, 1));
    }

    /**
     * Get container information
     *
     * @param $vid vocabulary id
     * @param $tid term id
     * @return container information
     */
    function get_container($vid, $tid) {
        $forum = array();
        $result = db_query(db_rewrite_sql('SELECT t.*, parent FROM {term_data} t INNER JOIN {term_hierarchy} h ON t.tid = h.tid WHERE t.vid = %d and t.tid = %d ORDER BY weight, name', 't', 'tid'), $vid, $tid);
          while ($term = db_fetch_object($result)) {
              $forum[] = $term;
          }
          return $forum;
    }

    /**
     * Get forum information
     *
     * @param $tid term id
     * @return forum information
     */
    function get_forum($tid) {
        $forum = null;
        $result = db_query(db_rewrite_sql('SELECT t.name FROM {term_data} t WHERE t.tid = %d'), $tid);
          while ($term = db_fetch_object($result)) {
              $forum = $term;
          }
          return $forum;
    }

    /**
     * Get the number forum of specified container
     *
     * @param $vid vocabulary id
     * @param $tid term id
     * @return the number forum
     */
    function get_forum_count($vid, $tid) {
        $forum = null;
        $sql = 'SELECT count(t.tid) FROM term_data as t '.
                'INNER JOIN term_hierarchy as h ON t.tid = h.tid '.
                'WHERE t.vid = %d AND h.parent = %d ';
        $result = db_result(db_query(db_rewrite_sql($sql), $vid, $tid));
          return $result;
    }

    /**
     * Get Cook of Weed Admin comment from specified user id
     *
     * @param $uid user id
     *
     * @return Admin comment
     */
    function get_cow_admin_comment($uid)
    {
        $strSQL = "SELECT admin_content FROM recipe_cookofweek WHERE uid = %d";
        $admin_comment = db_result(db_query($strSQL, $uid));
        return $admin_comment;
    }

    /**
     * Get private setting from specified user id
     *
     * @param $uid user id
     *
     * @return private setting
     */
    function get_private_setting($uid)
    {
        $strSQL = "SELECT value FROM profile_values WHERE uid=%d AND fid=%d";
        $private_setting = db_result(db_query($strSQL, $uid, PROFILE_PRIVACY_SETTINGS_FID));
        return $private_setting;
    }

    /**
     * Get receive mail setting from specified user id
     *
     * @param $uid user id
     *
     * @return receive mail setting
     */
    function get_receive_mail_setting($uid)
    {
        $strSQL = "SELECT value FROM profile_values WHERE uid=%d AND fid=%d";
        $private_setting = db_result(db_query($strSQL, $uid, PROFILE_RECEIVE_MAIL_SETTINGS_FID));
        return $private_setting;
    }

    /**
     * Check user, if your friend or not
     *
     * @param $uid user id
     * @param $fid friend user id
     * @return Empty:Not Friend; 0:Request Pending; 1: Friend
     */
    function check_user_friend($uid, $fid)
    {
        $strSQL = "SELECT friend_flg FROM recipe_friend WHERE uid=%d AND fid=%d";
        $count = db_result(db_query($strSQL, $uid, $fid));

        return $count;
    }

    /**
     * Get list user's friend from user id
     *
     * @param $uid user id
     * @return array friend user id
     */
    function count_user_friend($uid,$flg=1)
    {
        $listuser = array();
        $strSQL = "SELECT COUNT(*) FROM recipe_friend rf INNER JOIN users u ON u.uid = rf.fid WHERE rf.uid=%d AND rf.friend_flg=%d AND rf.fid NOT IN (0,1)";
        return db_result(db_query($strSQL, $uid, $flg));
    }

    /**
     * Get list user's friend from user id
     *
     * @param $uid user id
     * @param $flg 0: All Inbox Messages; 1: New Messages; 2: Sent Message
     * @return array friend user id
     */
    function count_user_message($uid,$flg=0)
    {
        $listuser = array();
        $strSQL = "SELECT COUNT(*) FROM recipe_message WHERE receiver=%d AND delete_flg = 0";
        if($flg==0){
             $strSQL .= " AND message_flg IN (0,1)";
        }
        else if($flg==1){
             $strSQL .= " AND message_flg=1";
        }
        else if($flg==2){
             $strSQL = "SELECT COUNT(*) FROM recipe_message WHERE author=%d AND message_flg=2 AND delete_flg = 0";
        }
        return db_result(db_query($strSQL, $uid));
    }

    /**
     * Get list user's friend from user id
     *
     * @param $uid user id
     * @return array friend user id
     */
    function list_user_friend($uid,$flg=1)
    {
        $listuser = array();
        $strSQL = "SELECT fid FROM recipe_friend WHERE uid=%d AND friend_flg=%d";
        $result = db_query($strSQL, $uid, $flg);
          while ($term = db_fetch_object($result)) {
              $listuser[] = $term->fid;
          }
        return $listuser;
    }
 }
?>
