<?php
// $Id: recipe_pager.inc,v 1.63.2.1 2009/07/01 20:51:55 goba Exp $

/**
 * @file
 * Functions to aid in presenting database results as a set of pages.
 */
/**
 * Create a query pager in number paging format
 *
 * @param $record_per_page
 * 			The number of records per page
 * @param $element
 * 			An optional integer to distinguish between multiple pagers on one page.
 * @param $url submitted url
 * @param $form_name submitted  form name
 * @return An HTML string that generates the query pager.
 */
function number_paging($record_per_page, $element = 0, $num_pager = 1, $form_name, $url = '') {
    global $pager_page_array, $pager_total_items;
    $total_record = $pager_total_items[$element];
    if ($total_record == 0) {
        $page_total = 0;
    }
    $arrUrl = array("search_result", "recipes", "category", "units", "aisles",
                    "cookofweek", "news", "advertise", "entertext",
                    "maincontent", "staticpage", "user");
    $page_total = ceil($total_record / $record_per_page);
    $page_index = $pager_page_array[$element];
    $paging_link = '<div id="recipe_number_paging">';
    if ($url != '') {
        $prev_image = '<a href="'.$url.'"><img src="'.C_IMAGE_PATH.'pasgeof_01.gif" width="11" height="30" /></a>';
        $next_image = '<a href="'.$url.'"><img src="'.C_IMAGE_PATH.'pasgeof_06.gif" width="12" height="30" /></a>';
    } else {
        $prev_image = '<img src="'.C_IMAGE_PATH.'pasgeof_01.gif" width="11" height="30" onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\');" style="cursor:pointer"/>';
        $next_image = '<img src="'.C_IMAGE_PATH.'pasgeof_06.gif" width="12" height="30" onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\');" style="cursor:pointer"/>';
    }

    if((in_array(arg(1), $arrUrl) && arg(0) == 'adminrecipe') || (arg(0) == "search_result")){
        $prev_image = '<img src="'.C_IMAGE_PATH.'pasgeof_01.gif" width="11" height="30" onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');" style="cursor:pointer"/>';
        $next_image = '<img src="'.C_IMAGE_PATH.'pasgeof_06.gif" width="12" height="30" onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');" style="cursor:pointer"/>';
    }


    $prev_link = "";
    $next_link = "";
    $index_link = "";

    if ($page_total > 0) {
        if ($page_index > 0) {
            $query = create_query_url($num_pager, 1, $element);
            //$prev_link = l($prev_image, $_GET['q'], array('class' => 'pager-previous', 'title' => t('Go to previous page'), 'html' => TRUE, 'query' => $query));
            $prev_link = str_replace('%page%', $query, $prev_image);
            $prev_link = '<div class="prev_image_paging">'.$prev_link.'</div>';
        }

        $index_link .= '<div class="nav_number_paging">&nbsp;Page';
        $query = create_query_url($num_pager, 0, $element);
        $index_link .= "&nbsp;".($page_index + 1)."&nbsp;";
        $index_link .= "of";
        $index_link .= "&nbsp;".$page_total."&nbsp;</div>";
        if ($page_index < ($page_total - 1)) {
            $query = create_query_url($num_pager, 2, $element);
            //$next_link = l($next_image, $_GET['q'], array('class' => 'pager-next', 'title' => t('Go to next page'), 'html' => TRUE, 'query' => $query));
            $next_link = str_replace('%page%', $query, $next_image);
            $next_link = '<div class="prev_image_paging">'.$next_link.'</div>';
        }
    }
    $paging_link .= $next_link.$index_link.$prev_link.'</div>';
    return $paging_link;
}

/**
 * Create a query pager in number paging format
 *
 * @param $record_per_page
 * 			The number of records per page
 * @param $element
 * 			An optional integer to distinguish between multiple pagers on one page.
 * @param $url submitted url
 * @param $form_name submitted  form name
 * @return An HTML string that generates the query pager.
 */
function detail_number_paging($record_per_page, $element = 0, $num_pager = 1, $form_name, $url = '') {
    global $pager_page_array, $pager_total_items;
    $total_record = $pager_total_items[$element];
    if ($total_record == 0) {
        $page_total = 0;
    }

    $page_init = 1;
    $continous_page_num = 3;
    $page_total = ceil($total_record / $record_per_page);
    $page_index = $pager_page_array[$element];
    $paging_link = '<div id="recipe_detail_number_paging">';
    if ($url != '') {
        $prev_text = '<a href="'.$url.'">&#60;&#60; Previous</a>';
        $next_text = '<a href="'.$url.'">Next &#62;&#62;</a>';
    } else {
        $prev_text = '<a onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');">&#60;&#60; Previous</a>';
        $next_text = '<a onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');">Next &#62;&#62;</a>';
    }

    if($page_index + 1 == $page_init) {
        $prev_text = '<span>&#60;&#60; Previous</span>';
    }
    if($page_index + 1 == $page_total) {
        $next_text = '<span>Next &#62;&#62;</span>';
    }

    $prev_link = "";
    $next_link = "";
    $index_link = "";

    if ($page_total > 0) {
        //if ($page_index > 0) {
            $query = create_query_url($num_pager, 1, $element);
            //$prev_link = l($prev_image, $_GET['q'], array('class' => 'pager-previous', 'title' => t('Go to previous page'), 'html' => TRUE, 'query' => $query));
            //$prev_link = str_replace('%page%', $query, $prev_image);
            $prev_link = str_replace('%page%', $query, $prev_text);
            $prev_link = '<div class="prev_paging">'.$prev_link.'</div>';
        //}

        $index_link .= '<div class="nav_detail_number_paging">';
        $query = create_query_url($num_pager, 0, $element);
        if(($page_total == $continous_page_num + 1 && $page_index + 1 <= $continous_page_num) || $page_total != $continous_page_num + 1) {
            if($page_index + 1 == $page_init) {
                $index_link .= '<a style="font-weight: normal;">'.$page_init.'</a>';
            }else{
                $attr_click = ' onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');"';
                $attr_click = str_replace('%page%', $page_init - 1, $attr_click);
                $index_link .= '<a href="#"'.$attr_click.'>'.$page_init.'</a>';
                if($page_total > $continous_page_num + 2 && $page_index + 1 > $continous_page_num) {
                    $index_link .= "...";
                }
            }
        }

        if($continous_page_num < $page_total && $page_index + 1 <= $continous_page_num){
            for($i = 1; $i <= $continous_page_num; $i++){
                if($i == $page_index){
                    $index_link .= '&nbsp;<a style="font-weight: normal;">'.($i + 1).'</a>';
                }else{
                    $attr_click = ' onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');"';
                    $attr_click = str_replace('%page%', $i, $attr_click);
                    $index_link .= '&nbsp;<a href="#"'.$attr_click.'>'.($i + 1).'</a>';
                }
            }
        }else if($continous_page_num < $page_total && $page_index + 1 > $page_total - $continous_page_num){
            for($i = $page_total - $continous_page_num - 1; $i < $page_total - 1; $i++){
                if($i == $page_index){
                    $index_link .= '&nbsp;<a style="font-weight: normal;">'.($i + 1).'</a>';
                }else{
                    $attr_click = ' onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');"';
                    $attr_click = str_replace('%page%', $i, $attr_click);
                    $index_link .= '&nbsp;<a href="#"'.$attr_click.'>'.($i + 1).'</a>';
                }
            }
        }else{
            for($i = 1; $i < $page_total - 1; $i++){
                if($i == $page_index){
                    $index_link .= '&nbsp;<a style="font-weight: normal;">'.($i + 1).'</a>';
                }else if($i == $page_index - 1 || $i == $page_index + 1){
                    $attr_click = ' onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');"';
                    $attr_click = str_replace('%page%', $i, $attr_click);
                    $index_link .= '&nbsp;<a href="#"'.$attr_click.'>'.($i + 1).'</a>';
                }
            }
        }

        if(($page_total == $continous_page_num + 1 && $page_index + 1 > $continous_page_num) || ($page_total > $page_init && $page_total != $continous_page_num + 1)) {
            if($page_index + 1 == $page_total) {
                $index_link .= '&nbsp;<a style="font-weight: normal;">'.$page_total.'</a>';
            }else{
                if($page_total > $continous_page_num + 2 && $page_index + 1 <= $page_total - $continous_page_num) {
                    $index_link .= "...";
                }
                $attr_click = ' onClick="changePaging(\''.$form_name.'\', \''.$url.'\', \'%page%\', \'txtKeyword\');"';
                $attr_click = str_replace('%page%', $page_total - 1, $attr_click);
                $index_link .= '&nbsp;<a href="#"'.$attr_click.'>'.$page_total.'</a>';
            }
        }
        $index_link .= '</div>';
        //if ($page_index < ($page_total - 1)) {
            $query = create_query_url($num_pager, 2, $element);
            //$next_link = l($next_image, $_GET['q'], array('class' => 'pager-next', 'title' => t('Go to next page'), 'html' => TRUE, 'query' => $query));
            //$next_link = str_replace('%page%', $query, $next_image);
            $next_link = str_replace('%page%', $query, $next_text);
            $next_link = '<div class="next_paging">'.$next_link.'</div>';
        //}
    }
    $paging_link .= '<div class="name_paging">&nbsp;Page:&nbsp;&nbsp;</div>'.$prev_link.$index_link.$next_link.'</div>';
    return $paging_link;
}

/**
 * Create a query pager in number paging format
 *
 * @param $record_per_page
 * 			The number of records per page
 * @param $element
 * 			An optional integer to distinguish between multiple pagers on one page.
 * @param $url submitted url
 * @param $form_name submitted  form name
 * @return An HTML string that generates the query pager.
 */
function detail_number_paging_recipe($record_per_page, $element = 0, $num_pager = 1, $form_name, $url = '') {
    global $pager_page_array, $pager_total_items;
    $total_record = $pager_total_items[$element];
    if ($total_record == 0) {
        $page_total = 0;
    }

    $page_init = 1;
    $continous_page_num = 3;
    $page_total = ceil($total_record / $record_per_page);
    $page_index = $pager_page_array[$element];
    $paging_link = '<div id="recipe_detail_number_paging">';
    if ($url != '') {
        $prev_text = '<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.($page_index -1).');">&#60;&#60; Previous</a>';
        $next_text = '<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.($page_index + 1).');">Next &#62;&#62;</a>';
    }

    if($page_index + 1 == $page_init) {
        $prev_text = '<span>&#60;&#60; Previous</span>';
    }
    if($page_index + 1 == $page_total) {
        $next_text = '<span>Next &#62;&#62;</span>';
    }

    $prev_link = "";
    $next_link = "";
    $index_link = "";

    if ($page_total > 0) {
        //if ($page_index > 0) {
            $query = create_query_url($num_pager, 1, $element);
            $prev_link = str_replace('%page%', $query, $prev_text);
            $prev_link = '<div class="prev_paging">'.$prev_link.'</div>';
        //}

        $index_link .= '<div class="nav_detail_number_paging">';
        $query = create_query_url($num_pager, 0, $element);
        if(($page_total == $continous_page_num + 1 && $page_index + 1 <= $continous_page_num) || $page_total != $continous_page_num + 1) {
            if($page_index + 1 == $page_init) {
                $index_link .= '<a style="font-weight: normal;">'.$page_init.'</a>';
            }else{

                $index_link .= '<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.($page_init -1).');">'.$page_init.'</a>';

                if($page_total > $continous_page_num + 2 && $page_index + 1 > $continous_page_num) {
                    $index_link .= "...";
                }
            }
        }

        if($continous_page_num < $page_total && $page_index + 1 <= $continous_page_num){
            for($i = 1; $i <= $continous_page_num; $i++){
                if($i == $page_index){
                    $index_link .= '&nbsp;<a style="font-weight: normal;">'.($i + 1).'</a>';
                }else{
                    $index_link .= '&nbsp;<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.$i.');">'.($i + 1).'</a>';
                }
            }
        }else if($continous_page_num < $page_total && $page_index + 1 > $page_total - $continous_page_num){
            for($i = $page_total - $continous_page_num - 1; $i < $page_total - 1; $i++){
                if($i == $page_index){
                    $index_link .= '&nbsp;<a style="font-weight: normal;">'.($i + 1).'</a>';
                }else{
                    $index_link .= '&nbsp;<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.$i.');">'.($i + 1).'</a>';
                }
            }
        }else{
            for($i = 1; $i < $page_total - 1; $i++){
                if($i == $page_index){
                    $index_link .= '&nbsp;<a style="font-weight: normal;">'.($i + 1).'</a>';
                }else if($i == $page_index - 1 || $i == $page_index + 1){
                    $index_link .= '&nbsp;<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.$i.');">'.($i + 1).'</a>';
                }
            }
        }

        if(($page_total == $continous_page_num + 1 && $page_index + 1 > $continous_page_num) || ($page_total > $page_init && $page_total != $continous_page_num + 1)) {
            if($page_index + 1 == $page_total) {
                $index_link .= '&nbsp;<a style="font-weight: normal;">'.$page_total.'</a>';
            }else{
                if($page_total > $continous_page_num + 2 && $page_index + 1 <= $page_total - $continous_page_num) {
                    $index_link .= "...";
                }

                $index_link .= '&nbsp;<a href="#" onClick="goPaging(\''.$form_name.'\', \'/recipes\', '.($page_total-1).');">'.$page_total.'</a>';
            }
        }
        $index_link .= '</div>';
        //if ($page_index < ($page_total - 1)) {
            $query = create_query_url($num_pager, 2, $element);
            $next_link = str_replace('%page%', $query, $next_text);
            $next_link = '<div class="next_paging">'.$next_link.'</div>';
        //}
    }
    $paging_link .= '<div class="name_paging">&nbsp;Page:&nbsp;&nbsp;</div>'.$prev_link.$index_link.$next_link.'</div>';
    return $paging_link;
}

/**
 * Create query string in url
 *
 * @param $page_index the index of page
 * @param $flag paging flag (0:item page link, 1:previous page link, 2: next page link)
 * @param $element An optional integer to distinguish between multiple pagers on one page.
 *
 * @return query string in url
 */
function create_query_url($num_pager, $flag = 0,  $element = 0) {
    global $pager_page_array;
    $param = array();
    //$querystring = 'page=';
    $querystring = '';
    if ($flag == 1) { // previous link
        $param[$element] = $pager_page_array[$element] - 1;
    } else if ($flag == 2) { // next link
        $param[$element] = $pager_page_array[$element] + 1;
    } else {
        $param[$element] = $pager_page_array[$element];
    }
    for ($i = 0; $i < $element; $i++) {
        $param[$i] = $pager_page_array[$i];
        $querystring .= $param[$i].',';
    }
    $querystring .= $param[$element];
    return $querystring;
}
