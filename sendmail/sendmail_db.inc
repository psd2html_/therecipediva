<?php
// MySQL Class
class MySQL {
    // Base variables
    var $sLastError; // Holds the last error
    var $sLastQuery; // Holds the last query
    var $aResult; // Holds the MySQL query result
    var $iRecords; // Holds the total number of records returned
    var $iAffected; // Holds the total number of records affected
    var $aRawResults; // Holds raw 'arrayed' results
    var $aArrayedResult; // Holds a single 'arrayed' result
    var $aArrayedResults; // Holds multiple 'arrayed' results (usually with a set key)

    var $sHostname = ''; // MySQL Hostname
    var $sUsername = ''; // MySQL Username
    var $sPassword = ''; // MySQL Password
    var $sDatabase = ''; // MySQL Database

    var $sDBLink; // Database Connection Link

    // Class Constructor
    // Assigning values to variables
    function MySQL(){
        $this->OpenConn();
    }

    // Connects class to database
    // $bPersistant (boolean) - Use persistant connection?
    function OpenConn($bPersistant = false){
        if($this->sDBLink){
            mysql_close($this->sDBLink);
        }

        if($bPersistant){
            $this->sDBLink = mysql_pconnect($this->sHostname, $this->sUsername, $this->sPassword);
        }else{
            $this->sDBLink = mysql_connect($this->sHostname, $this->sUsername, $this->sPassword);
        }

        if (!$this->sDBLink){
            $this->sLastError = 'Could not connect to server: ' . mysql_error($this->sDBLink);
            return false;
        }

        if(!$this->CheckConn()){
            $this->sLastError = 'Could not connect to database: ' . mysql_error($this->sDBLink);
            return false;
        }
        return true;
    }

    // Select database to use
    function CheckConn(){
        if (!mysql_select_db($this->sDatabase, $this->sDBLink)) {
            $this->sLastError ='Cannot select database: ' . mysql_error($this->sDBLink);
            return false;
        }else{
            return true;
        }
    }

    // close connection
    function CloseConn(){
        mysql_close($this->sDBLink);
        return true;
    }

    // Executes MySQL query
    function ExecuteSQL($sSQLQuery){
        $this->sLastQuery = $sSQLQuery;
        if($this->aResult = mysql_query($sSQLQuery, $this->sDBLink)){
            $this->iRecords = @mysql_num_rows($this->aResult);
            $this->iAffected = @mysql_affected_rows($this->sDBLink);
            return true;
        }else{
            $this->sLastError = mysql_error($this->sDBLink);
            return false;
        }
    }

    //Fetch a result row as an object
    function db_fetch_object($result) {
        if ($result) {
            return mysql_fetch_object($result);
        }
        return false;
    }

}

?>
