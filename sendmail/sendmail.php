<?php
require_once 'sendmail_db.inc';
require_once 'sendmail_utils.inc';
require_once 'sendmail_constant.php';
require_once 'lib/recipe.sendmail.php';

class sendmail {

    var $smtpmail;
    var $log_dir = '/var/www/html/sendmail/log/';
    var $sendmail_constant = 200;
    var $sendmail_constant_test = 200;
    var $newsletter_constant = 20;

    function sendmail(){

    }
    function send_message_schedule()
    {
        //write log
        $content = "Begin send_message_schedule of send_mail";
        $dest = $this->log_dir . 'send_message.log';
        $file = sendmail_utils::file_write_log($content, $dest);

        $db = new MySQL();

        $sql = "SELECT is_processing
                    FROM recipe_sendmail
                    WHERE recipe_sendmail_type = 'recipe_message'";
        $result = $db->ExecuteSQL($sql);
        $recipe_sendmail = $db->aResult;
        $item = $db->db_fetch_object($recipe_sendmail);
        $is_processing = $item->is_processing;

        if($is_processing == 0) {

            //write log
            $content = "Begin UPDATE is_processing = 1 of table recipe_sendmail ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sqlUpdate = "UPDATE recipe_sendmail SET is_processing = 1, recipe_sendmail_time = now() WHERE recipe_sendmail_type = 'recipe_message'";
            $result = $db->ExecuteSQL($sqlUpdate);

            //write log
            $content = "End UPDATE is_processing = 1 of table recipe_sendmail  ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);


            $sql = "SELECT u.uid
                        , u.name
                        , u.mail
                        , rm.receiver
                        , rm.subject
                        , rm.body
                        , rm.mid
                        FROM recipe_message rm
                        LEFT JOIN users u ON u.uid = rm.receiver";
            //$sql .= " where name <> '' ORDER BY rm.created ASC limit 1";
            $sql .= " WHERE rm.send_flg = 0 ORDER BY rm.created ASC LIMIT 0," . $this->sendmail_constant;
            // Execute sql stament
            $result = $db->ExecuteSQL($sql);

            $messages = $db->aResult;

            $strMesId = "";

            //write log
            $content = "Begin send_message_schedule ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $count_of_amount = 0;

            while ($item = $db->db_fetch_object($messages)){
                $this->send_message($item->uid,$item->name,$item->mail,$item->subject,$item->body);
                if($strMesId != '')
                    $strMesId .= ',';
                $strMesId .= $item->mid;

                $count_of_amount++;
            }

            //write log
            $content = "End send_message_schedule ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);



            //write log
            $content = "Begin UPDATE recipe_message (".$count_of_amount.")";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            if($strMesId != '') {
                $sqlUpdate = "UPDATE recipe_message SET send_flg=1 WHERE mid IN (".$strMesId.")";
                $result = $db->ExecuteSQL($sqlUpdate);
            }

            //write log
            $content = "End UPDATE recipe_message ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);


            //write log
            $content = "Begin UPDATE is_processing = 0 of table recipe_sendmail ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sqlUpdate = "UPDATE recipe_sendmail SET is_processing = 0, recipe_sendmail_time = now() WHERE recipe_sendmail_type = 'recipe_message'";
            $result = $db->ExecuteSQL($sqlUpdate);

            //write log
            $content = "End UPDATE is_processing = 0 of table recipe_sendmail  ";
            $dest = $this->log_dir . 'send_message.log';
            $file = sendmail_utils::file_write_log($content, $dest);
        }

        $db->CloseConn();

        //write log
        $content = "End send_message_schedule of send_mail";
        $dest = $this->log_dir . 'send_message.log';
        $file = sendmail_utils::file_write_log($content, $dest);
    }


    function send_message($uid, $name, $mail, $subject, $body)
    {
        $site_url = C_SITE_URL.C_BASE_PATH;
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'From: '.C_SITE_NAME. ' <'.C_SITE_EMAIL.'>';

        $mail_body = '<span style="font-size: 36px; font-family: Arial, Helvetica, sans-serif;color: rgb(221, 0, 33);">'.$name.',</span ><br><img src="'.C_SITE_URL.C_IMAGE_PATH.'bg/black_line.gif"><br>'.$body;
        $mail_body .='<br><br>
            Please do not reply to this email. If you would not like to receive these email notifications in the future, please update your <a style="color:#dd0021;" href="'.$site_url.'mydiva/profile/resetnr">Account Settings</a>
             or <a style="color:#dd0021;" href="'.$site_url.'mydiva/profile/save_email_setting/'.$uid.'?destination=index%3Fopenunsubscribe=1">click here</a> if you would like to unsubscribe from all future email notifications.';
        $arr_main = array(
                        '%siteurl%' => C_SITE_URL.C_BASE_PATH ,
                        '%reset_password_content%' => $mail_body,
                        '%image_path%' => C_SITE_URL.C_IMAGE_PATH
                     );
        $mail_body = sendmail_utils::read_html_template("/var/www/html/sendmail/sendmail_template.html",$arr_main);
        //mail($mail,$subject,$mail_body,$headers);
        RecipeSendmail::mail($mail, $subject, $mail_body, $headers);

        //write log
        $content = "Send message (" . $subject . ") to users: " . $mail;
        $dest = $this->log_dir . 'send_message.log';
        $file = sendmail_utils::file_write_log($content, $dest);
    }



    function send_newsletter_schedule()
    {
        //write log
        $content = "Begin send_newsletter_schedule of send_mail";
        $dest = $this->log_dir . 'send_newsletter.log';
        $file = sendmail_utils::file_write_log($content, $dest);

        $db = new MySQL();

        $sql = "SELECT is_processing
                    FROM recipe_sendmail
                    WHERE recipe_sendmail_type = 'recipe_newsletter'";
        $result = $db->ExecuteSQL($sql);
        $recipe_sendmail = $db->aResult;
        $item = $db->db_fetch_object($recipe_sendmail);
        $is_processing = $item->is_processing;

        if($is_processing == 0) {

            //write log
            $content = "Begin UPDATE is_processing = 1 of table recipe_sendmail ";
            $dest = $this->log_dir . 'send_newsletter.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sqlUpdate = "UPDATE recipe_sendmail SET is_processing = 1, recipe_sendmail_time = now() WHERE recipe_sendmail_type = 'recipe_newsletter'";
            $result = $db->ExecuteSQL($sqlUpdate);

            //write log
            $content = "End UPDATE is_processing = 1 of table recipe_sendmail  ";
            $dest = $this->log_dir . 'send_newsletter.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sql = "SELECT u.uid, u.mail, u.send_flg
                        FROM users u, profile_fields f, profile_values v
                        WHERE f.fid = v.fid
                        AND v.uid = u.uid
                        AND f.name = 'profile_newsletters'
                        AND v.value = '1'
                        AND u.send_flg = 0
                        AND u.mail IN ('vantrung_tg@yahoo.com', 'vantrung_cntt01b@yahoo.com', 'vantrungevina@yahoo.com', 'huynhvantrung@gmail.com','huynh.van.trung@quantic.com.vn','trung.huynhvan@live.com')
                        LIMIT 0 , " . $this->newsletter_constant;
            // AND u.mail IN ('vantrung_tg@yahoo.com', 'vantrung_cntt01b@yahoo.com', 'vantrungevina@yahoo.com', 'huynhvantrung@gmail.com','huynh.van.trung@quantic.com.vn','trung.huynhvan@live.com')
            // Execute sql stament
            $result = $db->ExecuteSQL($sql);
            $users = $db->aResult;
            $strUserEmail = "";
            $strUserId = "";

            //write log
            $content = "Begin send_newsletter_schedule";
            $dest = $this->log_dir . 'send_newsletter.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            while ($item = $db->db_fetch_object($users)){
                if($item->send_flg == "0") {
                    $this->send_newsletter($db,$item->mail,$item->uid);

                    if($strUserEmail != "")
                        $strUserEmail .= ",";
                    $strUserEmail .= $item->mail;

                    if($strUserId != "")
                        $strUserId .= ",";
                    $strUserId .= $item->uid;
                }
            }

            //write log
            $content = "End send_newsletter_schedule";
            $dest = $this->log_dir . 'send_newsletter.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            if($strUserId != '') {
                $sqlUpdate = "UPDATE users SET send_flg=1 WHERE uid IN (".$strUserId.")";
                $result = $db->ExecuteSQL($sqlUpdate);
            }

    //        if($strUserEmail != "") {
    //            $sql = "SELECT *
    //                        FROM newsletter
    //                        ORDER BY id DESC
    //                        LIMIT 1";
    //
    //            $result = $db->ExecuteSQL($sql);
    //            $newletters = $db->aResult;
    //            $item = $db->db_fetch_object($newletters);
    //
    //            $dest = $this->log_dir . 'send_newsletter.log';
    //            $content = "Send newletter (" . $item->id . ". " . $item->subject . ") to users: " . $strUserEmail;
    //            $file = sendmail_utils::file_write_log($content, $dest);
    //
    //        }

            //write log
            $content = "Begin UPDATE is_processing = 0 of table recipe_sendmail ";
            $dest = $this->log_dir . 'send_newsletter.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sqlUpdate = "UPDATE recipe_sendmail SET is_processing = 0, recipe_sendmail_time = now() WHERE recipe_sendmail_type = 'recipe_newsletter'";
            $result = $db->ExecuteSQL($sqlUpdate);

            //write log
            $content = "End UPDATE is_processing = 0 of table recipe_sendmail  ";
            $dest = $this->log_dir . 'send_newsletter.log';
            $file = sendmail_utils::file_write_log($content, $dest);
        }

        $db->CloseConn();

        //write log
        $content = "End send_newsletter_schedule of send_mail";
        $dest = $this->log_dir . 'send_newsletter.log';
        $file = sendmail_utils::file_write_log($content, $dest);
    }

    function send_newsletter($db,$mail,$uid)
    {

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'From: '.C_SITE_NAME. ' <'.C_SITE_EMAIL.'>';

        $arr_main = array(
                        '%siteurl%' => C_SITE_URL.C_BASE_PATH ,
                        '%uid%' => $uid,
                        '%image_path%' => C_SITE_URL.C_IMAGE_PATH
                     );

        $sql = "SELECT *
                    FROM newsletter
                    ORDER BY id DESC
                    LIMIT 1";
        $result = $db->ExecuteSQL($sql);
        $newletters = $db->aResult;
        $item = $db->db_fetch_object($newletters);

        $directory = $item->file;
        $mail_body = sendmail_utils::read_html_template('/var/www/html/'.$directory,$arr_main);
        $subject   = $item->subject;
        //mail($mail, $subject, $mail_body, $headers);
        RecipeSendmail::mail($mail, $subject, $mail_body, $headers);

        //write log
        $content = "Send newsletter (" . $subject . ") to users: " . $mail;
        $dest = $this->log_dir . 'send_newsletter.log';
        $file = sendmail_utils::file_write_log($content, $dest);

    }





    function send_message_schedule_test()
    {
        //write log
        $content = "Begin send_message_schedule_test of send_mail";
        $dest = $this->log_dir . 'send_message_test.log';
        $file = sendmail_utils::file_write_log($content, $dest);

        $db = new MySQL();

        $sql = "SELECT is_processing
                    FROM recipe_sendmail
                    WHERE recipe_sendmail_type = 'recipe_message_test'";
        $result = $db->ExecuteSQL($sql);
        $recipe_sendmail = $db->aResult;
        $item = $db->db_fetch_object($recipe_sendmail);
        $is_processing = $item->is_processing;

        if($is_processing == 0) {

            //write log
            $content = "Begin UPDATE is_processing = 1 of table recipe_sendmail ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sqlUpdate = "UPDATE recipe_sendmail SET is_processing = 1, recipe_sendmail_time = now() WHERE recipe_sendmail_type = 'recipe_message_test'";
            $result = $db->ExecuteSQL($sqlUpdate);

            //write log
            $content = "End UPDATE is_processing = 1 of table recipe_sendmail  ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);


            $sql = "SELECT u.uid
                        , u.name
                        , u.mail
                        , rm.receiver
                        , rm.subject
                        , rm.body
                        , rm.mid
                        FROM recipe_message_test rm
                        LEFT JOIN users u ON u.uid = rm.receiver";
            //$sql .= " where name <> '' ORDER BY rm.created ASC limit 1";
            $sql .= " WHERE rm.send_flg = 0 ORDER BY rm.created ASC LIMIT 0," . $this->sendmail_constant_test;
            // Execute sql stament
            $result = $db->ExecuteSQL($sql);

            $messages = $db->aResult;

            $strMesId = "";

            //write log
            $content = "Begin send_message_schedule ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $count_of_amount = 0;

            while ($item = $db->db_fetch_object($messages)){
                $this->send_message_test($item->uid,$item->name,$item->mail,$item->subject,$item->body);
                if($strMesId != '')
                    $strMesId .= ',';
                $strMesId .= $item->mid;

                $count_of_amount++;
            }

            //write log
            $content = "End send_message_schedule ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);



            //write log
            $content = "Begin UPDATE recipe_message (".$count_of_amount.")";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            if($strMesId != '') {
                $sqlUpdate = "UPDATE recipe_message_test SET send_flg=1 WHERE mid IN (".$strMesId.")";
                $result = $db->ExecuteSQL($sqlUpdate);
            }

            //write log
            $content = "End UPDATE recipe_message ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);


            //write log
            $content = "Begin UPDATE is_processing = 0 of table recipe_sendmail ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);

            $sqlUpdate = "UPDATE recipe_sendmail SET is_processing = 0, recipe_sendmail_time = now() WHERE recipe_sendmail_type = 'recipe_message_test'";
            $result = $db->ExecuteSQL($sqlUpdate);

            //write log
            $content = "End UPDATE is_processing = 0 of table recipe_sendmail  ";
            $dest = $this->log_dir . 'send_message_test.log';
            $file = sendmail_utils::file_write_log($content, $dest);
        }

        $db->CloseConn();

        //write log
        $content = "End send_message_schedule_test of send_mail";
        $dest = $this->log_dir . 'send_message_test.log';
        $file = sendmail_utils::file_write_log($content, $dest);
    }

    function send_message_test($uid, $name, $mail, $subject, $body)
    {
        $site_url = C_SITE_URL.C_BASE_PATH;
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'From: '.C_SITE_NAME. ' <'.C_SITE_EMAIL.'>';

        $mail_body = '<span style="font-size: 36px; font-family: Arial, Helvetica, sans-serif;color: rgb(221, 0, 33);">'.$name.',</span ><br><img src="'.C_SITE_URL.C_IMAGE_PATH.'bg/black_line.gif"><br>'.$body;
        $mail_body .='<br><br>
            Please do not reply to this email. If you would not like to receive these email notifications in the future, please update your <a style="color:#dd0021;" href="'.$site_url.'mydiva/profile/resetnr">Account Settings</a>
             or <a style="color:#dd0021;" href="'.$site_url.'mydiva/profile/save_email_setting/'.$uid.'?destination=index%3Fopenunsubscribe=1">click here</a> if you would like to unsubscribe from all future email notifications.';
        $arr_main = array(
                        '%siteurl%' => C_SITE_URL.C_BASE_PATH ,
                        '%reset_password_content%' => $mail_body,
                        '%image_path%' => C_SITE_URL.C_IMAGE_PATH
                     );
        $mail_body = sendmail_utils::read_html_template("/var/www/html/sendmail/sendmail_template.html",$arr_main);
        //mail($mail,$subject,$mail_body,$headers);
        RecipeSendmail::mail($mail, $subject, $mail_body,$headers);

        //write log
        $content = "Send message (" . $subject . ") to users: " . $mail;
        $dest = $this->log_dir . 'send_message_test.log';
        $file = sendmail_utils::file_write_log($content, $dest);
    }

}
?>
