<?php
/**
 * Sendmail function utilities
 * **/
class sendmail_utils
{


    /**
     * Read content from html file
     * @param $filename full file name (path & file name)
     * @param $arrItemReplace array values need to replace
     *
     * @return string of html content
     **/
     function read_html_template($filename, $arrItemReplace = NULL)
     {
        $template_Content = sendmail_utils::get_html_Content($filename);
        if ($template_Content != NULL && $template_Content != "")
        {
            $template_Content = sendmail_utils::replace_array_data($template_Content, $arrItemReplace);
        }
        return $template_Content;
    }

    /**
     * Replace array key value from string
     * @param $strData string of data
     * @param $arrItemReplace array values need to replace
     * @return string of html content
     * **/
    function replace_array_data($strData, $arrItemReplace)
    {
         if (is_array($arrItemReplace))
         {
             foreach ($arrItemReplace as $key => $val)
             {
                 if(strstr($strData,$key))
                 {
                     $strData = ereg_replace($key, $val, $strData);
                 }
             }
         }
         return $strData;
    }

    /**
     * Get the content of html file
     *
     * @param $filename : html file name
     *
     * @return the content of html file
     */
    function get_html_Content($filename) {
        $template_Content = '';
        if (file_exists ($filename))
        {
            $template_Content = file_get_contents($filename);
        }
        return $template_Content;
    }

    function file_write_log($msg, $dest) {
        if ($fh = @fopen($dest, 'a+')) {
            $msg = "[".date("F j, Y, g:i:s a")."] ".$msg."\r"; //backslash r backslach n
            fputs($fh, $msg, strlen($msg));
            fclose($fh);
            return true;
        }
        else {
            return false;
        }
    }
}
?>
