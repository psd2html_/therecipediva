<?php

/**
 * Recipe Constants
 * **/
 define('C_BASE_PATH', '/');
 define('C_IMAGE_PATH', C_BASE_PATH . 'sites/all/themes/recipe_diva/images/');
 define('C_HTML_PATH',  '/');
 define('C_SEPERATE_CHARACTER', '/');

/**
 * Define Base Url
 */
//define("C_SITE_URL", "http://".$_SERVER["HTTP_X_FORWARDED_HOST"]);
define("C_SITE_URL", "http://www.therecipediva.com");

/**
 * Define Site Information
 */
define('C_SITE_NAME', 'The Recipe Diva');
define('C_SITE_EMAIL', 'noreply@therecipediva.com');

?>
