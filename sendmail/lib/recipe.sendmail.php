<?
include "recipe.phpmailer.php";
include "recipe.smtp.php";

class RecipeSendmail {
    function mail($to, $subject, $mail_body, $headers) {

        //return mail($to, $subject, $mail_body, $headers);

        $mail = new PHPMailer();
        $mail->IsSMTP(); // set mailer to use SMTP
        $mail->Host = ""; // specify main and backup server
        $mail->Port = 587; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        //$mail->SMTPSecure = 'ssl';
        $mail->Username = ""; // your SMTP username or your gmail username
        $mail->Password = ""; // your SMTP password or your gmail password
        //$from = ""; // Reply to this email
        //$to=$mail; // Recipients email ID
        //$name=""; // Recipient's name
        $mail->From = "";
        $mail->FromName = "The Recipe Diva"; // Name to indicate where the email came from when the recepient received
        $mail->AddAddress($to);
        //$mail->AddReplyTo("","");
        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(true); // send as HTML
        $mail->Subject = $subject;
        $mail->Body = $mail_body; //HTML Body
        $mail->AltBody = $mail_body; //Text Body
        //$mail->SMTPDebug = 2;
        if(!$mail->Send())
        {
            return false;
        }
        else
        {
            return true;
        }

    }
}
?>
