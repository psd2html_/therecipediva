<?php
// include needed files
include('includes/bootstrap.inc');
include('includes/database.inc');
include('includes/database.mysql.inc');

// Launch drupal start: configuration and database bootstrap
conf_init();
drupal_bootstrap(DRUPAL_BOOTSTRAP_CONFIGURATION);
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

// Now you can use drupal database with drupal's dbal:
// Unlock user admin if blocked
$return = db_query(" SELECT node.nid AS nid,
    node_data_field_ingredients.field_ingredients_value AS node_data_field_ingredients_field_ingredients_value,
    node.type AS node_type,
    node.vid AS node_vid
    FROM node node
    LEFT JOIN content_type_recipe node_data_field_ingredients
    ON node.vid = node_data_field_ingredients.vid
    WHERE node_data_field_ingredients.field_ingredients_value
    IS NOT NULL");
$row = db_fetch_object($return);
print_r($row);


?>